({
    //Change History
    //1.2    31/10/2019, 3:00 pm	      Rex.David	            extra/task26558315-AddPartnerPageUPDATES - Additional RTs
    // init - Runs on component load
    onInit: function (component, event, helper) {
        console.log('leadID: '+component.get("v.leadId"));
        console.log('fromLead: '+component.get("v.fromLead"));
        var showLogs = component.get("v.showConsoleLogs");
        if (showLogs){
            console.log('NewPartnerCmp - onInit ');
            console.log('NewPartnerCmp - branchParentId : ', component.get("v.branchParentId"));
            console.log('NewPartnerCmp - byPassRTSelection : ', component.get("v.byPassRTSelection"));
        } 

        // For "New Partner" override button
        if($A.util.isEmpty(component.get("v.branchParentId")) && !component.get("v.byPassRTSelection")){//run this only on Parent Partner creation. Don't run on Partner Branch creation.
            if (showLogs) console.log('NewPartnerCmp - invoke  getValidPartnerRTs');
            helper.getValidPartnerRTs(component,showLogs);
        }

        else{

            var page = component.get("v.page");
            // For Person Contact
            if(page == 'PC'){
                // console.log('-----------------------------> PC');
                component.set("v.selectedTab","relationship_details");
                helper.openPartnerCmp(component,helper); //RDAVID extra/task26558315-AddPartnerPageUPDATES
            }
            // For Partner Branch
            else if(page == 'PB'){
                console.log('partner branch');
                helper.getValidPartnerRTs(component,showLogs);
            }
        }
    },

    // next - Invoked from event when Next btn is clicked.
    next: function (component, event, helper) {
        var value = event.getParam("param");
        var rtname = event.getParam("rtName");
        var leadObj = event.getParam("leadObj");
        var newPartner = component.get("v.newPartner");
        newPartner.RecordTypeId = value;
        component.set("v.newPartner",newPartner);
        component.set("v.selRTName",rtname);
        component.set("v.leadObj",leadObj);
        //Opens NewPartnerSidebarMenu cmp
        helper.openPartnerCmp(component,helper); //RDAVID extra/task26558315-AddPartnerPageUPDATES
    },
    
    // handleCreateBranch - Invoked from event when Create Branch is clicked.
    handleCreateBranch: function (component, event, helper) {
        var parentPartnerId = event.getParam("branchParentId");
        var branchRTName = event.getParam("branchRTName");
        var branchRTId = event.getParam("branchRTId");
        var branchParentRTName = event.getParam("branchParentRTName");
        var newPartner = component.get("v.newPartner");
        newPartner.RecordTypeId = branchRTId;
        component.set("v.newPartner",newPartner);
        component.set("v.selRTName",branchParentRTName);
        component.set("v.branchParentId",parentPartnerId);
        component.set("v.branchRTName",branchRTName);
        helper.openPartnerBranch(component);
    },

    // handleCustomToastEvt - Invoked from evennt when Custom Toast event is fired.
    handleCustomToastEvt: function (component, event, helper) {
        var messageType = event.getParam("messageType");
        var message = event.getParam("message");
        helper.showCustomToast(component,messageType,message);
    },






















    
    // closeModal: function (component, event, helper) {
    //     var cmpTarget = component.find('Modalbox');
    //     var cmpBack = component.find('Modalbackdrop');
    //     $A.util.removeClass(cmpBack, 'slds-backdrop--open');
    //     $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
    // },

    // init: function (component, event, helper) {
    //     var modalBody;
    //     var modalFooter;
    //     console.log('showpopuup0');
    //     $A.createComponents([
    //         ["c:modalContent", {}],
    //         ["c:modalFooter", {}]
    //     ],
    //         function (components, status) {
    //             if (status === "SUCCESS") {
    //                 console.log('showpopuup');
    //                 modalBody = components[0];
    //                 modalFooter = components[1];
    //                 component.find('overlayLib').showCustomModal({
    //                     header: "Application Confirmation",
    //                     body: modalBody,
    //                     footer: modalFooter,
    //                     showCloseButton: true,
    //                     cssClass: "my-modal,my-custom-class,my-other-class",
    //                     closeCallback: function () {
    //                         alert('You closed the alert!');
    //                     }
    //                 })
    //             }
    //         }
    //     );
    // },
})