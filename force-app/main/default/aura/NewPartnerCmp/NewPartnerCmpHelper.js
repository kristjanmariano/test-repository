({
    //Change History
    //1.2    31/10/2019, 3:00 pm	      Rex.David	            extra/task26558315-AddPartnerPageUPDATES - Additional RTs
    // call server getNewPartnerValidRTMap side apex method to retrieve valid Partner RTs for creation.
	getValidPartnerRTs : function(component,showLogs) {

        var action = component.get("c.getNewPartnerValidRTMap");
        action.setStorable();
        // Create a callback that is executed after the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                var items = [];
                var rts = response.getReturnValue();
                var rtMap = {};
                if(showLogs) console.log("NewPartnerCmp - getValidPartnerRTs - SUCCESS - response.getReturnValue : " ,rts);
                for ( var key in rts ) {
                    items.push({label:key, value:rts[key]});
                    rtMap[rts[key]] = key;
                }

                component.set("v.newPartnerValidRTList", items);
                component.set("v.rtObjectMap", rtMap);
                console.log('rtMap == ',rtMap);
                
                // For New Partner Creation
                if($A.util.isEmpty(component.get("v.branchParentId"))){ //Dont open RT Selection for Partner Branch creation
                    if (showLogs) console.log('NewPartnerCmp - getValidPartnerRTs - SUCCESS - OPEN RT SELECTION.');
                    this.openPartnerRTModal(component);
                }

                // For New Partner Branch Creation
                else if(!$A.util.isEmpty(component.get("v.branchParentId"))){
                    if (showLogs) console.log('NewPartnerCmp - getValidPartnerRTs - SUCCESS - BYPASS RT SELECTION FOR BRANCH CREATION.');
                    this.openPartnerBranch(component);
                }
            }

            else if (state === "INCOMPLETE") {
                // do something
            }

            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        this.showCustomToast(component,'error',errors[0].message);
                        if (showLogs) console.log('NewPartnerCmp - getValidPartnerRTs - ERROR - ',errors[0].message);
                    }
                } else {
                    this.showCustomToast(component,'error',"Unknown error");
                    if (showLogs) console.log('NewPartnerCmp - getValidPartnerRTs - ERROR - Unknown error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    // Opens Recordtype modal selection.
    openPartnerRTModal : function(component) {
        var showLogs = component.get("v.showConsoleLogs");
        if(showLogs){
            console.log('NewPartnerCmp - openPartnerRTModal - newPartnerValidRTList : ',component.get("v.newPartnerValidRTList"));
            console.log('NewPartnerCmp - openPartnerRTModal - selValidPartnerRTId : ',component.get("v.selValidPartnerRTId"));
            console.log('NewPartnerCmp - openPartnerRTModal - rtObjectMap : ',component.get("v.rtObjectMap"));
        }
        let modalHeaderStr;
        if (!component.get("v.fromLead")) modalHeaderStr = 'New Partner';
        else modalHeaderStr = 'Convert Lead to Partner';
        $A.createComponent(
            "c:RecordTypeSelection",
            {
                "aura:id": "partnerRTSelectCmp",
                "modalHeaderStr": modalHeaderStr,
                "rtList": component.getReference("v.newPartnerValidRTList"),
                "value": component.getReference("v.selValidPartnerRTId"),
                "rtObjectMap":component.getReference("v.rtObjectMap"),
                "leadId" : component.get("v.leadId"),
                "fromLead" : component.get("v.fromLead")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.showSpinner", false)
                    component.set("v.body", newCmp);
                }
                else{
                    this.showCustomToast(component,'error',"Error in showing Recordtype selection. Please contact System Admin.");
                }
            }
        );
    },

    // Opens NewPartner Sidebar Menu.
    openPartnerCmp : function(component,helper) {

        console.log('>> openPartnercmp fromLead: '+component.get("v.fromLead"));
        console.log('>> openPartnercmp leadObj: '+component.get("v.leadObj"));

        var recId = (!$A.util.isEmpty(component.get("v.recordId"))) ? component.get("v.recordId") : null;
        var showLogs = component.get("v.showConsoleLogs");

        if(showLogs){
            console.log('NewPartnerCmp - openPartnerCmp - newPartner : ',component.get("v.newPartner"));
            console.log('NewPartnerCmp - openPartnerCmp - component.get("v.newPartner").RecordTypeId : ',component.get("v.newPartner").RecordTypeId);
            console.log('NewPartnerCmp - openPartnerCmp - selRTName : ',component.get("v.selRTName"));
            console.log('NewPartnerCmp - openPartnerCmp - selectedTab : ',component.get("v.selectedTab"));
            console.log('NewPartnerCmp - openPartnerCmp - page : ',component.get("v.page"));
        }
        //RDAVID extra/task26558315-AddPartnerPageUPDATES - Start
        var selRTName = component.get("v.selRTName");
        var hideLegEntTab = false;
        var hideAddressTab = false;
        if(!$A.util.isEmpty(selRTName)) {
            hideLegEntTab = helper.hideLegalEntityTab(component,selRTName);
            if(selRTName == 'Lender Parent'){
                hideAddressTab = true;
            }
        }
        //RDAVID extra/task26558315-AddPartnerPageUPDATES - End
        $A.createComponent(
            "c:NewPartnerSidebarMenu",
            {
                "aura:id": "partnerFormCmp",
                "newPartner":component.getReference("v.newPartner"),
                "recordTypeId":component.get("v.newPartner").RecordTypeId,
                "selRTName":component.getReference("v.selRTName"),
                "selectedTab" :component.getReference("v.selectedTab"),
                "page" :component.getReference("v.page"),
                "recordId":recId,
                "leadId" : component.get("v.leadId"),
                "leadObj" : component.get("v.leadObj"),
                "fromLead" : component.get("v.fromLead"),
                "hideLegalEnt" : hideLegEntTab, //RDAVID extra/task26558315-AddPartnerPageUPDATES
                "hideAddressTab" : hideAddressTab 
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.showSpinner", false);
                    component.set("v.body", newCmp);
                }
                else{
                    this.showCustomToast(component,'error',"Error in showing New Partner Form. Please contact System Admin.");
                }
            }
        );
    },

    // Opens NewPartner Sidebar Menu for Partner Branch.
    openPartnerBranch : function(component) {
        var showLogs = component.get("v.showConsoleLogs");

        if(showLogs){
            console.log('NewPartnerCmp - openPartnerBranch - newPartner : ',component.get("v.newPartner"));
            console.log('NewPartnerCmp - openPartnerBranch - component.get("v.newPartner").RecordTypeId : ',component.get("v.newPartner").RecordTypeId);
            console.log('NewPartnerCmp - openPartnerBranch - selRTName : ',component.get("v.selRTName"));
            console.log('NewPartnerCmp - openPartnerBranch - branchParentId : ',component.get("v.branchParentId"));
            // component.set("v.showSpinner", false);
        }

        $A.createComponent(
            "c:NewPartnerSidebarMenu",
            {
                "aura:id": "partnerBranchFormCmp",
                "newPartner":component.getReference("v.newPartner"),
                "recordTypeId":component.get("v.newPartner").RecordTypeId,
                "selRTName":component.getReference("v.selRTName"),
                "isBranch": true,
                "branchParentId": component.getReference("v.branchParentId")
            },
            function(newCmp){
                if (component.isValid()) {
                    component.set("v.showSpinner", false);
                    component.set("v.body", newCmp);
                }
                else{
                    this.showCustomToast(component,'error',"Error in showing New Partner Form for Branch. Please contact System Admin.");
                }
            }
        );
    },

    showCustomToast : function(component,messageType,message) {
        component.find("toastCmp").showToastModel(message, messageType);
    },
    
    //RDAVID extra/task26558315-AddPartnerPageUPDATES
    hideLegalEntityTab : function(component,selectedRTName){
        var partnerRTWithNoLegEnt = component.get("v.partnerRTWithNoLegalEntity");
        var rtIsInArr = partnerRTWithNoLegEnt.includes(selectedRTName); 
        return rtIsInArr;
    }
})