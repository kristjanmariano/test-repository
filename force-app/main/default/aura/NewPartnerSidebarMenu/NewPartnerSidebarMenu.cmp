<!--
  @Page Name          : NewPartnerSidebarMenu.cmp
  @Description        : Component for New Partner Page Sidebar Menu and Forms
  @Author             : Rex.David
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0   31/05/2019, 2:37 pm          Rex.David              Initial Version
  1.1   14/06/2019, 7:46 am          Rex.David              Applied fix for alignment Issues   
  1.2   1/11/2019,  6:01 am          Rex.David              extra/task26558315-AddPartnerPageUPDATES - Additional Partner Recordtypes
-->
<aura:component>
    <aura:attribute name="newPartner" type="Partner__c" />
    <aura:handler name="init" value="{! this }" action="{! c.init }" />
    <aura:attribute name="selectedTab" type="String" />
    <aura:attribute name="fieldSetName" type="String" description="The api name of the field set to use from the given object." />
    <aura:attribute name="recordId" type="String" />
    <aura:attribute name="recordTypeId" type="String" />
    <aura:attribute name="selRTName" type="String" />
    <aura:attribute name="relId" type="String" />
    <aura:attribute name="assAccId" type="String" />
    <aura:attribute name="legEntId" type="String" />
    <aura:attribute name="parAddId" type="String" />
    <aura:attribute name="branchParentId" type="String" />
    <aura:attribute name="title" type="String" />
    <aura:attribute name="showLogs" type="Boolean" default="true"/>
    <aura:attribute name="showConsoleLogs" type="Boolean"/>
    <aura:attribute name="fields" type="Object[]" access="private" />
    <aura:attribute name="user" type="User" access="public" />
    <aura:handler event="force:refreshView" action="{!c.init}" />
    <aura:attribute name="page" type="String" />
    <aura:handler name="creBraBtnClk" event="c:CreateBranchBtnClkEvt" action="{!c.handleCreateBranch}" />
    <aura:registerEvent name="creBraBtnClk2" type="c:CreateBranchBtnClkEvt" />

    <aura:attribute name="leadId" type="string" />
    <aura:attribute name="leadObj" type="object" />
    <aura:attribute name="fromLead" type="Boolean" default="false" />
    <!--RDAVID extra/task26558315-AddPartnerPageUPDATES-->
    <aura:attribute name="hideLegalEnt" type="Boolean" default="false" />
    <aura:attribute name="hideAddressTab" type="Boolean" default="false" />
    <aura:attribute name="partnerRTWithNoLegalEntity" type="String[]" default="['Insurance Category','Lender Division','Lender Parent','Supplier']" />
    <aura:attribute name="partition" type="String"/>
    <div>
        <lightning:layout>
            <lightning:layoutItem>
                <lightning:verticalNavigation selectedItem="{! v.selectedTab }" onselect="{! c.handleMenuSelected }" class="navigation">
                    <aura:if isTrue="{!v.page != 'PC'}">
                        <lightning:verticalNavigationSection label="Partner">
                            <lightning:verticalNavigationItemIcon label="{!v.title}" name="partner_details"
                                                                iconName="custom:custom15" />
                            <aura:if isTrue="{!!v.hideLegalEnt}"> <!-- RDAVID extra/task26558315-AddPartnerPageUPDATES -->
                                <lightning:verticalNavigationItemIcon label="Legal Entity" name="legal_entity"
                                                                    iconName="standard:partners" />
                            </aura:if>
                            <aura:if isTrue="{!!v.hideAddressTab}"> <!-- RDAVID extra/task26558315-AddPartnerPageUPDATES -->
                                <lightning:verticalNavigationItemIcon label="Address" name="address"
                                                                    iconName="standard:address" />
                            </aura:if>
                        </lightning:verticalNavigationSection>
                    </aura:if>
                    <lightning:verticalNavigationSection label="Partner Contact">
                        <lightning:verticalNavigationItemIcon label="Relationship Details" name="relationship_details"
                                                              iconName="standard:relationship" />
                        
                        <lightning:verticalNavigationItemIcon label="Associated Account" name="assoc_account"
                                                              iconName="standard:person_account" />
                    </lightning:verticalNavigationSection>
                    
                </lightning:verticalNavigation>
            </lightning:layoutItem>
            
            <lightning:layoutItem padding="around-medium" class="contentCls">
                <!-- <div class="c-container"> -->
                    <aura:if isTrue="{!v.selectedTab == 'partner_details'}">
                        <c:NewPartnerFormCmp recordTypeId="{!v.recordTypeId}" recordId="{!v.recordId}"
                                            selRTName="{!v.selRTName}" selectedTab="{!v.selectedTab}" legEntId="{!v.legEntId}"
                                            parAddId="{!v.parAddId}" branchParentId="{!v.branchParentId}" title="{!v.title}" user="{!v.user}" leadId="{!v.leadId}" fromLead="{!v.fromLead}" leadObj="{!v.leadObj}" hideCreatePartnerBranchBtn="{!v.hideLegalEnt}" partnerRTWithNoLegalEntity="{!v.partnerRTWithNoLegalEntity}" partition="{!v.partition}"/> <!-- RDAVID extra/task26558315-AddPartnerPageUPDATES -->
                    </aura:if>
                    
                    <aura:if isTrue="{!v.selectedTab == 'relationship_details'}">
                        <c:NewPartnerContactFormCmp partnerId="{!v.recordId}" selectedTab="{!v.selectedTab}"
                                                    relationshipId="{!v.relId}" personAccountId="{!v.assAccId}" user="{!v.user}" leadId="{!v.leadId}" fromLead="{!v.fromLead}" leadObj="{!v.leadObj}" partition="{!v.partition}" partnerRecordtype="{!v.selRTName}"/> <!-- RDAVID extra/task26558315-AddPartnerPageUPDATES -->
                    </aura:if>
                    
                    <aura:if isTrue="{!v.selectedTab == 'assoc_account'}">
                        <c:NewPersonAccountFormCmp partnerId="{!v.recordId}" selectedTab="{!v.selectedTab}"
                                                relationshipId="{!v.relId}" personAccountId="{!v.assAccId}" user="{!v.user}" leadId="{!v.leadId}" fromLead="{!v.fromLead}" leadObj="{!v.leadObj}" />
                    </aura:if>
                    
                    <aura:if isTrue="{!v.selectedTab == 'legal_entity'}">
                        <c:NewLegalEntityFormCmp selectedTab="{!v.selectedTab}" abnId="{!v.legEntId}"
                                                partnerId="{!v.recordId}" relationshipId="{!v.relId}" personAccountId="{!v.assAccId}" />
                    </aura:if>
                   
                    <aura:if isTrue="{!v.selectedTab == 'address'}">
                        <c:NewAddressFormCmp selectedTab="{!v.selectedTab}" addressId="{!v.parAddId}"
                                            partnerId="{!v.recordId}" />
                    </aura:if>

                    <aura:if isTrue="{!!v.showLogs}">
                        <p>RecordType :{! v.selRTName}:{! v.newPartner.RecordTypeId } <br />
                            Tab : {! v.selectedTab } <br />
                            Partner Id : {!v.recordId} <br />
                            Legal Entity Id : {!v.legEntId} <br />
                            Relationship Id : {!v.relId}<br />
                            Person Account Id : {!v.assAccId} <br />
                            Address Id : {!v.parAddId}<br />
                            Branch Parent Id : {!v.branchParentId}
                        </p>
                    </aura:if>
                <!-- </div> -->
            </lightning:layoutItem>
        </lightning:layout>
        
    </div>
</aura:component>