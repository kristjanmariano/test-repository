({
  //Change History
  //1.2    	31/10/2019, 3:00 pm		Rex.David	extra/task26558315-AddPartnerPageUPDATES - Additional RTs
  //1.3		13/11/2019, 12:11 pm	Rex.David	extra/task26762679-NewPartnerLightingPageUpdates_13_11_2019 - setRelationship Recordtypes (Either Partner Contact OR Lender Contact)
	fireCustomToast: function(component,messageType,message) {
		var customToastEvent = $A.get("e.c:CustomToastAppEvt");
		customToastEvent.setParams({    "messageType" : messageType,
										"message" : message });
		customToastEvent.fire();
	},

	setDefaultValues: function(component) {
		// alert(component.get("v.user").Profile.Name);
		var currentUserProfile = component.get("v.user").Profile.Name;
		if(!$A.util.isEmpty(currentUserProfile)){
			if(currentUserProfile.includes('Admin') ||  currentUserProfile.includes('Nodifi')){
				component.find("partitionIf").set("v.value", "Nodifi");
				// component.find("channelIf").set("v.value", "NODIFI");
			}
			else if(currentUserProfile.includes('PLS')){
				component.find("partitionIf").set("v.value", "Positive");
				// component.find("channelIf").set("v.value", "PLS");
			}
      //RDAVID extra/task26558315-AddPartnerPageUPDATES
			if(!$A.util.isEmpty(component.get("v.partition"))) component.find("partitionIf").set("v.value", component.get("v.partition")); //extra/task26558315-AddPartnerPageUPDATES
			if(component.find("recordOwnerID")) {
				component.find("recordOwnerID").set("v.value", $A.get("$Label.c.API_Admin_User_Id"));//Set Record Owner to API Admin User
			}
		}
	},	

	getCurrentProfile: function(component) {
		var action = component.get("c.getCurrentUser");
        action.setStorable();
        // Create a callback that is executed after the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {              
                var user = response.getReturnValue();
				component.set("v.user",user);
				component.set("v.initContentloaded",true);
				// alert(component.get("v.user").Profile.Name);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        // console.log("Error message: " + errors[0].message);
                    }
                } else {
                    // console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	},	
	//13/11/2019 RDAVID	extra/task26762679
	setRelationshipRT: function(component) {
		var partnerRecordType = component.get("v.partnerRecordtype");
        var partnerRTWithNoLegalEntity = component.get("v.partnerRTWithNoLegalEntity");
        if(partnerRTWithNoLegalEntity.includes(partnerRecordType)) { 
			component.set("v.recordTypeId",$A.get("$Label.c.Relationship_RT_Id_NM_Lender_Contact"));
			component.set("v.relType","Lender");
		}
		else { 
			component.set("v.recordTypeId",$A.get("$Label.c.Relationship_RT_Id_NM_Partner_Contact"));
			component.set("v.relType","Partner");
		}
	}
})