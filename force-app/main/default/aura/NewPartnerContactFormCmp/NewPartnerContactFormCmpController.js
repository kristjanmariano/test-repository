({
  //Change History
  //1.2     31/10/2019, 3:00 pm     Rex.David   extra/task26558315-AddPartnerPageUPDATES - Additional RTs
  //1.3		13/11/2019, 12:11 pm    Rex.David	extra/task26762679-NewPartnerLightingPageUpdates_13_11_2019 - setRelationship Recordtypes (Either Partner Contact OR Lender Contact)

	init: function(component, event, helper) {

        helper.setRelationshipRT(component); //13/11/2019 RDAVID extra/task26762679
        
        if(!$A.util.isEmpty(component.get("v.user"))){
            // console.log('User is not null');
            component.set("v.initContentloaded",true);
            
        }
        else{
            // console.log('User is null call apex');
            helper.getCurrentProfile(component);
        }
    },
    
    handleButton: function(component, event, helper) {
        // console.log('whatBtn --> ',event.getSource().get("v.name"));
        var btnName = event.getSource().get("v.name");//component.get('v.whatBtn');
        var assAccValid = component.find('assAcc').get("v.value");
        if (btnName == 'save') {
            // alert('assAccValid == ' + assAccValid);
            if(!$A.util.isEmpty(assAccValid) && assAccValid.replace(/\s/g, '').length){
                // alert('assAccValid valid == ' + assAccValid);
                component.set('v.showSpinner', true);
                component.set('v.disabled', true);
                component.set('v.whatBtn', btnName);
                component.find("partnerContactForm").submit();
            }
            else{
                window.scrollTo(0, 0);
                helper.fireCustomToast(component,'error','Please link an Account.');
            }
        }
    },

    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
        if (openSections.length === 0) {
            component.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            component.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    
	handleLoad: function(component, event, helper) {
        // alert('handleLoad');
		component.set("v.showSpinner", false);
        component.find('partnerInput').set('v.value',component.get("v.partnerId"));
        component.find('assAcc').set('v.value',component.get("v.personAccountId"));
        var toggleNewAccBtn = component.find("newAccBtn");
        $A.util.removeClass(toggleNewAccBtn, "slds-hidden");
        helper.setDefaultValues(component);      
    },

    handleSubmit: function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.disabled', true);
    },

    handleSuccess: function(component, event, helper) {
        var payload = event.getParams().response;
        var whatBtn = component.get('v.whatBtn');
		console.log('Relationship Saved! payload = '+payload.id);
		// console.log('Relationship Saved! assAcc = '+component.find('assAcc').get('v.value'));
		component.set('v.relationshipId', payload.id);
		component.set('v.personAccountId', component.find('assAcc').get('v.value'));
		component.set('v.saved', true);
        component.set('v.disabled', false);
        // alert(whatBtn);
        // component.set('v.selectedTab', 'relationship_details');
        if(whatBtn == 'save'){
            let isFromLead = component.get("v.fromLead");
            if (isFromLead){
                //Change Lead Status/Stage to Cloed-Won upon completion of New Partner page from Lead Convert
                console.log('isFromLead');
                let partnerId = component.find("partnerInput").get("v.value");
                let leadObj = component.get("v.leadObj");
                leadObj.Status = 'Closed';
                leadObj.Stage__c = 'Won';
                var updateLeadToWon = component.get('c.updateNodifiPartnerLeadToWon');
                updateLeadToWon.setParams({
                    leadObj: leadObj,
                    partnerId : partnerId
                });
                updateLeadToWon.setCallback(this,
                    function (response) {
                        var state = response.getState();
                        console.log('state: '+state);
                        if (state === "SUCCESS") {
                            console.log('Lead response: ' + JSON.stringify(response.getReturnValue()));
                            
                            component.set('v.showSpinner', false);
                            helper.fireCustomToast(component,'success','Partner Contact successfully saved!');
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                                "recordId": payload.id,
                                "Obj": 'Rel'//component.get('v.partnerId')//
                            });
                            navEvt.fire();
                        }
                        else{
                            var errors = response.getError();
                            if (errors) {
                                if (errors[0] && errors[0].message) {
                                    console.log("Error message: " +errors[0].message);
                                }
                            } else {
                                console.log("Unknown error");
                            }
                        }
                    }
                );
                $A.enqueueAction(updateLeadToWon);                
            }
            else{
                component.set('v.showSpinner', false);
                helper.fireCustomToast(component,'success','Partner Contact successfully saved!');
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": payload.id,
                    "Obj": 'Rel'//component.get('v.partnerId')//
                });
                navEvt.fire();
            }
        }
        else if (whatBtn == 'New Account') { 
            component.set('v.showSpinner', false);
            component.set('v.selectedTab', 'assoc_account');
        }
    },

    handleError: function(component, event, helper) {
        component.set('v.saved', false);
        component.set('v.disabled', false);
        component.set('v.showSpinner', false);
    },
    
	// handleClick - Invokes when button in lightning:accordionSection is clicked
	handleClick : function (component, event, helper) {
        var btnLbl = event.getSource().get("v.label");
        if(btnLbl == 'Edit'){
            component.set('v.selectedTab','assoc_account');
        }
        else if (btnLbl == 'New Account'){
            // component.set('v.selectedTab','assoc_account');
            component.set('v.whatBtn', btnLbl);
            component.find("partnerContactForm").submit();
        }
    },

    // 1.1  01/08/2019, 12:37 pm    Rex.David   tasks/24746570 Changes in Relationship Object
	handleAccountChange : function (component, event, helper) {
        console.log('Account change.');
        // component.find('contactMobile').set('v.value','test');
        console.log(component.find("assAcc").get("v.value"));
        
        if(!$A.util.isEmpty(component.find("assAcc").get("v.value"))){
            console.log('account selected!');
            component.set("v.personAccountId",component.find("assAcc").get("v.value"));
            component.find("recordLoader").reloadRecord();
        }
        else{
            console.log('account removed!');
            component.set("v.personAccountId",component.find("assAcc").get("v.value"));
            component.find("recordLoader").reloadRecord();
        }
    },

    recordUpdated: function(component, event, helper) {
        var changeType = event.getParams().changeType;
        console.log('changeType == ',changeType);
        if (changeType === "ERROR") { 
            component.find('contactMobile').set('v.value','');
            component.find('contactOther').set('v.value','');
            component.find('contactEmail').set('v.value','');
        }
        else if (changeType === "LOADED") { 
            /* handle record load */ 
            console.log("account loaded:::::" + component.get("v.accountRecord").Name);
            component.find('contactMobile').set('v.value',component.get("v.accountRecord").Rel_Mobile_Phone__c);
            component.find('contactOther').set('v.value',component.get("v.accountRecord").Rel_Other_Phone__c);
            component.find('contactEmail').set('v.value',component.get("v.accountRecord").Rel_Email__c);
        }
        else if (changeType === "REMOVED") { /* handle record removal */ }
        else if (changeType === "CHANGED") { /* handle record change */ }
    },
    
    //RDAVID extra/task26558315-AddPartnerPageUPDATES
    partnerUpdated:  function(component, event, helper) {
        var changeType = event.getParams().changeType;
        console.log('changeType == ',changeType); 
        if (changeType === "LOADED") { 
            console.log('changeType1 ');
            if(!$A.util.isEmpty(component.get("v.partnerRecord").Partition__c)){
                console.log('changeType2');
                console.log('changeType2.1' , component.find('partitionIf'));
                if(!$A.util.isEmpty(component.find('partitionIf'))) component.find('partitionIf').set('v.value',component.get("v.partnerRecord").Partition__c);
                if($A.util.isEmpty(component.get("v.partition"))){
                    console.log('changeType3 ');
                    component.set("v.partition",component.get("v.partnerRecord").Partition__c);
                }
            }
        }
        else if (changeType === "REMOVED") { /* handle record removal */ }
        else if (changeType === "CHANGED") { /* handle record change */ }
    },
})