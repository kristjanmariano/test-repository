<!--
  @Page Name          : NewPartnerContactFormCmp.cmp
  @Description        : Component showing Relationship form
  @Author             : Rex.David
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0    31/05/2019, 2:37 pm          Rex.David             Initial Version
  1.1    01/08/2019, 12:37 pm         Rex.David             tasks/24746570 Changes in Relationship Object
  1.2    14/09/2019, 7:35 am         Jesfer.Baculod         extra/tasks25900826-LeadToPartner Added Lead to Partner support
  1.3    31/10/2019, 3:00 pm	      Rex.David	            extra/task26558315-AddPartnerPageUPDATES - Additional RTs
  1.4    13/11/2019, 1:53 pm          Rex.David             extra/task26762679-NewPartnerLightingPageUpdates_13_11_2019 - Added layout for Lender Contact Relationship RT
-->
<aura:component controller="NewPartnerController">
    <!-- ATTRIBUTES -->
    <aura:handler name="init" value="{!this}" action="{!c.init}" />
    <aura:attribute name="activeSections" type="List"  default="['access_details','rel_details','con_details']"/>
    <aura:attribute name="partnerId" type="String"/>
    <aura:attribute name="relationshipId" type="String"/>
    <aura:attribute name="personAccountId" type="String"/>
    <aura:attribute name="selectedTab" type="String" />
    <aura:attribute name="recordTypeId" type="String" default="{!$Label.c.Relationship_RT_Id_NM_Partner_Contact}"/> 
    <aura:attribute name="title" type="String" default="Relationship Details"/> 
    <aura:attribute name="whatBtn" type="String"/>
    <aura:attribute name="activeSectionsMessage" type="String" />
    <aura:attribute name="pageSubHeader" type="String" default="Use this page to add Partner Contact record."/>
    <aura:attribute name="showSpinner" type="Boolean" default="true"/>
    <aura:attribute name="saved" type="Boolean" default="false" />
    <aura:attribute name="disabled" type="Boolean" default="false" />
    <aura:attribute name="showHeaderButtons" type="Boolean" default="false" />
    <aura:attribute name="initContentloaded" type="Boolean" default="false" />
    <aura:attribute name="showConsoleLogs" type="Boolean"/>
    <aura:registerEvent name="customToastEvt" type="c:CustomToastAppEvt"/>
    <aura:attribute name="user" type="User" access="public" />
    <aura:attribute name="leadId" type="string" />
    <aura:attribute name="leadObj" type="object" />
    <aura:attribute name="fromLead" type="boolean" default="false" />
    <aura:attribute name="accountRecord" type="Object"/>
    <aura:attribute name="recordLoadError" type="String"/>
    <force:recordData   aura:id="recordLoader"
                        recordId="{!v.personAccountId}"
                        fields="Name,PersonMobilePhone,PersonEmail,Rel_Email__c,Rel_Mobile_Phone__c,Rel_Other_Phone__c"
                        targetFields="{!v.accountRecord}"
                        targetError="{!v.recordLoadError}"
                        recordUpdated="{!c.recordUpdated}" />
    <!--RDAVID extra/task26558315-AddPartnerPageUPDATES-->	
    <aura:attribute name="partnerRecordtype" type="String"/>
    <aura:attribute name="partnerRTWithNoLegalEntity" type="String[]" default="['Insurance Category','Lender Division','Lender Parent','Supplier']" />
    <aura:attribute name="partition" type="String"/>

    <aura:attribute name="partnerRecord" type="Object"/>
    <force:recordData   aura:id="partnerRecLoader"
                        recordId="{!v.partnerId}"
                        fields="Partition__c"
                        targetFields="{!v.partnerRecord}"
                        targetError="{!v.recordLoadError}"
                        recordUpdated="{!c.partnerUpdated}" />
    <aura:attribute name="relType" type="String"/> <!-- RDAVID extra/task26762679-NewPartnerLightingPageUpdates_13_11_2019 - Either Lender Contact OR Partner Contact, depends on Partner RT -->
    <!-- ATTRIBUTES -->
    <div class="slds-align--absolute-center slds-is-relative">
        <aura:if isTrue="{!v.showSpinner}">
            <lightning:spinner />
        </aura:if>
    </div>
    <aura:if isTrue="{!and(!v.saved,v.initContentloaded,!v.showSpinner)}">
        <div class="c-container">
            <!-- NEW ALIGNMENT START -->
            <lightning:layout multipleRows="true">
                <!--HEADER START-->
                <lightning:layoutItem size="12">                           
                    <div class="slds-page-header">
                        <div class="slds-page-header__row">
                            <div class="slds-page-header__col-title">
                                <div class="slds-media">
                                    <div class="slds-media__figure">
                                        <lightning:icon iconName="standard:relationship" alternativeText="ABN" />
                                    </div>
                                    <div class="slds-media__body">
                                        <div class="slds-page-header__name">
                                            <div class="slds-page-header__name-title">
                                                <h1>
                                                    <span class="slds-page-header__title slds-truncate" title="{!v.title}">{!v.title}</span>
                                                </h1>
                                            </div>
                                        </div>
                                        <p class="slds-page-header__name-meta">{!v.pageSubHeader}</p>
                                    </div>
                                </div>
                            </div>
                            <aura:if isTrue="{!v.showHeaderButtons}">
                                <div class="slds-page-header__col-actions">
                                    <div class="slds-page-header__controls">                                            
                                        <div class="slds-page-header__control">
                                            <lightning:buttonGroup>
                                                <lightning:button disabled="{!v.disabled}" variant="brand" type="button" name="save" label="Save" onclick="{!c.handleButton}" />
                                            </lightning:buttonGroup>
                                        </div>
                                    </div>
                                </div>
                            </aura:if>
                        </div>
                    </div>
                </lightning:layoutItem>
                <!--HEADER END-->
                <!--BODY START-->
                <lightning:layoutItem size="12">   
                    <div class="page-section page-main">
                        <div class="demo-only demo-only--sizing slds-wrap">
                            <lightning:recordEditForm   aura:id="partnerContactForm"
                                                        objectApiName="Relationship__c"
                                                        recordId="{!v.relationshipId}"
                                                        recordTypeId="{!v.recordTypeId}"
                                                        onload="{!c.handleLoad}"
                                                        onsuccess="{!c.handleSuccess}"
                                                        onsubmit="{!c.handleSubmit}"
                                                        onerror="{!c.handleError}">
                                <!-- the messages component is for error messages -->
                                <lightning:messages />
                                <aura:if isTrue="{!(v.relType == 'Partner')}">
                                    <lightning:accordion   allowMultipleSectionsOpen="true"
                                                            onsectiontoggle="{! c.handleSectionToggle }"
                                                            activeSectionName="{! v.activeSections }">
                                        <lightning:accordionSection name="access_details" label="Access Details">
                                            <div class="slds-grid">
                                                <!--<div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Nodifi_Access__c" />
                                                </div>-->
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium"> 
                                                    <lightning:inputField fieldName="Nodifi_Tenancy__c" />
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Partition__c" aura:id="partitionIf"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid"> <!--tasks/24746570 1/08/2019 RDAVID-->
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Nodifi_User_Role__c"/>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                            </div> <!--tasks/24746570 1/08/2019 RDAVID-->
                                        </lightning:accordionSection>
                                        <lightning:accordionSection name="rel_details" label="Relationship Details">
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField disabled="true" aura:id="partnerInput" fieldName="Partner__c"/>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:outputField fieldName="RecordTypeId"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Partner_Contact_Reference_Number1__c"/>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Relationship_Type__c"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                            </div>                                        
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Broker_Relationship_Manager1__c"/>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                            </div>
                                            <div class="slds-grid">                                            
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Start_Date__c"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">                                            
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="End_Date__c"/>
                                                </div>
                                            </div>
                                        </lightning:accordionSection>
                                        <lightning:accordionSection name="con_details" label="Contact Details">
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-grid slds-p-right_medium">
                                                    <div class="slds-col slds-size_10-of-12">
                                                        <lightning:inputField aura:id="assAcc" fieldName="Associated_Account__c" onchange="{! c.handleAccountChange }"/>
                                                    </div>
                                                    <div class="slds-col slds-size_2-of-12 slds-hidden" aura:id="newAccBtn">
                                                        <lightning:buttonGroup>
                                                            <!-- <lightning:button variant="base" type="" name="editAccount" label="Edit" class="slds-p-right_xx-small" onclick="{! c.handleClick }"/> 
                                                            <lightning:button variant="base" disabled="true" type="" name="line" label="|"/>  -->
                                                            <lightning:button   variant="base" 
                                                                                type="button" 
                                                                                name="newAccount" 
                                                                                label="New Account" 
                                                                                class="slds-p-left_xx-small" 
                                                                                onclick="{! c.handleClick }"/>  
                                                        </lightning:buttonGroup>
                                                    </div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <!-- <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div> -->
                                                    <lightning:inputField fieldName="Contact_Mobile1__c" aura:id="contactMobile"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <!-- <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div> -->
                                                    <lightning:inputField fieldName="Contact_Other_Phone1__c" aura:id="contactOther"/>
                                                </div>
                                            </div>    
                                            <div class="slds-grid">
                                                <!-- <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div> -->
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Job_Title__c"/>
                                                </div>
                                                <!-- <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Contact_Email1__c"/>
                                                </div> -->
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div> 
                                            </div>    
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Contact_Email1__c" aura:id="contactEmail"/>
                                                </div>  
                                                <!-- <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Credit_Rep_or_Employee__c"/>
                                                </div> -->
                                            </div>                                        
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                            </div>  
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>                            
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Credit_Rep_or_Employee__c"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="ACR_Number__c"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                            </div>    
                                        </lightning:accordionSection>
                                    </lightning:accordion>
                                </aura:if>
                                <!-- RDAVID extra/task26762679-NewPartnerLightingPageUpdates_13_11_2019 -->
                                <aura:if isTrue="{!(v.relType == 'Lender')}">
                                    <lightning:accordion   allowMultipleSectionsOpen="true"
                                                            onsectiontoggle="{! c.handleSectionToggle }"
                                                            activeSectionName="{! v.activeSections }">
                                        <lightning:accordionSection name="rel_details" label="Relationship Details">
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium"> 
                                                    <lightning:inputField disabled="true" aura:id="partnerInput" fieldName="Partner__c"/>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:outputField fieldName="RecordTypeId"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">                                            
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                        <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                    </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField disabled="true" fieldName="Partition__c" aura:id="partitionIf"/>
                                                </div>
                                            </div>
                                        </lightning:accordionSection>
                                        <lightning:accordionSection name="con_details" label="Contact Details">
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-grid slds-p-right_medium">
                                                    <div class="slds-col slds-size_10-of-12">
                                                        <lightning:inputField aura:id="assAcc" fieldName="Associated_Account__c" onchange="{! c.handleAccountChange }"/>
                                                    </div>
                                                    <div class="slds-col slds-size_2-of-12 slds-hidden" aura:id="newAccBtn">
                                                        <lightning:buttonGroup>
                                                            <lightning:button   variant="base" 
                                                                                type="button" 
                                                                                name="newAccount" 
                                                                                label="New Account" 
                                                                                class="slds-p-left_xx-small" 
                                                                                onclick="{! c.handleClick }"/>  
                                                        </lightning:buttonGroup>
                                                    </div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Contact_Mobile1__c" aura:id="contactMobile"/>
                                                </div>
                                            </div>
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Contact_Other_Phone1__c" aura:id="contactOther"/>
                                                </div>
                                            </div>    
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Job_Title__c"/>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div> 
                                            </div>    
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <lightning:inputField fieldName="Contact_Email1__c" aura:id="contactEmail"/>
                                                </div>  
                                            </div>                                        
                                            <div class="slds-grid">
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                                <div class="slds-col slds-size_5-of-12 slds-p-right_medium">
                                                    <div class="slds-box slds-box_x-small slds-text-align_center slds-m-around_x-small borderno"></div>
                                                </div>
                                            </div>  
                                        </lightning:accordionSection>
                                    </lightning:accordion>
                                </aura:if>
                                <aura:if isTrue="{!v.partnerRecordtype == 'Lender Parent' || v.partnerRecordtype == 'Lender Division' || v.partnerRecordtype == 'Insurance Category' || v.partnerRecordtype == 'Supplier'}">
                                    <lightning:inputField aura:id="recordOwnerID" fieldName="OwnerId" class="slds-hidden"/>
                                </aura:if> <!--RDAVID extra/task26558315-AddPartnerPageUPDATES-->
                            </lightning:recordEditForm>
                        </div>
                    </div>
                </lightning:layoutItem>
                <!--BODY END-->
                <!--FOOTER START-->
                <lightning:layoutItem size="12">                           
                    <div class="slds-docked-form-footer">
                        <lightning:button disabled="{!v.disabled}" variant="brand" type="button" name="save" label="Save" onclick="{!c.handleButton}" />
                    </div>
                </lightning:layoutItem>
                <!--FOOTER END-->
            </lightning:layout>
        </div>
        
    </aura:if>
</aura:component>