<!--
  @Page Name          : EquifaxLightningApp.cmp
  @Description        : Main application of Equifax Page
  @Author             : Rex.David
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0    10/12/2019, 9:12 am          Rex.David             Initial Version - tasks/22445169 - Equifax Integration
-->
<aura:application extends="ltng:outApp" >
	<aura:dependency resource="c:EquifaxLightningMainNM" />
    <aura:dependency resource="markup://force:*" type="EVENT"/>
    <aura:dependency resource="markup://force:showToast" type="EVENT"/>
</aura:application>