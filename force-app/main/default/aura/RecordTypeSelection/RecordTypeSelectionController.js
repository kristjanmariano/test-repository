({
	// init - Runs on component load
	onInit: function (component, event, helper) {

		let isFromLead = component.get("v.fromLead");
		console.log('>> RecordTypeSelection isFromLead: '+isFromLead);
		//New Partner is accessed from Convert button on Nodifi Partner Lead
		if (isFromLead){
			console.log('isFromLead');
			var getLeadForConvert = component.get('c.getNodifiPartnerLead');
			getLeadForConvert.setStorable();
			getLeadForConvert.setParams({
				recID: component.get("v.leadId")
			});
			getLeadForConvert.setCallback(this,
				function (response) {
					var state = response.getState();
					console.log('state: '+state);
					if (state === "SUCCESS") {
						console.log('Lead response: ' + JSON.stringify(response.getReturnValue()));
						component.set("v.leadObj", response.getReturnValue());

						var cmpTarget = component.find('Modalbox');
						var cmpBack = component.find('Modalbackdrop');
						$A.util.addClass(cmpTarget, 'slds-fade-in-open');
						$A.util.addClass(cmpBack, 'slds-backdrop--open');
					}
					else{
						var errors = response.getError();
						if (errors) {
							if (errors[0] && errors[0].message) {
								console.log("Error message: " +errors[0].message);
							}
						} else {
							console.log("Unknown error");
						}
					}
				}
			);
			$A.enqueueAction(getLeadForConvert);
		}
		//New Partner page is access from Partner object
		else{
			var cmpTarget = component.find('Modalbox');
			var cmpBack = component.find('Modalbackdrop');
			$A.util.addClass(cmpTarget, 'slds-fade-in-open');
			$A.util.addClass(cmpBack, 'slds-backdrop--open');
		}
	},

	// cancel - fires event when Cancel btn is clicked (Available for Lead to Partner)
	cancel: function (component, event, helper){

		console.log('>> cancel');
		let retURL = "/" + component.get("v.leadId") + '?isdtp=vw';

		//Cancel for SF Classic Console
		let enclosedPrimTab = function(res){
			let tabId = res.id;
			sforce.console.openPrimaryTab(tabId, retURL, true);
		};
		sforce.console.getEnclosingPrimaryTabId(enclosedPrimTab); 
		
		/* For LeX 
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
		  "url": retURL
		}); 
		urlEvent.fire(); */
	},

	// next - fires event when Next btn is clicked
	next: function (component, event, helper) {
		component.set("v.disableNext", true);
		component.set("v.nextProcessing", true);
		
		window.setTimeout($A.getCallback(function () {
			if (component.isValid()) {
				// $A.util.addClass(div, "slds-transition-show");
				// component.set("v.boooo", !component.get("v.boooo"));
				var myEvent = component.getEvent("nxtBtnClkEvt");
				myEvent.setParams({
					"param": component.get("v.value"),
					"rtName": component.get("v.rtObjectMap")[component.get("v.value")],
					"leadObj" : component.get("v.leadObj")
				});
				myEvent.fire();
				// component.set("v.disableNext", false);
				// component.set("v.nextProcessing", false);
			}
		}), 3000);	
	},

	// optSelected - fires when an option is selected
	optSelected: function (component, event, helper) {
		// console.log('optSelected');
		component.set("v.disableNext", false)
	},

	handleClick: function (component, event, helper) {
		// var div = component.find('msg');
		//Show the success message after 3 seconds...
		
	}
})