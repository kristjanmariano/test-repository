({
    // init - Runs on component load
    init: function (component, event, helper) {
        console.log('>> New Partner Form init');
        console.log('fromLead: '+component.get("v.fromLead"));
        console.log('leadId: '+component.get("v.leadId"));
        console.log('leadObj: '+JSON.stringify(component.get("v.leadObj")));
        var sobjectName = 'Partner__c';//component.get('v.sObjectName');
        var recordId = component.get('v.recordId');
        var selPartnerRT = component.get('v.selRTName');
        var branchParentId = component.get('v.branchParentId');

        var getFormAction = component.get('c.getForm');
        getFormAction.setStorable();
        console.log('test -> '+ selPartnerRT + ' --- '+branchParentId);
        getFormAction.setParams({
            recordId: null,
            objectName: sobjectName,
            rtName: selPartnerRT,
            branchParentId: branchParentId
        });

        getFormAction.setCallback(this,
            function (response) {
                var state = response.getState();

                if (component.isValid() && state === "SUCCESS") {
                    var form = response.getReturnValue();
                    helper.getActiveSections(component, form[selPartnerRT]);
                    if(!$A.util.isEmpty(form[selPartnerRT][0].BranchRTId)){
                        component.set('v.branchRTId', form[selPartnerRT][0].BranchRTId);
                        if(!$A.util.isEmpty(branchParentId))  component.set('v.recordTypeId', form[selPartnerRT][0].BranchRTId); //RDAVID 24/06/2019 1:57pm
                        component.set('v.branchRTName', form[selPartnerRT][0].BranchRTName);
                        component.set('v.user', form[selPartnerRT][0].currentUser);
                        helper.getPartnerType(component);
                        helper.prepopulatePartnerFieldsFromLead(component);
                    }
                    else{ //RDAVID extra/task26558315-AddPartnerPageUPDATES
                        component.set('v.user', form[selPartnerRT][0].currentUser);
                        helper.getPartnerType(component);
                    }
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            }
        );
        $A.enqueueAction(getFormAction);
    },

    // cancel - fires event when Cancel btn is clicked (Available for Lead to Partner)
	cancel: function (component, event, helper){

		console.log('>> cancel');
		let retURL = "/" + component.get("v.leadId") + '?isdtp=vw';

		//Cancel for SF Classic Console
		let enclosedPrimTab = function(res){
			let tabId = res.id;
			sforce.console.openPrimaryTab(tabId, retURL, true);
		};
		sforce.console.getEnclosingPrimaryTabId(enclosedPrimTab);
		
		/* For LeX 
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
		  "url": retURL
		}); 
		urlEvent.fire(); */
	},

    // handleButton - Runs when Footer btns were clicked
    handleButton: function (component, event, helper) {
        var partnerNameValid = component.find('partnerNameIf').get("v.value");
        var partnerType = component.get("v.partnerType");
        var partnerRTWithNoLegEnt = component.get("v.partnerRTWithNoLegalEntity");

        //extra/task26558315-AddPartnerPageUPDATES : Set Partition to Open if RT = 'Lender Parent' OR 'Supplier'
        if(partnerRTWithNoLegEnt.includes(partnerType)){
            if(component.find("partitionIf")) component.set("v.partition",component.find("partitionIf").get("v.value"));
        }
        
        if(!$A.util.isEmpty(partnerNameValid) && partnerNameValid.replace(/\s/g, '').length){
            component.set('v.showSpinner', true);
            component.set('v.disabled', true);
            component.set('v.whatBtn', event.getSource().get("v.name"));
            component.find("partnerForm").submit();
        }
        else{
            window.scrollTo(0, 0);
            helper.fireCustomToast(component,'error','Please enter Partner Name');
        }
    },

    // handleLoad - Invokes when lightning:recordEditForm is loaded
    handleLoad: function (component, event, helper) {
        var partnerType = component.get("v.value");
        //RDAVID extra/task26558315-AddPartnerPageUPDATES
        if(component.find('legEntNam')) component.find('legEntNam').set('v.value', component.get("v.legEntId"));        
        if(component.find('partnerParent')) component.find('partnerParent').set('v.value', component.get("v.branchParentId"));
        if(component.find('partAddress')) component.find('partAddress').set('v.value', component.get("v.parAddId"));
        var toggleNewAddress;
        
        if(component.find('newAddress')){
            toggleNewAddress = component.find("newAddress");
            $A.util.removeClass(toggleNewAddress, "slds-hidden");
        } 
    
        if(component.find('newLen')){
            var toggleNewLen = component.find("newLen");
            $A.util.removeClass(toggleNewLen, "slds-hidden");
        } 
       
        // var toggleForm = component.find("newPartnerForm");
        // $A.util.removeClass(toggleForm, "slds-hidden");
        component.set("v.showSpinner", false);
        helper.setDefaultValues(component);

    },

    // handleSubmit - Invokes when lightning:recordEditForm is submitted
    handleSubmit: function (component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.disabled', true);
    },

    // handleSuccess - Invokes when record is successfully saved
    handleSuccess: function (component, event, helper) {
        var payload = event.getParams().response;
        var whatBtn = component.get('v.whatBtn');

        component.set('v.saved', true);
        component.set('v.disabled', false);
        component.set('v.recordId', payload.id);
        let isFromLead = component.get("v.fromLead");

        if (whatBtn == 'save') { //Save and Redirect to Partner record
            if (isFromLead){
                helper.updateLeadToClosedWonAndAddPartnerNotes(component,helper);
            }
            else{
                component.set('v.showSpinner', false);
                helper.fireCustomToast(component,'success','Partner record successfully saved!');
                var navEvt = $A.get("e.force:navigateToSObject");
                var reltxt = 'Relationship';
                console.log('reltxt -- ',reltxt);
                navEvt.setParams({
                    "recordId": payload.id
                });
                navEvt.fire();
            }
        }

        else if (whatBtn == 'saveandpc') { //Save and Create to Person Contact (Relationship)
            if (isFromLead){
                helper.updateLeadToClosedWonAndAddPartnerNotes(component,helper);
            }
            else{
                component.set('v.showSpinner', false);
                helper.fireCustomToast(component,'success','Partner record successfully saved!');
                component.set('v.selectedTab', 'relationship_details');
            }
        }

        else if (whatBtn == 'saveandpb') { //Save and Create to Partner Branch
            component.set('v.showSpinner', false);
            helper.fireCustomToast(component,'success','Partner record successfully saved!');
            helper.handleCreateBranch(component);
        }

        else if (whatBtn == 'Link to ABN') { 
            component.set('v.showSpinner', false);
            component.set('v.selectedTab', 'legal_entity');
        }

        else if (whatBtn == 'Link to Address') {
            component.set('v.showSpinner', false);
            component.set('v.selectedTab', 'address');
        }
    },

    // handleError - Invokes when record is NOT successfully saved
    handleError: function (component, event, helper) {
        component.set('v.saved', false);
        component.set('v.disabled', false);
        component.set('v.showSpinner', false); //RDAVID extra/task26558315-AddPartnerPageUPDATES
        helper.fireCustomToast(component,'error','Something went wrong. Please contact System Admin.');
    },
    // handleSectionToggle - Invokes when lightning:accordion is toggled
    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
    },

    // handleClick - Invokes when button in lightning:accordionSection is clicked
    handleClick: function (component, event, helper) {
        var btnLbl = event.getSource().get("v.label"); 
        var partnerNameValid = component.find('partnerNameIf').get("v.value");
        // alert(partnerNameValid);
        if(!$A.util.isEmpty(partnerNameValid) && partnerNameValid.replace(/\s/g, '').length){
            component.set('v.showSpinner', true);
            if (btnLbl == 'Link to ABN') {
                component.set('v.whatBtn', btnLbl);
                component.find("partnerForm").submit();
            }
            else if (btnLbl == 'Link to Address') {
                component.set('v.whatBtn', btnLbl);
                component.find("partnerForm").submit();
            }
        }
        else{
            window.scrollTo(0, 0);
            helper.fireCustomToast(component,'error','Please enter Partner Name');
        }
    }
})