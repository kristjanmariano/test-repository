({
  //Change History
  //1.2    31/10/2019, 3:00 pm	      Rex.David	            extra/task26558315-AddPartnerPageUPDATES - Additional RTs
	getActiveSections : function(component, fieldsets) {
		var openSections = [];
		for(var fs in fieldsets){
			openSections.push(fieldsets[fs].SectionId);
		}
		// component.set("v.activeSections",openSections);
		component.set('v.partnerFieldSets', fieldsets);
		component.set("v.initContentloaded",true);
	},

	handleCreateBranch : function(component) {
		var myEvent = component.getEvent("creBraBtnClk");
		myEvent.setParams({	"branchParentId": component.get('v.recordId'),
							"branchRTName": component.get('v.branchRTName'),
							"branchRTId": component.get('v.branchRTId'),
							"branchParentRTName": component.get('v.selRTName'),
						});
		myEvent.fire(); 
	},	

	fireCustomToast: function(component,messageType,message) {
		var customToastEvent = $A.get("e.c:CustomToastAppEvt");
		customToastEvent.setParams({    "messageType" : messageType,
										"message" : message });
		customToastEvent.fire();
	},	

	prepopulatePartnerFieldsFromLead: function(component){
		console.log('>> prepopulatePartnerFieldsFromLead ');
		console.log('>> partnerType: '+component.get("v.partnerType"));
		let isFromLead = component.get("v.fromLead");
		if (isFromLead){ //New Partner is accessed from Convert button of Lead record
			let leadToConvert = component.get("v.leadObj");
			//Pre-populate Partner with Lead fields for conversion
			if (leadToConvert != undefined){
				component.find("partitionIf").set("v.value", leadToConvert.Partition__c); //Partition__c
				component.find("channelIf").set("v.value", leadToConvert.Channel__c); //Channel
                component.find("leadsource").set("v.value", leadToConvert.LeadSource); //Lead Source
				component.find("rBusiName").set("v.value", leadToConvert.Business_Name__c); //Business Name
				component.find("legEntNam").set("v.value", leadToConvert.ABN__c); //Legal Entity Name
				component.find("boxFolderID").set("v.value", leadToConvert.Box_Folder_ID__c); //Box Folder ID
				component.find("busiSummary").set("v.value", leadToConvert.Business_Summary__c); //Business Summary
				component.find("latestcomment").set("v.value", leadToConvert.Latest_Comment__c); //Latest Comment
				component.find("status").set("v.value", 'Accredited'); //Set Status to Accredited
				component.find("stage").set("v.value", '0-5 Deals'); //Set Stage to 0-5 Deals
				component.find("hleadID").set("v.value", leadToConvert.Id); //Lead ID
				component.find("pevent").set("v.value", leadToConvert.Event__c); //Event ID
				component.find("ownrId").set("v.value", leadToConvert.OwnerId); //OwnerId
				component.find("ownrName").set("v.value", leadToConvert.Owner.Name); //OwnerId
				if (component.find("curAggregator") != undefined) component.find("curAggregator").set("v.value", leadToConvert.Current_Aggregator__c);
			}
		}
	},

	getPartnerType: function(component) {
		// var myEvent = component.getEvent("creBraBtnClk");
		// 'Dealership' , 'Dealer Branch', 'Finance', 'Broker Branch'
		var partnerRTWithNoLegEnt = component.get("v.partnerRTWithNoLegalEntity"); //RDAVID extra/task26558315-AddPartnerPageUPDATES
		if($A.util.isEmpty(component.get("v.branchParentId"))){
			var selectedRTName = component.get("v.selRTName");
			// alert(component.get("v.selRTName") + ' IS NOT A BRANCH RT');
			if(!$A.util.isEmpty(selectedRTName)){
				// alert('selectedRTName is NOT EMPTY selectedRTName = '+selectedRTName);
				if(selectedRTName.includes('Dealership')){
					component.set("v.partnerType","Dealership");
				}
				else if (selectedRTName.includes('Finance')){
					component.set("v.partnerType","Finance");
				}
				else if(partnerRTWithNoLegEnt.includes(selectedRTName)){
					component.set("v.partnerType",selectedRTName); //RDAVID extra/task26558315-AddPartnerPageUPDATES
				}
			}
		}
		else if(!$A.util.isEmpty(component.get("v.branchParentId"))){
			// alert('helper - '+component.get("v.branchRTName"));
			var selectedRTName = component.get("v.branchRTName");
			if(!$A.util.isEmpty(selectedRTName)){
				if(selectedRTName.includes('Dealer Branch')){
					component.set("v.partnerType","Dealer Branch");
				}
				else if (selectedRTName.includes('Broker Branch')){
					component.set("v.partnerType","Broker Branch");
				}
			}
		}
	},	

	setDefaultValues: function(component) {
    //RDAVID extra/task26558315-AddPartnerPageUPDATES
		var currentUserProfile = (!$A.util.isEmpty(component.get("v.user"))) ? component.get("v.user").Profile.Name : undefined;
		var partnerType = component.get("v.partnerType");
		let isFromLead = component.get("v.fromLead");
		var partnerRTWithNoLegEnt = component.get("v.partnerRTWithNoLegalEntity");
		if(!$A.util.isEmpty(currentUserProfile)){
			if(currentUserProfile.includes('Admin') ||  currentUserProfile.includes('Nodifi')){
				if (!isFromLead){
					if(component.find("partitionIf")) component.find("partitionIf").set("v.value", "Nodifi");
					if(component.find("channelIf")) component.find("channelIf").set("v.value", "NODIFI");
				}
			}
			else if(currentUserProfile.includes('PLS')){
				if(component.find("partitionIf"))  component.find("partitionIf").set("v.value", "Positive");
				if(component.find("channelIf")) component.find("channelIf").set("v.value", "PLS");
			}
			//extra/task26558315-AddPartnerPageUPDATES : Set Partition to Open if RT = 'Lender Parent' OR 'Supplier'
			if(!isFromLead){
				if(partnerRTWithNoLegEnt.includes(partnerType)){
					if(component.find("partitionIf")) component.find("partitionIf").set("v.value", "Open");				
				}
			}
			if(component.find("recordOwnerID")) {
				component.find("recordOwnerID").set("v.value", $A.get("$Label.c.API_Admin_User_Id"));//Set Record Owner to API Admin User
				
			}
		}
	},	

	updateLeadToClosedWonAndAddPartnerNotes: function(component,helper){

		var whatBtn = component.get('v.whatBtn');
		//Change Lead Status/Stage to Closed-Won upon completion of New Partner page from Lead Convert
		let leadObj = component.get("v.leadObj");
		leadObj.Status = 'Closed';
		leadObj.Stage__c = 'Won';
		var updateLeadToWon = component.get('c.updateNodifiPartnerLeadToWon');
		updateLeadToWon.setParams({
			leadObj: leadObj,
			partnerID: component.get('v.recordId')
		});
		updateLeadToWon.setCallback(this,
			function (response) {
				var state = response.getState();
				console.log('state: '+state);
				if (state === "SUCCESS") {
					console.log('Lead response: ' + JSON.stringify(response.getReturnValue()));
					component.set('v.showSpinner', false);
					if (whatBtn == 'save'){
						helper.fireCustomToast(component,'success','Partner record successfully saved!');
						var navEvt = $A.get("e.force:navigateToSObject");
						var reltxt = 'Relationship';
						console.log('reltxt -- ',reltxt);
						navEvt.setParams({
							"recordId": component.get('v.recordId')
						});
						navEvt.fire();
					}
					else if (whatBtn == 'saveandpc'){
						component.set('v.showSpinner', false);
						helper.fireCustomToast(component,'success','Partner record successfully saved!');
						component.set('v.selectedTab', 'relationship_details');
					}
				}
				else{
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.log("Error message: " +errors[0].message);
						}
					} else {
						console.log("Unknown error");
					}
				}
			}
		);
		$A.enqueueAction(updateLeadToWon);

	}

})