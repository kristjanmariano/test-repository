({
	setPicklistValues : function(component, initWrap) {
		component.set("v.permissionTypes", initWrap.permissionTypes);
		component.set("v.genderCodes", initWrap.genderCodes);
		component.set("v.equiAccTypeCodes", initWrap.equiAccTypeCodes);
		component.set("v.equiRelationshipCodes", initWrap.equiRelationshipCodes);
		component.set("v.equiEmploymentTypes", initWrap.equiEmploymentTypes);
		component.set("v.equiStateCodes", initWrap.equiStateCodes);
		component.set("v.equiCountryCodes", initWrap.equiCountryCodes);
	},

	validateInputFields : function(component) {
		console.log('Im validateInputFields');
		var allFieldsValid = false;
		var allValid = component.find('requestData').reduce(function (validSoFar, inputCmp) {
			inputCmp.showHelpMessageIfInvalid();
			return validSoFar && inputCmp.get('v.validity').valid;
		}, true);
		if (allValid) {
			allFieldsValid = true;
			console.log('All form entries look valid. Ready to submit!');
		} else {
			allFieldsValid = false;
			console.log('Please update the invalid form entries and try again.');
		}
		return allFieldsValid;
	},

	showPopup : function(component,action) {
		// alert(action);
		var initWrapper = component.get("v.initWrapper");
		component.set("v.equifaxEnquiryId",initWrapper.equifaxEnquiryId);
		var apiTransfer = component.get("v.apiTransfer");
		var JSONStr = JSON.stringify(apiTransfer); 
		var myEvent = component.getEvent("submitBtnClkEvt");
		console.log('equifaxEnquiryId == '+JSON.stringify(component.get("v.equifaxEnquiryId")));
		console.log('apiTransfer = ' + JSONStr);

		if(action == 'save'){
			if($A.util.isEmpty(initWrapper.equifaxEnquiryId)){ //Role doesn't have existing Equifax Enquiry Id
				myEvent.setParams({	"apiTransfer": component.get("v.apiTransfer"),
									"action": 'saveAPITransferInvokeEquifaxApply'});
				myEvent.fire();
			}
			else{
				myEvent.setParams({	"apiTransfer": component.get("v.apiTransfer"),
									"action": 'showModalHasCurrentEquifaxEnquiryId'});
				myEvent.fire();
			}
		}
		else if (action == 'select'){
			myEvent.setParams({	"apiTransfer": component.get("v.apiTransfer"),
								"action": 'showRoleSelect'});
			myEvent.fire();
		}
	}
})