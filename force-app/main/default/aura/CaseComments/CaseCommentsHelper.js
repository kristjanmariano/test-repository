({
	loadCaseComments : function(cmp, helper) {
		console.log('>> loadCaseComments');
		console.log('Current RecordID: '+ cmp.get('v.recordId'));
		cmp.set("v.loading", true);
		var loadCtr = cmp.get("v.loadCtr");
		// Create the action
		var action = cmp.get("c.getCaseAndComments");
		// Add callback behavior for when response is received
		console.log('pre enqueueAction Get State: '+action.getState());
		action.setParams(
			{ cseID : cmp.get('v.recordId') }
		);
		action.setCallback(this, function(response) {
			console.log('loadCtr: '+loadCtr);
			var state = response.getState();
			console.log('STATE: '+state);
			if (state === 'SUCCESS'){
				cmp.set("v.returnObj", response.getReturnValue());
				cmp.set("v.userSFId", response.getReturnValue().userSFId);
				cmp.set("v.loading", false);
				cmp.set("v.loadCtr", loadCtr);
			}
			else{
				console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);
	},

	validateNewCaseComment : function(cmp, evt, helper){
		var tacomment = cmp.find('taComment');
		var tacommentVal = tacomment.get("v.value");
		//Check if Comment is null or empty - Don't allow saving of Case Comment
		var checkCommentIfNull = function checkCommentIfNull(str){ 
			return str === null || str.match(/^\s*$/) !== null; 
		}
		if ( checkCommentIfNull(tacommentVal) ){
			cmp.set("v.disableSave", true);
		}
		else{ 
			cmp.set("v.disableSave", false);
		}
		
	},

	createNewCaseComment : function(cmp, evt, helper){
		console.log('>> createNewCaseComment');
		var returnObjComment = cmp.get('v.returnObj').curComment;
		cmp.set("v.onSave", true);
		cmp.set("v.loadCtr", 1);
		// Create the action
		var action = cmp.get("c.saveCaseComment");
		// Add callback behavior for when response is received
		action.setParams(
			{ comment : returnObjComment }
		);
		action.setCallback(this, function(response) {
			var state = response.getState();
			console.log('STATE: '+state);
			if (state === 'SUCCESS'){
				cmp.set("v.saveSuccess", response.getReturnValue());
				cmp.set("v.loading", false);
				//Reload Case Comment sidebar due to refresh Sub Tab (will call doInit)
				var refreshSubtabByIDCallback = function(){
					console.log('Successfully created Case Comment, Case tab refreshed');
					//For Case as Sub-tab
					console.log('loadCtr on save: '+cmp.get("v.loadCtr"));
					if (cmp.get("v.loadCtr") === 1){ 
						//Only refresh if current subtab is Case record
						if ( cmp.get("v.recordId").startsWith('500')){
							location.reload();
						}
					}
				};
				var refreshSubTab = function refreshSubTab(result){
					//Refresh focused Sub Tab
					console.log('refreshSubTab result: '+result.id);
					sforce.console.refreshSubtabById(result.id, true, refreshSubtabByIDCallback);
				};
				sforce.console.getFocusedSubtabId(refreshSubTab);
				//helper.loadCaseComments(cmp, helper);
			}
			else{
				cmp.set("v.loading", false);
				cmp.set("v.onSave", false);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				cmp.set("v.errmessage", errmsg);
				console.log("Failed with state: " + state);
				console.error('Return message:' + errmsg);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);
	},

	toggleCaseCommentLC : function(cmp, helper){
		console.log('>> toggleCaseCommentsLC');
		var show = cmp.get("v.showCCommentLC");
		var recID = cmp.get("v.recordId");
		console.log('recID: '+ recID);
		if (recID === undefined || recID === null || recID === '') show = false;
		else show = ( String(recID).startsWith('500') ) ? true : false; //Check if Record is a Case
		cmp.set("v.showCCommentLC", show);
		console.log('Show Case Comment sidebar: ' + show);
		var sidebarcallback = function(result) {};
		sforce.console.setSidebarVisible(show, null, sforce.console.Region.RIGHT, sidebarcallback);
		//Reload Case Comment sidebar upon switching of Tab
		if (show) helper.loadCaseComments(cmp, helper);
		
	},

	//Classic Service Cloud Console
	sfCCommentsConsoleFunctions : function(cmp,helper){
		console.log('>> CaseComments sfConsoleFunctions');
		var show = cmp.get("v.showCCommentLC");
		var loadCtr = cmp.get("v.loadCtr");

		var showObjectId = function showObjectId(result) {
			console.log('>> CaseComments getFocusedSubtabObjectId callback');
			console.log('Current Record ID: ' + result.id);
			cmp.set("v.recordId", result.id);
			//Will call toggleCaseCommentLC due to recordID Change
		};
		var onFocusTab = function (result){
			sforce.console.getFocusedSubtabId(onFocusEvent);
		}
		var onFocusEvent = function (result){
			console.log('Focused SubtabID : '+ result.id);
			sforce.console.getFocusedSubtabObjectId(showObjectId);
		};
		sforce.console.onFocusedSubtab(onFocusTab);  

		//Needed for Primary Tab and Sub Tab to detect CDC Streaming Event.
		var enclosingTabRefreshloadHelperCtr = 0;
		var eventHandler = function eventHandler(result) {
			console.log('Result: '+ result.id + ' ' + result.objectId);
			var curRecID = result.objectId;
			enclosingTabRefreshloadHelperCtr++;
			if (enclosingTabRefreshloadHelperCtr == 1){ 
				if (show){ 
					//Only reload if current tab is a Case record
					if ( String(curRecID).startsWith('500')){
						//Reload CaseComments sidebar on TabRefresh (will call doInit)
						if (loadCtr === 1) location.reload();	
					}
				}
			}
		};
		//Already being handled by Streaming API (Change Data Capture on Case)
		//sforce.console.onEnclosingTabRefresh(eventHandler); 
	},

	subscribeStreamingChannel : function (cmp, helper){
		//if (!$.cometd.isDisconnected()) $.cometd.disconnect(); //Disconnect previous Channel
		console.log('>> subscribeStreamingChannel');
		console.log('Current Channel: '+ cmp.get("v.streamingChannel"));
		console.log('>> Comet Status subscribeStreamingChannel: ' + $.cometd.getStatus());
		var subscribeloadHelperCtr = 0;
		console.log('subscribeloadHelperCtr from subscribeStreamingChannel: '+ subscribeloadHelperCtr);
		 //subscribe
		 if (!$.cometd.isDisconnected()){
			$.cometd.subscribe(cmp.get("v.streamingChannel"), $A.getCallback(function (message) {
				console.log('in subscribe');
				subscribeloadHelperCtr++;
				if (subscribeloadHelperCtr == 1){ 
					//Only refresh Case Comment sidebar display on change of current Committed User and current object record ID and if tab is on current object 
					var cdcPayload = message.data.payload.ChangeEventHeader;
					console.log('Current User ID: '+cmp.get("v.userSFId"));
					console.log('Committed User ID: '+ cdcPayload.commitUser);
					console.log('Updated Record ID: '+ cdcPayload.recordIds);
					var matchingRecID = false;
					var matchingUserID = ( cmp.get("v.userSFId") == cdcPayload.commitUser) ? true : false;
					var updatedRecordIDs = cdcPayload.recordIds;
					console.log('matchingUserID: '+ matchingUserID);
					for (var i=0; i < updatedRecordIDs.length; i++){
						if (cmp.get("v.recordId") == updatedRecordIDs[i]){
							matchingRecID = true; 
							break;
						}
					}
					console.log('matchingRecID: '+ matchingRecID);
					if (matchingRecID && matchingUserID){ 
						helper.loadCaseComments(cmp, helper);
					}
				}
				console.log('>> Comet Status subscribeStreamingChannel callback: ' + $.cometd.getStatus());
			}));
		}
	},

	connectToStreamingAPI : function (cmp, evt, helper){
		console.log('>> connectToStreamingAPIAndBasicInfo');
		console.log('>> Comet Status pre callback connectToStreamingAPIAndBasicInfo: ' + $.cometd.getStatus());

		var sessionaction = cmp.get("c.getSessionID");
		var connect_loadHelperCtr = 0;
		console.log('connect_loadHelperCtr from connectToStreamingAPIAndBasicInfo: '+ connect_loadHelperCtr);
		sessionaction.setCallback(this, function(response) {
			console.log('Session ID: ' + response.getReturnValue());
			$.cometd.init({
				url: '/cometd/39.0',
				requestHeaders: {Authorization: 'OAuth ' + response.getReturnValue() },
				appendMessageTypeToURL: false,
				logLevel: [ "warn" | "info" | "debug" ]
			});
			console.log('loadHelperCtr from connectToStreamingAPIAndBasicInfo callback: '+ connect_loadHelperCtr);
			connect_loadHelperCtr++;
			if (connect_loadHelperCtr == 1){ 
				//Subscribe to CaseChangeEvent - Will load Case Comments on callback
				helper.subscribeStreamingChannel(cmp, helper);
			}
			console.log('>> Comet Status during connectToStreamingAPIAndBasicInfo: ' + $.cometd.getStatus());
		});
		$A.enqueueAction(sessionaction); 

	}

})