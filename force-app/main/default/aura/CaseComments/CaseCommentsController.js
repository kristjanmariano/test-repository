({
	doInit : function(cmp, evt, helper) {
		console.log('>> doInit');
		console.log('recID: '+cmp.get("v.recordId"));
		cmp.set("v.showCCommentLC", true);
		helper.toggleCaseCommentLC(cmp,helper);
		var showCComments = cmp.get("v.showCCommentLC");
		helper.sfCCommentsConsoleFunctions(cmp, helper); //Console Behaviour Support
		if (showCComments){ 
			helper.connectToStreamingAPI(cmp, evt, helper); //Remove once on Lightning Experience
			//Call Load Comments from connectToStreamingAPI
		}
	},

	validateComment : function(cmp, evt, helper){
		helper.validateNewCaseComment(cmp, evt, helper);
	},

	saveComment : function(cmp, evt, helper){
		helper.createNewCaseComment(cmp, evt, helper);
	},

	openCaseComments : function(cmp, evt, helper){
		console.log('>> openCaseComments');
		var openCommentsSubTab = function openCommentsSubTab(result) {
			console.log('>> openCommentsSubTab');
			var primaryTabId = result.id;
			var commURL = '/apex/CaseCommentsList?id='+cmp.get("v.recordId") +'&isdtp=vw';
			//var stabName = cmp.get("v.cse").CaseNumber + ' Comments';
			console.log('primaryTabId : ' + primaryTabId);
			console.log(commURL);
			//Open Case Comments subtab
			var openSubtabCallback = function openSubtabCallback(result){
				console.log('>> openSubtabCallback');
				var subtabId = result.id;
				console.log('subTabID: '+ subtabId);
                sforce.console.focusSubtabById(subtabId);
            }
			sforce.console.openSubtab(primaryTabId,commURL,false,'Comments',null,openSubtabCallback);
			
		};
		//SF Console Classic, verify if working on Lightning Console
		sforce.console.getEnclosingPrimaryTabId(openCommentsSubTab);
		
	},

	recordChange : function(cmp, evt, helper){
		console.log('>> CaseComment recordChange');
		console.log("old value: " + evt.getParam("oldValue"));
		console.log("current value: " + evt.getParam("value"));
		helper.toggleCaseCommentLC(cmp, helper);
	},

	manualReload : function(cmp, evt, helper){
		console.log('>> manual reload');
		location.reload();
	}

})