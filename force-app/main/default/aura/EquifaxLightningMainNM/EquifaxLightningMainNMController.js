({
    init: function(component,event,helper) {
        // helper.waitForAPITransferUpdate(component,event);
        var getInitWrapper = component.get("c.initMethod");
        getInitWrapper.setParams({  recId : component.get("v.recordId")  });
        getInitWrapper.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
				var initWrap = response.getReturnValue();
				console.log('initWrap = ',initWrap);
                component.set("v.initWrapper", initWrap);
                component.set("v.record",initWrap.mainRec);
                console.log(component.get("v.record"));
                component.set("v.initWrapperValid",initWrap.valid);
                component.set("v.errMsg",initWrap.errMsg);
                component.set("v.showPopup",initWrap.showRoleSelectionOnInit);
                component.set("v.showRoleSelect",initWrap.showRoleSelectionOnInit);
                component.set("v.doneInitApexCall", true);
                //Set the apiTransferData if 1 Role exist
                if(!component.get("v.showRoleSelect")){
                    component.set("v.apiTransfer", initWrap.apiTransferData);
                    component.set("v.apiTransfer.EQ_Role__c",initWrap.selectedRoleId);
                } 
            }
            else if(state === 'ERROR'){
                //var list = response.getReturnValue();
                //component.set("v.picvalue", list);
                component.set("v.doneInitApexCall", true);
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error('ERROR - ',message);
            }
        })
        $A.enqueueAction(getInitWrapper);
    },
    
	// handleMenuSelected - Invoked when user clicks menu item
    handleMenuSelected: function(component, event, helper) {
		var selected = event.getParam('name');
		// console.log(selected);
        component.set('v.selectedTab', selected);

    },

    submitDetails: function(component, event, helper) {
        // Set isModalOpen attribute to false
        //Add your code to call apex method or do some processing
        component.set("v.isModalOpen", false);
    },

    itemsChange: function(component, event, helper) {
        console.log("selectedRole has changed");
        var oldValue = event.getParam("oldValue");
        var currentValue = event.getParam("value");
        if(oldValue != currentValue && currentValue != null){
            component.set("v.showPopup",false);
            component.set("v.showRoleSelect",false);
            var initWrapper = component.get("v.initWrapper");
            initWrapper.selectedRoleId = currentValue;
            component.set("v.initWrapper",initWrapper);
            helper.loadFormData(component, event);
        }
    },

    recordUpdated: function(component, event, helper) {
        // Set isModalOpen attribute to false
        //Add your code to call apex method or do some processing
        console.log("im handling recordUpdated.");
    },

    submitButtonClicked: function(component,event,helper) {
        var action = JSON.parse(JSON.stringify(event.getParam("action")));
        if(action == 'saveAPITransferInvokeEquifaxApply' || action == 'apply' || action == 'retrieve'){
            helper.saveAPITransfer(component,event,helper);
        }
        else if (action == 'showModalHasCurrentEquifaxEnquiryId'){
            component.set("v.showPopup",true);
            component.set("v.progressMessage",helper.buildHasCurrentEnquiryMsg(component,event,helper));
        }
        else if (action == 'openBox'){
            // component.set("v.showPopup",true);
            helper.fireEquifaxAppEvt(component, 'redirecttobox');
            // component.set("v.progressMessage",helper.buildHasCurrentEnquiryMsg(component,event,helper));
        }
        else if (action == 'showRoleSelect'){
            var init = component.get('c.init');
            $A.enqueueAction(init);
        }
    },
})