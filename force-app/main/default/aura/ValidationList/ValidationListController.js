({
	doInit : function(cmp, evt, helper){
		console.log('>> doInit');
		console.log('Passed RecordId: ' + cmp.get("v.recordId"));

		let doneInit = cmp.get("v.doneInit");
		console.log('doneInit: '+doneInit);
		//Identify Record ID base from opened console Tab (Inside LC)
		var showObjectIdOnLoad = function showObjectIdOnLoad(result) {
			console.log('>> getFocusedSubtabObjectId callback');
			console.log('Current Record ID: ' + result.id);
			cmp.set("v.recordId", result.id);
			helper.connectToStreamingAPIAndBasicInfo(cmp, evt, helper);
		};
		var onFocusOnLoad = function (result){
			console.log('Focused SubtabID : '+ result.id);
			sforce.console.getFocusedSubtabObjectId(showObjectIdOnLoad);
		}; 
		helper.connectToStreamingAPIAndBasicInfo(cmp, evt, helper); //Needed for Primary Tab on Load
		sforce.console.getFocusedSubtabId(onFocusOnLoad); 
		//helper.loadObjectRecord(cmp, helper, cmp.get("v.recordId")); //Moved insider connectToStreamingAPI
		
	},

	manualValidation: function(cmp, evt, helper){
		console.log('>> manualValidation');
		cmp.set("v.displayValResults", true);
		console.log('displayValResults: '+cmp.get("v.displayValResults"));
		helper.loadObjectRecord(cmp, helper, cmp.get("v.recordId"));
	},

    recordChange: function(cmp, evt, helper) {
		console.log('>> recordChange');
        console.log("recordId has changed");
        console.log("old value: " + evt.getParam("oldValue"));
		console.log("current value: " + evt.getParam("value"));
		//$.cometd.disconnect(true);
		helper.connectToStreamingAPIAndBasicInfo(cmp, evt, helper);
		//helper.loadObjectRecord(cmp, helper,evt.getParam("value"));

	},

	validateOverrideReason: function(cmp, evt, helper){
		helper.validateORreason(cmp);
	},

	checkOverride : function(cmp, evt, helper){
		helper.checkOverride(cmp);
	},

	overrideValidations : function(cmp, evt, helper){
		console.log('>>overrideValidation');
		helper.overrideValidations(cmp,helper);
	}


})