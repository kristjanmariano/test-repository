({
	progressbarChange : function(cmp, helper, valctr, valsize) {
		var times=0;
		var current=cmp.get('v.progress');
		var final=cmp.get('v.progressEnd');
		var increment= 10 ;
		if (final<current) {
			increment=-10;
		}
		var self=this;
		var timeoutRef = window.setInterval($A.getCallback(function() {
			if (cmp.isValid()) {
				var value=cmp.get('v.progress');
				value+=increment;
				if (value==final) {
					window.clearInterval(cmp.get('v.timeoutRef'));
					cmp.set('v.timeoutRef', null);
				}
				cmp.set('v.progress', value);
			}
		}), 100);
		cmp.set('v.timeoutRef', timeoutRef);
	}, 

	loadObjectRecord : function(cmp,helper,recordId) {
		console.log('>> loadObjectRecord');
		console.log('Current RecordID: '+ recordId);
		//console.log('Current Record Basic Data '+JSON.stringify(cmp.get("v.basicRecInfo")));
		cmp.set("v.progressEnd", 10);
		let validateRecRecords = cmp.get("v.validateRecRecords");
		
		cmp.set("v.loading", true);
		cmp.set("v.validToCheck", true);
		if (recordId != undefined && recordId != 'null' && recordId != null && recordId != '' ){
			if (!cmp.get("v.manualValidation") || cmp.get("v.displayValResults")){ //Do Automated Validation - auto show Validation Results
				cmp.set("v.isProgressing", true);
				var relRecs = cmp.get("v.basicRecInfo").relRecsWr;
				console.log('relRecs : '+ JSON.stringify(relRecs));			
				let chain = Promise.resolve();  	
				//Validate related Records of main record
				if (relRecs != undefined && relRecs != '' && relRecs != null && validateRecRecords){
					chain = chain.then(
						(rr,main) => {
							helper.autoValaction(cmp,helper,relRecs,false);
						})
				}
				//Validate Main Record
				else{
					helper.autoValaction(cmp,helper,undefined,true);
				}
			}
			else{ //Do Manual Validation - show Validate Button
				cmp.set("v.displayValResults", false);
				cmp.set("v.loading", false);
				cmp.set("v.onSFCommit", false);
				helper.sfConsoleFunctions(cmp,helper);
				//helper.callStreamingAPISubscribeAndSfConsoleFunctions(cmp, helper, cmp.get("v.basicRecInfo").sobjName);
			}
		}
		else{
			helper.notValidForChecking(cmp,helper);
		}
	},

	autoValaction : function(cmp, helper, relRecs, main){ //function(cmp, helper, curRecID, curSobjRT, sequence, curSobjCtr, curSobjSize, main){
		var returnResponse; var endVal = false;
		console.log('main : '+main);
		//console.log('sequence: '+sequence);
		return new Promise($A.getCallback(
			function(resolve, reject){
			var action = cmp.get("c.getValidationListForObject");
			if (relRecs != undefined && !main){
				let objseqCtr = 0; let j = 0;
				let relRecWr = relRecs[objseqCtr].recordRTs;
				console.log('@@relRecWr.length: '+relRecWr.length);
				let curRecID = relRecWr[j].recID;
				let curSobjRT = relRecWr[j].sobjRT;
				console.log('@@curRecID:'+curRecID);
				action.setParams(
					{ recordId : curRecID, 
					  sobjRT : curSobjRT, 
					  ovdmt : null,
					  valOverride : false,
					}
				);
				action.setCallback(this, function(response){
					var state = response.getState();
					console.log('state: '+state);
					console.log('endVal: '+endVal);
					if (state == 'SUCCESS'){
						returnResponse = response.getReturnValue();
							console.log('returnresponse: '+JSON.stringify(returnResponse));
							cmp.set("v.objBeingvalidated", returnResponse.sobjLabel);
							console.log('@@objBeingvalidated: ' + returnResponse.sobjLabel);
							cmp.set("v.progressEnd", (j / relRecWr.length) * 100 );
							cmp.set("v.progress", (j / relRecWr.length) * 100 );
							//cmp.set("v.progressEnd", 50 );
							cmp.set('v.manualValidation', returnResponse.manualValidate);
							console.log('manualValidation: '+ cmp.get("v.manualValidation"));
							cmp.set("v.onSFCommit", true);
							cmp.set("v.returnObj", returnResponse);
							if (main){ 
								if (returnResponse.outcome.includes('MISSED')) cmp.set("v.valpassed", false);
								else if (returnResponse.outcome.includes('PASSED')) cmp.set("v.valpassed", true);
								cmp.set("v.returnObjMain", returnResponse);
							}
							//helper.progressbarChange(cmp, helper, j, relRecWr.length);
							console.log('object being validated auto: '+cmp.get("v.objBeingvalidated"));
							console.log('current relRecWr = '+curRecID + ' :  ' + curSobjRT);
							console.log('>> resolve');
						
						if (!endVal){
							if (j < relRecWr.length){
								if (j != ( relRecWr.length - 1) ){ 
									j++;
									console.log('@@j: '+j);
									action.setParams(
										{ recordId : relRecWr[j].recID, 
										sobjRT : relRecWr[j].sobjRT, 
										ovdmt : null,
										valOverride : false,
										}
									);
									$A.enqueueAction(action);
								}
								else{
									if (objseqCtr < relRecs.length){
										j = 0;
										if (objseqCtr != ( relRecs.length - 1) ) {
											objseqCtr++;
											console.log('@@objseqCtr: '+objseqCtr);
											relRecWr = relRecs[objseqCtr].recordRTs;
											action.setParams(
												{ recordId : relRecWr[j].recID, 
												sobjRT : relRecWr[j].sobjRT, 
												ovdmt : null,
												valOverride : false,
												}
											);
											$A.enqueueAction(action);
										}
										else{
											main = true;
											console.log('enqueue action for main Record');
											action.setParams(
												{ 	recordId : cmp.get("v.basicRecInfo").sobj.Id, 
													sobjRT : cmp.get("v.basicRecInfo").sobj.RecordType.Name, 
													ovdmt : null,
													valOverride : cmp.get("v.override")
												}
											);
											endVal = true;
											$A.enqueueAction(action);
										}
									}
								}
							}
						}
						else{
							console.log('end');
							cmp.set("v.loading", false);
							cmp.set("v.onSFCommit", false);
							cmp.set("v.displayValResults", true);
							//helper.callStreamingAPISubscribeAndSfConsoleFunctions(cmp, helper, cmp.get("v.basicRecInfo").sobjName);
							helper.sfConsoleFunctions(cmp,helper);
							resolve();
						}
					}
					else{
						console.log('>> reject');
						//console.log('basicRecInfo ovdmt: '+ JSON.stringify(cmp.get("v.basicRecInfo").ovdmt));
						console.log("Failed with state: " + state);
						console.log('Return message:' + response.getReturnValue());
						helper.notValidForChecking(cmp,helper); 
						reject();
					}
				});
				$A.enqueueAction(action); 
			}
			else{
				console.log('cmp.get("v.override"): '+cmp.get("v.override"));
				action.setParams(
					{ 	recordId : cmp.get("v.basicRecInfo").sobj.Id, 
						sobjRT : cmp.get("v.basicRecInfo").sobj.RecordType.Name, 
						ovdmt : null,
						valOverride : cmp.get("v.override")
					}
				);
				action.setCallback(this, function(response){
						var state = response.getState();
						console.log('state: '+state);
						if (state == 'SUCCESS'){
							returnResponse = response.getReturnValue();
							console.log('returnresponse: '+JSON.stringify(returnResponse));
							cmp.set("v.objBeingvalidated", returnResponse.sobjLabel);
							console.log('@@objBeingvalidated: ' + returnResponse.sobjLabel);
							//cmp.set("v.progressEnd", (curSobjCtr / curSobjSize) * 100 );
							cmp.set("v.progressEnd", 50 );
							cmp.set('v.manualValidation', returnResponse.manualValidate);
							console.log('manualValidation: '+ cmp.get("v.manualValidation"));
							cmp.set("v.onSFCommit", true);
							cmp.set("v.returnObj", returnResponse);
							if (main || cmp.get("v.objBeingvalidated") == 'Case'){ 
								if (returnResponse.outcome.includes('MISSED')) cmp.set("v.valpassed", false);
								else if (returnResponse.outcome.includes('PASSED')) cmp.set("v.valpassed", true);
								cmp.set("v.returnObjMain", returnResponse);
							}
							//helper.progressbarChange(cmp, helper, curSobjCtr, curSobjSize);
							console.log('object being validated auto: '+cmp.get("v.objBeingvalidated"));
							console.log('current relRecWr = '+cmp.get("v.basicRecInfo").sobj.Id + ' :  ' + cmp.get("v.basicRecInfo").sobj.RecordType.Name);
							console.log('>> resolve');
							//console.log('returnResponse : '+ JSON.stringify(returnResponse));
							console.log('end');
							cmp.set("v.loading", false);
							cmp.set("v.onSFCommit", false);
							cmp.set("v.displayValResults", true);
							//helper.callStreamingAPISubscribeAndSfConsoleFunctions(cmp, helper, cmp.get("v.basicRecInfo").sobjName);
							helper.sfConsoleFunctions(cmp,helper);
							resolve();
						}
						else{
							console.log('>> reject');
							//console.log('basicRecInfo ovdmt: '+ JSON.stringify(cmp.get("v.basicRecInfo").ovdmt));
							console.log("Failed with state: " + state);
							console.log('Return message:' + response.getReturnValue());
							helper.notValidForChecking(cmp,helper); 
							reject();
						}
				});
				$A.enqueueAction(action); 
			} 

			
		}));
	},

	/*callStreamingAPISubscribeAndSfConsoleFunctions: function(cmp,helper,sobjName){
		console.log('>> callStreamingAPISubscribeAndSfConsoleFunctions ');
		console.log('@sobjName:'+ sobjName);
		if (sobjName.includes('__c')){ //sobject is Custom
			cmp.set("v.streamingChannel", "/data/" + sobjName.substring(0, sobjName.length - 1) + 'ChangeEvent');
			//Helper method for Streaming API Support for SF Classic (use lightning:empapi once on LX)
			helper.subscribeStreamingChannel(cmp, helper);
		}
		else cmp.set("v.streamingChannel", "/data/" + sobjName + 'ChangeEvent'); 
		helper.sfConsoleFunctions(cmp,helper);
	}, */

	notValidForChecking : function(cmp,helper){
		console.log('>> notValidForChecking');
		cmp.set("v.loading", false);
		cmp.set("v.validToCheck", false);
		helper.sfConsoleFunctions(cmp,helper);
	},

	//Classic Service Cloud Console
	sfConsoleFunctions : function(cmp,helper){
		console.log('>> sfConsoleFunctions');
		if (!cmp.get("v.basicRecInfo") != undefined){
			if (!cmp.get("v.basicRecInfo").validToCheck) cmp.set("v.validToCheck", false);
		}
		else cmp.set("v.validToCheck", true); //On First Load
		
		helper.sfConsoleSetSidebarVisible(cmp);

		var showtablinkcallback = function(result){
			var showtablinkCtr = 0; //Prevents callback from executing multiple times during page load
			var showObjectId = function showObjectId(result) {
				console.log('>> getFocusedSubtabObjectId callback');
				console.log('Current Record ID: ' + result.id); 
				console.log('showtablinkCtr :'+showtablinkCtr);
				cmp.set("v.recordId", result.id); //Returns 15 digit SF ID
				var isValidToCheck = cmp.get("v.validToCheck");
				helper.sfConsoleSetSidebarVisible(cmp);
			};
			var tlink = result.tabLink;
			console.log('tabLink: '+tlink);
			//identify if current tab is a custom VF page / Lightning App / Component
			if (tlink.includes('apex') || tlink.includes('.app')){
				cmp.set("v.isCustomConsoleTab", true);
			}
			else cmp.set("v.isCustomConsoleTab", false);
			showtablinkCtr++;
			if (showtablinkCtr == 1) sforce.console.getFocusedSubtabObjectId(showObjectId);
		} 

		var onFocusTab = function (result){
			var onFocusCtr = 0; //Prevents callback from executing multiple times during page load
			var onFocusEvent = function (result){
				console.log('Focused SubtabID : '+ result.id);
				console.log('onFocusCtr :'+onFocusCtr);
				if (onFocusCtr == 1) sforce.console.getTabLink(sforce.console.TabLink.TAB_ONLY, result.id, showtablinkcallback);
			};
			onFocusCtr++;
			sforce.console.getFocusedSubtabId(onFocusEvent);
		} 
		sforce.console.onFocusedSubtab(onFocusTab);

		//Needed for Primary Tab and Sub Tab to detect CDC Streaming Event.
		//Child of Sub Tabs will be detected directly by Streaming Event.
		/*var enclosingTabRefreshloadHelperCtr = 0;
		console.log('enclosingTabRefreshloadHelperCtr from sfConsoleFunctions: '+ enclosingTabRefreshloadHelperCtr);
		var eventHandler = function eventHandler(result) {
			console.log('>> onEnclosingTabRefresh callback');
			console.log('current tab refreshed');
			console.log('Result: '+ result.id + ' ' + result.objectId);
			enclosingTabRefreshloadHelperCtr++;
			if (enclosingTabRefreshloadHelperCtr == 1) helper.loadObjectRecord(cmp, helper, result.objectId);
		}; */
		//tabRefresh now being handled by Streaming API Change Data Capture
		//sforce.console.onEnclosingTabRefresh(eventHandler); 
	},

	//Helper sfConsole method for 
	sfConsoleSetSidebarVisible : function(cmp){
		console.log('>> sfConsoleSetSidebarVisible');
		var isValidToCheck = cmp.get("v.validToCheck");
		var isCustomConsoleTab = cmp.get("v.isCustomConsoleTab");
		console.log('isCustomConsoleTab: '+isCustomConsoleTab);
		var sidebarcallback = function(result) {};
		if (!isValidToCheck || isCustomConsoleTab){
			console.log('is not Valid');
			sforce.console.setSidebarVisible(false, null, sforce.console.Region.LEFT, sidebarcallback);
		}
		else{ 
			console.log('is Valid');
			sforce.console.setSidebarVisible(true, null, sforce.console.Region.LEFT, sidebarcallback);
		}
	},

	/*subscribeStreamingChannel : function (cmp, helper){
		//if (!$.cometd.isDisconnected()) $.cometd.disconnect(); //Disconnect previous Channel
		console.log('>> subscribeStreamingChannel');
		console.log('Current Channel: '+ cmp.get("v.streamingChannel"));
		console.log('>> Comet Status subscribeStreamingChannel: ' + $.cometd.getStatus());
		var subscribeloadHelperCtr = 0;
		console.log('subscribeloadHelperCtr from subscribeStreamingChannel: '+ subscribeloadHelperCtr);
		 //subscribe
		 if (!$.cometd.isDisconnected()){
			$.cometd.subscribe(cmp.get("v.streamingChannel"), $A.getCallback(function (message) {
				console.log('in subscribe');
				subscribeloadHelperCtr = 0;
				//console.log('Message '+ JSON.stringify(message) );

				/*Sample Change Data Capture Event JSON response
				{"schema":"lzM0YDnL0BYZZ0XB9MvPTA",
					"payload":
					{
						"LastModifiedDate":"2019-08-06T21:55:01Z",    
					     "ChangeEventHeader": {
							"commitNumber":89273020208,        
							"commitUser":"0056F0000078Op4QAE",           
							"sequenceNumber":1,                   
							"entityName":"Application__c",
							"changeType":"UPDATE",
							"changeOrigin":"com/salesforce/api/soap/46.0;client=devconsole",
							"transactionKey":"000809fb-665e-4a7d-df84-e14de4aa8485",
							"commitTimestamp":1565128501000,
							"recordIds":["a3z4Y00000002jKQAQ"]
						}
					},"event":{"replayId":1005157}
				}
				
				//Recall loadObjectRecord to display updated Record
				console.log('subscribeloadHelperCtr 1: '+subscribeloadHelperCtr);
				subscribeloadHelperCtr++;
				console.log('subscribeloadHelperCtr 2: '+subscribeloadHelperCtr);
				if (subscribeloadHelperCtr == 1){ 	
					//Only refresh Validation List display on change of current Committed User and current object record ID 
					var cdcPayload = message.data.payload.ChangeEventHeader;
					console.log('Current User ID: '+cmp.get("v.userSFId"));
					console.log('Committed User ID: '+ cdcPayload.commitUser);
					console.log('Current Record ID: '+cmp.get("v.recordId"));
					console.log('Updated Record ID: '+ cdcPayload.recordIds);
					var matchingRecID = false;
					var matchingUserID = ( cmp.get("v.userSFId") == cdcPayload.commitUser) ? true : false;
					var updatedRecordIDs = cdcPayload.recordIds;
					var currentRecordID = cmp.get("v.recordId");
					console.log('matchingUserID: '+ matchingUserID);
					for (var i=0; i < updatedRecordIDs.length; i++){
						if ( currentRecordID.length == 15) updatedRecordIDs[i] = updatedRecordIDs[i].substring(0,15); //Check for 15-digit SF ID
						if ( currentRecordID == updatedRecordIDs[i]){
							matchingRecID = true; 
							break;
						}
					}
					console.log('matchingRecID: '+ matchingRecID);
					if (matchingRecID && matchingUserID){ 
						helper.loadObjectRecord(cmp, helper, cmp.get("v.recordId"));
					}
					console.log('>> Comet Status subscribeStreamingChannel callback: ' + $.cometd.getStatus());
				}

			}));
		}
	}, */
	
	connectToStreamingAPIAndBasicInfo : function (cmp, evt, helper){
		console.log('>> connectToStreamingAPIAndBasicInfo');
		//console.log('>> Comet Status pre callback connectToStreamingAPIAndBasicInfo: ' + $.cometd.getStatus());
		console.log('recordID: '+cmp.get('v.recordId'));

		var sessionaction = cmp.get("c.getBasicInfoAndRecord");
		sessionaction.setParams(
			{ recordId : cmp.get('v.recordId') }
		);
		var connect_loadHelperCtr = 0;
		console.log('connect_loadHelperCtr from connectToStreamingAPIAndBasicInfo: '+ connect_loadHelperCtr);
		sessionaction.setCallback(this, function(response) {
			var state = response.getState();
			console.log('State:'+state);
			if (state === "SUCCESS") {
				console.log('Session ID: ' + response.getReturnValue().sessionId );
				cmp.set("v.basicRecInfo", response.getReturnValue());
				cmp.set("v.objBeingvalidated", response.getReturnValue().sobjLabel);
				if (response.getReturnValue().sobjLabel == 'Case'){
					console.log('response.getReturnValue().allowOverride: '+response.getReturnValue().allowOverride);
					console.log('response.getReturnValue().isOverride: '+response.getReturnValue().isOverride);
					console.log('response.getReturnValue().overrideReason: '+response.getReturnValue().overrideReason);
					cmp.set("v.showOverride", response.getReturnValue().allowOverride);
					cmp.set("v.override", response.getReturnValue().isOverride);
					cmp.set("v.overrideReason", response.getReturnValue().overrideReason);
					if (response.getReturnValue().isOverride){ 
						cmp.set("v.valpassed", true);
						cmp.set("v.displayValResults", false);
						cmp.set("v.disableOverride", false);
					}
				}
				else cmp.set("v.showOverride", false);
				console.log('v.showOverride: '+ cmp.get("v.showOverride"));
				console.log('response.getReturnValue().manualValidate: '+response.getReturnValue().manualValidate);
				var manualValidation = response.getReturnValue().manualValidate;
				console.log('manualValidation: '+manualValidation);
				if (manualValidation) cmp.set("v.displayValResults", false);
				else cmp.set("v.displayValResults", true);
				cmp.set("v.manualValidation", manualValidation);
				cmp.set("v.userSFId", response.getReturnValue().userID);
				cmp.set("v.validateRecRecords", response.getReturnValue().includeRelRecordToValidate);
				console.log('@@validaterecRecords: '+ cmp.get("v.validateRecRecords"));
				/*$.cometd.init({
					url: '/cometd/39.0',
					requestHeaders: {Authorization: 'OAuth ' + response.getReturnValue().sessionId },
					appendMessageTypeToURL: false,
					logLevel: [ "warn" | "info" | "debug" ]
				}); */
				console.log('loadHelperCtr from connectToStreamingAPIAndBasicInfo callback: '+ connect_loadHelperCtr);
				connect_loadHelperCtr++;
				//Check if current Record is a Child Case or not allowed for Validation sidebar display
				let sobjRTName = response.getReturnValue().sobj.RecordType.Name;
				console.log('sobjRTName: '+sobjRTName);
				if (sobjRTName.startsWith('KAR:') || sobjRTName.startsWith('POS: PHL') || sobjRTName.startsWith('PWM:') || sobjRTName.includes('Scenario') || response.getReturnValue().sobjLabel == 'Support Request'){
					console.log('is Child Case');
					helper.notValidForChecking(cmp,helper); 
				}
				else{
					console.log('continue load');
					if (connect_loadHelperCtr == 1) helper.loadObjectRecord(cmp, helper, cmp.get("v.recordId"));				
				}
				//console.log('>> Comet Status during connectToStreamingAPIAndBasicInfo: ' + $.cometd.getStatus());
			}
			else {
				console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());
				helper.notValidForChecking(cmp,helper);
			}
		});
		$A.enqueueAction(sessionaction); 

	}, 

	validateORreason : function(cmp){
		let orreason = cmp.find('orreason');
		let orreasonVal = orreason.get("v.value");
		let checkORreasonIfNull = function checkORreasonIfNull(str){ 
			return str === null || str.match(/^\s*$/) !== null; 
		}
		if ( checkORreasonIfNull(orreasonVal) ){
			cmp.set("v.disableOverride", true);
		}
		else{ 
			cmp.set("v.disableOverride", false);
		}
	},

	checkOverride : function(cmp){
		let override = cmp.get("v.override");
		//Clear Override Reason; Disable Override button
		if (!override){
			cmp.set("v.overrideReason", '');
			cmp.set("v.disableOverride", true);
		}
		else{ //Focus to Override Reason
			let orreason = cmp.find('orreason');
			orreason.focus();
		}
	},

	overrideValidations : function(cmp, helper){
		var overrideValaction = cmp.get("c.overrideValidation");
		overrideValaction.setParams(
			{ recordId : cmp.get("v.recordId"),
			  overrideReason : cmp.get("v.overrideReason")
			}
		);
		cmp.set("v.loading", true);
		overrideValaction.setCallback(this, function(response) {
			var state = response.getState();
			console.log('State:'+state);
			cmp.set("v.loading", true);
			if (state === "SUCCESS") {
				console.log('Response: ' + response.getReturnValue());
				cmp.set("v.loading", false);
				let returnobj = cmp.get("v.returnObjMain");
				returnobj.allpassed = 'CASE PASSED! [OVERRIDE]';
				cmp.set("v.valpassed", true);
				cmp.set("v.returnObjMain", returnobj);
				helper.sfConsoleFunctions(cmp,helper);
			}
			else {
				//console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());
				cmp.set("v.loading", false);
				cmp.set("v.onSFCommit", false);
				helper.sfConsoleFunctions(cmp,helper);
			}
		});
		$A.enqueueAction(overrideValaction); 

	}

	//Not available for SF Classic
	/*streamingAPISupport : function(cmp,helper){
		console.log('>> streamingAPISupport');
		// Get the empApi component
		const empApi = cmp.find('empApi');
		// Uncomment below line to enable debug logging (optional)
		empApi.setDebugFlag(true);
		// Register error listener and pass in the error handler function
        empApi.onError($A.getCallback(error => {
            // Error can be any type of error (subscribe, unsubscribe...)
            console.error('EMP API error: ', error);
		}));
		
	},

	// Invokes the subscribe method on the empApi component
    subscribe : function(cmp, helper) {
		console.log('>> subscribe');
        // Get the empApi component
        const empApi = cmp.find('empApi');
        // Get the channel from the input box
		//const channel = cmp.get("v.streamingChannel");
		const channel = cmp.find('channel').get('v.value'); 

		console.log('Subscribed Channel: '+ channel);
        // Replay option to get new events
        const replayId = -1;

		// Subscribe to an event
		var subscribeCallback = function (eventReceived){
			console.log('>> subscribeCallback');
			// Process event (this is called each time we receive an event)
			console.log('Received event ', JSON.stringify(eventReceived));

			//$A.getCallback(eventReceived => { 
				// Process event (this is called each time we receive an event)
				//console.log('Received event ', JSON.stringify(eventReceived));
			//} 
		}.bind(this);
        empApi.subscribe(channel, replayId, subscribeCallback)
        .then(function (subscription){
            // Confirm that we have subscribed to the event channel.
            // We haven't received an event yet.
			console.log('Subscribed to channel ', subscription.channel);
            // Save subscription to unsubscribe later
            cmp.set('v.subscription', subscription);
        });
    },

	// Invokes the unsubscribe method on the empApi component
    unsubscribe : function(cmp, helper) {
		console.log('>> unsubscribe');
        // Get the empApi component
        const empApi = cmp.find('empApi');
		// Get the subscription that we saved when subscribing
        const subscription = cmp.get('v.subscription');

		// Unsubscribe from event
		var unsubscribeCallback = function (unsubscribed){
			console.log('>> unsubscribeCallback');
			// Confirm that we have unsubscribed from the event channel
			console.log('Unsubscribed from channel '+ unsubscribed.subscription);
			cmp.set('v.subscription', null);			
			//$A.getCallback(unsubscribed => {
				// Confirm that we have unsubscribed from the event channel
				//console.log('Unsubscribed from channel '+ unsubscribed.subscription);
				//cmp.set('v.subscription', null);
			//}
		}.bind(this);
        empApi.unsubscribe(subscription, unsubscribeCallback);
	}, */

})