({
	doInit : function(component, event, helper) {
		// Create the action
		var action = component.get("c.getExistingRoleAndRelatedData");
		action.setParams(
			{ recordId : component.get('v.recordId'),
			  disregardedFieldSet : component.get('v.disregardedFieldSet')
		 	}
		);
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				console.log('Success');
				component.set("v.wrappedRoleData", response.getReturnValue());
				helper.setRelatedListRows(component, response, helper);
				helper.setRelatedListColumns(component, helper);
				var newRole = response.getReturnValue().role;
				if (newRole.Id != undefined) delete newRole.Id; //Removes Id from source Role
				newRole.Role_Type__c = 'Primary Applicant';
				component.set("v.newRole", newRole);
				component.set("v.loaded", true);
				component.set("v.screen", "Role__c");
				var roleRTs = component.get("v.wrappedRoleData").roleRTsWrlist;
				var selRoleRTId = component.get("v.selRoleRT");
				helper.setRoleRTHelper(component,roleRTs,selRoleRTId);
            }
            else {
				console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());
				
            }
        });
        // Send action off to be executed
		$A.enqueueAction(action);
	},

	//Set Role Record Type Name, Record Type ID from selected RT
	setRoleRT : function(component, event, helper) {
		var roleRTs = component.get("v.wrappedRoleData").roleRTsWrlist;
		var selRoleRTId = component.get("v.selRoleRT");
		helper.setRoleRTHelper(component,roleRTs, selRoleRTId);
	},

	handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');
        /*if (openSections.length === 0) {
            //cmp.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            //cmp.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
		} */
	},
	
	continueClone : function(component, event, helper){
		component.set("v.screen", "Case");
	},
	
	handleConnectCloneRole : function(component, event, helper){
		//console.log('event param: '+event.getParam('screen'));
		component.set("v.screen", "Role__c");
		component.set("v.loaded", true);
	},

	cloneRoleAndRelatedData : function(component, event, helper){
		console.log('test connected component from NewCase');
		var createdCase = event.getParam("cse");
		console.log('Event Case Attribute: ' + createdCase.Id);

		//Clone Role
		var newRole = component.get("v.newRole");
		if (newRole.Case__c != undefined && createdCase == undefined) delete newRole.Case__c;
		else newRole.Case__c = createdCase.Id; //Set created Case ID
		if (newRole.Application__c != undefined) delete newRole.Application__c;

		//Remove fields to be disregardeded for cloning on Role
		var disregardsfields = component.get("v.wrappedRoleData").disregardedFieldsOnClone;
		if (disregardsfields != undefined){
			for(var i=0; i < disregardsfields.length; i++){
				if (newRole.hasOwnProperty(disregardsfields[i])){
					delete newRole[disregardsfields[i]];
				}
			}
		}

		// Create the action
		var action = component.get("c.cloneRole");
		action.setParams(
			{ newRole : component.get('v.newRole') }
		);
		// Add callback behavior for when response is received
        action.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
				var createdRole = response.getReturnValue();
				component.set("v.createdRole", createdRole);
				console.log("Cloned Role ID: "+ response.getReturnValue().Id);
				helper.cloneRoleAddress(component, createdRole, createdCase, helper);
			}
			else {
				console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());	
            }
		});
        // Send action off to be executed
		$A.enqueueAction(action); 

	},


})