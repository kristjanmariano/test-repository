({
	
	clearBooleans : function(component){
		component.set("v.smssent", false);
		component.set("v.withErrors", false);
		component.set("v.smsReturnMsg", '');
	},
	
	onSaving : function(component,helper){
		component.set("v.saving", true);
		helper.clearBooleans(component);
	},

	validateSMSToSend : function(component, event, helper){
		var isValid = true;
		//Check Input Text For Compose SMS
		if (component.get("v.compose") == true){
			var inputComp = component.find("inputSMS").get("v.value");
			if (inputComp == '' || inputComp == undefined){
				component.set("v.withErrors", true);
				component.set("v.smsReturnMsg", 'No Entered Text');
				isValid = false;
			}
		}
		//Check Mobile Number
		var mobileno = component.get("v.mobilephone");
		console.log('mobileno: '+mobileno);
		if (mobileno == undefined || mobileno == '' || mobileno == null){
			component.set("v.withErrors", true);
			component.set("v.smsReturnMsg", 'Mobile Number is Blank');
			isValid = false;
		}
		console.log('Valid: '+isValid);
        return isValid;
	},

	sendSMSTemplate : function(component, event, helper){
		if(helper.validateSMSToSend(component,event,helper)){

			//Disable all SMS buttons
			/*var wrappedAllowedSMS = component.get("v.wrappedAllowedSMS");
			for (var i=0; i< wrappedAllowedSMS.length; i++){
				wrappedAllowedSMS[i].toDisable = true;
			}
			component.set("v.wrappedAllowedSMS", wrappedAllowedSMS); */
			
			helper.onSaving(component, helper);

			var sobj = component.get("v.sobj");
			//Construct Converse App Task to Create
			var ctask = component.get("v.newConverseTask");
			if (sobj == 'Role__c') ctask.role__c = component.get("v.recordId");
			else if (sobj == 'Case') ctask.case__c = component.get("v.recordId");
			ctask.smagicinteract__Automation_Key_Reference__c = event.getSource().get("v.name");
			console.log("ctask: "+ctask);

			// Create the action
			var action = component.get("c.createSMSNotification");
			action.setParams(
				{ ctask : ctask,
				  recordId : component.get('v.recordId')
				}
			);
			// Add callback behavior for when response is received
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log('Success');
					component.set("v.currentSMS", event.getSource().get("v.label"));
					
					/*//Re-enable all SMS buttons
					for (var i=0; i< wrappedAllowedSMS.length; i++){
						wrappedAllowedSMS[i].toDisable = false;
					}
					component.set("v.wrappedAllowedSMS", wrappedAllowedSMS); */

					component.set("v.saving", false);
					var returnResponse = response.getReturnValue();
					component.set("v.smsReturnMsg", returnResponse);
					console.log('returnResponse : '+returnResponse);

					//Display Error Message if response returned with an Error
					if (returnResponse != '' && returnResponse != null && returnResponse != undefined){ 
						component.set("v.withErrors", true);
					}
					else{ 
						component.set("v.smssent", true);
					}
					console.log('withErrors: '+component.get('v.withErrors'));
					console.log('saving: '+component.get('v.saving'));
					console.log('smssent: '+component.get('v.smssent'));

				}
				else {
					component.set("v.smsReturnMsg", returnResponse);
					component.set("v.withErrors", true);
					console.log("Failed with state: " + state);
					console.log('Return message:' + response.getReturnValue());
				}
			});
			// Send action off to be executed
			$A.enqueueAction(action);
		}
	},
	
	sendSMSCompose : function(component, event, helper){
		if(helper.validateSMSToSend(component,event,helper)){
			helper.onSaving(component, helper);

			//Construct SMS History data
			var freebodySMS = component.get("v.freebodySMS");
			var partition = component.get("v.partition");
			var sobj = component.get("v.sobj");
			var senderID;
			if (partition == 'Positive') senderID = '61488883615';
			else if (partition == 'Nodifi') senderID = 'NODIFI';
			console.log('Sender ID: '+senderID);


			freebodySMS.smagicinteract__SenderId__c  = senderID; //partition.toUpperCase(); //POSITIVE or NODIFI
			freebodySMS.smagicinteract__SMSText__c  = component.get("v.smsText");
			freebodySMS.smagicinteract__disableSMSOnTrigger__c = 0;
			freebodySMS.smagicinteract__ObjectType__c = sobj;
			freebodySMS.smagicinteract__PhoneNumber__c = component.get("v.mobilephone");
			freebodySMS.smagicinteract__Name__c = component.get("v.recipient");
			if (sobj == 'Role__c') freebodySMS.role__c = component.get("v.recordId");
			else if (sobj == 'Case') freebodySMS.case__c = component.get("v.recordId");

			// Create the action
			var action = component.get("c.sendComposedSMS");
			action.setParams(
				{ freebodySMS : freebodySMS }
			);
			// Add callback behavior for when response is received
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log('Success');
					component.set("v.currentSMS", 'Composed');
					component.set("v.saving", false);
					var returnResponse = response.getReturnValue();
					component.set("v.smsReturnMsg", returnResponse);
					console.log('returnResponse : '+returnResponse);

					//Display Error Message if response returned with an Error
					if (returnResponse != '' && returnResponse != null && returnResponse != undefined){ 
						component.set("v.withErrors", true);
					}
					else{ 
						component.set("v.smssent", true);
					}
					console.log('withErrors: '+component.get('v.withErrors'));
					console.log('saving: '+component.get('v.saving'));
					console.log('smssent: '+component.get('v.smssent'));
					component.set("v.compose", false);

				}
				else {
					component.set("v.smsReturnMsg", returnResponse);
					component.set("v.withErrors", true);
					console.log("Failed with state: " + state);
					console.log('Return message:' + response.getReturnValue());
				}
			});
			// Send action off to be executed
			$A.enqueueAction(action);
		}
	}


})