({
	doInit : function(component, event, helper) {

		//Verify Parameters passed from Sobj
		console.log('sobj:'+component.get('v.sobj'));
		console.log('partition:'+component.get('v.partition'));
		console.log('mobilephone:'+component.get('v.mobilephone'));
		console.log('recipient:'+component.get('v.recipient'));

		// Create the action
		var action = component.get("c.getSMSBUttonSettingForObject");
		action.setParams(
			{ curObj : component.get('v.sobj'),
			  partition : component.get('v.partition'),
			  recordId : component.get('v.recordId')
		 	}
		);
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				console.log('Success');
				component.set('v.wrappedAllowedSMS', response.getReturnValue());
				component.set("v.newConverseTask", response.getReturnValue().newConverseTask);
				component.set("v.freebodySMS", response.getReturnValue().freebodySMS);
				console.log('Allow Compose SMS: '+ response.getReturnValue().allowCompose);
				component.set('v.loaded', true);
            }
            else {
				console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());
            }
        });
        // Send action off to be executed
		$A.enqueueAction(action);
	},

	sendSMSNotification : function(component, event, helper){
		console.log('send SMS Template from Button click');
		helper.sendSMSTemplate(component,event,helper);
	},

	composeSMS : function(component, event, helper){
		//Toggle Compose SMS section
		var compSMS = component.get("v.compose");
		if (!compSMS) compSMS = true;
		else compSMS = false;
		component.set("v.compose", compSMS);
		helper.clearBooleans(component, helper);
	},

	keyCheck : function(component, event, helper){
		if (event.which == 13){
			console.log('send Compose SMS from Keypress');
			helper.sendSMSCompose(component,event,helper);
		}    
	},

	sendComposedSMSNotification : function(component, event, helper){
		console.log('send Compose SMS from Button click');
		helper.sendSMSCompose(component,event,helper);
	}


})