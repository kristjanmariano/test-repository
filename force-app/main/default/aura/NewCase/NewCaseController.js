({
	doInit : function(component, event, helper) {
		var loaded = component.get("v.loaded");
		//Initialize only if not yet loaded
		if (!loaded){
			// Create the action
			var action = component.get("c.CaseInit");
			action.setParams(
				{ flowType : component.get('v.flowType'),
				partition : component.get('v.partition'),
				channel : component.get('v.channel'),
				sourceAppID : component.get('v.sourceAppID')
				}
			);
			// Add callback behavior for when response is received
			action.setCallback(this, function(response) {
				var state = response.getState();
				if (state === "SUCCESS") {
					var newCase = response.getReturnValue().newCase;
					var appRTs = response.getReturnValue().appRecTypeWrlist;
					component.set("v.newCase", newCase);
					component.set("v.sourcechannel", component.get('v.channel'));
					component.set("v.wrappedCaseData", response.getReturnValue());
					component.set("v.appRTs", appRTs);
					component.set("v.loaded", true);
					component.set("v.pLoanReasons", response.getReturnValue().pLoanReasons);
					var caseRTs = component.get("v.wrappedCaseData").caseRecTypeWrlist;
					var roleRT = component.get("v.roleRT");
					//Set default Case Record Type
					helper.setCaseRTs(component, roleRT, caseRTs);
					caseRTs = component.get("v.caseRTs"); //Get updated Case RTs from setCaseRTs
					var fldCaseRT = caseRTs[0].name;
					console.log('fldCaseRT: '+fldCaseRT);
					//Set default Application Record Type
					helper.setApplicationRTs(component, fldCaseRT, appRTs);
					helper.setLeadPurposes(component, fldCaseRT); //Set Lead Purposes base on Case Record Type
				}
				else {
					console.log("Failed with state: " + state);
					console.log('Return message:' + response.getReturnValue());
				}
			});
			// Send action off to be executed
			$A.enqueueAction(action);
		}
	},

	//Get Application RT dynamically base from selected Case Record Type
	getAppRTs : function(component, event, helper) {
		var fldCaseRTVal = event.getSource().get("v.value");
		var fldCaseRT = '';
		var caseRTs = component.get("v.caseRTs");
		//console.log('fldCaseRTVal: '+ fldCaseRTVal);
		//console.log("Selected Case RT Id: "+ fldCaseRTVal);
		for(var i = 0; i < caseRTs.length; i++) {
			if (caseRTs[i].id === fldCaseRTVal){
				fldCaseRT = caseRTs[i].name;
				break;
			}
		}
		if (!fldCaseRT.includes('Personal Loan')){
			component.set("v.newCase.Personal_Loan_Reason1__c", null); //Clear Personal Loan value
			console.log('Cleared Personal Loan value');
		}
		//console.log("Selected Case RT: "+ fldCaseRT);
		var fldAppRT = component.find("lsAppRT");
		var appRTs = component.get("v.wrappedCaseData").appRecTypeWrlist;
		helper.setApplicationRTs(component, fldCaseRT, appRTs); //Set Application Record Types and default
		helper.setLeadPurposes(component, fldCaseRT); //Set Lead Purposes base on Case Record Type
		fldAppRT.set("v.disabled", false);
		
	},

	submitCase : function(component, event, helper){
		component.set("v.onSave", true);
		component.set("v.onSaveMSG", "Creating Case...");
		var newCase = component.get("v.newCase");
		var partition = component.get("v.partition");
		var curUser = component.get("v.wrappedCaseData.curUser"); //Current User
		newCase.Partition__c = component.get("v.partition");
		newCase.Channel__c = component.get("v.channel");
		newCase.RecordTypeId = component.get("v.caseRT");
		newCase.Application_Record_Type__c = component.get("v.appRT");
		newCase.Lead_Source__c = 'Manual Clone';
        newCase.Lead_Creation_Method__c = 'Clone Function';
        newCase.Status = 'New';
		newCase.Stage__c = 'Owner Assigned';
		newCase.OwnerId = curUser.Id; //Set Ownership to current User due Stage being set as Owner Assigned
		console.log('newCase.OwnerId: '+ newCase.OwnerId);
		if (partition == 'Nodifi'){ 
			var app = component.get("v.wrappedCaseData.sourceapp");
			console.log('sourceapp : '+app);
			if (app != undefined){
				newCase.Partner_Group__c = app.Case__r.Partner_Group__c; 
				newCase.Referral_Partner__c = app.Case__r.Referral_Partner__c
				newCase.Connective_Full_App_or_Lead__c = app.Case__r.Connective_Full_App_or_Lead__c;
				newCase.Introducer1__c = app.Case__r.Introducer1__c;
				newCase.Introducer1_Support__c = app.Case__r.Introducer1_Support__c;
			}
			else{
				newCase.Connective_Full_App_or_Lead__c = 'Lead';
			}
		}
		// Create the action
		var action = component.get("c.saveCase");
		action.setParams( { cse : newCase } );
		// Add callback behavior for when response is received
		action.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				console.log('success');
				var createdCase = response.getReturnValue(); //Returns created Case
				component.set("v.createdCase", createdCase); 
				if (component.get("v.roleRT") != undefined && component.get("v.source") == 'Role__c' ){
					console.log('saveCase on Role');
					//Do Clone Role and Related Data from RoleClone 
					component.set("v.onSaveMSG", 'Cloning Role and Related Data. Please wait...');
					var roleCloneEvent = component.getEvent("cloneRoleAndRelatedData");
					roleCloneEvent.setParams({"cse" : createdCase });
					roleCloneEvent.fire();
				}
				else{
					if (component.get("v.source") == 'Application__c'){
						console.log('saveCase on Application');
						//Do Clone Roles and Related Data from AppRolesClone
						component.set("v.onSaveMSG", 'Cloning Roles and Related Data. Please Wait...');
						var AppRolesCloneEvent = component.getEvent("cloneRolesAndRelatedData");
						AppRolesCloneEvent.setParams({"cse" : createdCase });
						AppRolesCloneEvent.fire();
					}
					else{
						console.log('saveCase standalone');
						//Display Okay Prompt for Created Case
						component.set("v.onSave", false);
						component.set("v.saveStatus", "success");
						component.set("v.submitDone", true);
						component.set("v.onSaveMSG", 'Case ' + createdCase.CaseNumber + ' has been created');
					}
				}
			}
			else {
				//component.set("v.loaded", true);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				component.set("v.submitDone", true);
				helper.displayErrorPrompt(component,event, errmsg);
				console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());
				
			}
		});
		// Send action off to be executed
		$A.enqueueAction(action);  
	},

	cancelSubmit : function(component, event, helper){
		component.set("v.loaded", true);
		component.set("v.submitDone", false);
	},

	displaySuccessPrompt : function(component, event){
		//Display Okay Prompt for Created Case w/ cloned Role and related Data
		console.log('test connected Component from RoleClone/AppRoleClone');
		component.set("v.submitDone", true);
		component.set("v.onSave", false);
		var createdCase = component.get("v.createdCase");
		component.set("v.saveStatus", "success");
		component.set("v.saveTheme", "slds-modal__header slds-theme_alert-texture slds-theme_success");
		component.set("v.onSaveMSG", 'Case ' + createdCase.CaseNumber + ' and related data have been created.');
	},

	//SF Classic
	redirectToCase : function(component, event, helper){
		//Redirect to Created Case
		component.set("v.loaded", false);
		component.set("v.submitDone", false);
		component.set("v.onSave", false);
		var createdCase = component.get("v.createdCase");
		var url = '/' + createdCase.Id + '?isdtp=vw';
		if (sforce.console.isInConsole()) { //Should only work for SF Classic Console
			//Open created Case on a new Primary Tab
			sforce.console.openPrimaryTab(null, url, true);
			//Close current Console Tab
			var closeSubtab = function closeSubtab(result) {
				var tabId = result.id;
				console.log(tabId);
				sforce.console.closeTab(tabId);
			};
			sforce.console.getEnclosingTabId(closeSubtab);
			//location.href =  url;
			//sforce.console.setTabTitle(createdCase.CaseNumber); 
		} else { 
			window.open(url,'_target'); 
		}
	},

	backToCloneFromRole : function(component, event, helper) {
		component.set("v.channel", component.get("v.sourcechannel") );
		var roleCloneEvent = component.getEvent("backToCloneFromRole");
		roleCloneEvent.setParams({"loaded" : true, "screen" : 'Role__c' });
		roleCloneEvent.fire();
	},

	backToCloneFromApplication : function(component, event, helper) {
		var roleCloneEvent = component.getEvent("backToCloneFromApplication");
		roleCloneEvent.setParams({"loaded" : true, "screen" : 'Application__c' });
		roleCloneEvent.fire();
	},

	



})