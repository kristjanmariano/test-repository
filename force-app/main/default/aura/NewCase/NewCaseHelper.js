({
	setCaseRTs : function (component, roleRT, caseRTs){
		var options = []; var x = 0;
		console.log('roleRT: '+roleRT);
		var exCaseRT = component.get("v.exCaseRTName");
		var exAppRT = component.get("v.exAppRTName");
		for (var i=0; i < caseRTs.length; i++){
			var caseRTName = caseRTs[i].name;
			//Clone From Role
			if (roleRT != 'Applicant - Sole Trader' && roleRT != undefined){ //is Applicant - Individual (can select Consumer Case RTs)
				if (caseRTName.includes('Consumer')){
					options[x] = {id:caseRTs[i].id, name: caseRTName};
					x++;
				}
			}
			else if (roleRT == 'Applicant - Sole Trader' && roleRT != undefined){ //Selected Role RT is Applicant - Sole Trader / no selected Role RT (can Commercial - Asset Finance/ Commercial - Business Funding Case RTs)
				if (caseRTName.includes('Commercial')){
					options[x] = {id:caseRTs[i].id, name: caseRTName};
					x++;
				}
			}
			//Clone From Application
			else if (roleRT == undefined && exAppRT != undefined){ 
				if (exAppRT == 'Commercial - Sole Trader'){
					options[x] = {id:caseRTs[i].id, name: caseRTName};
					x++;
				}
				else if (exAppRT != 'Commercial - Sole Trader'){
					if (exCaseRT.includes('Consumer') && caseRTName.includes('Consumer')){
						options[x] = {id:caseRTs[i].id, name: caseRTName};
						x++;
					}
					else if (exCaseRT.includes('Commercial') && caseRTName.includes('Commercial')){
						options[x] = {id:caseRTs[i].id, name: caseRTName};
						x++;
					}
				}
				//options = caseRTs;
				//break;
			}
		}
		component.set("v.caseRTs", options);
		if (exAppRT != undefined){ //Clone From Application Defaults
			component.set("v.caseRT", component.get("v.exCaseRTId")); //Default Case RT from existing Case
		}
		else{ //Clone From Role Defaults
			component.set("v.caseRT", options[0].id); //Default 1st option
		}
	},

	setApplicationRTs : function(component, fldCaseRT, appRTs) {
		var options = []; var x = 0;
		var newCase = component.get("v.newCase");
		var roleRT = component.get("v.roleRT");
		var exAppRT = component.get("v.exAppRTName");
		for(var j = 0; j < appRTs.length; j++) {
			var appRTName = appRTs[j].name;
			if (fldCaseRT.includes('Consumer')){
				newCase.Finance_For_Business_Purposes__c = 'No';
				//console.log('Consumer'+j);
				if (appRTName.includes('Consumer') && appRTName != 'Consumer - Risk Insurance' && appRTName != 'Consumer - Multi' ){
					options[x] = {id:appRTs[j].id, name: appRTName};
					//console.log('option '+x + ' : ' + options[x].name);
					x++;
				}
			}
			else if (fldCaseRT.includes('Commercial') && roleRT == undefined){
				newCase.Finance_For_Business_Purposes__c = 'Yes';
				//console.log('Commercial '+j);
				if (appRTName.includes('Commercial')){
					options[x] = {id:appRTs[j].id, name: appRTName};
					//console.log('option '+x + ' : ' + options[x].name);
					x++;
				}
			}
			else if (roleRT != undefined && roleRT == 'Applicant - Sole Trader'){
				if (appRTName == 'Commercial - Sole Trader'){
					newCase.Finance_For_Business_Purposes__c = 'Yes';
					options[x] = {id:appRTs[j].id, name: appRTName};
					break;
				}
			}
			else{
				//console.log('Skipped '+j);
			}
		}
		component.set("v.newCase", newCase); //Set Financial Business Purpose
		component.set("v.appRTs", options);
		if (exAppRT != undefined){ //Clone From Application Defaults
			component.set("v.appRT", exAppRT); //Default App RT from existing Application
		}
		else{ //Clone From Role Defaults
			component.set("v.appRT", options[0].name); //Default 1st option
		}
	},

	setLeadPurposes : function(component, fldCaseRT){
		var partition = component.get("v.partition");
		var leadPurposes = ['Car Loan', 'Bike Loan', 'Agricultural Machinery Loan', 'Boat Loan', 'Business Funding', 'Caravan Loan', 'Equipment Loan', 'Heavy Vehicle Loan', 'Other Loan', 'Personal Loan', 'Property Loan', 'Yellow Goods Loan']; //Default Lead Purposes
		var options = []; var x = 0;
		for (var i = 0; i < leadPurposes.length; i++){
			if (fldCaseRT.includes('Consumer - Personal Loan') && fldCaseRT != undefined && leadPurposes[i] == 'Personal Loan'){
				options[x] = {id: 'Personal Loan', name : 'Personal Loan'};
				component.set("v.channel", component.get("v.sourcechannel")); //Set channel to sourceChannel
				break;
			}
			else if (fldCaseRT.includes('Commercial - Business Funding') && fldCaseRT != undefined && leadPurposes[i] == 'Business Funding'){
				options[x] = {id: 'Business Funding', name : 'Business Funding'};
				component.set("v.channel", component.get("v.sourcechannel")); //Set channel to sourceChannel
				break;
			}
			else if (fldCaseRT.includes('Consumer - Property Finance') && fldCaseRT != undefined && leadPurposes[i] == 'Property Loan'){
				options[x] = {id: 'Property Loan', name : 'Property Loan'};
				component.set("v.channel", 'PHL'); //JBACU 11/07/19 Set Channel to PHL when selected Case RT is Property Finance
				break;
			}
			else if(!fldCaseRT.includes('Consumer - Personal Loan') && !fldCaseRT.includes('Commercial - Business Funding') && !fldCaseRT.includes('Consumer - Property Finance') ){
				if (leadPurposes[i] == 'Business Funding') continue; //Skip Business Funding // && partition == 'Nodifi'
				if (leadPurposes[i] == 'Personal Loan') continue; //Skip Personal Loan
				if (leadPurposes[i] == 'Property Loan') continue; //Skip Property Loan
				options[x] = {id: leadPurposes[i], name : leadPurposes[i]};
				component.set("v.channel", component.get("v.sourcechannel")); //Set channel to sourceChannel
				x++;
			}
		}
		component.set("v.newCase.Lead_Purpose__c", options[0].name); //Default 1st option
		component.set("v.leadpurposes", options);
	},

	displaySuccess: function(component, event){
		console.log('test display error');
		component.set("v.onSave", false);
		component.set("v.saveStatus", "success");
		component.set("v.submitDone", true);
		component.set("v.saveTheme", "slds-modal__header slds-theme_alert-texture slds-theme_success");
		var createdCase = component.get("v.createdCase");
		component.set("v.onSaveMSG", 'Case ' + createdCase.CaseNumber + ' and related data have been created.');
	},

	displayErrorPrompt: function(component, event, errmsg){
		console.log('test display error');
		component.set("v.onSave", false);
		component.set("v.saveStatus", "error");
		component.set("v.onSaveMSG", errmsg);
		component.set("v.saveTheme", "slds-modal__header slds-theme_alert-texture slds-theme_error");
	}


})