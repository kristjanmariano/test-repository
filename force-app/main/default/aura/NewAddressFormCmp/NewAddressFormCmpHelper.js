({
    getData: function (component, helper, fromSearch) {
        var action = component.get('c.getAddress');
        action.setStorable();
        
        action.setParams({
            'nameFilterString': component.get('v.searchStr')
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var rows = response.getReturnValue();
                // console.log('Response Time: ' + ((new Date().getTime()) - requestInitiatedTime));
                component.set("v.resultSize", rows.length);
                component.set("v.allAddressData", rows);
                // console.log('address count ------------ ', component.get("v.allAddressData").length);
                // component.set('v.doneloading', true);
                component.set('v.initContentloaded', true);
                component.set('v.showSpinner', false);
                // component.set('v.disableSearchBtns', false);
                // var toggleAddressForm = component.find("addressForm");
                // $A.util.removeClass(toggleAddressForm, "slds-hidden");
                // console.log('show ADDRESS PAGE NOW.');
                if (fromSearch && rows.length > 0) {
                    var childCmp = component.find("addressTblCmp");
                    var auraMethodResult = childCmp.searchMethod('test');
                    //  component.set("v.searchErrMsg", 'No Results found. Please try to create an New record.');
                }

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }

        }));
        var requestInitiatedTime = new Date().getTime();
        $A.enqueueAction(action);
    },
    
    fireCustomToast: function(component,messageType,message) {
		var customToastEvent = $A.get("e.c:CustomToastAppEvt");
		customToastEvent.setParams({    "messageType" : messageType,
										"message" : message });
		customToastEvent.fire();
	},
})