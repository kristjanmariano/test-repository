({
    // init - Runs on component load
	init: function(component, event, helper) {
        var actions = [
            { label: 'Link to Partner', name: 'linkToPartner' }
        ]
        component.set('v.abnTblColumns', [
            { label: 'Name', fieldName: 'Name', type: 'text' },
            { label: 'Registered ABN', fieldName: 'Registered_ABN__c', type: 'text' },
            {
                label: 'Action', type: 'button', typeAttributes: {
                    label: 'Link to Partner',
                    name: 'linkToPartner',
                    size: 'medium',
                    disabled: { fieldName: 'actionDisabled' },
                    variant: 'base',
                    title: 'Click to Link ABN to Partner'
                }
            },
        ]);
        // component.set("v.showSpinner",false);
        helper.getData(component, helper);
    },

    // handleButton - handles button action of the New Legal Entity footer form/card
    handleButton: function(component, event, helper) {
               
        var btnLbl = event.getSource().get("v.label");
        // alert(btnLbl);
        if (btnLbl == 'Back to Search') {
            // component.set('v.searchStr', component.find("enter-search").get("v.value"));
            // component.set('v.disableSearchBtns', false);
            component.set('v.title', 'ABN Search');
            component.set('v.searchABN', true);
            component.set('v.pageSubHeader', 'Use this page to search ABN record.');
            // component.set('v.showSpinner', true);
        }
        else if (btnLbl == 'Save & Link to Partner') {
            // alert('save address');
            component.set('v.whatBtn', btnLbl);
            component.find("abnForm").submit();
        }
    },

    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
        if (openSections.length === 0) {
            component.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            component.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
        
    // handleLoad - Invokes when lightning:recordEditForm is loaded
	handleLoad: function(component, event, helper) {
        component.set("v.showSpinner", false);
        var toggleNewLegEntForm = component.find("newLegEntForm");
        var toggleNewLegFoot = component.find("newLegEntFooter");
        // $A.util.removeClass(toggleNewLegEntForm, "slds-hidden");
        $A.util.removeClass(toggleNewLegFoot, "slds-hidden");
		// component.find('partnerInput').set('v.value',component.get("v.partnerId"));
    },

    // handleSubmit - Invokes when lightning:recordEditForm is submitted
    handleSubmit: function(component, event, helper) {
        component.set('v.showSpinner', true);
        component.set('v.disabled', true);
    },

    // handleSuccess - Invokes when record is successfully saved
    handleSuccess: function(component, event, helper) {
        
        var payload = event.getParams().response;
        var whatBtn = component.get('v.whatBtn');

		// console.log('ABN Saved! payload = '+payload.id);
		component.set('v.abnId', payload.id);
		// component.set('v.personAccountId', component.find('assAcc').get('v.value'));
		component.set('v.saved', true);
        component.set('v.disabled', false);
        component.set('v.showSpinner', false);

        // alert(whatBtn);

        if (whatBtn == 'Save & Link to Partner') {
            helper.fireCustomToast(component,'success','ABN record successfully saved.')
            component.set('v.selectedTab', 'partner_details');
        }
    },

    // handleError - Invokes when record is NOT successfully saved
    handleError: function(component, event, helper) {
        component.set('v.saved', false);
		component.set('v.disabled', false);
		component.set('v.showSpinner', false);
    },
    
    // handleButtonClick - handle Search Bar buttons
    handleButtonClick: function(component, event, helper) {
        // console.log('search bar buttons clicked.');
        var btnLbl = event.getSource().get("v.title");
        // console.log('btnLbl -> '+btnLbl);
        if (btnLbl == 'Search') {
            component.set('v.searchStr', component.find("enter-search").get("v.value"));
            component.set('v.searchABN', true);
            helper.getData(component, helper, true);
        }
        else if (btnLbl == 'New') {
            component.set('v.showSpinner', true);
            component.set('v.title', 'New ABN');
            component.set('v.pageSubHeader', 'Use this page to create ABN records.');
            component.set('v.searchABN', false);
        }
	},
	
})