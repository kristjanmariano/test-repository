({
	fireCustomToast: function(component,messageType,message) {
		var customToastEvent = $A.get("e.c:CustomToastAppEvt");
		customToastEvent.setParams({    "messageType" : messageType,
										"message" : message });
		customToastEvent.fire();
	},

	getData: function (component, helper, fromSearch) {
		// console.log('getData ------------ ABN!');
        var action = component.get('c.getABNs');
        action.setStorable();
        
        action.setParams({
            'nameFilterString': component.get('v.searchStr')
        });

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var rows = response.getReturnValue();
                // console.log('Response Time: ' + ((new Date().getTime()) - requestInitiatedTime));
                component.set("v.resultSize", rows.length);
                component.set("v.allABNData", rows);
                // console.log('abn count ------------ ', component.get("v.allABNData").length);
                // component.set('v.doneloading', true);
                component.set('v.initContentloaded', true);
                component.set('v.showSpinner', false);
                // component.set('v.disableSearchBtns', false);
                // var toggleAddressForm = component.find("addressForm");
                // $A.util.removeClass(toggleAddressForm, "slds-hidden");
				// console.log('show ABN PAGE NOW.');
				// console.log('fromSearch -- ',fromSearch);
                if (fromSearch && rows.length > 0) {
                    var childCmp = component.find("abnTblCmp");
                    var auraMethodResult = childCmp.searchMethod('test');
                }

            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }

        }));
        var requestInitiatedTime = new Date().getTime();
        $A.enqueueAction(action);
    },
})