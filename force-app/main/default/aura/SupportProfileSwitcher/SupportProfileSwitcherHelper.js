({
    getData : function(cmp) {
        var action = cmp.get('c.getUsers');
        var nameFilterString = cmp.find("enter-search").get("v.value");
        action.setParams({
            nameFilterString: nameFilterString
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    
                    if (row.Profile.Name) row.ProfileName = row.Profile.Name;
                    if (row.UserRole.Name) row.UserRoleName = row.UserRole.Name;
                    
                    if(row.Profile.Name.includes('PLS')){
                        row.actionLabel = 'Switch to NOD Support';
                        row.actionVariant = 'neutral';
                    } 
                    else if(row.Profile.Name.includes('Nod')){
                        row.actionLabel = 'Switch to PLS Support';
                        row.actionVariant = 'brand';
                    } 
                    if(!row.IsActive || !row.Box_Configured__c){
                        row.actionDisabled = true; 
                        row.actionVariant = 'neutral';
                    }
                    
                }
                cmp.set('v.userRecords', rows);
                cmp.set('v.doneloading', true);
                //this.setActionLabel(cmp);
                console.log(cmp.get('v.userRecords'));
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
            
        }));
        $A.enqueueAction(action);
    },
  
    switchProfile : function(row,cmp,evt) {
        // eslint-disable-next-line no-alert
        cmp.set('v.doneloading', false);
        console.log(row.Name);
        var action = cmp.get('c.switchUserProfile');
        //var nameFilterString = cmp.find("enter-search").get("v.value");
        action.setParams({
            u: row,
            switchStr: row.actionLabel//'test'
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseStr = response.getReturnValue();
                cmp.set('v.updateProfileIsSuccess',this.isUpdateSuccess(responseStr,cmp));
                // cmp.set('v.updateProfileIsSuccess', response.getReturnValue());
                // console.log('updateProfileIsSuccess --> ',cmp.get('v.updateProfileIsSuccess'));
                //this.handleShowToast(cmp,evt);
                cmp.set('v.doneloading', true);
                this.getData(cmp);
                cmp.set('v.userNameUpdated', row.Name);
                // cmp.set('v.profileNameUpdated', row.ProfileName);
                cmp.set('v.isModalOpen', true);
            } else if (state === "ERROR") {
                var errors = response.getError();
                cmp.set('v.updateProfileIsSuccess',false);
                console.error(errors);
            }
            
        }));
        $A.enqueueAction(action);
    },
    
    handleShowToast : function(component, event) {
        component.find('notifLib').showToast({
            "title": "Notif library Success!",
            "message": "The record has been updated successfully."
        });
    },

    isUpdateSuccess : function (responseStr, cmp) {
        if(responseStr.includes(';')){
            var res = responseStr.split(';');
            if(res[0] === 'true'){
                cmp.set('v.profileNameUpdated', res[1]);
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

})