({
	doInit : function(component, event, helper) {
		// Create the action
		var action = component.get("c.getExistingRolesAndRelatedData");
		action.setParams(
			{ recordId : component.get('v.recordId'),
			  disregardedFieldSet : component.get('v.disregardedFieldSet') 
			}
		);
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				console.log('Success');
				component.set("v.wrappedAppRolesData", response.getReturnValue());
				var roleWrlist = response.getReturnValue().roleWrlist;
				console.log('RoleWrlist: '+ roleWrlist);
				if (roleWrlist != undefined){
					for (var i = 0; i < roleWrlist.length; i++){
						console.log('@@i: '+i);
						console.log('roleWrlist[i].raddrlist: '+roleWrlist[i].raddrlist);
						if (roleWrlist[i].role.Id != undefined) delete roleWrlist[i].role.Id; //Removes Id from source Roles
						if (roleWrlist[i].raddrlist != undefined){
							for (var j = 0; j < roleWrlist[i].raddrlist.length; j++){
								if (roleWrlist[i].raddrlist[j].Id != undefined) delete roleWrlist[i].raddrlist[j].Id; //Removes Id from source Role Address
								if (roleWrlist[i].raddrlist[j].Role__c != undefined) delete roleWrlist[i].raddrlist[j].Role__c; //Removes Role ID  from source Role Address
							}
						} 
					}
				} 
				var app = response.getReturnValue().app;
				if (app.RecordType.Name.includes('Consumer')){
					component.set("v.apptype", "Consumer");	
				}
				else if (app.RecordType.Name.includes('Commercial')){
					component.set("v.apptype", "Commercial");	
				}
				helper.setRelatedListRows(component, response, helper);
				helper.setRelatedListColumns(component, helper);
				component.set("v.newRoleWrlist", roleWrlist);
				component.set("v.screen", "Application__c");
				component.set("v.loaded", true);
            }
            else {
				console.log("Failed with state: " + state);
				console.log('Return message:' + response.getReturnValue());
				
            }
        });
        // Send action off to be executed
		$A.enqueueAction(action);
	},

	continueClone : function(component, event, helper){
		component.set("v.screen", "Case");
	},

	handleConnectAppRolesClone : function(component, event, helper){
		//console.log('event param: '+event.getParam('screen'));
		component.set("v.screen", "Application__c");
		component.set("v.loaded", true);
	},

	handleSectionToggle: function (cmp, event) {
        var openSections = event.getParam('openSections');
	},

	cloneRolesAndRelatedData : function(component, event, helper){
		console.log('test connected component from NewCase: cloneRolesAndRelatedData');
		var createdCase = event.getParam("cse");
		console.log('Event Case Attribute: ' + createdCase.Id);
		console.log('Cloning Roles');

		//Clone Roles
		var newRolesWr = component.get("v.newRoleWrlist");
		for (var i=0; i < newRolesWr.length; i++){
			if (newRolesWr[i].role.Case_c != undefined && createdCase == undefined) delete newRolesWr[i].role.Case__c;
			else newRolesWr[i].role.Case__c = createdCase.Id; //Set created Case ID
			if (newRolesWr[i].role.Application__c != undefined) delete newRolesWr[i].role.Application__c;

			//Remove fields to be disregardeded for cloning on Role
			var disregardsfields = component.get("v.wrappedAppRolesData").disregardedFieldsOnClone;
			if (disregardsfields != undefined){
				for(var j=0; j < disregardsfields.length; j++){
					if (newRolesWr[i].role.hasOwnProperty(disregardsfields[j])){
						delete newRolesWr[i].role[disregardsfields[j]];
					}
				}
			}

		}
		// Create the action
		var action = component.get("c.cloneRoles");
		action.setParams(
			{ roleWrlist : newRolesWr }
		);
		// Add callback behavior for when response is received
        action.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
				var creRolesWr = response.getReturnValue(); //Created Roles
				component.set("v.newAppID", createdCase.Application_Name__c);
				//console.log('Response:'+response.getReturnValue());
				//console.log("Cloned Role ID: "+ response.getReturnValue().Id);

				//Clone Role Addresses
				helper.cloneRoleAddresses(component, creRolesWr, createdCase, helper);
			}
			else {
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				// Send action off to be re-executed
				$A.enqueueAction(action);
            }
		});
        // Send action off to be executed
		$A.enqueueAction(action); 

	},

})