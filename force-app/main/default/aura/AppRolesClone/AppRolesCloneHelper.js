({
	setRelatedListRows : function(component, response, helper){
		
		//Set FOs
		var incRows = []; var expRows = []; var assRows = []; var liaRows = [];
		var incData = response.getReturnValue().inclist;
		var expData = response.getReturnValue().explist;
		var assData = response.getReturnValue().asslist;
		var liaData = response.getReturnValue().lialist;

		if (incData != undefined){
			for (var i=0; i < incData.length; i++){
				if (incData[i].inc.RecordType) incData[i].inc.recordTypeName = incData[i].inc.RecordType.Name;
				incRows.push(incData[i].inc);
			}
		}
		if (expData != undefined){
			for (var i=0; i < expData.length; i++){
				if (expData[i].exp.RecordType) expData[i].exp.recordTypeName = expData[i].exp.RecordType.Name;
				expRows.push(expData[i].exp);
			}
		}
		if (assData != undefined){
			for (var i=0; i < assData.length; i++){
				if (assData[i].ass.RecordType) assData[i].ass.recordTypeName = assData[i].ass.RecordType.Name;
				assRows.push(assData[i].ass);
			}
		}
		if (liaData != undefined){
			for (var i=0; i < liaData.length; i++){
				if (liaData[i].lia.RecordType) liaData[i].lia.recordTypeName = liaData[i].lia.RecordType.Name;
				liaRows.push(liaData[i].lia);
			}
		}
		
		component.set("v.incRows", incRows);
		component.set("v.expRows", expRows);
		component.set("v.assRows", assRows);
		component.set("v.liaRows", liaRows); 
	},

	setRelatedListColumns : function(component, helper){
		//Incomes
		component.set('v.incColumns',
			[
				{label: 'Income', fieldName: 'Name', type:'text'},
				{label: 'FO Record Type', fieldName: 'recordTypeName', type:'text'},
				{label: 'Type', fieldName: 'Income_Type__c', type:'text'},
				{label: 'Income Value', fieldName: 'Income_Value__c', type:'currency', cellAttributes : { alignment : 'left'} },
				{label: 'Income Situation', fieldName: 'Income_Situation__c', type:'text'},
			]
		);
		//Expenses
		component.set('v.expColumns',
			[
				{label: 'Expense', fieldName: 'Name', type:'text'},
				{label: 'FO Record Type', fieldName: 'recordTypeName', type:'text'},
				{label: 'Type', fieldName: 'Expense_Type__c', type:'text'},
				{label: 'Monthly Payment', fieldName: 'Monthly_Payment__c', type:'currency', cellAttributes : { alignment : 'left'} },
				{label: 'To Be Paid Out', fieldName: 'To_Be_Paid_Out__c', type:'boolean' }
				
			]
		);
		//Assets
		component.set('v.assColumns',
			[
				{label: 'Asset', fieldName: 'Name', type:'text'},
				{label: 'FO Record Type', fieldName: 'recordTypeName', type:'text'},
				{label: 'Type', fieldName: 'Asset_Type__c', type:'text'},
				{label: 'Asset Value', fieldName: 'Asset_Value__c', type:'currency', cellAttributes : { alignment : 'left'}},
				{label: 'Being Sold/Traded', fieldName: 'Being_Sold_Traded__c', type:'boolean' }
			]
		);
		//Liabilities
		component.set('v.liaColumns',
			[
				{label: 'Liability', fieldName: 'Name', type:'text'},
				{label: 'FO Record Type', fieldName: 'recordTypeName', type:'text'},
				{label: 'Type', fieldName: 'Liability_Type__c', type:'text'},
				{label: 'Amount Owing', fieldName: 'Amount_Owing__c', type:'currency', cellAttributes : { alignment : 'left'}},
				{label: 'To Be Paid Out', fieldName: 'To_Be_Paid_Out__c', type:'boolean' }
			]
		);
	},

	cloneRoleAddresses : function(component, creRolesWr, createdCase, helper){
		console.log('Cloning Role Addresses');
		// Create the action
		var actionRA = component.get("c.cloneRoleAddresses");
		actionRA.setParams(
			{ roleWrlist : creRolesWr }
		);
		// Add callback behavior for when response is received
        actionRA.setCallback(this, function(response) {
			var state = response.getState();
            if (state === "SUCCESS") {
				console.log('Role Addresses created');
				console.log('createdCase: '+createdCase.Id);
				helper.afterRoleAddress(component,creRolesWr,createdCase,helper);
			}
			else {
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
            }
		});
		// Send action off to be executed
		$A.enqueueAction(actionRA); 
	},

	afterRoleAddress : function(component, creRolesWr, createdCase, helper){
		var oldRoleIDs = [];
		var newRoleIDs = [];
		for (var i=0; i < creRolesWr.length; i++){
			//push old and new Role IDs
			var ogRoleID = creRolesWr[i].ogRoleID;
			var newRoleID = creRolesWr[i].role.Id;
			oldRoleIDs.push(ogRoleID.substring(0,15)); //pass 15 digit SF ID
			newRoleIDs.push(newRoleID.substring(0,15)); //pass 15 digit SF ID
		}
		console.log('cloning Income');
		helper.cloneIncome(component, oldRoleIDs, newRoleIDs, createdCase, helper);
	},

	afterCloning : function(createdCase){
		if (createdCase != undefined){
			//Display Prompt on New Case
			var displayPromptCaseEvent = $A.get("e.c:NewCaseEvent");
			displayPromptCaseEvent.fire();
			console.log('event '+displayPromptCaseEvent);
		} 
	},

	cloneIncome : function(component, oldRoleIDs, newRoleIDs, createdCase, helper){
		var wRD = component.get("v.wrappedAppRolesData");
		var newappID = component.get("v.newAppID");
		var inclist = wRD.inclist;
		var foLinklist = wRD.foLinklist;
		var newinclist = [];
		var retryctr = 0;
		if (inclist != undefined){
			for(var i=0; i< inclist.length; i++){
				var incWr = inclist[i];
				var incRole15D = incWr.inc.Role__c.substring(0,15);
				if (oldRoleIDs.includes(incRole15D)){
					if (incWr.inc.Id != undefined){
						delete incWr.inc.Id; //Removes Id from source Income
					}
					if (incWr.inc.Role__c != undefined) delete incWr.inc.Role__c;
					incWr.inc.Role__c = newRoleIDs[oldRoleIDs.indexOf(incRole15D)]; //Set new Role ID base from Old Role ID
					incWr.inc.Application1__c = newappID;
					incWr.inc.h_Auto_Create_FO_Share__c = true;
					incWr.inc.Equal_Share_for_Roles__c = true;
					newinclist.push(incWr);
				}
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneIncome");
		actionFO.setParams(
			{ incWrlist : newinclist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Income created');
				else console.log('No Income increated');
				component.set("v.newIncome", res);
				//Clone Expense next
				setTimeout(
					$A.getCallback(function() {
						console.log('Cloning Expenses');
						helper.cloneExpenses(component, oldRoleIDs, newRoleIDs, createdCase, helper) 
					}), 1000); 
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				console.log('Retrying Cloning of Income [' + retryctr + ']' );
				//Retry Cloning of Income
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			} 
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneExpenses : function(component, oldRoleIDs, newRoleIDs, createdCase, helper){
		var wRD = component.get("v.wrappedAppRolesData");
		var app = wRD.app;
		var newappID = component.get("v.newAppID");
		var explist = wRD.explist;
		var newexplist = [];
		var retryctr = 0;
		if (explist != undefined){
			for(var i=0; i< explist.length; i++){
				var expWr = explist[i];
				/*if ( ( (expWr.exp.To_Be_Paid_Out__c || !expWr.exp.To_Be_Paid_Out__c) && app.Case__r.Status != 'Closed' && app.Case__r.Stage__c != 'Won' ) ||
					( !expWr.exp.To_Be_Paid_Out__c && app.Case__r.Status == 'Closed' && app.Case__r.Stage__c == 'Won' )
				){ */
				if (!expWr.exp.To_Be_Paid_Out__c){
					if (expWr.exp.h_FO_Share_Role_IDs__c != undefined){
						if (expWr.exp.Id != undefined) delete expWr.exp.Id; //Removes Id from source Expense
						if (expWr.exp.Application1__c != undefined) delete expWr.exp.Application1__c;
						expWr.exp.Application1__c = newappID;
						expWr.exp.h_Auto_Create_FO_Share__c = true;
						expWr.exp.Equal_Share_for_Roles__c = true;
						var rolesplit = expWr.exp.h_FO_Share_Role_IDs__c.split(';');
						var newFORoleShareIDs = '';
						for (var j=0; j < rolesplit.length; j++){
							var rolesplit15D = rolesplit[j].substring(0,15);
							if (rolesplit[j] != ''){
								if (oldRoleIDs.includes(rolesplit15D)){
									newFORoleShareIDs+= newRoleIDs[oldRoleIDs.indexOf(rolesplit15D)] + ';';
								}
							}
						}
						expWr.exp.h_FO_Share_Role_IDs__c = newFORoleShareIDs;
						newexplist.push(expWr);
					}	
				}
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneExpenses");
		actionFO.setParams(
			{ expWrlist : newexplist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Expenses created');
				else console.log('No Expense created');
				component.set("v.newExpenses", res);
				//Clone Asset next
				setTimeout(
					$A.getCallback(function() {
						console.log('Cloning Assets');
						helper.cloneAssets(component, oldRoleIDs, newRoleIDs, createdCase, helper) 
					}), 1000); 
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				console.log('Retrying Cloning of Expenses [' + retryctr + ']' );
				//Retry Cloning of Expenses
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneAssets : function(component, oldRoleIDs, newRoleIDs, createdCase, helper){
		var wRD = component.get("v.wrappedAppRolesData");
		var app = wRD.app;
		var newappID = component.get("v.newAppID");
		var asslist = wRD.asslist;
		var newasslist = [];
		var retryctr = 0;
		if (asslist != undefined){
			for(var i=0; i< asslist.length; i++){
				var assWr = asslist[i];
				if (!assWr.ass.Being_Sold_Traded__c){
					if (assWr.ass.h_FO_Share_Role_IDs__c != undefined){
						if (assWr.ass.Id != undefined) delete assWr.ass.Id; //Removes Id from source Asset
						if (assWr.ass.Application1__c != undefined) delete assWr.ass.Application1__c;
						assWr.ass.Application1__c = newappID;
						assWr.ass.h_Auto_Create_FO_Share__c = true;
						assWr.ass.Equal_Share_for_Roles__c = true;
						var rolesplit = assWr.ass.h_FO_Share_Role_IDs__c.split(';');
						var newFORoleShareIDs = '';
						for (var j=0; j < rolesplit.length; j++){
							var rolesplit15D = rolesplit[j].substring(0,15);
							if (rolesplit[j] != ''){
								if (oldRoleIDs.includes(rolesplit15D)){
									newFORoleShareIDs+= newRoleIDs[oldRoleIDs.indexOf(rolesplit15D)] + ';';
								}
							}
						}
						assWr.ass.h_FO_Share_Role_IDs__c = newFORoleShareIDs;
						newasslist.push(assWr);
					}
				}
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneAssets");
		actionFO.setParams(
			{ assWrlist : newasslist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Assets created');
				else console.log('No Assets created');
				component.set("v.newAssets", res);
				//Clone Liabilities next
				setTimeout(
					$A.getCallback(function() {
						console.log('cloning liabilities');
						helper.cloneLiabilities(component, oldRoleIDs, newRoleIDs, createdCase, helper) 
					}), 1000);
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				console.log('Retrying Cloning of Assets [' + retryctr + ']' );
				//Retry Cloning of Liabilities
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneLiabilities : function(component, oldRoleIDs, newRoleIDs, createdCase, helper){
		var wRD = component.get("v.wrappedAppRolesData");
		var app = wRD.app;
		var newappID = component.get("v.newAppID");
		var lialist = wRD.lialist;
		var newlialist = [];
		var retryctr = 0;
		if (lialist != undefined){
			for(var i=0; i< lialist.length; i++){
				var liaWr = lialist[i];
				/*if ( ( (liaWr.lia.To_Be_Paid_Out__c || !liaWr.lia.To_Be_Paid_Out__c) && app.Case__r.Status != 'Closed' && app.Case__r.Stage__c != 'Won' ) ||
					( !liaWr.lia.To_Be_Paid_Out__c && app.Case__r.Status == 'Closed' && app.Case__r.Stage__c == 'Won' )
				){ */
				if (!liaWr.lia.To_Be_Paid_Out__c){
					if (liaWr.lia.h_FO_Share_Role_IDs__c != undefined){
						if (liaWr.lia.Id != undefined) delete liaWr.lia.Id; //Removes Id from source Liability
						if (liaWr.lia.Application1__c != undefined) delete liaWr.lia.Application1__c;
						liaWr.lia.Application1__c = newappID;
						liaWr.lia.h_Auto_Create_FO_Share__c = true;
						liaWr.lia.Equal_Share_for_Roles__c = true;
						var rolesplit = liaWr.lia.h_FO_Share_Role_IDs__c.split(';');
						var newFORoleShareIDs = '';
						for (var j=0; j < rolesplit.length; j++){
							var rolesplit15D = rolesplit[j].substring(0,15);
							if (rolesplit[j] != ''){
								if (oldRoleIDs.includes(rolesplit15D)){
									newFORoleShareIDs+= newRoleIDs[oldRoleIDs.indexOf(rolesplit15D)] + ';';
								}
							}
						}
						liaWr.lia.h_FO_Share_Role_IDs__c = newFORoleShareIDs;
						newlialist.push(liaWr);
					}
				}
			}
		}
		// Create the action
		var actionFO = component.get("c.cloneLiabilities");
		actionFO.setParams(
			{ liaWrlist : newlialist }
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('Liabilities created');
				else console.log('No Liabilities created');
				component.set("v.newLiabilities", res);
				//Clone FO Links next
				console.log('cloning FO Links');
				helper.cloneFOLinks(component, createdCase, helper);
			}
			else {
				retryctr++;
				console.log("Failed with state: " + state);
				let errors = response.getError();
				var errmsg;
				if (errors && Array.isArray(errors) && errors.length > 0) {
					errmsg = errors[0].message;
					console.error(errors[0].message);
				} else {
					errmsg = 'Unknown error';
					console.error("Unknown error");
				}
				console.log('Retrying Cloning of Liabilities [' + retryctr + ']' );
				//Retry Cloning of Liabilities
				// Send action off to be re-executed
				$A.enqueueAction(actionFO);
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	},

	cloneFOLinks : function(component, createdCase, helper){
		var incWrlist = component.get("v.newIncome");
		var expWrlist = component.get("v.newExpenses");
		var assWrlist = component.get("v.newAssets");
		var liaWrlist = component.get("v.newLiabilities");
		var folinklist = component.get("v.wrappedAppRolesData.foLinklist");
		if (folinklist != undefined){
			for (var i=0; i < folinklist.length; i++){
				if (folinklist[i].Id != undefined) delete folinklist[i].Id;
				if (folinklist[i].h_Application__c != undefined) delete folinklist[i].h_Application__c;
				if (folinklist[i].h_Case__c != undefined) delete folinklist[i].h_Case__c;
			}
		}
		var appID = component.get("v.newAppID");
		// Create the action
		var actionFO = component.get("c.cloneFOLinks");
		actionFO.setParams(
			{ incWrlist : incWrlist,
			  expWrlist : expWrlist,
			  assWrlist : assWrlist,
			  liaWrlist : liaWrlist,
			  folinklist : folinklist,
			  appID : appID,
			  caseID : createdCase.Id
			}
		);
		// Add callback behavior for when response is received
		actionFO.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var res = response.getReturnValue();
				if (res.length > 0) console.log('FO Links created');
				else console.log('No FO Links created');
				//Display Message Prompt
				helper.afterCloning(createdCase);
			}
			else {
				let errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    console.error(errors[0].message);
                } else {
                    console.error("Unknown error");
                }
			}
		});
		// Send action off to be executed
		$A.enqueueAction(actionFO);
	}

})