({
	init: function(component,event,helper) {
		var openModal = component.get('c.openModal');
        $A.enqueueAction(openModal);
	},
	
	openModal: function(component, event, helper) {
		// Set isModalOpen attribute to true
        component.set("v.isModalOpen", true);
		helper.getRoleOptions(component);
		var progressMessageChange = component.get('c.progressMessageChange');
        $A.enqueueAction(progressMessageChange);
	},
	
	progressMessageChange: function (component) {
        var progressMessage = component.get("v.progressMessage");
        // alert('progressMessageChange');
        if(progressMessage.includes("has existing Equifax Enquiry Id")){
            component.set("v.hasExistingEnquiryId",true);
        }
        if(progressMessage.includes("Equifax Operation failed")){
            component.set("v.equifaxRequestFailed",true);
        }
        else {
            component.set("v.equifaxRequestFailed",false);
        }  
        // if (cmp.get('v.isProgressing')) {
        //     // stop
        //     cmp.set('v.isProgressing', false);
        //     clearInterval(cmp._interval);
        // } else {
        //     // start
        //     cmp.set('v.isProgressing', true);
        //     cmp._interval = setInterval($A.getCallback(function () {
        //         var progress = cmp.get('v.progress');
        //         cmp.set('v.progress', progress === 100 ? 0 : progress + 1);
        //     }), 200);
        // }
    },

    closeModal: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        var whichBtn = event.getSource().get("v.title");
        if(whichBtn == 'Cancel'){
            console.log('showRoleSelect = '+ component.get("v.showRoleSelect"));
            console.log('hasExistingEnquiryId = '+ component.get("v.hasExistingEnquiryId"));
            if(component.get("v.showRoleSelect")){
                var closePage = component.get('c.closePage');
                $A.enqueueAction(closePage);
            }
            else if(component.get("v.hasExistingEnquiryId")){
                component.set("v.isModalOpen", false);
            }
        }
        else if(whichBtn == 'Update Manually'){
            component.set("v.isModalOpen", false);
            component.set("v.errMsg", '');
            component.set("v.progressMessage", '');
            component.set("v.progress", 0);
            //alert(component.get("v.equifaxRequestFailed"));
        }
	},
	
	handleChange: function(component, event, helper) {
		// Set isModalOpen attribute to false  
		// console.log('im handling change.');
    },

    recordUpdated: function(component, event, helper) {
		// Set isModalOpen attribute to false  
		console.log('im handling recordUpdated.');
    },
    
	submitDetails: function(component, event, helper) {
        console.log('submitDetails showRoleSelect -> '+component.get("v.showRoleSelect"));
        console.log('submitDetails progress -> '+component.get("v.progress"));
        console.log('submitDetails isProgressing -> '+component.get("v.isProgressing"));
        console.log('submitDetails progressMessage -> '+component.get("v.progressMessage"));
        console.log('submitDetails errMsg -> '+component.get("v.errMsg"));
        console.log('submitDetails selectedRecId -> '+component.get("v.selectedRecId"));
        console.log('submitDetails hasExistingEnquiryId -> '+component.get("v.hasExistingEnquiryId"));
        
        var showRoleSelect = component.get("v.showRoleSelect");
        if(showRoleSelect){
            var inputField = component.find('dualListBox');
            var value = inputField.get('v.value');
            var selectedRole = component.find("dualListBox").get("v.value");
            console.log('selectedRole = ',selectedRole);
            if(value.length == 0){
                inputField.setCustomValidity('Select at least 1 option.');
                inputField.reportValidity();
                
            }
            else if(value.length == 1){
                inputField.setCustomValidity('');
                inputField.reportValidity();
                component.set("v.selectedRecId",selectedRole[0]);
            } 
        }
        else if(!showRoleSelect){ //Role Select is not shown in popup
            var hasExistingEnquiryId = component.get("v.hasExistingEnquiryId");
            console.log('hasExistingEnquiryId == ' + hasExistingEnquiryId); 
            if(hasExistingEnquiryId){//Role has existing Enquiry Id
                var getSelectedOption = component.find("existEqAction").get("v.value");
                console.log('getSelectedOption == ' + getSelectedOption);
                helper.proceedExisting(component,event,helper,getSelectedOption);
            }
        }
    },

    closePage: function(component, event, helper) {
        helper.fireEquifaxAppEvt(component, 'cancel');
	},
})