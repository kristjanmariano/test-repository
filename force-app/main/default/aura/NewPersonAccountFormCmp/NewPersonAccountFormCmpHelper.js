({
	fireCustomToast: function(component,messageType,message) {
		var customToastEvent = $A.get("e.c:CustomToastAppEvt");
		customToastEvent.setParams({    "messageType" : messageType,
										"message" : message });
		customToastEvent.fire();
	},
})