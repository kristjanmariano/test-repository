@isTest
private class LoanDocumentRequestFormCaseNMCCTest {

    @testsetup static void setup(){
        //Create test data for (Case, Application, Role, Account)
        TestDataFactory.Case2FOSetupTemplate();

        //Create test data for Partner
        Id ldivRTId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Lender Division').getRecordTypeId(); //Lender Division
        Partner__c lender = new Partner__c(
            Name = 'Test Lender',
            RecordTypeId = ldivRTId
        );
        insert lender;
    }

    static testmethod void testLDRNM(){

        //Retrieve created Test Data
        Case cse = [Select Id, Approval_Conditions__c, Lender1__c, Application_Name__c, CaseNumber From Case limit 1];
        Partner__c lender = [Select Id From Partner__c limit 1];
        
        //Load LDR Form from Case
        PageReference pref_cse = Page.LoanDocumentRequestFormCaseNM;
        pref_cse.getParameters().put('id',cse.Id);
        Test.setCurrentPage(pref_cse);

        Test.startTest();

            cse.Approval_Conditions__c = null;
            cse.Lender1__c = null;
            LoanDocumentRequestFormCaseNMCC ldrcon_cse = new LoanDocumentRequestFormCaseNMCC();
            cse.Approval_Conditions__c = 'Test approval condition';
            cse.Lender1__c = lender.Id;

            LoanDocumentRequestFormCaseNMCC ldrcon_cse2 = new LoanDocumentRequestFormCaseNMCC();
            ldrcon_cse2.saveLDR();
            ldrcon_cse2.loanReq.Notes_to_Lender__c = 'Test Notes to Lender';
            ldrcon_cse2.loanReq.Notes_to_Support__c = 'Test Notes to Support';
            ldrcon_cse2.saveLDR();

            //Verify Loan Document Request is created
            list <Loan_Document_Request_Form_CT__c> loanReqList = [Select Id, Name From Loan_Document_Request_Form_CT__c Where Client_Name__c =: cse.Id];
            system.assertEquals(loanReqList.isEmpty(), false);
            //Verify Case Comments are created
            list <CaseComment> cseComments = [Select Id, ParentId From CaseComment Where ParentId = : cse.Id];
            system.assertEquals(cseComments.size(), 2); //Comments for Notes To Lender and Notes to SUpport

            //Test Save Exception
            ldrcon_cse2.loanReq.Vehicle_Price__c = Decimal.valueof('111111111111111111111111111111111111111111111111111111111111');
            ldrcon_cse2.saveLDR();

            //Load LDR Form from Application
            system.debug('@@cse.Application_Name__c:'+cse.Application_Name__c);
            PageReference pref_app = Page.LoanDocumentRequestFormCaseNM;
            pref_app.getParameters().put('id',cse.Application_Name__c);
            Test.setCurrentPage(pref_app);
            LoanDocumentRequestFormCaseNMCC ldrcon_app = new LoanDocumentRequestFormCaseNMCC();

        Test.stopTest();

    }


}