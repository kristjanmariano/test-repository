/*

    Purpose: Supports VF page for printable version of the Veda credit score.
    
*/

public with sharing class VedaIndividualPrintableCC {
    
   public VedaDTO.IndividualResponse response { get; set; }   
   public boolean savedPDFFile;
      
   public Contact contact { get; set;}
   public Lead leadObj { get; set;}
   public Account acctObj { get; set;}
   public Opportunity oppObj { get; set;}
   public Applicant_2__c app2Obj {get; set; }
   
   public Id idOfObjToAttach { get; set;}
   public String sObjectType { get; set;}
   
   public Attachment xmlFile { get; set;}
   
    /*
        Search Attachments for a xml document associated which
        contains the VEDA advantage response, when you find it, populate the Credit
        report response object for the VF Page

    */   
   public VedaIndividualPrintableCC()
    {
        savedPDFFile = false;
        xmlFile = [Select ParentId, OwnerId, Name, Id, Description, ContentType, 
                                  BodyLength, Body From Attachment where id =:ApexPages.currentPage().getParameters().get('attachID')] ; 
                                  
        idOfObjToAttach = ApexPages.currentPage().getParameters().get('objAttId');
        sObjectType = ApexPages.currentPage().getParameters().get('objType');
        system.debug('sObjectType' + sObjectType);        
        if(sObjectType == 'Account'){
              acctObj = [Select a.Website, a.Type, a.State__c, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, 
                 a.ShippingCity,  a.Salutation, a.RecordTypeId, a.Postcode__c, a.Phone, a.PersonMobilePhone, a.PersonLastCUUpdateDate, 
                 a.PersonLastCURequestDate, a.PersonEmailBouncedReason, a.PersonEmailBouncedDate, a.PersonEmail, a.PersonContactId, a.LastName, 
                 a.IsPersonAccount, a.Id,a.Gender__c, a.FirstName, a.Fax, 
                 a.Drivers_Licence_Number__c,  a.Description,  a.Date_of_Birth__c, a.Customer_Comments__c, 
                 a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, 
                 a.BillingCity, a.Account_PersonMobilePhone__c From Account a where a.id = :idOfObjToAttach];                                 
        
        }else if(sObjectType  == 'Opportunity'){            
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This is apex:pageMessages'));
            //return new PageReference('/'+idOfObjToAttach);    
            oppObj = [Select o.Name, o.Id,
                 a.Website, a.Type, a.State__c, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, 
                 a.ShippingCity,  a.Salutation, a.RecordTypeId, a.Postcode__c, a.Phone, a.PersonMobilePhone, a.PersonLastCUUpdateDate, 
                 a.PersonLastCURequestDate, a.PersonEmailBouncedReason, a.PersonEmailBouncedDate, a.PersonEmail, a.PersonContactId, a.LastName, 
                 a.IsPersonAccount, a.Id, a.Gender__c, a.FirstName, a.Fax, 
                 a.Drivers_Licence_Number__c,  a.Description,  a.Date_of_Birth__c, a.Customer_Comments__c, 
                 a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, 
                 a.BillingCity From Opportunity o,o.Account a where 
                 o.id = :idOfObjToAttach];
        }else if(sObjectType == 'Lead'){
            leadObj = [Select  l.Title, l.Street, l.State, l.Salutation, l.PostalCode, l.Phone,  l.Name, 
                       l.MobilePhone, l.LastName,  l.Id, l.FirstName, l.Fax, l.Email, l.Description, l.Country,
                       l.ConvertedOpportunityId, l.ConvertedDate,l.Drivers_licence_number__c,
                       l.ConvertedContactId, l.Date_of_Birth__c, l.ConvertedAccountId, l.Company,
                       l.City From Lead l where l.id = :idOfObjToAttach];           
        }else if(sObjectType == 'Applicant_2__c'){               
            app2Obj = [SELECT app2.Name, app2.Id, app2.Email__c, app2.Mobile__c, app2.Current_Street_Number__c, app2.Current_Street_Name__c, app2.Current_Street_Type__c,
                app2.Current_Suburb__c, app2.Current_State__c, app2.Current_Postcode__c, app2.Current_Country__c, app2.Date_of_Birth__c, app2.Gender__c, app2.Drivers_Licence_Number__c,
                a.Id, a.isPersonAccount FROM Applicant_2__c app2, app2.Account__r a WHERE app2.Id =:idOfObjToAttach ];
        }else {
            contact = [select id, salutation, gender__c, drivers_license_Number__c, firstname, lastname, birthDate, mailingStreet, mailingCity, mailingState, mailingPostalcode, 
                    mailingCountry from Contact where id = :idOfObjToAttach];
        }
                                          
        response = new VedaDTO.IndividualResponse().fromXml(xmlFile.body.toString()); 
    }
   
   public boolean getSavedPDFFile(){
        return savedPDFFile;
   }
   
   
   public PageReference savePdfFile(){
        System.debug('save the pdf file');
        savedPDFFile = true;
        Attachment a;
        Blob pdfBlob;
        
        if (Test.IsRunningTest())
            pdfBlob=Blob.valueOf('UNIT.TEST');      
        else
            pdfBlob=Page.VedaIndividualPagePrintable.getContentAsPDF();
         system.debug(CustomSettingUtil.getVedaConfigBooleanValue('SaveResultToContact') + 'test');    
        if(sObjectType == 'Account'){
                a = new Attachment(parentId = acctObj.id, name=acctObj.firstname + ' ' +acctObj.lastName + DateTime.now().format()+ '-CC.pdf', body = pdfBlob);
            }
        else if (sObjectType == 'Lead'){
            a = new Attachment(parentId = leadObj.id, name=leadObj.firstname + ' ' +leadObj.lastName + DateTime.now().format()+ '-CC.pdf', body = pdfBlob);
        }else if (sObjectType == 'Opportunity'){
            a = new Attachment(parentId = oppObj.id, name=oppObj.Account.firstname + ' ' +oppObj.Account.lastName + DateTime.now().format()+ '-CC.pdf', body = pdfBlob);
        }else if (sObjectType == 'Applicant_2__c'){
            a = new Attachment(parentId = app2Obj.id, name=app2Obj.Name + DateTime.now().format()+ '-CC.pdf', body = pdfBlob);
        }
       
        else if (CustomSettingUtil.getVedaConfigBooleanValue('SaveResultToContact')) {
            a = new Attachment(parentId = contact.id, name=contact.firstname + ' ' +contact.lastName + '-Credit Check.pdf', body = pdfBlob);                            
       	system.debug(a + 'test');
        }
        insert a;
        delete xmlFile;
            
        return null;
            
    }

}