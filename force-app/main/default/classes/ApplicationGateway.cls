/** 
* @FileName: ApplicationGateway
* @Description: Provides finder methods for accessing data in the Application object.
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 4/05/18 JBACULOD Created Class, added SyncApp2Case
* 1.1 5/06/18 JBACULOD Updated SyncApp2Case, added new fields to sync in Case
* 1.2 5/12/18 JBACULOD Added syncing of TrustPilot Unique Link field in Case
* 1.3 7/06/18 JBACULOD Added population of MC Lookups in Case
* 1.4 7/24/18 JBACULOD Added populatePrimaryApplicant
* 1.5 8/3/18 JBACULOD Added checking of Partition and population of Applicants' Phone and Email from Role
* 1.6 9/24/18 JBACULOD Added toUpdate flag to only update records with field changes
**/ 
public without sharing class ApplicationGateway {
    
    public static void populatePrimaryApplicant(Application__c oldapp, Application__c app, map <Id, list<Role__c>> appRolesByAppMap){

        System.debug('size>>>>>'+appRolesByAppMap.size());
        System.debug('values>>>>'+ appRolesByAppMap.values());
        if (appRolesByAppMap.containskey(app.Id)){
            string primapplicant = ''; integer approleCtr = 0;
            for (Role__c role : appRolesByAppMap.get(app.Id)){
                if (String.valueof(role.RecordType.Name).startsWith('Applicant')){
                    if (appRoleCtr > 0) primapplicant+= ' and ';
                    primapplicant+= role.Account_Name__c;
                    approleCtr++;
                }
            }
            if (oldapp.Primary_Applicant__c != primapplicant && primapplicant != ''){
                app.Primary_Applicant__c = primapplicant;
            }
        }
    }

    /* CURRENT FIELDS SYNCED FROM APPLICATION TO CASE:
        - Lead 12 Months Address
        - Lead 12 Months Income
        - Application Record Type
        - Primary Applicant
        - Combined Surplus (for Consumer)
        - Number of Applicants (for CONSUMER)
        - Non Borrowing Spouse
        - Other Active Cases for Applicants
        - Inconsistent Application Details
        - Primary Contact for Application
        - Primary Contact Preferred Call Time
        - Surplus After New Loan (for CONSUMER / COMMERCIAL)
        - Prim. Applicant TrustPilot Unique Link
        - Co-Applicant TrustPilot Unique Link
    */
    public static map <id,Case> SyncApp2Case(map <Id,sObject> oldsobjMap, list <Application__c> applist, Map<Id,Schema.RecordTypeInfo> appRTMapById){
        
        map <id, Application__c> oldMap = new map <id, Application__c>();
        for (sobject sobj : oldsobjMap.values()){
            Application__c app = (Application__c) sobj;
            oldMap.put( (Id) sobj.get('Id'), app);
        }

        map <Id,Case> CaseToSyncMap = new map <Id,Case>();
        for (Application__c app : [Select Id, RecordTypeId, Name, Primary_Applicant__c, Primary_Contact_for_Application__c, Partition__c,
                                    Number_of_Applicants__c, 
                                    Inconsistent_Application_Details__c, Other_Active_Cases_for_Applicants__c, 
                                    Non_Borrowing_Spouse__c, Primary_Contact_Preferred_Call_Time__c, Comb_Total_Surplus__c, Case__c,Net_Profit_After_Tax__c,Case__r.Regular_Monthly_Payment__c,
                                    Primary_Contact_Name_MC__c, Spouse_Supporting_Servicing_Name_MC__c, Co_Applicant_Name_MC__c, Primary_Contact_Location_MC__c,
                                    Primary_Contact_Email_MC__c, Primary_Contact_Phone_MC__c, Co_Applicant_Email_MC__c, Co_Applicant_Phone_MC__c, Spouse_Supporting_Servicing_Email_MC__c, Spouse_Supporting_Servicing_Phone_MC__c,
                                    Prim_Applicant_TrustPilot_Unique_Link__c, Co_Applicant_TrustPilot_Unique_Link__c
                                    From Application__c Where Id in : trigger.new]){
            Case cse = new Case();
            system.debug('@@app:'+app);
            system.debug('@@oldapp:'+oldMap.get(app.Id));
            boolean toUpdate = false;

            //Lead_12_Months_Address__c, Lead_12_Months_Income__c, 

            //Sync Lead 12 Months Address
            // if (oldMap.get(app.Id).Lead_12_Months_Address__c != app.Lead_12_Months_Address__c){
            //     if (app.Lead_12_Months_Address__c != null){
            //         cse.Lead_12_Months_Address__c = app.Lead_12_Months_Address__c; 
            //     }
            // }

            // //Sync Lead 12 Months Income
            // if (oldMap.get(app.Id).Lead_12_Months_Income__c != app.Lead_12_Months_Income__c){
            //     if (app.Lead_12_Months_Income__c != null){
            //         cse.Lead_12_Months_Income__c = app.Lead_12_Months_Income__c; 
            //     }
            // }
            
            if (oldMap.get(app.Id).RecordTypeId != app.RecordTypeId){ //Application RT was changed
                cse.Application_Record_Type__c = appRTMapById.get(app.RecordTypeId).Name; //Update Case's Applicant Type with current Application RT Name
                toUpdate = true;
            }

            //Sync Primary Applicant
            if (oldMap.get(app.Id).Primary_Applicant__c != app.Primary_Applicant__c){ //&& app.Primary_Applicant__c != null){
                cse.Primary_Applicant__c = app.Primary_Applicant__c;
                toUpdate = true;
            }

            //Sync Non - Borrowing Spouse
            if (oldMap.get(app.Id).Non_Borrowing_Spouse__c != app.Non_Borrowing_Spouse__c){
                if (app.Non_Borrowing_Spouse__c != null){
                    cse.Spouse_Supporting_Servicing_Name_MC__c = app.Spouse_Supporting_Servicing_Name_MC__c;
                    cse.Spouse_Supporting_Servicing_Email_MC__c = app.Spouse_Supporting_Servicing_Email_MC__c;
                    cse.Spouse_Supporting_Servicing_Phone_MC__c = app.Spouse_Supporting_Servicing_Phone_MC__c;
                    cse.Non_Borrowing_Spouse__c = app.Non_Borrowing_Spouse__c; 
                    toUpdate = true;
                }
            }

            //Sync Other Active Cases for Applicants - previously set as a formula field in Case
            if (oldMap.get(app.Id).Other_Active_Cases_for_Applicants__c != app.Other_Active_Cases_for_Applicants__c){
                if (app.Other_Active_Cases_for_Applicants__c != null){
                    cse.Other_Active_Cases_for_Applicants__c = app.Other_Active_Cases_for_Applicants__c; 
                    toUpdate = true;
                }
            }

            //Sync Co-Applicant
            if (oldMap.get(app.Id).Co_Applicant_Name_MC__c != app.Co_Applicant_Name_MC__c){
                cse.Co_Applicant_Name_MC__c = app.Co_Applicant_Name_MC__c;
                toUpdate = true;
            }
            if (oldMap.get(app.Id).Co_Applicant_Email_MC__c != app.Co_Applicant_Email_MC__c){
                cse.Co_Applicant_Email_MC__c = app.Co_Applicant_Email_MC__c;
                toUpdate = true;
            }
            if (oldMap.get(app.Id).Co_Applicant_Phone_MC__c != app.Co_Applicant_Phone_MC__c){
                cse.Co_Applicant_Phone_MC__c = app.Co_Applicant_Phone_MC__c;
                toUpdate = true;
            }

            //Sync Primary Contact for Application
            //if (oldMap.get(app.Id).Primary_Contact_for_Application__c != app.Primary_Contact_for_Application__c){ //app.Primary_Contact_for_Application__c != null){
            if (oldMap.get(app.Id).Primary_Contact_Name_MC__c != app.Primary_Contact_Name_MC__c){ 
                cse.Primary_Contact_Name_MC__c = app.Primary_Contact_Name_MC__c;
                if (app.Partition__c != null && app.Partition__c != ''){
                    cse.AccountId = cse.Primary_Contact_Name_MC__c;
                }
                toUpdate = true;
            }
            if (oldMap.get(app.Id).Primary_Contact_Location_MC__c != app.Primary_Contact_Location_MC__c){
                cse.Primary_Contact_Location_MC__c = app.Primary_Contact_Location_MC__c;
                toUpdate = true;
            }
            if (oldMap.get(app.Id).Primary_Contact_for_Application__c != app.Primary_Contact_for_Application__c){
                cse.Primary_Contact_for_Application__c = app.Primary_Contact_for_Application__c;
                toUpdate = true;
            }
            if (oldMap.get(app.Id).Primary_Contact_Email_MC__c != app.Primary_Contact_Email_MC__c){
                cse.Primary_Contact_Email_MC__c = app.Primary_Contact_Email_MC__c;
                toUpdate = true;
            }
            if (oldMap.get(app.Id).Primary_Contact_Phone_MC__c != app.Primary_Contact_Phone_MC__c){
                cse.Primary_Contact_Phone_MC__c = app.Primary_Contact_Phone_MC__c;
                toUpdate = true;
            }
            //}

            //Sync Primary Contact Preferred Call Time
            if (oldMap.get(app.Id).Primary_Contact_Preferred_Call_Time__c != app.Primary_Contact_Preferred_Call_Time__c){
                if (app.Primary_Contact_Preferred_Call_Time__c != null){
                    cse.Primary_Contact_Preferred_Call_Time__c = app.Primary_Contact_Preferred_Call_Time__c;
                    toUpdate = true;
                }
            }

            //Sync Inconsistent Application Details - previously set as a formula field in Case
            if (oldMap.get(app.Id).Inconsistent_Application_Details__c != app.Inconsistent_Application_Details__c){
                if (app.Inconsistent_Application_Details__c != null){
                    cse.Inconsistent_Application_Details__c = app.Inconsistent_Application_Details__c; 
                    toUpdate = true;
                }
            }

            if ( String.valueof(appRTMapById.get(app.RecordTypeId).Name).contains('Commercial')){ 
                if(app.Case__r.Regular_Monthly_Payment__c != NULL){
                	cse.Surplus_After_New_Loan__c = app.Case__r.Regular_Monthly_Payment__c; //Sync Surplus After New Loan - previously set as a formula field in Case
                    toUpdate = true;
                }
            }
            if ( String.valueof(appRTMapById.get(app.RecordTypeId).Name).contains('Commercial')){ 
                if(app.Net_Profit_After_Tax__c != NULL){
                	cse.Business_Net_Profit_After_Tax__c = app.Net_Profit_After_Tax__c; //Sync Surplus After New Loan - previously set as a formula field in Case
                    toUpdate = true;
                }
            }
            //else 
            if ( String.valueof(appRTMapById.get(app.RecordTypeId).Name).contains('Consumer') ){ 
                //Sync Number of Applicants - previously set as a formula field in Case
                if (oldMap.get(app.Id).Number_of_Applicants__c != app.Number_of_Applicants__c){
                    if (app.Number_of_Applicants__c != null){
                        cse.Number_of_Applicants__c = app.Number_of_Applicants__c; 
                        toUpdate = true;
                    }
                }
                //Sync Combined Surplus - previously set as a formula field in Case
                if (oldMap.get(app.Id).Comb_Total_Surplus__c != app.Comb_Total_Surplus__c){ 
                    cse.Combined_Surplus__c = app.Comb_Total_Surplus__c; 
                    Decimal appCaseRegularMonthlyPayment = (app.Case__r.Regular_Monthly_Payment__c != NULL)?app.Case__r.Regular_Monthly_Payment__c:0;
                    cse.Surplus_After_New_Loan__c = app.Comb_Total_Surplus__c - appCaseRegularMonthlyPayment; //Sync Surplus After New Loan - previously set as a formula field in Case
                    toUpdate = true;
                }                    
            }

            //Sync Primary Applicant TrustPilot Unique Link
            if (oldMap.get(app.Id).Prim_Applicant_TrustPilot_Unique_Link__c != app.Prim_Applicant_TrustPilot_Unique_Link__c){
                if (app.Prim_Applicant_TrustPilot_Unique_Link__c != null){
                    cse.Prim_Applicant_TrustPilot_Unique_Link__c = app.Prim_Applicant_TrustPilot_Unique_Link__c;
                    toUpdate = true;
                }
            }
            //Sync Co-Applicant TrustPilot Unique Link
            if (oldMap.get(app.Id).Co_Applicant_TrustPilot_Unique_Link__c != app.Co_Applicant_TrustPilot_Unique_Link__c){
                if (app.Co_Applicant_TrustPilot_Unique_Link__c != null){
                    cse.Co_Applicant_TrustPilot_Unique_Link__c = app.Co_Applicant_TrustPilot_Unique_Link__c;
                    toUpdate = true;
                }
            }

            if (toUpdate){
                if (app.Case__c != null){ 
                    cse.Id = app.Case__c;
                    CaseToSyncMap.put(cse.Id, cse);
                }
            }

        }
        
        return CaseToSyncMap;
    }

}