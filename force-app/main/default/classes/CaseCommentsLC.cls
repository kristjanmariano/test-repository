/**
 * @File Name          : CaseCommentsLC.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 10/10/2019, 10:03:02 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    01/08/2019, 8:15:21 am   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
 * 1.1    10/10/2019, 9:41:29 am   jesfer.baculod@positivelendingsolutions.com.au     Added checking of user preference if Comment will be defaulted to Private or Public
**/
public class CaseCommentsLC {

    @AuraEnabled
    public static string getSessionID(){
        return userInfo.getSessionID();
    }

    @AuraEnabled
    public static caseCommentWrapper getCaseAndComments(ID cseID){
        caseCommentWrapper cseCommWr = new caseCommentWrapper();
        if (cseID != null){
            cseCommWr.cse = getCurrentCase(cseID);
            cseCommWr.curComment = new CaseComment(
                parentID = cseID
            );
            cseCommwr.curComment.IsPublished = [Select Id, Default_New_Case_Comment_as_Private__c From User Where Id = : UserInfo.getUserId()].Default_New_Case_Comment_as_Private__c ? false : true;
            cseCommWr.comments = getExistingCaseComments(cseID);
            cseCommWr.userSFId = userInfo.getUserId();
        }
        system.debug('@@cseCommWr:'+cseCommWr);
        return cseCommWr;
    }

    @AuraEnabled
    public static string saveCaseComment(CaseComment comment){
        system.debug('@@comment: '+comment);
        string saveSuccess = 'SUCCESS';
        Savepoint sp = Database.setSavepoint();
        try{
            insert comment;
        }
        catch(Exception e){
            Database.rollback(sp); 
            if (e.getMessage().contains('CANNOT_EXECUTE_FLOW_TRIGGER')){
				System.debug('e = '+e.getMessage());
                saveSuccess = Label.Case_Comment_Flow_Trigger_Err_Message;
			}
            else if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION,')){
				String errmsg = String.valueof(e.getMessage()).substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,',':');
				saveSuccess = errmsg;
			}
			else {
				saveSuccess = 'ERROR: '+ e.getLineNumber()+e.getMessage();
			}
            AuraHandledException ae = new AuraHandledException(saveSuccess);
            ae.setMessage('Error: '+ saveSuccess);
            system.debug('@@saveSuccess:'+saveSuccess);
            system.debug('@@Error: '+e.getLineNumber()+e.getMessage());
            throw ae;
        }
        return saveSuccess;
    }

    private static Case getCurrentCase(ID cseID){
        Case excse = [Select Id, CaseNumber, Status, Stage__c, Latest_Comment__c, Latest_Comment_Full__c From Case Where Id = : cseID];
        return excse;
    }

    private static list <CaseComment> getExistingCaseComments(ID cseID){
        list <CaseComment> exComments = [Select Id, ParentId, CommentBody, IsPublished, CreatedDate,CreatedById, CreatedBy.Name From CaseComment Where ParentId = : cseID Order By CreatedDate DESC];
        return exComments;
    }

    public class caseCommentWrapper{
        @AuraEnabled public Case cse {get;set;} //Current Case
        @AuraEnabled public CaseComment curComment {get;set;} //Current Case Comment
        @AuraEnabled public list <CaseComment> comments {get;set;} //Existing Case Comment        
        @AuraEnabled public string userSFId {get;set;}
    }

}