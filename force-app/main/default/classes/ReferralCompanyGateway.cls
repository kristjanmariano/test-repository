/** 
* @FileName: ReferralCompanyGateway
* @Description: Helper class of ReferralCompanyHandler which contains most of the logic needed on triggers for Referral Company
* @Copyright: Positive (c) 2019
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 7/01/19 RDAVID - extra/PartnerAndDealerDirectory - Created Class
* 1.2 2/07/2019 RDAVID Updated Class - extra/task25040162-ReferralCompanySyncToPartnerTrigger - Applied logic to map the following:
        Referral_Company__c.Inbound_Partner_ID__c > Partner__c.Inbound_Partner_ID__c
        Referral_Company__c.Name > Partner__c.Name
        Referral_Company__c.Inbound_Business_Entity_Name__c > Partner__c.Inbound_Business_Entity_Name__c
**/ 
public class ReferralCompanyGateway {
    public static Map<String,Schema.RecordTypeInfo> partnerRTMap = Schema.SObjectType.Partner__c.getRecordTypeInfosByName();

    public static Partner__c createUpdatePartner(Referral_Company__c refCompany, Map<String,Partner__c> relatedSiteMap, Map<Id,ABN__c> abnMap){//, Map<Id,Address__c> refCompAddressMapToInsert
        Partner__c partnerToCreate = new Partner__c(    Name = refCompany.Name,
                                                        m_Referral_Company_SFID__c = refCompany.Id,
                                                        Partner_Type__c = refCompany.Company_Type__c,
                                                        RecordTypeId = getPartnerRecordTypeId(refCompany.Company_Type__c),
                                                        Franchise_Partner__c = (refCompany.Dealer_Type__c == 'Franchise') ? true : false,
                                                        Partner_Parent__c = (relatedSiteMap.containsKey(refCompany.Related_Site__c)) ? relatedSiteMap.get(refCompany.Related_Site__c).Id : null,
                                                        Partner_Phone__c = refCompany.Business_Phone_Number__c,
                                                        Partner_Website__c = refCompany.Company_Website__c,
                                                        m_Registered_Business_Name__c = refCompany.Registered_Business_Name__c,
                                                        m_Legal_Entity_name__c = refCompany.Legal_Entity_Name__c,
                                                        Partner_ABN__c = (abnMap.containsKey(refCompany.Legal_Entity_Name__c)) ? abnMap.get(refCompany.Legal_Entity_Name__c).Registered_ABN__c : null,
                                                        Partner_Legal_Name__c = (abnMap.containsKey(refCompany.Legal_Entity_Name__c)) ? abnMap.get(refCompany.Legal_Entity_Name__c).Name : null,
                                                        m_Nodifi_Onboarding_Stage__c = refCompany.Nodifi_Onboarding_Stage__c,
                                                        OwnerId = refCompany.OwnerId,
                                                        Inbound_Partner_ID__c = refCompany.Inbound_Partner_ID__c, //1.2 2/07/2019 RDAVID Updated Class - extra/task25040162-ReferralCompanySyncToPartnerTrigger
                                                        Inbound_Business_Entity_Name__c = refCompany.Inbound_Business_Entity_Name__c//,
                                                        // Partner_Address__c = (refCompAddressMapToInsert.containsKey(refCompany.Id)) ? refCompAddressMapToInsert.get(refCompany.Id).Id : null
                                                        );
        return partnerToCreate;
    }

    public static Address__c createUpdateAddress(Referral_Company__c refCompany, Map<String,Location__c> locationMap){
        Address__c addressIns = new Address__c( Street_Number__c = (refCompany.Address_Line_1__c != NULL) ? refCompany.Address_Line_1__c.substringBefore(' ') : '',
                                                Street_Name__c = (refCompany.Address_Line_1__c != NULL) ? getStreetName(refCompany.Address_Line_1__c) : '',
                                                Street_type__c = (refCompany.Address_Line_1__c != NULL) ? refCompany.Address_Line_1__c.substringAfterLast(' ') : '',
                                                Postcode__c = refCompany.Postcode__c,
                                                State_Acr__c = refCompany.State__c,
                                                State__c = (refCompany.State__c != NULL) ? getState(refCompany.State__c) : null,
                                                Suburb__c = refCompany.Suburb__c,
                                                Location__c = (!locationMap.isEmpty() && locationMap.containsKey(refCompany.Postcode__c + '-' +refCompany.Suburb__c)) ? locationMap.get(refCompany.Postcode__c + '-' +refCompany.Suburb__c).Id : locationMap.get('UNKNOWN').Id,
                                                Country__c = 'Australia',
                                                m_Referral_Company_SFID__c = refCompany.Id,
                                                m_MIgration_Full_Address__c = refCompany.Address_Line_1__c + ', ' + refCompany.Address_Line_2__c + ', ' + refCompany.Suburb__c + ' ' + refCompany.State__c + ' ' + refCompany.Postcode__c + ' ' + 'Australia');
        return addressIns;
    }

    public static String getState(String stateShort){
        if(stateShort == 'ACT') return 'Australian Capital Territory';
        else if(stateShort == 'NSW') return 'New South Wales';
        else if(stateShort == 'NT') return 'Northern Territory';
        else if(stateShort == 'QLD') return 'Queensland';
        else if(stateShort == 'SA') return 'South Australia';
        else if(stateShort == 'TAS') return 'Tasmania';
        else if(stateShort == 'VIC') return 'Victoria';
        else if(stateShort == 'WA') return 'Western Australia';

        return null;
    }

    public static String getStreetName (String addressline1){
        //String address;
        String strnum = addressline1.substringBefore(' ');
        String strtype = addressline1.substringAfterLast(' ');
        System.debug('@@@strnum = '+strnum);
        System.debug('@@@strtype = '+strtype);
        addressline1 = addressline1.remove(strnum+' ');
        addressline1 = addressline1.remove(' '+strtype);
        return addressline1;
    }

    public static Map<String,Location__c> getLocationMap(Set<String> locName){
        
        Map<String,Location__c> locMap = new Map<String,Location__c>();

        for(Location__c loc : [SELECT Id, Name FROM Location__c WHERE Name IN: locName OR Name =: 'UNKNOWN']){
            locMap.put(loc.Name,loc);
        }
        return locMap;
    }

    public static Id getPartnerRecordTypeId (String companyType){
        Id partnerRTId;
        if(companyType == 'Professional Services' || companyType == 'Real Estate Agent' || companyType == 'Accountant') partnerRTId = partnerRTMap.get('Other - Site').getRecordTypeId();
        else if (companyType == 'Financial Planner') partnerRTId = partnerRTMap.get('Planner - Financial Planners').getRecordTypeId(); 
        else if (companyType == 'Mortgage Broker') partnerRTId = partnerRTMap.get('Finance Brokerage').getRecordTypeId(); 
        else if (companyType == 'Mortgage Aggregator') partnerRTId = partnerRTMap.get('Finance Aggregator').getRecordTypeId();  
        return partnerRTId;
    }

    public static Map<String,Partner__c> getRelatedSiteMap(Set<Id> relatedSiteIds){
        Map<String,Partner__c> relatedSiteMap = new Map<String,Partner__c>();
        Map<String,Id> refCompNameMap = new Map<String,Id>();

        for(Referral_Company__c refComp : [SELECT Id, Name FROM Referral_Company__c WHERE Id IN: relatedSiteIds]){
            refCompNameMap.put(refComp.Name,refComp.Id);
        }
        
        if(!refCompNameMap.isEmpty()){
            for(Partner__c partner : [SELECT Id, Name FROM Partner__c WHERE Name IN: refCompNameMap.keySet()]){
                relatedSiteMap.put(refCompNameMap.get(partner.Name),partner);
            }
        }
        return relatedSiteMap;
    }

    public static Boolean isReferralCompanyUpdated(Referral_Company__c newRefComp, Referral_Company__c oldRefComp){
        if( newRefComp.Name != oldRefComp.Name || newRefComp.Company_Type__c != oldRefComp.Company_Type__c 
            || newRefComp.Dealer_Type__c != oldRefComp.Dealer_Type__c || newRefComp.Related_Site__c != oldRefComp.Related_Site__c 
            || newRefComp.Business_Phone_Number__c != oldRefComp.Business_Phone_Number__c || newRefComp.Company_Website__c != oldRefComp.Company_Website__c 
            || newRefComp.Registered_Business_Name__c != oldRefComp.Registered_Business_Name__c || newRefComp.Legal_Entity_Name__c != oldRefComp.Legal_Entity_Name__c 
            || newRefComp.Nodifi_Onboarding_Stage__c != oldRefComp.Nodifi_Onboarding_Stage__c
            || newRefComp.Address_Line_1__c != oldRefComp.Address_Line_1__c
            || newRefComp.Address_Line_2__c != oldRefComp.Address_Line_2__c
            || newRefComp.Suburb__c != oldRefComp.Suburb__c
            || newRefComp.Postcode__c != oldRefComp.Postcode__c
            || newRefComp.State__c != oldRefComp.State__c
            || newRefComp.Inbound_Partner_ID__c != oldRefComp.Inbound_Partner_ID__c //1.2 2/07/2019 RDAVID Updated Class - extra/task25040162-ReferralCompanySyncToPartnerTrigger
            || newRefComp.Inbound_Business_Entity_Name__c != oldRefComp.Inbound_Business_Entity_Name__c
            ){
                return true;
        }
        return false;
    }

    public static Boolean isAddressUpdated(Referral_Company__c newRefComp, Referral_Company__c oldRefComp){
        if( newRefComp.Address_Line_1__c != oldRefComp.Address_Line_1__c
            || newRefComp.Address_Line_2__c != oldRefComp.Address_Line_2__c
            || newRefComp.Suburb__c != oldRefComp.Suburb__c
            || newRefComp.Postcode__c != oldRefComp.Postcode__c
            || newRefComp.State__c != oldRefComp.State__c
            ){
                return true;
        }
        return false;
    }

    // public static Map<Id,List<Relationship__c>> getPartnerRelationshipMap (Map<Id,Relationship__c> relationshipMap){
    //     Map<Id,List<Relationship__c>> partnerRelationshipMap = new Map<Id,List<Relationship__c>>();

    //     for(Relationship__c rel : relationshipMap.values()){
    //         if(!partnerRelationshipMap.containsKey(rel.Partner__c)){
    //             partnerRelationshipMap.put(rel.Partner__c,new List<Relationship__c>{rel});  
    //         } 
    //         else{
    //             List<Relationship__c> relList = partnerRelationshipMap.get(rel.Partner__c);
    //             relList.add(rel);
    //             partnerRelationshipMap.put(rel.Partner__c,relList);
    //         }
    //     }
    //     return partnerRelationshipMap;
    // }

    public static Map<Id,Relationship__c> getRelationshipMapToUpdate (Set<Id> deletedRefIds){
        Map<Id,Relationship__c> relationshipMapUpdate = new Map<Id,Relationship__c>();
        for(Relationship__c rel : [SELECT Id,End_Date__c FROM Relationship__c WHERE h_ReferralCompanyId__c IN: deletedRefIds]){
            rel.End_Date__c = System.today();
            relationshipMapUpdate.put(rel.Id,rel);
        }
        return relationshipMapUpdate;
    }
}