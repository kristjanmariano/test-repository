/** 
* @FileName: AccountHandler
* @Description: Trigger Handler for the Account SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 5/8/18 RDAVID Created Class
* 1.1 7/28/18 JBACULOD added pushAssocRelationshipsInRole
* 1.2 12/27/18 RDAVID Added Logic to keep the Partner objects (Partner, Person Account, Relationship) and Contact records in synch.  
* 1.3 3/20/19 JBACULOD added auto-adding of Account as campaign Member to either FB Lead Campaign or Delacon Campaign
* 1.4 3/21/19 JBACULOD added checking of matching Delacon Lead
* 1.5 3/26/19 JBACULOD added Mobile Phone correction for accurate Mobile Phones of Person Accounts and Delacon Leads
* 1.6 6/05/19 JBACULOD added checking of hContactId for preventing duplicates
**/ 
public without sharing class AccountHandler implements ITrigger{
    //SD-92 Sync Account Name to Role-> Application
    public static Map<Id,String> updatedAccountIdNameMap = new Map<Id,String>();
    private static final String PRIMARYAPPICANT = 'Primary Applicant';
    public static Map<Id,Role__c> roleToUpdateMap = new Map<Id,Role__c>(); //key - Role ID
    public static Map<Id, Role__c> updateRolesMap = new map <Id, Role__c>(); //key - Role ID
    
    private List<Contact> contactListToUpdate = new List<Contact>();
    private list <CampaignMember> newCMlist = new list <CampaignMember>();
    private set <ID> AccCampaignIDs = new set <ID>();

    private map <string,boolean> matchingDelaconLeadMap = new map <string,boolean>(); //key: FirstName+LastName+Email

    public void bulkBefore(){

        if(Trigger.isInsert){ //JBACU 03/22/19
            if (TriggerFactory.trigset.Enable_Account_Is_Delacon_Lead__c){
                List<Account> newAccList = Trigger.new;
                for (Account acc : newAccList){
                    if (acc.IsPersonAccount && acc.PersonMobilePhone != null && acc.From_Positive_Path__c){ //Only check for Person Accounts created from Positive Path
                        string accPhone = acc.PersonMobilePhone;
                        accPhone = accPhone.trim(); //Remove white spaces
                        accPhone = accPhone.replace(' ',''); //Remove spaces
                        if (accPhone.startsWith('+61')) accPhone = accPhone.replace('+61', '0'); //Auto Correct Mobile Phone to match with Mobile Phone from Delacon Lead
                        else if (accPhone.startsWith('61')) accPhone = accPhone.replace('61', '0');  //Auto Correct Mobile Phone to match with Mobile Phone from Delacon Lead
                        acc.PersonMobilePhone = accPhone;
                        string accmapkey = acc.PersonMobilePhone;
                        if (!matchingDelaconLeadMap.containskey(accmapkey)) matchingDelaconLeadMap.put(accmapkey,false);
                    }
                }
                if (matchingDelaconLeadMap.size() >0){
                    for (Lead delaconLead : [Select Id, DELAPLA__PLA_Caller_Phone_Number__c From Lead Where LeadSource = 'delacon']){
                        string leadmapkey = delaconLead.DELAPLA__PLA_Caller_Phone_Number__c;
                        if (matchingDelaconLeadMap.containskey(leadmapkey)) matchingDelaconLeadMap.put(leadmapkey, true); //Match has found
                    }
                }
            }
        }

    }

    public void bulkAfter(){
        Map<Id,Account> oldAccountMap = (Trigger.isUpdate)?(Map<Id,Account>)Trigger.oldMap:null;
        Map<Id,Account> newAccountMap = (Trigger.isUpdate)?(Map<Id,Account>)Trigger.newMap:null;
        List<Account> newAccList = Trigger.new;
        Set<Id> accUpdatedIds = new Set<Id>();
        list<Account> accAssocRelsList = new list <Account>();
        map <string, Account_Campaign_Mapping__mdt> acmmtmap = new map <string, Account_Campaign_Mapping__mdt>();
        map <string, string> exCMMap = new map <string, string>();

        if(Trigger.isInsert){ //JBACU 03/20/19
            if (TriggerFactory.trigSet.Enable_Account_Add_as_Campaign_Member__c){
                for (Account_Campaign_Mapping__mdt acmmt : [Select Id, Account_Source__c, Campaign_Name__c, Campaign_ID__c From Account_Campaign_Mapping__mdt]){
                    acmmtmap.put(acmmt.Account_Source__c, acmmt);
                }
                for (Account acc : newAcclist){
                    if (acmmtmap.containskey(acc.AccountSource) && acc.IsPersonAccount){
                        CampaignMember cm = new CampaignMember(
                            CampaignId = acmmtmap.get(acc.AccountSource).Campaign_ID__c,
                            ContactId = acc.PersonContactId,
                            Status = 'Sent',
                            bizible2__Bizible_Touchpoint_Date__c  = System.now()
                        );
                        newCMlist.add(cm);
                    }
                }
            }
        }
        else if(Trigger.isUpdate){
            if (TriggerFactory.trigSet.Enable_Account_Add_as_Campaign_Member__c){
                for (Account_Campaign_Mapping__mdt acmmt : [Select Id, Account_Source__c, Campaign_Name__c, Campaign_ID__c From Account_Campaign_Mapping__mdt]){
                    acmmtmap.put(acmmt.Account_Source__c, acmmt);
                    AccCampaignIDs.add(acmmt.Campaign_ID__c);
                }
                for (CampaignMember exCM : [Select Id, ContactId, CampaignId From CampaignMember Where CampaignID in : AccCampaignIDs]){
                    exCMMap.put(exCM.ContactId+'-'+exCM.CampaignID, exCM.Id);
                }
            }
            for(Account acc : newAccList){
                System.debug(' acc.Name = '+ acc.IsPersonAccount);
                System.debug(' acc.Name = '+ acc.Name);
                System.debug(' acc.Name = '+oldAccountMap.get(acc.Id).Name);
                if (TriggerFactory.trigSet.Enable_Account_Add_as_Campaign_Member__c){ //JBACU 03/20/19
                    //On change of AccountSource (by scheduled job)
                    if (acmmtmap.containskey(acc.AccountSource) && acc.IsPersonAccount && acc.AccountSource != oldAccountMap.get(acc.Id).AccountSource && acc.AccountSource != '' && acc.AccountSource != null){ 
                        CampaignMember cm = new CampaignMember(
                            CampaignId = acmmtmap.get(acc.AccountSource).Campaign_ID__c,
                            ContactId = acc.PersonContactId,
                            Status = 'Sent',
                            bizible2__Bizible_Touchpoint_Date__c  = System.now()
                        );
                        if (exCMMap.containskey(cm.ContactId+'-'+cm.CampaignId)) cm.Id = exCMMap.get(cm.ContactId+'-'+cm.CampaignId); //Update Campaign Member to New Campaign
                        newCMlist.add(cm);
                    }
                }
                if(TriggerFactory.trigset.Enable_Account_Sync_Name_to_Role__c){
                    if(!acc.IsPersonAccount && oldAccountMap.get(acc.Id).Name != acc.Name ||acc.IsPersonAccount && (oldAccountMap.get(acc.Id).FirstName != acc.FirstName || oldAccountMap.get(acc.Id).MiddleName != acc.MiddleName || oldAccountMap.get(acc.Id).LastName != acc.LastName)){
                        if(acc.IsPersonAccount){
                            String fName = (!String.isBlank(acc.FirstName))?acc.FirstName + ' ':'';
                            String mName = (!String.isBlank(acc.MiddleName))?acc.MiddleName + ' ':'';
                            String lName = (!String.isBlank(acc.LastName))?acc.LastName:'';
                            updatedAccountIdNameMap.put(acc.Id,fName+mName+lName);
                        }
                        else if(!acc.IsPersonAccount){
                            updatedAccountIdNameMap.put(acc.Id,acc.Name);
                        }
                        
                    }
                }
                if (TriggerFactory.trigSet.Enable_Account_Assocs_in_Role__c){
                    if (oldAccountMap.get(acc.Id).Associated_Relationships__c != acc.Associated_Relationships__c){
                        accAssocRelsList.add(acc);
                    }
                }
                //RDAVID 12/27/18
                if(TriggerFactory.trigSet.Enable_Partner_Sync_Account_Contact__c){
                    Account oldAcc = oldAccountMap.get(acc.Id);
                    if(AccountGateway.isAccountMainFieldUpdated(acc,oldAcc)){
                        accUpdatedIds.add(acc.Id);
                    }
                }
            }
            if(accUpdatedIds.size() > 0){
                set <string> relhConIds = new set <string>();
                for(Relationship__c rel : [SELECT Id, Associated_Account__c, Partner__c, h_ContactId__c FROM Relationship__c WHERE Associated_Account__c IN: accUpdatedIds AND h_ContactId__c != NULL]){
                    if (!relhConIds.contains(rel.h_ContactId__c)){ //JBACU 6/05/19
                        Account acc = newAccountMap.get(rel.Associated_Account__c);
                        Contact contoUpdate = AccountGateway.getContactToUpdate(acc,rel.h_ContactId__c);
                        contactListToUpdate.add(contoUpdate);
                        relhConIds.add(rel.h_ContactId__c);
                    }
                }
            }

            if (accAssocRelsList.size() > 0) updateRolesMap = AccountGateway.pushAssocRelationshipsInRole(accAssocRelsList);
            System.debug(updatedAccountIdNameMap);
            if(!updatedAccountIdNameMap.isEmpty()){
                for(Role__c role : [SELECT Id,Account__c,Account_Name1__c FROM Role__c WHERE Account__c IN: updatedAccountIdNameMap.keySet()]){
                    if(updatedAccountIdNameMap.containsKey(role.Account__c)){
                        role.Account_Name1__c = updatedAccountIdNameMap.get(role.Account__c);
                        roleToUpdateMap.put(role.Id,role);
                    }
                }
                //Merge updates with Associated Relationships
                if (!updateRolesMap.isEmpty()){                  
                    for (Role__c role : updateRolesMap.values()){
                        if (roleToUpdateMap.containskey(role.Id)){
                            role.Account_Name1__c = roleToUpdateMap.get(role.Id).Account_Name1__c;
                        }
                    }
                }
            }
        }
    }
    public void beforeInsert(SObject so){
        Account acc = (Account)so;
        if (matchingDelaconLeadMap.size() > 0){
            if (acc.IsPersonAccount && acc.PersonMobilePhone != null){
                string accmapkey = acc.PersonMobilePhone;
                if (matchingDelaconLeadMap.containskey(accmapkey)){
                    if (matchingDelaconLeadMap.get(accMapKey)){
                        acc.AccountSource = 'delacon'; //Update Account Source to delacon if matching to a Delacon Lead
                    }
                }
            }
        }
    }
    public void beforeUpdate(SObject oldSo, SObject so){
        
    }
    public void beforeDelete(SObject so){	
    
    }
    
    public void afterInsert(SObject so){
        
    }
    public void afterUpdate(SObject oldSo, SObject so){
        
    }    
    public void afterDelete(SObject so){
	}
    public void andFinally(){
        if (updateRolesMap.size() > 0){
            database.update(updateRolesMap.values());
        }
        else{
            if(!roleToUpdateMap.isEmpty()){
                Database.update(roleToUpdateMap.values());
                System.debug('roleToUpdateMap '+roleToUpdateMap);
                System.debug('Object Account -START-');
                System.debug('Queries :' + Limits.getQueries() + ' / '+Limits.getLimitQueries());
                System.debug('Rows Queried :' + Limits.getDmlRows() + ' / '+Limits.getLimitDmlRows());
                System.debug('DML : ' +  Limits.getDmlStatements() + ' / '+Limits.getLimitDmlStatements());
                System.debug('Object Account -END-');
            }
        }
        if(contactListToUpdate.size() > 0){
            Database.update(contactListToUpdate);
        }
        if (newCMlist.size() > 0){
            Database.upsert(newCMlist); //Insert or Updates Campaign Member
        }
    }
}