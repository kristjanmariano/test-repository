@isTest(seeAllData=true)
public class LKDataSetupControllerTest {
    public static testMethod void testLkDataSetupController() {
        Account acc = LkIntegrationInsertTestData.insertAccount();
        Opportunity opp = LkIntegrationInsertTestData.insertOpportunity(acc.id);
        Applicant_2__c apl2 = LkIntegrationInsertTestData.insertApplicant2(acc);
        Applicant_2_Mapping__c apl2M = LkIntegrationInsertTestData.insertApplicant2Mapping(apl2,opp);
        Liability__c liab = LKIntegrationInsertTestData.insertLiability(acc,opp);
        LkIntegrationInsertTestData.insertLiabilityMapping(liab,opp);
        Assets__c asset = LkIntegrationInsertTestData.insertAsset(acc,opp,'Real Estate');
        LkIntegrationInsertTestData.insertAssetMapping(asset,opp);
        Employee__c emp = LkIntegrationInsertTestData.insertEmployment(acc);
        LkIntegrationInsertTestData.insertEmploymentMapping(emp,opp);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        String oppId = opp.id;
        ApexPages.currentPage().getParameters().put('id',oppId);
        LoanKitDataSetupController controller = new LoanKitDataSetupController(sc);
        controller.autoCreateData();
    }
}