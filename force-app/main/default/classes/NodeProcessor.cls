global Interface NodeProcessor {
    boolean process();
    String getNodeName();
    void setNodeName(String name);
    void setRecordId(String id);
    void setLoankitApplicantId(String id);
    void setQueryObjectName(String objName);
    String getQueryObjectName();
    String getLoankitApplicantId();
    void setQueryFilterString(String queryFilter);
}