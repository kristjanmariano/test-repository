/** 
* @FileName: RelationshipHandler
* @Description: Handler class of RelationshipTrigger
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 6/29/18 JBACULOD Created class
* 1.1 7/26/18 JBACULOD Added populateAssocRelationshipsInAcc
* 2.0 6/06/19 JBACULOD Added retry due to UNABLE_TO_LOCK_ROW issue
* 2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync  - Added logic to propagate fields from Relationship to Person Account 
                                                                    - Added logic to set the Nodifi access picklist fields to No Access once the EndDate is populated
**/ 
public class RelationshipHandler implements ITrigger {

    private list <Relationship__c> dupeSpouseRelInsertList = new list <Relationship__c>();
    private list <Relationship__c> dupeSpouseRelUpdateList = new list <Relationship__c>();
    private list <Relationship__c> dupeSpouseRelDeleteList = new list <Relationship__c>();
    private list <Account> accToUpdateList = new list <Account>();
    private list <Account> relToAccToUpdateList = new list <Account>(); //2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync
    public static boolean spouseRoute = false;

    // Constructor
    public RelationshipHandler(){}

    /** 
    * @FileName: bulkBefore
    * @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
    **/ 
    public void bulkBefore(){
        if (Trigger.isDelete){
            if (TriggerFactory.trigSet.Enable_Relationship_Spouse_Duplicate__c){
                if (!spouseRoute){
                    dupeSpouseRelDeleteList = RelationshipGateway.linkSpouseRelationship(trigger.old, 'Delete');
                    spouseRoute = true;
                }
            } 
        }
        if (Trigger.isUpdate){
            //2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync
            // if (TriggerFactory.trigSet.Enable_Sync_Relationship_to_Account){
                RelationshipGateway.pullValuesFromAccount(trigger.oldMap,trigger.new);
            // }
        }
    }

    public void bulkAfter(){

        if (Trigger.isInsert){
            if (TriggerFactory.trigSet.Enable_Relationship_Spouse_Duplicate__c){
                if (!spouseRoute){
                    dupeSpouseRelInsertList = RelationshipGateway.linkSpouseRelationship(trigger.new, 'Insert');
                    spouseRoute = true;
                }
            }
            if (TriggerFactory.trigSet.Enable_Relationship_Assocs_in_Account__c){
                accToUpdateList = RelationshipGateway.populateAssocRelationshipsInAcc(trigger.oldMap, trigger.newMap, 'Insert');
            }
            //2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync
            // if (TriggerFactory.trigSet.Enable_Sync_Relationship_to_Account){
                relToAccToUpdateList = RelationshipGateway.propagateRelToAcc(trigger.oldMap, trigger.newMap, 'Insert');
            // }
        }
        else if (Trigger.isUpdate){
            system.debug('@@spouseRoute:'+spouseRoute);
            if (TriggerFactory.trigSet.Enable_Relationship_Spouse_Duplicate__c){
                if (!spouseRoute){
                    dupeSpouseRelUpdateList = RelationshipGateway.linkSpouseRelationship(trigger.new, 'Update');
                    spouseRoute = true;
                }
            }
            if (TriggerFactory.trigSet.Enable_Relationship_Assocs_in_Account__c){
                accToUpdateList = RelationshipGateway.populateAssocRelationshipsInAcc(trigger.oldMap, trigger.newMap, 'Update');
            }
            //2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync
            // if (TriggerFactory.trigSet.Enable_Sync_Relationship_to_Account){
                relToAccToUpdateList = RelationshipGateway.propagateRelToAcc(trigger.oldMap, trigger.newMap, 'Update');
            // }
        }
        else if (Trigger.isDelete){
            if (TriggerFactory.trigSet.Enable_Relationship_Assocs_in_Account__c){
                accToUpdateList = RelationshipGateway.populateAssocRelationshipsInAcc(trigger.oldMap, trigger.newMap, 'Delete');
            }
        }

    }

    public void beforeInsert(SObject so){
    }
    
    public void beforeUpdate(SObject oldSo, SObject so){
    }

    /** 
    * @FileName: beforeDelete
    * @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
    **/ 
    public void beforeDelete(SObject so){   
    }
    
    public void afterInsert(SObject so){

    }
    
    public void afterUpdate(SObject oldSo, SObject so){

    }
    
    public void afterDelete(SObject so){
    }

    /** 
    * @FileName: andFinally
    * @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
    **/ 
    public void andFinally(){
        if (dupeSpouseRelInsertList.size() > 0){ 
            insert dupeSpouseRelInsertList;
            list <Relationship__c> oldRelUpdatelist = new list <Relationship__c>();
            for (Relationship__c rel : [Select Id, Spouse_Relationship__c From Relationship__c Where Spouse_Relationship__c in : trigger.new]){
                Relationship__c uprel = new Relationship__c(Id = rel.Spouse_Relationship__c, Spouse_Relationship__c = rel.Id);
                oldRelUpdatelist.add(uprel);
            }
            if (oldRelUpdatelist.size() > 0) update oldRelUpdatelist;
        }
        if (dupeSpouseRelUpdateList.size() > 0){ 
            update dupeSpouseRelUpdateList;
        }
        if (dupeSpouseRelDeleteList.size() > 0) delete dupeSpouseRelDeleteList;


        if (accToUpdateList.size() > 0){ //JBACU 6/06/19
            list <Account> AccountsLockForUpdate = [Select Id From Account Where Id = : accToUpdateList FOR UPDATE];
            try{
                Database.update(accToUpdateList);
            }
            catch (Exception e){
                Database.update(accToUpdateList); //Retry due to Unable_to_lock_Row issue
            }
        }

        if (relToAccToUpdateList.size() > 0){ //2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync
            Database.update(relToAccToUpdateList);
        }
    }



}