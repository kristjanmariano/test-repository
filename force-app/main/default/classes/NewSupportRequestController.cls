/**
 * @File Name          : NewSupportRequestController.cls
 * @Description        : 
 * @Author             : Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 20/12/2019, 9:47:58 am
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    10/06/2019, 7:17:45 am   Jesfer Baculod (jesfer.baculod@positivelendingsolutions.com.au)     Initial Version
 * 1.1    7/08/2019 RDAVID extra/task25582197-NewSupportRequestVFChanges 
 * 1.2    12/08/2019 RDAVID extra/task25582197-NewSupportRequestVFChanges 
 * 1.3    20/12/2019 JBACULOD extra/SFBAU-93 - Added population of Validation Override fields on Support Request creation
**/
global class NewSupportRequestController {

    public Case cse {get;set;}
    public Id curID {get;set;}
    public Id requestRTId {get;set;}
    public String type {get;set;}
    public Boolean showSection {get;set;}
    public Boolean saveSuccess {get;set;}
    public Boolean hasError {get;set;}
    public Boolean isSubmission {get;set;}
    public Boolean isContract {get;set;}
    public Boolean isCaseSentForSettlement {get;set;}
    public Boolean isSettlement {get;set;}
    public Boolean showForm {get;set;}
    public Boolean byPassRTSelection {get;set;}
    public Boolean submissionNotValid {get;set;}
    public Boolean isCaseCommittedFinalMod {get;set;}
    public Boolean isCaseCommittedTaxInvoiceReceived {get;set;} //7/08/2019 RDAVID extra/task25582197-NewSupportRequestVFChanges 
    public Boolean showInternalCustomerRating {get;set;}

    public Support_Request__c supportRequest {get;set;}
    public CaseComment cseCommentNotesForLender {get;set;}
    public CaseComment cseCommentReadyForSubmission {get;set;}
    
    public static Map<String,Schema.RecordTypeInfo> supportRequestRTMapByName = Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName();
    public Map<Id,String> supportRequestRTMap {get;set;}
    public Map<Id,String> supportRequestRTMapDummy {get;set;} //Used for RT Name display.

    private final String CASE_STATUS_REVIEW = 'Review'; 
    private final String CASE_STATUS_APPROVED = 'Approved'; 
    private final String CASE_STAGE_SENTFORSETTLEMENT = 'Sent for Settlement';
    private final String SETTLEMENT = 'Settlement'; 
    private final String CONTRACT = 'Contract';
    private final String SUBMISSION ='Submission';
    private final String CASE_STATUS_COMMITTED = 'Committed'; 
    private final String CASE_STAGE_TAXINVREC = 'Tax Invoice Received'; 

    private final String SUPPORTREQUEST_RT_SUBMISSION_NEW = 'Asset - Submission Request (New)';
    private final String SUPPORTREQUEST_RT_SUSPENSION_NEW = 'Asset - Settlement Suspension (New)';
    private final String SUPPORTREQUEST_RT_CONTRACTREQ_NEW = 'Asset - Contract Request (New)';
    
    private final String SUPPORTREQUEST_SUPPORTSUBTYPE_SUSPENDED = 'Suspended Settlement';
    private static Validation_Rule_Settings1__c valSet = Validation_Rule_Settings1__c.getInstance(UserInfo.getUserId());

    public List<Support_Request_Creation_Setting__mdt> srcsList = new List<Support_Request_Creation_Setting__mdt>(); //* 1.2    12/08/2019 RDAVID extra/task25582197-NewSupportRequestVFChanges
    public string sTypes {get;set;}
    public Map<String,List<String>> sRRTypeSTypeMapping = new Map<String,List<String>>();

    public NewSupportRequestController(){
        curID = Apexpages.currentpage().getparameters().get('id');
        supportRequest = new Support_Request__c();
        cseCommentNotesForLender = new CaseComment(isPublished=FALSE,ParentId=curID);
        cseCommentReadyForSubmission = new CaseComment(isPublished=FALSE,ParentId=curID);

        isSubmission = isContract = saveSuccess = hasError = isSettlement = isCaseSentForSettlement = submissionNotValid = isCaseCommittedFinalMod = showInternalCustomerRating = isCaseCommittedTaxInvoiceReceived = false; //byPassRTSelection
        showForm = true;
        showSection = true;
        supportRequestRTMap = new Map<Id,String>();
        supportRequestRTMapDummy = new Map<Id,String>();

        for(Schema.RecordTypeInfo srRTSchema : supportRequestRTMapByName.values()){
            supportRequestRTMap.put(srRTSchema.getRecordTypeId(),srRTSchema.getName());
            if(srRTSchema.getName().containsIgnoreCase('(New)')) supportRequestRTMapDummy.put(srRTSchema.getRecordTypeId(),srRTSchema.getName().replace('(New)',''));
        }
    
        if (curID != null){
            retrieveCase();
            //Retrieve Custom Metadata for Support Request
            srcsList = [SELECT Active__c,Case_Recordtypes__c,Case_Stage__c,SR_Recordtypes__c,SR_Subtypes__c FROM Support_Request_Creation_Setting__mdt WHERE Active__c = TRUE AND Case_Stage__c =: cse.Stage__c]; //1.2    12/08/2019 RDAVID extra/task25582197-NewSupportRequestVFChanges
            System.debug('byPassRTSelection == '+byPassRTSelection());
            if(byPassRTSelection()) initSupportRequest();
        }        
    }

    public Boolean byPassRTSelection (){
        if(srcsList.size()>0){ //If Case is valid in SR Mapping
            String[] srRType = srcsList[0].SR_Recordtypes__c.split('\\n');
            String[] srSType = srcsList[0].SR_Subtypes__c.split('\\n');
            sRRTypeSTypeMapping = getSRRTypeSTypeMapping (srRType,srSType);
            if(sRRTypeSTypeMapping.keySet().size() == 1){
                
                String rtypeName;
                for (string setElement : sRRTypeSTypeMapping.keySet()) {
                    rtypeName = setElement;
                    break;
                }
                supportRequest.RecordTypeId =  Schema.SObjectType.Support_Request__c.getRecordTypeInfosByName().get(rtypeName).getRecordTypeId();
                return true;
            }
        }
        return false;
    }

    public Map<String,List<String>> getSRRTypeSTypeMapping (String[] rType,String[] sType){
        Map<String,List<String>> sRRTypeSTypeMap = new  Map<String,List<String>>();
        Integer x = 0;
        for(String s : rType){
            List<String> arrStr = sType[x].substringBetween('[',']').split(',');
            if(sTypes == null) sTypes = sType[x].substringBetween('[',']');
            else{
                sTypes += ','+sType[x].substringBetween('[',']');
            }
            sRRTypeSTypeMap.put(s.substringBetween('[',']'),arrStr);
            x++;
        }
        return sRRTypeSTypeMap;
    }   

    public List<SelectOption> getSupportRequestRTs() {
        List<SelectOption> options = new List<SelectOption>();
        if(supportRequestRTMap != NULL){
            for(Id rtId : supportRequestRTMap.keySet()){
                if(!supportRequestRTMap.get(rtId).containsIgnoreCase('Master') && supportRequestRTMap.get(rtId).containsIgnoreCase('(New)')){
                    if(sRRTypeSTypeMapping.containsKey(supportRequestRTMap.get(rtId))) {
                        options.add(new SelectOption(rtId,supportRequestRTMap.get(rtId).replace('(New)','')));
                    }
                }   
            }
        }
        
        return options;
    }

    public pagereference initSupportRequest (){
        if(supportRequestRTMap.get(supportRequest.RecordTypeId).containsIgnoreCase(CONTRACT)) isContract = true;
        else if(supportRequestRTMap.get(supportRequest.RecordTypeId).containsIgnoreCase(SUBMISSION)) isSubmission = true;
        else if(supportRequestRTMap.get(supportRequest.RecordTypeId).containsIgnoreCase(SETTLEMENT)) isSettlement = true;
        
        showForm = false;

        if(isSettlement) supportRequest.Support_Sub_Type__c = SUPPORTREQUEST_SUPPORTSUBTYPE_SUSPENDED;
        
        supportRequest.Case_Number__c = cse.Id;
        supportRequest.Lender__c=cse.Lender1__c;
        supportRequest.Case_Owner__c = cse.Owner.Id;
        supportRequest.Status__c = 'New';
        supportRequest.Stage__c = 'New';
        supportRequest.Primary_Contact_for_Application__c = cse.Primary_Contact_Name_MC__c;
        supportRequest.Primary_Contact_Email__c = cse.Primary_Contact_Email_MC__c;
        supportRequest.Primary_Contact_Phone__c = cse.Primary_Contact_Phone_MC__c;
        supportRequest.Co_Applicant_for_Application__c = cse.Co_Applicant_Name_MC__c;
        supportRequest.Co_Applicant_Email__c = cse.Co_Applicant_Email_MC__c;
        supportRequest.Co_Applicant_Phone__c = cse.Co_Applicant_Phone_MC__c;
        supportRequest.Loan_Type__c = cse.Loan_Type__c;
        supportRequest.Origination_Fee__c = cse.Application_Fee__c;
        supportRequest.Loan_Term__c = cse.Loan_Term__c;
        supportRequest.Loan_Rate__c = cse.Loan_Rate__c;
        supportRequest.Risk_Fee__c	 = cse.Risk_Fee__c	;
        supportRequest.Regular_Monthly_Payment_inc_AKF__c = cse.Regular_Monthly_Payment__c;
        supportRequest.PPSR_Fee__c = cse.PPSR_Fee__c;
        supportRequest.Monthly_Account_Keeping_Fee__c = cse.Monthly_Account_Keeping_Fee__c;
        supportRequest.Financed_Amount__c = cse.Financed_Amount_Unverified__c;
        supportRequest.Credit_Available_After_Fees__c = cse.Required_Loan_Amount__c;
        supportRequest.Total_Bank_Fees__c = cse.Total_Bank_Fees__c;
        supportRequest.Payment_Frequency__c = cse.Payment_Frequency__c;
        supportRequest.Establishment_Fee__c = cse.Establishment_Fee__c;
        supportRequest.Partition__c = cse.Partition__c;
        supportRequest.Payment_Terms__c = cse.Paymen_Terms__c;
        if (cse.Validation_Override__c){ //JBACU 20/12/19 SFBAU-93
            supportRequest.Validation_Override__c = cse.Validation_Override__c;
            supportRequest.Validation_Override_By__c = cse.Validation_Override_By__c;
            supportRequest.Validation_Override_Reason__c = cse.Validation_Override_Reason__c;
        }
        return null;
    }

    //Method to Retrieve Case record and set Boolean fields
    private void retrieveCase(){
        
        string cseQuery = 'Select Id, CaseNumber, RecordTypeId, RecordType.Name, Application_Name__c, Stage__c, Status, Partition__c, Channel__c, Validated__c, ';
        cseQuery+= 'Application_Name__r.Name, Application_Name__r.RecordTypeId, Application_Name__r.RecordType.Name, Owner.Id, Owner.Name, Lender1__c,Lender1__r.Name, Sale_Type__c, Primary_Contact_Name_MC__c, Primary_Contact_Name_MC__r.Name, Primary_Contact_Email_MC__c, Primary_Contact_Phone_MC__c, Co_Applicant_Name_MC__c, Co_Applicant_Name_MC__r.Name, Co_Applicant_Email_MC__c, Co_Applicant_Phone_MC__c,';
        cseQuery+= 'Financed_Amount_Unverified__c, Establishment_Fee__c, Risk_Fee__c, PPSR_Fee__c, Total_Bank_Fees__c, Regular_Monthly_Payment__c, Monthly_Account_Keeping_Fee__c, Loan_Type__c, Application_Fee__c, Loan_Term__c, Loan_Rate__c, Required_Loan_Amount__c, Payment_Frequency__c, Paymen_Terms__c,';
        cseQuery+= 'Validation_Override__c, Validation_Override_By__c, Validation_Override_Reason__c, '; //JBACU 20/19/19 - SFBAU-93
        string roleQUery = 'Select Id, Name, RecordTypeId, RecordType.Name, Case__c, Account__c, Account__r.Name, Account__r.FirstName, Account__r.LastName, Account__r.PersonEmail, Account__r.PersonBirthDate, Account__r.PersonMobilePhone, Account__r.isPersonAccount, Account__r.ABN__c, Account__r.ACN__c, Account__r.Full_Address__c , Application__c ';
        roleQUery+= ' From Roles__r Order By Name ASC'; //Related Roles
        roleQUery= '(' + roleQUery + ')';
        cseQuery+= roleQUery + ' From Case ';

        if (curID.getSObjectType() == Schema.Case.SobjectType){ //ID is from Case
            cseQuery+= 'Where Id = : curID limit 1';
        }

        cse = Database.query(cseQuery);

        if(cse.Stage__c == 'Sent for Settlement') isCaseSentForSettlement = true;

        if(cse.Primary_Contact_Name_MC__c == NULL || cse.Financed_Amount_Unverified__c == NULL || cse.Loan_Rate__c == NULL || 
           cse.Loan_Term__c == NULL || cse.Regular_Monthly_Payment__c == NULL || cse.Total_Bank_Fees__c == NULL || cse.Required_Loan_Amount__c == NULL || 
           cse.Payment_Frequency__c == NULL || (cse.Sale_Type__c == null && String.valueof(cse.RecordType.Name).contains('Asset')) || 
           (cse.Paymen_Terms__c == NULL && String.valueof(cse.RecordType.Name).contains('Commercial - Asset')) ){ 
               submissionNotValid = true;
           }
        if(cse.Partition__c == 'Positive' && (cse.Channel__c == 'PLS' || cse.Channel__c == 'DLA' || cse.Channel__c == 'LFPWBC' )) showInternalCustomerRating = true;
    }

    private boolean caseValidationOnSave(){
        boolean passed = true;
        if (isSubmission){ //Validate if Support Request is for Submission
            if (valSet.Enable_Case_Submission_Validations__c){ //Checks if current user requires Submission Validations on Case
                retrieveCase(); //Check Case if already validated
                if (!cse.Validated__c && supportRequest.Support_Sub_Type__c != 'Final Modification'){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Case has not been fully validated.'));
                    passed = false;
                }
            }
        }
        return passed;
    }

    public Pagereference submit(){
        
        List<CaseComment> caseCommentToInsert = new List<CaseComment>();

        cseCommentReadyForSubmission.CommentBody = 'Notes to Support: '+supportRequest.Notes_to_Support__c;
        if(supportRequest.Notes_to_Support__c != NULL) caseCommentToInsert.add(cseCommentReadyForSubmission);
        
        cseCommentNotesForLender.CommentBody = 'Notes to Lender: '+supportRequest.Notes_to_Lender__c;
        if(supportRequest.Notes_to_Lender__c != NULL) caseCommentToInsert.add(cseCommentNotesForLender);

        if(confirmAgreement()){
            if (caseValidationOnSave()){
                try{
                    Database.insert(supportRequest);
                    if(supportRequest.Internal_Customer_Rating__c != NULL) {
                        Case newCase = new Case(Id = cse.Id, Internal_Customer_Rating__c = supportRequest.Internal_Customer_Rating__c);
                        Database.update(newCase);
                    }
                    if (caseCommentToInsert.size() > 0) Database.insert(caseCommentToInsert);
                    hasError = false;
                    saveSuccess = true;
                    
                }
                catch(Exception e){
                    System.debug(e.getMessage());
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
                    hasError = true;
                }
            }
            else return null;
        }
        else {
            supportRequest.I_confirm_the_SF_Record_is_accurate__c.adderror('Error: Please set the value to true');
            return null;
        }
        return null;
    }

    public pagereference caseid(){
        return new PageReference('/'+supportRequest.Id);
    } 

    public boolean confirmAgreement() {
        if(isCaseSentForSettlement) return true;
        if(!supportRequest.I_confirm_the_SF_Record_is_accurate__c) {
            return false;
        } else {
            return true;
        }   
    }

    webservice static void updateSupportRequest(Support_Request__c supportReq, Id cseId, Id settleOfficerId, Id submitId) { 
         // Do something
        System.debug('supportReq = '+supportReq);
        System.debug('userinfo.getuserid() = '+userinfo.getuserid());
        System.debug('cseId = '+cseId);
        System.debug('settleOfficerId = '+settleOfficerId);
        System.debug('submitId = '+submitId);
        
        try{
            Database.update(supportReq);
        }
        catch(Exception e){
            System.debug('error = '+e.getMessage()+e.getLineNumber());
        }
    }
}