/** 
* @FileName: IncomeGateway
* @Description: Provides finder methods for accessing data in the Income object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification 
* 1.0 2/14/18 RDAVID Created class, Added getRoleIds
* 1.1 5/19/18 JBACULOD Added updateRoleEmploymentStatus, roleToUpdateEmploymentStatus methods
* 1.2 25/06/18 RDAVID Updated Class - SD-103 SF - NM - Continuous Algorithm
* 1.3 18/07/19 RDAVID extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
* 1.4 4/10/19  RDAVID extra/task26245769 extra/task26161988 - Added logic to auto populate Lead_Income_Type__c, Lead_Employment_Situation__c and Lead_Employment_Type__c
* 1.5 20/11/19 JVGO extra/task26733594 - Added method setBusinessIncome to identify auto populate which business account is appropriate for the income record.
**/ 

public without sharing class IncomeGateway{
	public static Set<String> notGainfullyEmployedRTs = new Set<String> {'Centrelink','No Income','Investment Income','Child Support'};
	
	/**
	* @Description: Returns a set of Parent Role Id associated to Income record. 
	* @Arguments:	List<Income1__c> newIncomeList - trigger.new
	* @Returns:		Set<Id> getRoleIds - parent Role ids
	*/
    
    public static Set<Id> getRoleIds(List<Income1__c> newIncomeList){
        Set<Id> roleIdsSet = new Set<Id>();

        for(Income1__c inc : newIncomeList){
            roleIdsSet.add(inc.Role__c);
        }
        return roleIdsSet;
    }
	
	/**
	* @Description: RDAVID extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
	* @Arguments:	List<Income1__c> newIncomeList,Map<String,Schema.RecordTypeInfo> incomeRTByName
	* @Returns:		void
	*/
	public static void setApplicationLookup(List<Income1__c> newIncomeList,Map<String,Schema.RecordTypeInfo> incomeRTByName){
		
		for(Income1__c inc : newIncomeList){
			if(inc.Application1__c != NULL){
				if(incomeRTByName.get('Business Income').getRecordTypeId() == inc.RecordTypeId || incomeRTByName.get('Self-Employed').getRecordTypeId() == inc.RecordTypeId){
					inc.Commercial_Income__c = inc.Application1__c;
					inc.Consumer_Income__c = null;
				}
				else{
					inc.Consumer_Income__c = inc.Application1__c;
					inc.Commercial_Income__c = null;
				}
			}
		}
	}
	
	/**
	* @Description: RDAVID extra/task26245769 extra/task26161988 - Added logic to auto populate Lead_Income_Type__c, Lead_Employment_Situation__c and Lead_Employment_Type__c
	* @Arguments:	List<Income1__c> newIncomeList,Map<String,Schema.RecordTypeInfo> incomeRTByName
	* @Returns:		void
	*/
	public static void setLeadIncomeFields (Income1__c inc){
		String incomeRT = (inc.RecordTypeId != NULL) ? Schema.SObjectType.Income1__c.getRecordTypeInfosById().get(inc.RecordTypeId).getName() : null;
		System.debug('incomeRT == '+incomeRT);
		if(incomeRT != NULL){
			if(incomeRT == 'PAYG Employment'){
				inc.Lead_Income_Type__c = 'PAYG Employment';
				inc.Lead_Employment_Type__c = 'PAYG Employment';
			}
			else if(incomeRT == 'Self-Employed'){
				inc.Lead_Income_Type__c = 'Self-Employed';
				inc.Lead_Employment_Type__c = 'Self-Employed';
			}
			else if(notGainfullyEmployedRTs.contains(incomeRT)){
				inc.Lead_Income_Type__c = incomeRT;
				inc.Lead_Employment_Type__c = 'Not Gainfully Employed';
			}
		}
		
		System.debug('inc == '+inc);
	//Set Lead Employment Situation with Income Situation or with no update if Income Situation is blank due existing Jitterbit mapping
		inc.Lead_Employment_Situation__c = (inc.Income_Situation__c != NULL) ? setIncomeSituation(inc.Income_Situation__c) : inc.Lead_Employment_Situation__c; 
	}

	/** 
	* @Description: Identify the business account and primary applicant of the related income and assign it to the business account of the income record.
	* @Arguments:	List<Income1__c> newIncomeList, >
	* @Returns:		void
	**/
	
	public static void setBusinessAccount(List<Income1__c> newIncomeList, Map<String,Schema.RecordTypeInfo> incomeRTByName){
		
		Set<Id> applicationIdsFromIncome = new Set<Id>();
		Map<Id, Id> appIdAccountMap = new Map<Id, Id>();
		Map<Income1__c, Id> incomeAccountMap = new Map<Income1__c, Id>();

		//get the roles from application from income
		for(Income1__c inc: newIncomeList){
			
				for(Role__c role : [SELECT ID, Name, (Select ID, Role_Type__c, Account__c, Account_Type__c FROM Roles__r) 
										From Application__c WHERE Id = :inc.Application1__c].Roles__r){
		
					if(role.Role_Type__c != null && role.Role_Type__c.containsIgnoreCase('Primary Applicant')){
						appIdAccountMap.put(inc.Application1__c,role.Account__c);
						incomeAccountMap.put(inc, role.Account__c);
					}
				}
			
			
		}
		
		//locate the BusinessAccounts 
		List<Account> primaryBusinessAccounts = [SELECT ID, PersonContactID, (SELECT ID, Application__c FROM Roles__r) FROM Account WHERE PersonContactID = NULL AND ID in: appIdAccountMap.values()];

		//assign business account to income
		for(Income1__c inc: newIncomeList){
			if((incomeRTByName.get('Business Income').getRecordTypeId() == inc.RecordTypeId || incomeRTByName.get('Self-Employed').getRecordTypeId() == inc.RecordTypeId)
			&& !primaryBusinessAccounts.isEmpty()){
				
				for(Account acc : primaryBusinessAccounts){
					if(acc.Id == incomeAccountMap.get(inc)){
						inc.Business_Account__c = acc.Id;
					}
				}
			
			}
		}
		
		
	}


	public static String setIncomeSituation (String incomeSituation){
		if(incomeSituation.containsIgnoreCase('Current')) return 'Current';
		else if(incomeSituation.containsIgnoreCase('Secondary')) return 'Secondary';
		else if(incomeSituation.containsIgnoreCase('Previous')) return 'Previous';
		else return '';
	}
	/*public static map <id,string> updateRoleEmploymentStatus(List<Income1__c> newIncomeList){
		
		map <Id, String> roleEmpStatusMap = new map <Id, String>();
		for (Income1__c inc : [Select Id, Role__c, RecordTypeId, RecordType.Name, Income_Situation__c, Employment_Job_Status__c From Income1__c WHere Id in : newIncomeList]){
			//Income is PAYG Employment or Self-Employed
			if (inc.RecordType.Name == CommonConstants.INCOME_RT_PE || inc.RecordType.Name == CommonConstants.INCOME_RT_SE){
					if (inc.Income_Situation__c  == CommonConstants.INCOME_IS_CI){ //Current Income
						//Get Current Job Status
						roleEmpStatusMap.put(inc.Role__c, inc.Employment_Job_Status__c);
					}
			}
		}
		return roleEmpStatusMap;
	}

	//Helper method for updateRoleEmploymentStatus
	public static list <Role__c> roleToUpdateEmploymentStatus(map <Id, string> empjobStatusMap){
		list <Role__c> rolelist = new list <Role__c>();
		for (Role__c role : [Select Id, Employment_Status__c From Role__c Where Id in : empjobStatusMap.keySet() ]){
			if (empjobStatusMap.containskey(role.Id)){
				//Only update Role if Employment Status got changed from new current Income
				if (empjobStatusMap.get(role.Id) != role.Employment_Status__c){
					role.Employment_Status__c = empjobStatusMap.get(role.Id);
					rolelist.add(role);
				}
			}
		}
		return rolelist;
	}

	public static Set<Id> notValidRTs (Set<String> notValidRTsString, Map<String,Schema.RecordTypeInfo> incRTByName){
		Set<Id> notValidRTsSet = new Set<Id>();
		for(String notValidRT : notValidRTsString){
			if(incRTByName.containskey(notValidRT)) notValidRTsSet.add(incRTByName.get(notValidRT).getRecordTypeId());
		}
		return notValidRTsSet;
	} */

    /* public static List<Role__c> tick12MonthsIncome(List<SObject> records){
        List<Role__c> roleToTickList = new List<Role__c>();
        
		Set<Id> roleIds = new Set<Id>();
		for(SObject record : records){
			if(record.get('Role__c') != NULL){
				roleIds.add(String.valueOf(record.get('Role__c')));
			}
		}
		System.debug('roleIds - >>>>>>>>>>>>>>> '+roleIds);
		Date previousYear = system.today().addYears(-1);
		Integer daysInYear = Date.isLeapYear(system.today().year()) ? 366 : 365;

		for(SObject record : [SELECT Id,Lead_12_Months_Income__c, (SELECT Id, Date_Commenced__c,Date_Completed__c FROM  Incomes1__r WHERE RecordType.Name !=: 'No Income' ORDER BY Date_Commenced__c ASC) FROM Role__c WHERE Id IN: roleIds]){
			Boolean had12MonthsIncome = false;
			Integer daysWithIncome = 0;
			Date oldestStartDate;
			Date latestEndtDate;
			Integer counter = 0;
			if(record.getSObjects('Incomes1__r') != NULL && record.getSObjects('Incomes1__r').size() > 0){
				

				for(SObject childRecord : record.getSObjects('Incomes1__r')){
					
					Date startDate = Date.valueOf(childRecord.get('Date_Commenced__c'));
					Date endDate =  Date.valueOf(childRecord.get('Date_Completed__c'));
					
					if(startDate != NULL){
						if(startDate <= previousYear && endDate == NULL){
							had12MonthsIncome = true;
							break;
						}
						else{
							if(startDate <= previousYear && endDate != NULL){
								daysWithIncome += previousYear.daysBetween(endDate)+1;
							}				
							if(startDate > previousYear && endDate == NULL){
								daysWithIncome += startDate.daysBetween(system.today());	
							}
							if(startDate > previousYear && endDate != NULL){
								daysWithIncome += startDate.daysBetween(endDate);
							}
						}
						if(endDate != NULL){
							counter++;
						}
						oldestStartDate = (oldestStartDate == NULL)?startDate:(oldestStartDate > startDate)?startDate:oldestStartDate;
						latestEndtDate = (oldestStartDate == NULL && endDate != NULL)?endDate:(latestEndtDate < endDate)?endDate:latestEndtDate;
					}
				}
			}
			if(daysWithIncome >= daysInYear){
				had12MonthsIncome = true;
			}		

			if(counter == record.getSObjects('Incomes1__r').size() && latestEndtDate < system.today()){
				had12MonthsIncome = false;
			}

			if(oldestStartDate > previousYear){
				had12MonthsIncome = false;
			}
            
            if(!Boolean.valueOf(record.get('Lead_12_Months_Income__c')) && had12MonthsIncome || Boolean.valueOf(record.get('Lead_12_Months_Income__c')) && !had12MonthsIncome){
                record.put('Lead_12_Months_Income__c',had12MonthsIncome);
            	roleToTickList.add((Role__c)record);
            }
            
            System.debug('oldestStartDate -> '+oldestStartDate);
			System.debug('daysWithIncome -> '+daysWithIncome);
			System.debug('record ->>>>>> '+record);
		}
        return roleToTickList;
    } */
}