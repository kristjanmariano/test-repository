/*
	@Description: controller class of CreateCaseComments VF page
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	8/24/17 - Created
		-	8/25/17 - Updated
		-	7/16/19 - Extended Case Comment input box and replace Comment list with button and Latest Comment
		-   7/19/19 - Fixed issue on whitespace checking
*/
public class CreateCaseCommentsCC {

	public string latestcomment {get;set;}
	public CaseComment comment {get;set;}
	public Id cseID {get;set;} //current Case ID
	public string objType {get;set;}
	public boolean withErrors {get;set;}
	public boolean saveSuccess {get;set;}

	public CreateCaseCommentsCC() {
		withErrors = false;
		cseID = ApexPages.currentPage().getParameters().get('id');
		if (cseID != null){
			objType = String.valueof(cseID.getSobjectType()); 
			comment = new CaseComment(
					parentId = cseID,
					IsPublished = true
				);
			Case cse = [Select Id, Latest_Comment__c From Case Where Id = : cseID];
			if (cse.Latest_Comment__c != null){
				latestcomment = cse.Latest_Comment__c;
			}
		}
	}

	public pageReference saveComment(){
		Savepoint sp = Database.setSavepoint();
		try{
			withErrors = false;
			if (String.isBlank(comment.CommentBody)){ //JBACU 19/07/19
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter comment details'));
				withErrors = true;
			}
			if (withErrors) return null;
			insert comment;
			//Clear comment data
			comment = new CaseComment(
				parentId = cseID,
				IsPublished = true
			);
			saveSuccess = true;
		}
		catch(Exception e){
			withErrors = true;
			Database.rollback(sp);
			if (e.getMessage().contains('CANNOT_EXECUTE_FLOW_TRIGGER')){
				System.debug('e = '+e.getMessage());
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Case_Comment_Flow_Trigger_Err_Message));
				if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION:')){
					String errmsg = String.valueof(e.getMessage()).substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION:','You can look up ExceptionCode');
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errmsg));
				}
			}
			else {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+e.getLineNumber()));
			}
		}
		return null;
	}

	public list <CaseComment> getcomments(){
		list <CaseComment> commlist = new list <CaseComment>();
		commlist = [Select Id, ParentId, CommentBody, IsPublished, CreatedDate,CreatedById, CreatedBy.Name From CaseComment Where ParentId = : cseID Order By CreatedDate DESC];
		return commlist;
	}

	public pageReference refreshComments(){
		getcomments();
		return null;
	}
}