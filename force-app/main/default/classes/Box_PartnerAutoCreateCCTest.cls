/*
	@Description: testing auto create of Box Folders on Partner records
	@Author: Rexie David - Positive Group
	@History:
		-	4/04/19 - Created
*/
@isTest
public class Box_PartnerAutoCreateCCTest {
    @testsetup static void setup(){
        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = Trigger_Settings1__c.getOrgDefaults();
        trigSet.Enable_Triggers__c = true;
        trigSet.Enable_Partner_Box_Auto_Create__c = true;
        upsert trigSet Trigger_Settings1__c.Id;
	}

    static testmethod void testBox(){
		Test.startTest();
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId();
			Partner__c partner = new Partner__c(Name = 'Test Partner Box', Partner_Type__c = 'Mortgage Aggregator', m_Rating__c = 'Committed', m_Company_Type__c = 'Mortgage Aggregator', RecordTypeId = rtId);
            insert partner;
            ApexPages.StandardController st0 = new ApexPages.StandardController(partner);
            Box_PartnerAutoCreateCC boxautoCC = new Box_PartnerAutoCreateCC(st0);
            boxautoCC.test_objRecordFolderID = '37272335804';
            boxautoCC.autocreateBoxFolder();
		Test.stopTest();
        system.assertEquals(ApexPages.hasMessages(),false);
	}

    static testmethod void testBoxLenderDivision(){
		Test.startTest();
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Lender Division').getRecordTypeId();
			Partner__c partner = new Partner__c(Name = 'Test Partner Box', Partner_Type__c = 'Personal Loan', m_Rating__c = 'Committed', RecordTypeId = rtId);
            insert partner;
            ApexPages.StandardController st0 = new ApexPages.StandardController(partner);
            Box_PartnerAutoCreateCC boxautoCC = new Box_PartnerAutoCreateCC(st0);
            boxautoCC.test_objRecordFolderID = '37272335804';
            boxautoCC.autocreateBoxFolder();
		Test.stopTest();
        system.assertEquals(ApexPages.hasMessages(),false);
	}
    
    static testmethod void testBox2(){
		Test.startTest();
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId();
			Partner__c partner = new Partner__c(Name = 'Test Partner Box', Box_Folder_ID__c = '37272335804', Partner_Type__c = 'Mortgage Aggregator', m_Rating__c = 'Committed', m_Company_Type__c = 'Mortgage Aggregator', RecordTypeId = rtId);
            insert partner;

            ApexPages.StandardController st0 = new ApexPages.StandardController(partner);
            Box_PartnerAutoCreateCC boxautoCC = new Box_PartnerAutoCreateCC(st0);
            boxautoCC.test_objRecordFolderID = null;
            boxautoCC.autocreateBoxFolder();
		Test.stopTest();
        system.assertEquals(ApexPages.hasMessages(),true);
	}

    static testmethod void testBox3(){
		Test.startTest();
            Trigger_Settings1__c trigSet = Trigger_Settings1__c.getOrgDefaults();
            trigSet.Enable_Partner_Box_Auto_Create__c = false;
            upsert trigSet Trigger_Settings1__c.Id;
            Id rtId = Schema.SObjectType.Partner__c.getRecordTypeInfosByName().get('Finance Aggregator').getRecordTypeId();
			Partner__c partner = new Partner__c(Name = 'Test Partner Box', Box_Folder_ID__c = '37272335804', Partner_Type__c = 'Mortgage Aggregator', m_Rating__c = 'Committed', m_Company_Type__c = 'Mortgage Aggregator', RecordTypeId = rtId);
            insert partner;

            ApexPages.StandardController st0 = new ApexPages.StandardController(partner);
            Box_PartnerAutoCreateCC boxautoCC = new Box_PartnerAutoCreateCC(st0);
            boxautoCC.test_objRecordFolderID = null;
            boxautoCC.autocreateBoxFolder();
		Test.stopTest();
        system.assertEquals(ApexPages.hasMessages(),true);
	}
}