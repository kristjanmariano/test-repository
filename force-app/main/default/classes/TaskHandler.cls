/** 
* @FileName: TaskHandler
* @Description: Trigger Handler for the Task SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.1 23/08 RDAVID Added Case Comment creation based on Notes__c field
* 2.0 RDAVID 6/09/18 extra/DialerAutomationUpdates
* 2.1 RDAVID 14/09/18 extra/DialerAutomationUpdates - Added logic to set NVMConnect__NextContactTime__c to add 1000 years (to force the workflow to run)
* 2.2 RDAVID 21/09/18 extra/DialerAutomationUpdates - Karlon Logic from Tim - Add a condition not to progress Case Attempted Contact Status when the Channel = "Karlon"
* 2.3 RDAVID 21/09/18 extra/NVMTimeZoneBasedRouting - Added logic to support PWM Cases
* 2.4 JBACULOD 19/03/19 Added DML retries due to Unable_To_Lock_Row issue
* 2.5 RDAVID 14/06/2019 extra/task24928331-BugNVMCallSummaryTask - Added Logic to update exsiting NVM Call Summary Records when the Call Purpose in Task record is updated.
* 2.6 JBACULOD 24/06/19 Added FOR UPDATE due to Unable_To_Lock_Row issue
**/ 

public without sharing class TaskHandler implements ITrigger {	

	private Map <Id,Id> caseTaskMap = new Map <Id,Id>();
	private Set <Id> caseOutboundTaskSet = new Set <Id>();
	private Map <Id,Case> caseMap = new Map <Id,Case>();
	private List <Case> caseList = new List <Case>();
	private List <CaseComment> caseCommentList = new List <CaseComment>();

	private Map <Id,NVMStatsSF__NVM_Call_Summary__c> nvmCallSumMap = new Map <Id,NVMStatsSF__NVM_Call_Summary__c>();
	private List <NVMStatsSF__NVM_Call_Summary__c> nvmCallSummaryToUpdate = new List <NVMStatsSF__NVM_Call_Summary__c>();
	// Constructor
	public TaskHandler(){

	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){

	}
	
	public void bulkAfter(){
		Map<Id,Task> oldTaskMap = (Map<Id,Task>)Trigger.oldMap;
		Set<Id> updateCallPurposeTaskIds = new Set<Id>();
		Set<String> updateCallPurposeCallObjectIds = new Set<String>();
		if(Trigger.IsInsert || Trigger.isUpdate){
			for(Task newTask : (List<Task>)Trigger.new){
				System.debug('what Id - > '+newTask.Subject);
				System.debug('Notes - > '+newTask.Description);
				
				if(newTask.whatId != NULL && String.valueOf(newTask.whatId).containsIgnoreCase(CommonConstants.CASEIDPREFIX) 
				&& newTask.Subject != NULL && newTask.Subject.containsIgnoreCase(CommonConstants.TASKSUBJ_OUTCALL)){

					caseOutboundTaskSet.add(newTask.whatId);//RDAVID 6/09/18: Added this set to capture Task/Case with Outbound call, below if only gets call purpose is attempted contact.
					
					if(newTask.Call_Purpose__c != NULL && newTask.Call_Purpose__c.containsIgnoreCase(CommonConstants.TASKCALLPURPOSE_ATTEMPTCON)){
						System.debug('Task is link to Case -> '+newTask.whatId);
						if(TriggerFactory.trigset.Enable_Task_Update_Case_Attempt__c){
							caseTaskMap.put(newTask.whatId,newTask.Id);
						} 
					}
				}

				// 2.5 RDAVID 14/06/2019 extra/task24928331-BugNVMCallSummaryTask
				if(Trigger.isUpdate && !oldTaskMap.isEmpty()){
					if(newTask.Call_Purpose__c != NULL && newTask.Call_Purpose__c != oldTaskMap.get(newTask.Id).Call_Purpose__c && newTask.CallObject != NULL){
						updateCallPurposeTaskIds.add(newTask.Id);
						updateCallPurposeCallObjectIds.add(newTask.CallObject);
						System.debug(' extra/task24928331 newTask Id ------> '+newTask.Id + ' -- '+newTask.CallObject);
					}
				}
			}
			if(!caseTaskMap.isEmpty() || caseOutboundTaskSet.size()>0){
				caseMap = new Map<Id,Case>([SELECT Id, NVMContactWorld__NVMRoutable__c, RecordType.Name, Status, Stage__c, Attempted_Contact_Status__c,Channel__c FROM Case WHERE Id IN: caseTaskMap.keySet() OR Id IN:caseOutboundTaskSet]);
			}	
			if(updateCallPurposeTaskIds.size() > 0 && updateCallPurposeCallObjectIds.size() > 0){
				nvmCallSumMap = new Map<Id,NVMStatsSF__NVM_Call_Summary__c>();
				for(NVMStatsSF__NVM_Call_Summary__c nvmCallSummary : [SELECT Id, NVMStatsSF__TaskID__c,NVMStatsSF__CallGuid__c FROM NVMStatsSF__NVM_Call_Summary__c WHERE NVMStatsSF__TaskID__c IN: updateCallPurposeTaskIds AND NVMStatsSF__CallGuid__c IN: updateCallPurposeCallObjectIds]){
					
					nvmCallSumMap.put(nvmCallSummary.NVMStatsSF__TaskID__c,nvmCallSummary);

				}
				System.debug(' extra/task24928331nvmCallSumMap------> '+nvmCallSumMap);
			}
		}
	}
		
	public void beforeInsert(SObject so){

	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		Task newTask = (Task)so;
		Boolean updateCase = false;
		if(newTask.whatId != NULL && caseMap.containsKey(newTask.whatId)){
			Case cse = caseMap.get(newTask.whatId);
			if(cse.Status == 'New' && cse.Stage__c == 'Open' && newTask.Call_Purpose__c != NULL && newTask.Call_Purpose__c.containsIgnoreCase(CommonConstants.TASKCALLPURPOSE_ATTEMPTCON)){
				if(cse.Channel__c != 'Karlon'){ //RDAVID 21/09/18 - Do not progress attempted contact when channel is Karlon
					cse.Attempted_Contact_Status__c = 'Attempted 1';
				}
				cse.Stage__c = CommonConstants.TASKCALLPURPOSE_ATTEMPTCON;
				cse.NVMConnect__NextContactTime__c = System.now().addYears(1000);  //RDAVID 14/09/18 
				updateCase = true;
			}
			else if(cse.Status == 'New' && cse.Stage__c == CommonConstants.TASKCALLPURPOSE_ATTEMPTCON && newTask.Call_Purpose__c != NULL && newTask.Call_Purpose__c.containsIgnoreCase(CommonConstants.TASKCALLPURPOSE_ATTEMPTCON)){
				if(cse.Channel__c != 'Karlon'){ //RDAVID 21/09/18 - Do not progress attempted contact when channel is Karlon
					if(cse.Attempted_Contact_Status__c != NULL){
						Boolean isPWMCase = (cse.RecordType.Name.containsIgnoreCase('PWM')) ? true : false;
						if(cse.Attempted_Contact_Status__c == 'Attempted 1') cse.Attempted_Contact_Status__c = 'Attempted 2';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('2')) cse.Attempted_Contact_Status__c = 'Attempted 3';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('3')) cse.Attempted_Contact_Status__c = 'Attempted 4';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('4')) cse.Attempted_Contact_Status__c = 'Attempted 5';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('5')) cse.Attempted_Contact_Status__c = 'Attempted 6';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('6') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 7';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('7') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 8';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('8') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 9';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('9') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 10';		
					}
				}
				cse.NVMConnect__NextContactTime__c = System.now().addYears(1000); // RDAVID 14/09/18 
				updateCase = true;
			}
			if(!newTask.Dialer__c && cse.NVMContactWorld__NVMRoutable__c){
				//cse.NVMContactWorld__NVMRoutable__c = false; //extra/DialerAutomationUpdates - RDAVID 6/09/18: Added logic for Dialer issue from Tim Wells
				updateCase = true;
			}
			if(updateCase) caseList.add(cse);
		}
		if(newTask.Call_Purpose__c != NULL && newTask.Description != NULL && newTask.whatId != NULL && String.valueOf(newTask.whatId).containsIgnoreCase(CommonConstants.CASEIDPREFIX)){
			CaseComment cseComment = new CaseComment(isPublished=TRUE, CommentBody=newTask.Description, ParentId=newTask.whatId); 
			caseCommentList.add(cseComment);
		}
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		Task newTask = (Task)so;
		Task oldTask = (Task)oldSo;
		Boolean updateCase = false;
		if(newTask.whatId != NULL && caseMap.containsKey(newTask.whatId) && ((oldTask.whatId != newTask.whatId) || (oldTask.Subject != newTask.Subject)|| (oldTask.Call_Purpose__c != newTask.Call_Purpose__c))){
			Case cse = caseMap.get(newTask.whatId);
			
			if(cse.Status == 'New' && cse.Stage__c == 'Open' && newTask.Call_Purpose__c != NULL && newTask.Call_Purpose__c.containsIgnoreCase(CommonConstants.TASKCALLPURPOSE_ATTEMPTCON)){
				if(cse.Channel__c != 'Karlon'){ //RDAVID 21/09/18 - Do not progress attempted contact when channel is Karlon
					cse.Attempted_Contact_Status__c = 'Attempted 1';
				}
				cse.Stage__c = CommonConstants.TASKCALLPURPOSE_ATTEMPTCON;
				cse.NVMConnect__NextContactTime__c = System.now().addYears(1000); // RDAVID 14/09/18 
				updateCase = true;
			}

			else if(cse.Status == 'New' && cse.Stage__c == CommonConstants.TASKCALLPURPOSE_ATTEMPTCON && newTask.Call_Purpose__c != NULL && newTask.Call_Purpose__c.containsIgnoreCase(CommonConstants.TASKCALLPURPOSE_ATTEMPTCON)){
				if(cse.Channel__c != 'Karlon'){ //RDAVID 21/09/18 - Do not progress attempted contact when channel is Karlon
					if(cse.Attempted_Contact_Status__c != NULL){
						Boolean isPWMCase = (cse.RecordType.Name.containsIgnoreCase('PWM')) ? true : false;
						if(cse.Attempted_Contact_Status__c == 'Attempted 1') cse.Attempted_Contact_Status__c = 'Attempted 2';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('2')) cse.Attempted_Contact_Status__c = 'Attempted 3';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('3')) cse.Attempted_Contact_Status__c = 'Attempted 4';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('4')) cse.Attempted_Contact_Status__c = 'Attempted 5';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('5')) cse.Attempted_Contact_Status__c = 'Attempted 6';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('6') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 7';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('7') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 8';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('8') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 9';
						else if(cse.Attempted_Contact_Status__c.containsIgnoreCase('9') && !isPWMCase) cse.Attempted_Contact_Status__c = 'Attempted 10';	
					}
				}
				cse.NVMConnect__NextContactTime__c = System.now().addYears(1000); // RDAVID 14/09/18 
				updateCase = true;
			}

			if(!newTask.Dialer__c && oldTask.whatId != newTask.whatId && cse.NVMContactWorld__NVMRoutable__c){
				//cse.NVMContactWorld__NVMRoutable__c = false; //extra/DialerAutomationUpdates - RDAVID 6/09/18: Added logic for Dialer issue from Tim Wells
				updateCase = true;
			} 
			if(updateCase) caseList.add(cse);
		}

		if((oldTask.whatId != newTask.whatId || oldTask.Description != newTask.Description) && newTask.whatId != NULL && String.valueOf(newTask.whatId).containsIgnoreCase(CommonConstants.CASEIDPREFIX) && newTask.Call_Purpose__c != NULL && newTask.Description != NULL){
			CaseComment cseComment = new CaseComment(isPublished=TRUE, CommentBody=newTask.Description, ParentId=newTask.whatId); 
			caseCommentList.add(cseComment);
		}

		// 2.5 RDAVID 14/06/2019 extra/task24928331-BugNVMCallSummaryTask
		if(nvmCallSumMap.containsKey(newTask.Id)){
			NVMStatsSF__NVM_Call_Summary__c nvmCallSummary = nvmCallSumMap.get(newTask.Id);
			nvmCallSummary.Call_Purpose__c = newTask.Call_Purpose__c;
			nvmCallSummaryToUpdate.add(nvmCallSummary);
		}
	}
	
	public void afterDelete(SObject so){

	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
		if (caseList.size()>0){
			try{
				list <Case> recordLockForUpdateCase = [Select Id From Case Where Id in : caselist FOR UPDATE]; //JBACU 6/24/19
				Database.update(caseList);
			}
			catch (Exception e){
				Database.update(caseList); // Retry due to Unable to Lock Row issue
			}
		}
		if (caseCommentList.size()>0){
			try{
				Database.insert(caseCommentList);
			}
			catch (Exception e){
				Database.insert(caseCommentList); // Retry due to Unable to Lock Row issue
			}
		}
		// 2.5 RDAVID 14/06/2019 extra/task24928331-BugNVMCallSummaryTask
		System.debug(' extra/task24928331nvmCallSumMap------> '+nvmCallSummaryToUpdate);
		if (nvmCallSummaryToUpdate.size()>0){
			Database.update(nvmCallSummaryToUpdate);
		}
	}
}