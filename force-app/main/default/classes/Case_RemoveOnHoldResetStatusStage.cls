/*
	@Description: This invocable class is being used in Case_Set_Callback_To_Open_Leads Process Builder.
	@Author: Rexie David - Positive Group
	@History:
		-	3/10/2018 - extra/DialerAutomationUpdates - Created - Logic in Remove On Hold, Set Status/Stage action is moved in this class to support NVM Routing based on Timezone.
        -	22/11/2018 - extra/NVMTimeZoneBasedRouting - Update - Logic to exclude PWM Cases in NVM Routing.
        -	1/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID - Removed the catch to received the errror email once the Attempted Callback fails.
        -   2/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID - Applied A Queueable Apex to fix CPU Time Limit exceedeed error.
        -   23/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID - Applied fix to support bulk update for failing callbacks.
        -   24/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID - Added criteria to include "Open" Case Stage c/o Tim Wells.
        -   24/12/2019 - issuefix/SFBAU-20 - JBACULOD - Applied fix for Case Owner setting to PLS Initial Team Queue instead of current Case Owner Name
*/
public class Case_RemoveOnHoldResetStatusStage implements Queueable{
    private static final Process_Flow_Definition_Settings1__c processFlowSettings = Process_Flow_Definition_Settings1__c.getInstance(UserInfo.getUserId());
    private static final Trigger_Settings1__c trigSet = Trigger_Settings1__c.getInstance(UserInfo.getUserId());
	private List<Case> caseToUpdate;
    
    public Case_RemoveOnHoldResetStatusStage(List<Case> caseToUpdate){
        this.caseToUpdate = caseToUpdate;
    }

    // 2/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID
    public void execute (QueueableContext context){
        system.debug('@@caseToUpdate:'+caseToUpdate);
        if (caseToUpdate.size() > 0){
            Database.update(caseToUpdate);
        }
    } 

    @InvocableMethod(label='Remove On Hold, Set Status/Stage' description='Resets the Case to New and Open, Evaluate if current timezone of Case is valid for NVM call, otherwise schedule it based on Business Hours/Timezone.')
	public static void removeOnHoldSetStatusStage (list <String> caseIDs) {
        Savepoint sp = Database.setSavepoint();
        List<Case> csToUpdateList = new List<Case>();
        List<Case> csList = [SELECT Id, Status, Stage__c, BusinessHoursId, State_Code__c, Ready_For_Call__c, Ready_To_Dial_Time__c, PLS_Initial_Team_Queue__c, PB_Sched_Action__c, Run_PB_Sched_Action_Through_WFR__c, RecordType.Name FROM Case WHERE Id IN: caseIDs];
       
        // try {
            map <string, string> casePLSInitialTeamQueueMap = new map <string, string>();
            set <string> plsinitialteamqueueSet = new set <string>();
            if(csList.size() > 0){
                for(Case cs : csList){
                    Case caseInstance = cs;
                    if((caseInstance.Stage__c == 'Attempted Contact' || caseInstance.Stage__c == 'Open' ) && caseInstance.Status == 'New'){ //24/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID
                        
                        Boolean readyForCall = caseInstance.Ready_For_Call__c;
                        System.debug('readyForCall = '+readyForCall);

                        caseInstance.Call_Back_Set__c = null;
                        caseInstance.On_Hold__c = null;
                        caseInstance.Stage__c = 'Open';
                        caseInstance.Status = 'New';

                        if(!caseInstance.RecordType.Name.containsIgnoreCase('PWM') && trigSet.Enable_Case_NVM_Timezone_based_Routing__c){
                            System.debug('readyForCall 45 = '+trigSet.Enable_Case_NVM_Timezone_based_Routing__c);
                            caseInstance.Ready_To_Dial_Time__c = System.now();
                        }
                        if (caseInstance.PLS_Initial_Team_Queue__c != null){ //JBACU - 24/12/19 SFBAU
                            system.debug('@@caseInstance.PLS_Initial_Team_Queue__c: '+caseInstance.PLS_Initial_Team_Queue__c);
                            casePLSInitialTeamQueueMap.put(caseInstance.Id, caseInstance.PLS_Initial_Team_Queue__c); 
                            plsinitialteamqueueSet.add(caseInstance.PLS_Initial_Team_Queue__c);
                        }
                        caseInstance.PB_Sched_Action__c = null;
                        caseInstance.Run_PB_Sched_Action_Through_WFR__c = false;
                        csToUpdateList.add(caseInstance);
                    }

                    // readytodialtime = callbackset
                }
                //JBACU - 24/12/19 SFBAU - Set OwnerId base from PLS Initial Team Queue
                map <string, string> queueGroupMap = new map <string,string>();
                for (Group iniitalteamQueue : [SELECT Id, NAME FROM Group WHERE Name in : plsinitialteamqueueSet]){ 
                    queueGroupMap.put(iniitalteamQueue.Name, iniitalteamQueue.Id);
                }
                for (Case cse : csToUpdateList){
                    if (casePLSInitialTeamQueueMap.containskey(cse.Id)){
                        string queueGroupMapKey = casePLSInitialTeamQueueMap.get(cse.Id);
                        if (queueGroupMap.containskey(queueGroupMapKey)){
                            cse.OwnerId = queueGroupMap.get(queueGroupMapKey);
                        }
                    }
                }
                system.debug('@@csToUpdateList: '+csToUpdateList);

                // 2/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID
                Case_RemoveOnHoldResetStatusStage caseRemoveOnHoldResetStatusStagSer = new Case_RemoveOnHoldResetStatusStage(csToUpdateList);
                ID jobID = System.enqueueJob(caseRemoveOnHoldResetStatusStagSer);  
            }
        // }
        // Catch(Exception e){
            // System.debug('Error in Case_RemoveOnHoldResetStatusStage = '+e.getmessage());
        //     Database.rollback( sp );  
        // }

	}
}