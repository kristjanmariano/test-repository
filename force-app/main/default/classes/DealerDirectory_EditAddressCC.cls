/*
    @author: Jesfer Baculod - Positive Group
    @history: 10/03/19 - Created
    @description: controller extension class of DealerDirectory_EditAddress
    @update: RDAVID 27/9/2019 : Bug Fixed extra/task26195076-DealershipDirectoryEditAddressVFPage
*/
public class DealerDirectory_EditAddressCC {

    public Dealer_Directory__c refcomp {get;set;}
    public Address__c addr {get;set;}
    public Location__c loc {get;set;}
    private string exAddrID {get; set;}

    public boolean isCreateAddress {get;set;}
    public boolean saveSuccess {get;set;}

    public DealerDirectory_EditAddressCC(ApexPages.StandardController stdController) {
        saveSuccess = isCreateAddress = false;
        exAddrID = '';
        this.refcomp = (Dealer_Directory__c)stdController.getRecord();
        this.refcomp = [Select Id, Name, Address1__c, Full_Address1__c, Dealer_Type__c, Dealer_Website__c From Dealer_Directory__c Where Id = : stdController.getId()];
        if (refcomp.Address1__c != null){
            retrieveSelectedAddress();
            exAddrID = refComp.Address1__c;
        }
        else {
            resetAddressLocation();
        }
    }

    public void retrieveSelectedAddress(){
        system.debug('@@refComp.Address1__c:'+refComp.Address1__c);
        string srchaddrID = '';
        srchaddrID = refComp.Address1__c;
        addr = [Select Id, Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c, Country__c, Location__c, Location__r.Name From Address__c Where Id =: srchAddrID];
        if(addr.Location__c != NULL) retrieveLocation();
        system.debug('@@addr:'+addr);
    }

    public void retrieveLocation(){
        system.debug('@@addr.Location__c:'+addr.Location__c);
        if(addr.Location__c != null) loc = [Select Id, Postcode__c, Suburb_Town__c, State_Acr__c, State__c, Country__c, Name From Location__c Where Id =: addr.Location__c];
        system.debug('@@loc:'+loc);
    }

    public void resetAddressLocation(){
        if (refcomp.Address1__c != null) refcomp.Address1__c = null;
        addr = new Address__c( RecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Full Address').getRecordTypeId() );
        loc = new Location__c();
    }

    private boolean checkDuplicateAddress(){
        boolean dupeAddrfound = false;
        list <Address__c> exdupeAddresses = [Select Id, Name, Location__c, Suburb__c, Street_Name__c, Street_Type__c, Street_Number__c From Address__c Where Location__c =: addr.Location__c];
        if (!exdupeAddresses.isEmpty()){
            for (Address__c exaddr : exdupeAddresses){
                string addrmatchKey = exaddr.Location__c + '-' + exaddr.Street_Name__c + '-' + exaddr.Street_Number__c + '-' + exaddr.Street_Type__c;
                string enteredAddress = addr.Location__c + '-' + addr.Street_Name__c + '-' + addr.Street_Number__c + '-' + addr.Street_Type__c;
                if (addrmatchKey == enteredAddress){
                    addr.Id = exaddr.Id; //Populate Address ID with found Address
                    loc.Id = exaddr.Location__c;
                    dupeAddrfound = true;
                    break;
                }
            }
        }
        System.debug('dupeAddrfound == ' + dupeAddrfound);
        return dupeAddrfound;
    }

    //method for saving Dealer Directory
    public void saveReferralCompany(){
        Savepoint sp = Database.setSavepoint();
        try{
            system.debug('@@isCreateAddress:'+isCreateAddress);
            
            if (!checkDuplicateAddress() && isCreateAddress){ //Update if a new address has been added
                insert addr;
            }
            refComp.Address1__c = addr.Id;
            refComp.Location__c = loc.Id;
            update refcomp; 
            saveSuccess = true;
        }
        catch(Exception e){
            system.debug('@@@331 = '+ e.getLineNumber());
            Database.rollback( sp );
            saveSuccess = false;
        }
    }




}