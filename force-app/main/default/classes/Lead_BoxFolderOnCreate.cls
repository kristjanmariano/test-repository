/**
 * @File Name          : Lead_BoxFolderOnCreate.cls
 * @Description        :  class containing the invocable method for creating Box Folders on Lead 
    - (invoked via Lead - Create Box Folder process flow definition (extra/tasks26291984)
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 06/10/2019, 11:02:13 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/10/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
public class Lead_BoxFolderOnCreate implements Queueable, Database.AllowsCallouts {

	private static list <Box_Partition_Settings__mdt> boxpset {get;set;}
    public static string test_objRecordFolderID {get;set;}
    private static box.Toolkit boxToolkit = new box.Toolkit();
    private list <ID> PBoxleadIDs {get;set;}
	private static String metaObject = 'Lead';
    public Lead_BoxFolderOnCreate(list <ID> leadIDs){
        PBoxleadIDs = leadIDs;
    }

    @InvocableMethod(label='Create Box Folder for Lead' description='creates a Box Folder for Lead when Lead got created')
	public static void leadBoxFolderCreate (list <ID> leadIDs) {
        if (TriggerFactory.trigset.Enable_Partner_Box_Auto_Create__c){
            Lead_BoxFolderOnCreate cbox = new Lead_BoxFolderOnCreate(leadIDs);
            ID jobID = System.enqueueJob(cbox); //Need to be asynchronous due to post DMLs on Lead
        }

    }

    public void execute(QueueableContext context){
        system.debug('@@PBoxleadIDs:'+PBoxleadIDs);
        createBoxFolder(this.PBoxleadIDs);
    }

    public void createBoxFolder(list <ID> leadIDs){
        //Retrieve Box Partition Settings
        boxpset = [Select Id, DeveloperName, Partition__c, Box_Folder_ID__c, Sub_Folders__c, RecordType_Name__c From Box_Partition_Settings__mdt WHERE Object__c =:metaObject];

        //Retrieve Lead
        list <Lead> leadlist = [Select Id, Name, Lead_Number__c, Box_Folder_ID__c, RecordType.Name, Partition__c From Lead Where Id in : leadIDs]; 

        map <string,Lead> updateLeadMap = new map <string,Lead>();
        //Iterate on Lead w/ Box folders to be created
        for (Lead ld : leadlist){
            //Create Lead Box Folder
            if (createmoveParentFolders(ld, boxpset)){
                system.debug('@@0');
                updateLeadMap.put(ld.Id, ld);
            }
        }
        
        system.debug('@@updateLeadMap:'+updateLeadMap);
        if (updateLeadMap.size() > 0){
            update updateLeadMap.values();
        }

        // ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
        boxToolkit.commitChanges();
    }

    private static boolean createmoveParentFolders(Lead ld, list <Box_Partition_Settings__mdt> boxpset){
        for (Box_Partition_Settings__mdt bpset : boxpset){
            system.debug('@@bpSet DEVELOPERNAME:'+bpSet.DEVELOPERNAME);
            //Check Partner Recordtype if it exist in the Custom Metadata
            if (bpset.RecordType_Name__c.containsIgnoreCase(ld.RecordType.Name)){
                system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders MSG: ' + ld.RecordType.Name + ' is in ' + bpset.RecordType_Name__c);
                //Create Box folder
                //set Box Folder Name to Lead - ##### for Nodifi Partner Lead 
                string objRecordFolderId = boxToolkit.createFolderForRecordId(ld.Id, 'Lead - ' + ld.Lead_Number__c, true); 
                system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders most recent error: ' + boxToolkit.mostRecentError);
				
                if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
                system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders objRecordFolderId: ' + objRecordFolderId);
            
                boolean moveFolder = boxToolkit.moveFolder(objRecordFolderID, bpset.Box_Folder_ID__c,'');
                system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders moveFolder: ' + moveFolder);
				
                ld.Box_Folder_ID__c = objRecordFolderId;

                if(!String.isBlank(bpset.Sub_Folders__c)){
                    string sfolder = bpset.Sub_Folders__c;
                    set <string> subfolders = new set <string>();
                    boolean breakfolder = false;
                    do {
                        string mtype = sfolder.substringBetween('[',']');
                        if (mtype == null) breakfolder = true;
                        if (mtype != null) {
                            subfolders.add(mtype);
                            sfolder = sfolder.remove('['+mtype+']');
                        }
                    }
                    while (!breakfolder);
                    for (string strfolderName : subfolders){
                        if(strfolderName.containsIgnoreCase('>')){
                            String[] arrTest = strfolderName.split('>'); 
                            String childfolder = boxToolkit.createFolder(arrTest[0], objRecordFolderID, ''); 
                            String subchildfolder = boxToolkit.createFolder(arrTest[1], childfolder, ''); 
                            
                        }
                        else{
                            String childfolder = boxToolkit.createFolder(strfolderName, objRecordFolderID, ''); 
                        }
                    }    
                }
                
                return true; 
            }
        }
        return false;
    }
}