@isTest 
public class ScheduleUpdateNumberAttemptedTest 
{
    public static String stringSched = '';
    static testMethod void testMethod1() 
    {
        Lead lead = new Lead(
        LastName = 'Dionisio',
        FirstName = 'Emil',
        RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Individual').getRecordTypeId(),
        Last_Attempted_Contact_Date__c = System.now()-1,
        X_of_Attempts_Done_Today__c = 1,
        Status = 'Attempted Contact'
        );
        Database.insert(lead);
        Test.startTest();

            BatchUpdateNumberAttempted obj = new BatchUpdateNumberAttempted();
            DataBase.executeBatch(obj); 

            ScheduleUpdateNumberAttempted sh1 = new ScheduleUpdateNumberAttempted();
            String sch = '0 0 23 * * ?';
            system.schedule('Test Territory Check', sch, sh1);  
            stringSched = ScheduleUpdateNumberAttempted.ScheduleUpdateNumberAttempted();
            
        Test.stopTest();
    }
}