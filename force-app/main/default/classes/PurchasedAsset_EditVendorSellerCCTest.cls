/*
    @author: Jesfer Baculod - Positive Group
    @history: 08/04/18 - Created
    @description: test class of PurchasedAsset_EditVendorSeller
*/
@isTest
private class PurchasedAsset_EditVendorSellerCCTest{

    private static final string ACC_RT_PSELLER = 'nm: zPrivateSeller';
	private static final string CON_RT_BUSINESS = Label.Contact_Business_RT; //Business
	
	@testsetup static void setup(){
		
		Id rtBAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get(ACC_RT_PSELLER).getRecordTypeId();
		Account acc = new Account(
				LastName = 'test account',
				RecordTypeId = rtBAcc,
				Previous_Employment_Status__c = 'Full-time'
			);
		insert acc;

		ABN__c abn = new ABN__c(
				Name = 'Test Legal Entity ABN',
				Registered_ABN__c = '123456789'
			);
		insert abn;

		Referral_Company__c rc = new Referral_Company__c(
				Name = 'Test Referral Company'
			);
		insert rc;

		Id rtBusinessCon = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_BUSINESS).getRecordTypeId();
		Contact bcon = new Contact(
				RecordTypeId = rtBusinessCon,
				Email = 'test@test.com',
				LastName = 'Test BContact',
				Referral_Company__c = rc.Id
			);
		insert bcon;

        Id rtPLSCaseID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: Consumer - Asset Finance').getRecordTypeId();
        Case cse = new Case(
            RecordTypeId = rtPLSCaseId,
            Status = 'New',
            Stage__c = 'Open',
            Sale_Type__c = 'Dealer',
            Lead_Purpose__c = 'Car Loan',
            Partition__c = 'Positive',
            Channel__c = 'PLS'
        );
        insert cse;

        Id rtPAssetID = Schema.SObjectType.Purchased_Asset__c.getRecordTypeInfosByName().get('Motor Vehicle').getRecordTypeId();
        Purchased_Asset__c passet = new Purchased_Asset__c(
            RecordTypeId = rtPAssetID,
            Case__c = cse.Id
        );
        insert passet;

        Location__c loc = new Location__c(
            Postcode__c = '41235'
        );
        insert loc;
	}

	private static testmethod void testSearches(){

		//Retrieve Case and Purchased Asset to be used for editing vendor/Private Seller details
		Case cse = [Select Id, Sale_Type__c From Case limit 1];
        Purchased_Asset__c passet = [Select Id, Dealer_Sale_Vendor__c, Dealer_Contact_Person__c, Private_Seller__c From Purchased_Asset__c Where Case__c =:cse.Id limit 1];
		list <Referral_Company__c> rclist = [Select Id, Name From Referral_Company__c];
		list <Contact> bconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_BUSINESS];
		list <Account> acclist = [Select Id, Name From Account Where RecordType.name = : ACC_RT_PSELLER];

		PageReference tpref = Page.PurchasedAsset_EditVendorSeller;
		tpref.getParameters().put('id', passet.Id);
		Test.setCurrentPage(tpref);

		ApexPages.StandardController st0 = new ApexPages.StandardController(passet);
		PurchasedAsset_EditVendorSellerCC oevdcc0 = new PurchasedAsset_EditVendorSellerCC(st0);

		passet.Dealer_Sale_Vendor__c = rclist[0].Id;
		passet.Dealer_Contact_Person__c = bconlist[0].Id;
		update passet;

		ApexPages.StandardController st = new ApexPages.StandardController(passet);
		PurchasedAsset_EditVendorSellerCC oevdcc = new PurchasedAsset_EditVendorSellerCC(st);

		Test.startTest();
			oevdcc.pass_DV = 'Test Referral Company'; //search for existing Referral Company
			oevdcc.searchReferralCompany();
			oevdcc.searchMode = 1;
			oevdcc.refreshPageSize();
			oevdcc.pass_s_ID = rclist[0].Id; //a record has been selected on the search results
			oevdcc.assignLookupID();

			oevdcc.pass_DC = 'Test BContact'; //search for existing Business Contact 
			oevdcc.searchBusinessContact();
			oevdcc.searchMode = 2;
			oevdcc.refreshPageSize();
			oevdcc.pass_s_ID = bconlist[0].Id; //a record has been selected on the search results
			oevdcc.assignLookupID();

			oevdcc.pass_DV_id = null;
			oevdcc.searchBusinessContact();

			cse.Sale_Type__c = 'Private Sale';
			update cse;

			ApexPages.StandardController st2 = new ApexPages.StandardController(passet);
			PurchasedAsset_EditVendorSellerCC oevdcc2 = new PurchasedAsset_EditVendorSellerCC(st2);

			oevdcc2.pass_VC = 'Test account'; //search for exisitng Key Person Contact
			oevdcc2.searchPrivateSellerAccount();
			oevdcc2.searchMode = 3;
			oevdcc2.refreshPageSize();
			oevdcc2.pass_s_ID = acclist[0].Id; //a record has been selected on the search results
			oevdcc2.assignLookupID(); 

			passet.Private_Seller__c = acclist[0].Id;
			update passet;

			ApexPages.StandardController st3 = new ApexPages.StandardController(passet);
			PurchasedAsset_EditVendorSellerCC oevdcc3 = new PurchasedAsset_EditVendorSellerCC(st3);			

		Test.stopTest();

		system.assertEquals(passet.Dealer_Sale_Vendor__c,rclist[0].Id); //Verify that Dealer Sale Vendor is same with selected Referral Company
		system.assertEquals(passet.Dealer_Contact_Person__c,bconlist[0].Id); //Verify that Dealer Contact Person is same with selected Business Contact
		system.assertEquals(passet.Private_Seller__c,acclist[0].Id); //Verify that Private Seller is same with selected Individual Contact

	}

	private static testmethod void testSaveCreateOnLookups(){

		Case cse = [Select Id, Sale_Type__c From Case limit 1];
        Purchased_Asset__c passet = [Select Id, Dealer_Sale_Vendor__c, Dealer_Contact_Person__c, Private_Seller__c From Purchased_Asset__c Where Case__c =: cse.Id limit 1];
        list <Location__c> loclist = [Select Id From Location__c];

		ABN__c abn = [Select Id, Name, Registered_ABN__c From ABN__c];

		list <Referral_Company__c> rclist = [Select Id, Name From Referral_Company__c];
		list <Contact> bconlist = [Select Id, Name From Contact Where RecordType.Name = : CON_RT_BUSINESS];
		list <Account> acclist = [Select Id, Name From Account Where RecordType.name = : ACC_RT_PSELLER];

		ApexPages.StandardController st = new ApexPages.StandardController(passet);
		PageReference tpref = Page.PurchasedAsset_EditVendorSeller;
		tpref.getParameters().put('id', passet.Id);
		Test.setCurrentPage(tpref);

		PurchasedAsset_EditVendorSellerCC oevdcc = new PurchasedAsset_EditVendorSellerCC(st);

		Test.startTest();
			
			//Test save Opp errors
			oevdcc.pass.Dealer_Sale_Vendor__c = null;
			oevdcc.pass.Dealer_Contact_Person__c = null;
			oevdcc.savePAsset();

			//Test saving of new Referral Company
			oevdcc.refcomp.Name = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveReferralCompany();
			oevdcc.clearFormFields();
			oevdcc.refcomp.Name = 'Test Ref Comp 2';
			oevdcc.refcomp.Company_Type__c = 'Car Dealer';
			oevdcc.refcomp.Legal_Entity_Name__c = abn.Id;
			oevdcc.retrieveCurrentABN();
			oevdcc.saveReferralCompany();

			//Test saving of new Referral Company
			oevdcc.refcomp.Name = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveReferralCompany();
			oevdcc.clearFormFields();
			oevdcc.refcomp.Name = 'Test Ref Comp 2';
			oevdcc.refcomp.Company_Type__c = 'Car Dealer';
			oevdcc.saveReferralCompany();
			oevdcc.assignExistingReferralCompany();

			//Test saving of new Business Contact
			oevdcc.bcon.LastName = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.saveBusinessContact();
			oevdcc.clearFormFields();
			oevdcc.bcon.LastName = 'Test BContact 2';
			oevdcc.bcon.Email = 'test@testbcon.com';
			oevdcc.saveBusinessContact();

			//Test saving of new Private Seller Contact
			oevdcc.icon.LastName = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.savePrivateSellerAccount();
			oevdcc.clearFormFields();
			oevdcc.icon.LastName = 'Test IContact 2';
			oevdcc.iacc.PersonEmail = 'test@testpseller.com';
            oevdcc.addr.Location__c = loclist[0].Id;
            oevdcc.populateAddresswithLocation();
			oevdcc.savePrivateSellerAccount();

			oevdcc.pass.Vehicle_Colour__c = 'errorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerrorerror';
			oevdcc.savePAsset();
			oevdcc.pass.Vehicle_Colour__c = 'fixed';
			oevdcc.savePAsset();

			ApexPages.StandardController st0 = new ApexPages.StandardController(passet);
			PurchasedAsset_EditVendorSellerCC oevdcc0 = new PurchasedAsset_EditVendorSellerCC(st0);
			oevdcc0.pass.Dealer_Sale_Vendor__c = rclist[0].Id;
			oevdcc0.pass.Dealer_Contact_Person__c = null;
			oevdcc0.savePAsset();

			ApexPages.StandardController st1 = new ApexPages.StandardController(passet);
			PurchasedAsset_EditVendorSellerCC oevdcc1 = new PurchasedAsset_EditVendorSellerCC(st1);
			oevdcc1.pass.Dealer_Sale_Vendor__c = null;
			oevdcc1.pass.Dealer_Contact_Person__c = bconlist[0].Id;
			oevdcc1.savePAsset();

			cse.Sale_Type__c = 'Private Sale';
            update cse;
			ApexPages.StandardController st2 = new ApexPages.StandardController(passet);
			PurchasedAsset_EditVendorSellerCC oevdcc2 = new PurchasedAsset_EditVendorSellerCC(st2);
			oevdcc2.pass.Private_Seller__c = null;
			oevdcc2.savePAsset();

		Test.stopTest();

	}
}