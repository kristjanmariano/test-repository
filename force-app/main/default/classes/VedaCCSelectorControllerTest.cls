/** 
* @FileName: VedaCCSelectorControllerTest
* @Description: test class of VedaCCSelectorController
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 5/16/18 JBACULOD Created
**/ 
@isTest
private class VedaCCSelectorControllerTest {

    @testsetup static void setup(){
        //Create test data for (Case, Application, Role, Account)
        TestDataFactory.Case2FOSetupTemplate();
    }

    static testmethod void testVedaCCSelectorCase(){

        //Retrieve created Test Data
        Case cse = [Select Id, CaseNumber From Case limit 1];
        Application__c app = [Select Id, Case__c From Application__c Where Case__c = : cse.Id];
        list <Account> acclist = [Select Id, Name From Account];
        list <Role__c> rolelist = [Select Id, Account_Name__c, Case__c, Application__c From Role__c];

        Test.starttest();

            //Load VedaCCSelector from Case
            PageReference pref_cse = Page.VedaCCSelector;
            pref_cse.getParameters().put('id',cse.Id);
            Test.setCurrentPage(pref_cse);
            VedaCCSelectorController vcscon = new VedaCCSelectorController();

            //Simulate clicking of Cancel button in Selector page
            vcscon.pageCancel();
            //Simulate clicking of Select button in Selector page w/ Errors
            vcscon.hasErrors = true;
            vcscon.redirectToVedaCC();
            //Simulate clicking of Select button in Selector page w/o Errors (1st person selected by default)
            vcscon.hasErrors = false;
            vcscon.redirectToVedaCC();


        Test.stopTest();

    }

    static testmethod void testVedaCCSelectorApplication(){

        //Retrieve created Test Data
        Case cse = [Select Id, CaseNumber From Case limit 1];
        Application__c app = [Select Id, Case__c From Application__c Where Case__c = : cse.Id];
        list <Account> acclist = [Select Id, Name From Account];
        list <Role__c> rolelist = [Select Id, Account_Name__c, Case__c, Application__c From Role__c];

        Test.starttest();

            //Load VedaCCSelector from Case
            PageReference pref_cse = Page.VedaCCSelector;
            pref_cse.getParameters().put('id',app.Id);
            Test.setCurrentPage(pref_cse);
            VedaCCSelectorController vcscon = new VedaCCSelectorController();

            //Simulate clicking of Select button in Selector page w/o Errors (1st person selected by default)
            vcscon.redirectToVedaCC();


        Test.stopTest();

    }

    static testMethod void testMisc(){

        //Create test data for Case
        Id curCaseRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_N_CONS_AF).getRecordTypeId();
        Case cse = TestDataFactory.createGenericCase(curCaseRT, 'New', 'Open');
        insert cse;

        Test.starttest();

            //Load VedaCCSelector from Case w/o Role
            PageReference pref_cse = Page.VedaCCSelector;
            pref_cse.getParameters().put('id',cse.Id);
            Test.setCurrentPage(pref_cse);
            VedaCCSelectorController vcscon = new VedaCCSelectorController();

            //Create test data for Person Account
            Id curPAccRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.PACC_RT_I).getRecordTypeId();
            Account acc = TestDataFactory.createGenericPersonAccount(curPAccRT,'testf','testl');
            insert acc;

            //Create test data for Role
            Id curRoleRT = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get(CommonConstants.ROLE_RT_APP_INDI).getRecordTypeId();
            Role__c role = TestDataFactory.createGenericRole(curRoleRT, acc.Id, cse.Id, null);
            insert role;

            //Load VedaCCSelector from Case w/ 1 Role
            PageReference pref_cse2 = Page.VedaCCSelector;
            pref_cse2.getParameters().put('id',cse.Id);
            Test.setCurrentPage(pref_cse2);
            VedaCCSelectorController vcscon2 = new VedaCCSelectorController();

        Test.stopTest();

    }

}