/*
	@Description: class for calculating SLA Age of With Introducer (SLA-2) on Scenario
	@Author: Jesfer Baculod - Positive Group
	@History:
		- 11/14/2017 - Created
*/
public class Scenario_UpdateSLAWarning_SLA3 {
	
	@InvocableMethod(label='SLA Next Warning Time - With Lender' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void scenarioSLAWarningTimeWL (list <ID> scenIDs) {

		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Scenarios to update
		list <Complex_Scenario__c> scenlist = [Select Id, 
								SLA_Time_3_Start__c, SLA_Warning_Time_3__c, SLA_Started_Count_3__c, SLA_Completed_Count_3__c, SLA_Active_3__c, SLA_Time_3_mm__c
								From Complex_Scenario__c Where Id in : scenIds];
        
        Datetime warningdate; 
		for (Complex_Scenario__c scen : scenlist){
			warningdate = scen.SLA_Warning_Time_3__c;
			scen.SLA_Warning_Time_3__c = null;
			scen.SLA_Active_3__c = null;
		}
		update scenlist; //force update to retrigger SLA Warning
		system.debug('@@warningdate:'+warningdate);

		for (Complex_Scenario__c scen : scenlist){
            scen.SLA_Active_3__c = 'Yes';
            if (scen.SLA_Started_Count_3__c != scen.SLA_Completed_Count_3__c){ 
                scen.SLA_Warning_Time_3__c = BusinessHours.add(bh.Id, warningdate, 1800000); //Set succeeding warning of current SLA (add 30 minutes)
                DateTime warningtimeAR = scen.SLA_Warning_Time_3__c;
				scen.SLA_Warning_Time_3__c = Datetime.newInstance(warningtimeAR.year(), warningtimeAR.month(), warningtimeAR.day(), warningtimeAR.hour(), warningtimeAR.minute(), 0);
                scen.SLA_Time_3_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, scen.SLA_Time_3_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
                if (scen.SLA_Time_3_mm__c == 119) scen.SLA_Time_3_mm__c = 120; //workaround for business hours difference of SLA
				if (scen.SLA_Time_3_mm__c == 149) scen.SLA_Time_3_mm__c = 150; //workaround for business hours difference of SLA
				if (scen.SLA_Time_3_mm__c == 179) scen.SLA_Time_3_mm__c = 180; //workaround for business hours difference of SLA
				if (scen.SLA_Time_3_mm__c == 209) scen.SLA_Time_3_mm__c = 210; //workaround for business hours difference of SLA
				if (scen.SLA_Time_3_mm__c == 239) scen.SLA_Time_3_mm__c = 240; //workaround for business hours difference of SLA
                system.debug('@@slaWarningB');
            }
		}

		update scenlist;

	}


}