/** 
* @FileName: AttachmentHandler
* @Description: Trigger Handler for the Attachment SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 9/03/18 JBACULOD Created Class
* 1.1 9/12/18 JBACULOD Updated FileName of PDF being generated
* 1.2 1/09/19 JBACULOD Added creation of Box file from Case Attachments (NODIFI - Approval)
* 1.3 1/10/19 JBACULOD Added creation of Box file from Case Attachments (NODIFI - Settlement)
* 1.4 19/12/2019 RDAVID SFBAU-39/SFBAU-111 - Equifax Report Attachment to Box. Branch: extra/task22445169-Equifax
**/ 
public class AttachmentHandler implements ITrigger{

    private map <Id, Attachment> supportRequestAttMap = new map <Id, Attachment>();
    private map <Id, Attachment> caseAttMap = new map <Id, Attachment>();
    private map <Id, Id> srCaseMap = new map <id, Id>();
    private map <Id, Attachment> apiTransferAttachmentMap = new map <Id, Attachment>();
    private map <Id, API_Transfer__c> apiTransferMap = new map <id, API_Transfer__c>();

    /** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
    }

    public void bulkAfter(){

        if (Trigger.isInsert){
            if (TriggerFactory.trigSet.Enable_Attachment_Create_File_in_Box__c){
                set <Id> attParentIds = new set <Id>();
                set <Id> apiTransferIds = new set <Id>();
                for (Attachment att : [Select Id, ContentType, Name, Body, ParentId From Attachment Where Id in : trigger.new]){
                    if (Id.valueof(att.ParentId).getSObjectType() == Schema.Support_Request__c.SobjectType){ //ParentId is from Support Request
                        supportRequestAttMap.put(att.Id, att);
                        attParentIds.add(att.ParentId);
                    }
                    else if (Id.valueof(att.ParentId).getSObjectType() == Schema.Case.SobjectType){ //ParentId is from Case
                        caseAttMap.put(att.Id, att);
                        //attParentIds.add(att.ParentId);
                        srCaseMap.put(att.ParentId, att.ParentId);
                    }
                    //RDAVID SFBAU-39/SFBAU-111
                    else if (Id.valueof(att.ParentId).getSObjectType() == Schema.API_Transfer__c.SobjectType){
                        System.debug('>>>>>>> Equifax to Box.');
                        apiTransferAttachmentMap.put(att.Id,att);
                        apiTransferIds.add(Id.valueof(att.ParentId));
                    }
                }
                system.debug('@@caseAttMap:'+caseAttMap);
                system.debug('@@supportRequestAttMap:'+supportRequestAttMap);
                if (supportRequestAttMap.size() > 0){
                    for (Support_Request__c sr : [Select Id, Name, Case_Number__c, Channel__c From Support_Request__c Where Id in : attParentIds AND Case_Number__c != null]){
                        srCaseMap.put(sr.Id, sr.Case_Number__c); //Retrieve Parent Case Id of Support Requests
                        //srNameMap.put(sr.Id, sr.Name); //Support Request Name (will be used for naming Box File)
                    }
                }

                if(apiTransferIds.size() > 0){
                    for(API_Transfer__c apiTrans : [SELECT Id, Case__c, EQ_Role__c FROM API_Transfer__c WHERE RecordType.Name = 'Equifax' AND Case__c != NULL AND Id IN: apiTransferIds]){
                        apiTransferMap.put(apiTrans.Id,apiTrans);
                    }
                }
            }
        }

    }

    public void beforeInsert(SObject so){
    }

    public void beforeUpdate(SObject oldSo, SObject so){
    }

    public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
        Attachment att = (Attachment) so;
        if (TriggerFactory.trigSet.Enable_Attachment_Create_File_in_Box__c){
            if (supportRequestAttMap.size() > 0){
                AttachmentGateway attgate = new AttachmentGateway(supportRequestAttMap.get(att.Id), srCaseMap.get(att.ParentId)); //, srNameMap.get(att.ParentId));
                ID jobID = System.enqueueJob(attgate);
            }
            system.debug('@@caseAttMap:'+caseAttMap);
            if (caseAttMap.size() > 0){
                //system.debug('@@caseAttMap.get(att.Id).Name:'+caseAttMap.get(att.Id).Name);
                if (String.valueof(caseAttMap.get(att.Id).Name).startsWith('NODIFI - Approval') || String.valueof(caseAttMap.get(att.Id).Name).startsWith('NODIFI - Settle') ){ //Introducer, TnF
                    AttachmentGateway attgate = new AttachmentGateway(caseAttMap.get(att.Id), srCaseMap.get(att.ParentId)); //, srNameMap.get(att.ParentId));
                    ID jobID = System.enqueueJob(attgate);
                }
                
            }
            system.debug('@@apiTransferMap:'+apiTransferMap.keySet());
            system.debug('@@apiTransferAttachmentMap:'+apiTransferAttachmentMap.keySet());
            if (apiTransferMap.size() > 0 && apiTransferAttachmentMap.size() > 0){
                system.debug('@@line:100');
                if (String.valueof(apiTransferAttachmentMap.get(att.Id).Name).startsWith('EA-') || String.valueof(apiTransferAttachmentMap.get(att.Id).Name).containsIgnoreCase('Equifax Apply') || String.valueof(apiTransferAttachmentMap.get(att.Id).Name).containsIgnoreCase('Previous Enquiry')){
                    system.debug('@@line:102');
                    AttachmentGateway attgate = new AttachmentGateway(apiTransferAttachmentMap.get(att.Id), apiTransferMap.get(att.ParentId).Case__c);
                    ID jobID = System.enqueueJob(attgate);
                }
            }
        }
	}
	
	public void afterUpdate(SObject oldSo, SObject so){        
	}
	
	public void afterDelete(SObject so){
	}
    
    /** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
        // System.debug('AttachmentGateway.apiTransSetToUpd>>>>>>>>>>> '+AttachmentGateway.apiTransSetToUpd);

    }
}