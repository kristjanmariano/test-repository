/** 
* @FileName: TriggerFactory
* @Description: Used to instantiate and execute Trigger Handlers associated with sObjects.
* @Source: http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/15 RDAVID Created Class
* 1.1 2/16 JBACULOD Added Trigger Settings for enabling/disabling trigger on its object
* 1.2 9/21/18 JBACULOD Fixed Recursion issue
* 1.3 7/01/2020 JBACULOD Added ContentDocumentLink Handler, fromNewPartnerPage boolean (SFBAU-161)
**/ 

public without sharing class TriggerFactory{

	public static Trigger_Settings1__c trigSet = Trigger_Settings1__c.getInstance(UserInfo.getUserId());
	public static Boolean fromLeadPath = false;
	public static Boolean fromNewPartnerPage = false;
	public static map <Id, boolean> isProcessedMap = new map <Id, boolean>();
    public static sObjectType curSOType;
	public static String accConTriggerOrderExecution = '';
	/** 
	* @FileName: createHandler
	* @Description: Public static method to create and execute a trigger handler
	* @Arguments:	Schema.sObjectType soType - Object type to process (SObject.sObjectType)
	* @Throws a TriggerException if no handler has been coded.
	**/ 

	public static void createHandler(Schema.sObjectType soType){
		// Get a handler appropriate to the object being processed
		ITrigger handler = getHandler(soType);
        if (soType != curSoType) curSOType = soType;
		
		// Make sure we have a handler registered, new handlers must be registered in the getHandler method.
		if (handler == null){
			throw new TriggerException('No Trigger Handler registered for Object Type: ' + soType);
		}
        // Execute the handler to fulfil the trigger
		execute(handler);
	}

	/** 
	* @FileName: execute
	* @Description: private static method to control the execution of the handler
	* @Arguments:	ITrigger handler - A Trigger Handler to execute
	**/ 

	private static void execute(ITrigger handler){
		// Before Trigger
		// accConTriggerOrderExecution = ContactGateway.getAccConTriggerOrderExecutionStr(curSOType,accConTriggerOrderExecution); //RDAVID 26/03: Temporary Code to determine which runs first (Account or Contact Trigger)

		if (Trigger.isBefore){

			//Identify if Bulk will be processed or not
			boolean processBulkBefore = false;
			if (Trigger.isUpdate){ //Update/Delete event
			system.debug('@@isProcessedMap.size():'+isProcessedMap.size());
				if (isProcessedMap.size() >= 100) isProcessedMap = new map <Id, boolean>(); //Reset Record Checker if a batch (default: 200 records) has been processed
				for (ID sobjID : Trigger.newmap.keySet()){ 
					if (!isProcessedMap.containskey(sobjID)) isProcessedMap.put(sobjID, false);
				}
				for (ID sobjID : trigger.newMap.keySet()){
					if (!isProcessedMap.get(sobjID)){ 
						processBulkBefore = true;
						break;
					}
				}
			}
			else processBulkBefore = true; //Insert, Delete event

			if (processBulkBefore){
				// Call the bulk before to handle any caching of data and enable bulkification
				handler.bulkBefore();
			}
			
			// Iterate through the records to be deleted passing them to the handler.
			if (Trigger.isDelete){
				for (SObject so : Trigger.old){
					handler.beforeDelete(so);
				}
			}
			// Iterate through the records to be inserted passing them to the handler.
			else if (Trigger.isInsert){
				for (SObject so : Trigger.new){
					handler.beforeInsert(so);
				}
			}
			// Iterate through the records to be updated passing them to the handler.
			else if (Trigger.isUpdate){
				for (SObject so : Trigger.old){
					if (!isProcessedMap.get(so.Id)){
						handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
					}
				}
			}
		}
		else{

			//Identify if Bulk will be processed or not
			boolean processBulkAfter = false;
			if (Trigger.isUpdate){ 
				for (ID sobjID : trigger.newMap.keySet()){
					if (!isProcessedMap.get(sobjID)){ 
						processBulkAfter = true;
						break;
					}
				}
			}
			else processBulkAfter = true; //Insert, Delete event

			if (processBulkAfter){
				// Call the bulk after to handle any caching of data and enable bulkification
				handler.bulkAfter();
			}
			
			// Iterate through the records deleted passing them to the handler.
			if (Trigger.isDelete){
				for (SObject so : Trigger.old){
					handler.afterDelete(so);
				}
			}
			// Iterate through the records inserted passing them to the handler.
			else if (Trigger.isInsert){
				for (SObject so : Trigger.new){
					handler.afterInsert(so);
				}
			}
			// Iterate through the records updated passing them to the handler.
			else if (Trigger.isUpdate){
				for (SObject so : Trigger.old){
					if (!isProcessedMap.get(so.Id)){
						handler.afterUpdate(so, Trigger.newMap.get(so.Id));
						isProcessedMap.put(so.Id, true); 
					}
				}
			}
		}

		system.debug('Object = '+ curSOType + ' @@isProcessedMap ' +isProcessedMap);
		// Perform any post processing
		handler.andFinally();
        
        system.debug('@@LIMITS.getQueries()'+Limits.getQueries() + ' / ' + Limits.getLimitQueries());
        system.debug('@@LIMITS.getCPUTime()'+Limits.getCPUTime() + ' / ' + Limits.getLimitCPUTime());
        system.debug('@@LIMITS.getDMLStatements()'+Limits.getDMLStatements() + ' / ' + Limits.getLimitDMLStatements());
		system.debug('@@LIMITS.getQueueableJobs()'+ Limits.getQueueableJobs() + ' / ' +  Limits.getLimitQueueableJobs());
	}

	/** 
	* @FileName: getHandler
	* @Description: private static method to get the appropriate handler for the object type.
	* @Arguments:	Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
	* @Returns:		ITrigger - A trigger handler if one exists or null.
	**/ 

	private static ITrigger getHandler(Schema.sObjectType soType){
		if (soType == Case.sObjectType){
			return new CaseHandler();
		}
		if (soType == Income1__c.sObjectType){
			return new IncomeHandler();
		}
		if (soType == Expense1__c.sObjectType){
			return new ExpenseHandler();
		}
		if (soType == Asset1__c.sObjectType){
			return new AssetHandler();
		}
		if (soType == Liability1__c.sObjectType){
			return new LiabilityHandler();
		}
		if (soType == Role__c.sObjectType){
			return new RoleHandler();
		}
		if (soType == Role_Address__c.sObjectType){
			return new RoleAddressHandler();
		}
        if (soType == Application__c.sObjectType){
			return new ApplicationHandler();
		}
		if (soType == FO_Share__c.sObjectType){
			return new FOShareHandler();
		}
        if (soType == Account.sObjectType){
			return new AccountHandler();
		}
		if (soType == Address__c.sObjectType){
			return new AddressHandler();
		}
		if (soType == Relationship__c.sObjectType){
			return new RelationshipHandler();
		}
		if (soType == Task.sObjectType){
			return new TaskHandler();
		}
		if (soType == Frontend_Submission__c.sObjectType){
			return new FrontEndSubmissionHandler();
		}
		if (soType == CommissionCT__c.sObjectType){
			return new CommissionCTHandler();
		}
		if (soType == Commission_Line_Items__c.sObjectType){
			return new CommissionLineHandler();
		}
		if (soType == Attachment.sObjectType){
			return new AttachmentHandler();
		}
		if (soType == Support_Request__c.sObjectType){
			return new SupportRequestHandler();
		}
		if (soType == bizible2__Bizible_Touchpoint__c.sObjectType){
			return new BizibleTouchpointHandler();
		}
		if (soType == AgentWork.sObjectType){
			return new AgentWorkHandler();
		}
		if (soType == Contact.sObjectType){
			return new ContactHandler();
		}
		if (soType == CaseComment.sObjectType){
			return new CaseCommentHandler();
		}
        if (soType == Referral_Company__c.sObjectType){
			return new ReferralCompanyHandler();
		}
		if (soType == Partner__c.sObjectType){
			return new PartnerHandler();
		}
		if (soType == Case_Touch_Point__c.sObjectType){
			return new CaseTouchpointHandler();
		}
		if (soType == Sector__c.sObjectType){
			return new SectorHandler();
		}
		if (soType == NVMStatsSF__NVM_Call_Summary__c.sObjectType){
			return new NVMCallSummaryHandler();
		}
		if (soType == ContentDocumentLink.sObjectType){
			return new ContentDocumentLinkHandler();
		}
		
		return null;
	}
}