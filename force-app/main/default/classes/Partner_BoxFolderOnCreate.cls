/*
	@Description: class containing the invocable method for create Box Folders in Partner__c 
    - (invoked via Partner - Create Box Folder Process Builder (currently for Partner RTs)):
            - Lender Division
            - Lender Parent
            - Finance Aggregator
            - Supplier
	@Author: Rexie David - Positive Group
	@History:
		-	2/04/19 RDAVID Created extra/tasks24059615-PartnerObjectBox
        -   28/05/19 - JBACULOD - removed reference of Channel field
        -   4/09/19 - RDAVID - extra/task25870370-UpdateBoxFolderCreation-PartnerObject - Added logic to make Box folder creation automation work for other Partner RTs provided in this sheet. https://docs.google.com/spreadsheets/d/1oqzQDwjsEUonpRGsdyhKAzgkqp3RVRCCKWN9K4Gtccw/edit?ts=5d6ee2e7#gid=0
        -   7/10/19 - JBACULOD extra/tasks26291984 - Added logic for linking and renaming created Box Folder from Nodifi Partner Lead to Partner
*/
public class Partner_BoxFolderOnCreate implements Queueable, Database.AllowsCallouts {

	private static list <Box_Partition_Settings__mdt> boxpset {get;set;}
    public static string test_objRecordFolderID {get;set;}
    private static box.Toolkit boxToolkit = new box.Toolkit();
    private list <ID> PBoxpartIDs {get;set;}
    private static list <box__FRUP__c> boxFRUPlist = new list <box__FRUP__c>(); //for linking existing Box FOlder to created Partner
    private static list <box__Folder_Meta__c> boxFMetalist = new list <box__Folder_Meta__c>(); //for linking existing Box FOlder to created Partner
	private static String metaObject = 'Partner';
    public Partner_BoxFolderOnCreate(list <ID> partIDs){
        PBoxpartIDs = partIDs;
    }

    @InvocableMethod(label='Create Box Folder for Partner' description='creates a Box Folder for Partner__c when Partner__c got created')
	public static void partnerBoxFolderCreate (list <ID> partIDs) {
        if (TriggerFactory.trigset.Enable_Partner_Box_Auto_Create__c){
            Partner_BoxFolderOnCreate cbox = new Partner_BoxFolderOnCreate(partIDs);
            ID jobID = System.enqueueJob(cbox); //Need to be asynchronous due to post DMLs on Partner__c
        }

    }

    public void execute(QueueableContext context){
        system.debug('@@PBoxpartIDs:'+PBoxpartIDs);
        createBoxFolder(this.PBoxpartIDs);
    }

    public void createBoxFolder(list <ID> partIDs){
        //Retrieve Box Partition Settings
        boxpset = [Select Id, DeveloperName, Partition__c, Box_Folder_ID__c, Sub_Folders__c, RecordType_Name__c From Box_Partition_Settings__mdt WHERE Object__c =:metaObject];

        //Retrieve Partner__c
        list <Partner__c> partlist = [Select Id, Name, Box_Folder_ID__c, RecordType.Name, Partition__c From Partner__c Where Id in : partIDs]; //JBACU 28/05/19

        map <string,Partner__c> updatePartnerMap = new map <string,Partner__c>();
        //Iterate on Partner__c w/ Box folders to be created
        for (Partner__c part : partlist){
            //Create Partner__c Box Folder
            if (createmoveParentFolders(part, boxpset)){
                system.debug('@@0');
                updatePartnerMap.put(part.Id, part);
            }
        }
        
        system.debug('@@updatePartnerMap:'+updatePartnerMap);
        if (updatePartnerMap.size() > 0){
            update updatePartnerMap.values();
        }

        //Link Box Folder to created Partner if Folder was originally created from a Nodifi Partner Lead
        if (boxFMetalist.size() > 0 && boxFRUPlist.size() > 0){
            system.debug('@@boxFMetalist:'+boxFMetalist);
            system.debug('@@boxFRUPlist:'+boxFRUPlist);
            insert boxFMetalist;
            insert boxFRUPlist;
        }

        // ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
        boxToolkit.commitChanges();
    }

    private static boolean createmoveParentFolders(Partner__c part, list <Box_Partition_Settings__mdt> boxpset){
        for (Box_Partition_Settings__mdt bpset : boxpset){
            system.debug('@@bpSet DEVELOPERNAME:'+bpSet.DEVELOPERNAME);
            //Check Partner Recordtype if it exist in the Custom Metadata
            if (bpset.RecordType_Name__c.containsIgnoreCase(part.RecordType.Name)){
                if (String.isBlank(part.Box_Folder_ID__c)){
                    system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders MSG: ' + part.RecordType.Name + ' is in ' + bpset.RecordType_Name__c);
                    //Create Box folder
                    // string objRecordFolderId = boxToolkit.createObjectFolderForRecordId(part.Id); 
                    string objRecordFolderId = boxToolkit.createFolderForRecordId(part.Id, null, true); 
                    system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders most recent error: ' + boxToolkit.mostRecentError);
                    
                    if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
                    system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders objRecordFolderId: ' + objRecordFolderId);
                
                    boolean moveFolder = boxToolkit.moveFolder(objRecordFolderID, bpset.Box_Folder_ID__c,'');
                    system.debug('@@Partner_BoxFolderOnCreate createmoveParentFolders moveFolder: ' + moveFolder);
                    
                    part.Box_Folder_ID__c = objRecordFolderId;

                    if(!String.isBlank(bpset.Sub_Folders__c)){
                        string sfolder = bpset.Sub_Folders__c;
                        set <string> subfolders = new set <string>();
                        boolean breakfolder = false;
                        do {
                            string mtype = sfolder.substringBetween('[',']');
                            if (mtype == null) breakfolder = true;
                            if (mtype != null) {
                                subfolders.add(mtype);
                                sfolder = sfolder.remove('['+mtype+']');
                            }
                        }
                        while (!breakfolder);
                        for (string strfolderName : subfolders){
                            if(strfolderName.containsIgnoreCase('>')){
                                String[] arrTest = strfolderName.split('>'); 
                                String childfolder = boxToolkit.createFolder(arrTest[0], objRecordFolderID, ''); 
                                String subchildfolder = boxToolkit.createFolder(arrTest[1], childfolder, ''); 
                            }
                            else{
                                String childfolder = boxToolkit.createFolder(strfolderName, objRecordFolderID, ''); 
                            }
                        }    
                    }
                }
                else{ //Box Folder is from Nodifi Partner Lead. Create link to Partner
                    
                    //Link Box Folder to Parent record
                    box__Folder_Meta__c bFM = new box__Folder_Meta__c(
                        box__Folder_Name__c = part.Name,
                        box__Folder_Id__c = part.Box_Folder_ID__c 
                    );
                    boxFMetalist.add(bFM);

                    box__FRUP__c bFrup = new box__FRUP__c(
                        box__Record_ID__c = part.Id, 
                        box__Folder_ID__c = part.Box_Folder_ID__c 
                    );
                    boxFRUPlist.add(bFrup);

                    //Rename Nodifi Partner Lead Box Folder to Partner's Name
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint('https://api.box.com/2.0/folders/' + part.Box_Folder_ID__c);
                    req.setMethod('PUT');
                    req.setBody('{"name":"' + part.Name + '"}');
                    boxToolkit.sendRequest(req);          
                }                
                return true; 
            }
        }
        return false;
    }
}