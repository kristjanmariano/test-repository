/** 
* @FileName: CommissionLineHandler
* @Description: Trigger Handler for the Commission_Line_Items__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/8/18 RDAVID Created Class
**/ 

public without sharing class CommissionLineHandler implements ITrigger {	
	Map<ID,Schema.RecordTypeInfo> clRTMap = Commission_Line_Items__c.sObjectType.getDescribe().getRecordTypeInfosById();
	// Constructor
	public CommissionLineHandler(){

	}

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
	
	}
	
	public void bulkAfter(){
		
	}
		
	public void beforeInsert(SObject so){
		Commission_Line_Items__c newCommLine = (Commission_Line_Items__c)so;

		if(TriggerFactory.trigset.Enable_CommissionLine_GetCommission__c){
			if(clRTMap.get(newCommLine.recordTypeId).getName() == 'Asset Finance - Finance Commission'){
				newCommLine.h_CommissionCT_Finance__c = newCommLine.Commission_Parent__c;
			}
			else if(clRTMap.get(newCommLine.recordTypeId).getName() == 'Asset Finance - Insurance Commission'){
				newCommLine.h_CommissionCT_Insurance__c = newCommLine.Commission_Parent__c;
			} 
		}
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
		
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){
		
	}
	
	public void afterUpdate(SObject oldSo, SObject so){
		
	}
	
	public void afterDelete(SObject so){
	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){

	}
}