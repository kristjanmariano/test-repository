/********* Class that performs the integration operation ***********/
/********* Execute objects in the order defined in XMLConfig **********/

global class LKIntegration {
    public static String clientContactId;
    public static Map<String,BrokerDetails__c> mapNameToId = new Map<String,BrokerDetails__c>();
    public static CustomStringBuilder cStringBuilder = new CustomStringBuilder();//To write logs...
    public static ApiDetails__c objLKCst = ApiDetails__c.getValues(LKConstant.CUSTOM_SETTING_RESOURCE);
    public static Map<String,Object> mapGlobal = new Map<String,Object>(); //to save such values which we want to use in application
    public static Map<String,String> coBorrIdMap = new Map<String,String>();
    public static String brokerId;
    webservice static String requestApi(String appId) {
        try {
            cStringBuilder.add('\n----------------------- Execution started -------------------------\n');
            cStringBuilder.add('Parameters received:- Application Id = '+appId+'\n');
            mapNameToId = BrokerDetails__c.getAll();
            mapGlobal.put(LKConstant.SF_APPLICATION_ID ,appId);
            Dom.Document doc = XMLMappingAdapter.getDomDocument(LKConstant.CONFIG_FILE);
            cStringBuilder.add('Reading from Static Resource := '+LKConstant.CONFIG_FILE);
            Dom.XmlNode rnode = doc.getRootElement();
            List<Dom.XmlNode> cNodes = rnode.getChildElements();
            for(Dom.XmlNode node : cNodes) {
                cStringBuilder.add('\n\nNode => '+node.getText());
                String classname = node.getAttribute('class',null);
                String where_id = node.getAttribute('where_id',null);
                String sendAppId = node.getAttribute('sendAppId',null);
                String saveString =node.getAttribute('save',null); 
                String toString =node.getAttribute('to',null);
                String ofString =node.getAttribute('of',null);  
                String strSaveGlobally = node.getAttribute('saveGlobally',null);
                String asString = node.getAttribute('as',null);
                //String appIdFieldKey = node.getAttribute('clientAppIdFieldName',null);
                String sendToLK = node.getAttribute('sendToLoankit',null);
                String strLink = node.getAttribute('link',null); // link refers to the applicant to which this record is linked.
                String saveIds = node.getAttribute('saveIds',null); //If saveIds = true (i.e in case of CoBorrower), we save sfId and loankit id in a map.
                String queryObjString = node.getAttribute('queryObject',null); // this is used in query, eg:- Select ... from Account where <queryObject> = '005f00006ASXXM';
                String queryFilterString = node.getAttribute('queryFilter',null); // its value can be => "Asset__c != null", this is used in query, eg:- Select ... from Account where Asset__c = id and Asset__c != null;
                List<String> saveStringList = new List<String>();
                List<String> toStringList = new List<String>();
                List<String> ofStringList = new List<String>();
                List<String> saveGloballyList = new List<String>();
                List<String> asStringList= new List<String>();
                if(saveString !=null){
                saveStringList  = getListFromString(saveString );
                }
                
                if(toString!=null){
                toStringList = getListFromString(toString);
                }
                
                if(ofString !=null){
                ofStringList = getListFromString(ofString);
                }
                if(strSaveGlobally !=null){
                saveGloballyList = getListFromString(strSaveGlobally);
                }
                if(asString !=null){
                asStringList = getListFromString(asString);
                }
                String obNodeName = node.getText();
                NodeProcessor np = ObjectFactory.getInstance(className);
                np.setNodeName(obNodeName);
                np.setQueryObjectName(queryObjString);
                np.setQueryFilterString(queryFilterString);
                String recordId;
                if(where_id != null) {
                    Object o = mapGlobal.get(where_id);
                    if(o instanceOf List<LKRecord>) {
                        List<LKRecord> lstCoBorr = (List<LKRecord>)mapGlobal.get(where_id);
                        for(LKRecord coBorr : lstCoBorr) {
                            if(coBorr.sfObjectId != null) {
                                cStringBuilder.add('Record Id = '+coBorr.sfObjectId);
                                np.setRecordId(coBorr.sfObjectId);
                                np.process(); 
                            }
                        }
                    }else if(o instanceOf String) {
                        np.setRecordId(String.valueOf(o));
                        cStringBuilder.add('Record Id = '+String.valueOf(o));
                        np.process();
                    }
                }
                
                if(sendToLK != null && sendToLK.equalsIgnoreCase('false')) {
                    continue;
                }
                List<LKRecord> lstLKRecord = XMLMappingAdapter.lkRecordsMap.get(obNodeName);
                if(lstLKRecord == null) {
                    continue;
                }
                for(Integer k=0; k<lstLKRecord.size(); k++) {
                    LKRecord rec = lstLKRecord.get(k);
                    Boolean isUpdate = false;
                    String clientRecId = '';
                    if(rec.checkForUpdate) {
                        for(Integer j=0; j<k; j++) {
                            LKRecord objLkRecInner = lstLKRecord.get(j);
                            if(rec.sfObjectId!=null && objLkRecInner.sfObjectId!=null && rec.sfObjectId.equalsIgnoreCase(objLkRecInner.sfObjectId)) {
                                isUpdate = true;
                                clientRecId = objLkRecInner.lkObjectId;
                            }
                        }
                    }
                    if(!rec.isProcessed && rec.getMappingName().equals(obNodeName)) {
                        putCommonData(rec.lkvaluesMap);
                        if(sendAppId != null && sendAppId.equalsIgnoreCase('true')) {
                            putAppId(rec.lkvaluesMap,String.valueOf(mapGlobal.get(LKConstant.APPLICATION_ID)));
                        }
                        
                        XMLMapping mappObj = rec.getMappingObject();
                        HttpResponse resp;
                        if((clientRecId == null || clientRecId.length()==0) && mappObj.clientFieldName!=null) {
                            clientRecId = String.valueOf(rec.lkvaluesMap.get(mappObj.clientFieldName));
                        }
                        for(String remValue:mappObj.removeElementList){                       
                            rec.lkvaluesMap.remove(remValue);
                        }
                        String jsonString = getJsonString(rec.lkValuesMap);
                        cStringBuilder.add('JSON Body ==> '+jsonString);
                        System.debug('JSON Body ==> '+jsonString);
                        cStringBuilder.add('Loankit Id:- '+clientRecId);
                        if((clientRecId != null && clientRecId.length() > 0) || isUpdate) {
                            cStringBuilder.add('Update ? :- '+'true');
                            String url = mappObj.getEndPointUrl(true,clientRecId);
                            String requestMethod = mappObj.getReqMethod(true);
                            cStringBuilder.add('URL := '+url);
                            cStringBuilder.add('METHOD := '+requestMethod);
                            resp = sendRequest(jsonString,url,requestMethod);
                        }else {
                            cStringBuilder.add('Update ? :- '+'false');
                            String url = mappObj.getEndPointUrl(false,null);
                            if(rec.applicantRefId != null) {
                                url = url+rec.applicantRefId+'?serializer=JSON';
                            }
                            
                            cStringBuilder.add('URL 111111111111111:= '+url);
                            cStringBuilder.add('strlinkkkkkkk '+strLink);
                            if(rec.applicantLKLink!= null){
                                url = url+rec.applicantLKLink+'?serializer=JSON';
                            }else {
                                if(strLink != null && strLink.length() > 0) {
                                    if(strLink.equalsIgnoreCase('coBorrower')) {
                                        cStringBuilder.add('CoBorrower Id Map == '+coBorrIdMap);
                                        cStringBuilder.add('rec.sfObjectId');
                                        url = url+coBorrIdMap.get(rec.sfObjectId)+'?serializer=JSON';
                                    }else {
                                        url = url+mapGlobal.get(strLink)+'?serializer=JSON';
                                    }
                                }
                            }
                            String method = mappObj.getReqMethod(false);
                            cStringBuilder.add('URL := '+url);
                            cStringBuilder.add('METHOD := '+method);
                            String tempUrl = url.toLowerCase();
                            if(tempUrl.contains('json')) {
                                resp = sendRequest(jsonString, url, method);
                            }
                        }
                        if(resp!=null) {
                            String respString = resp.getBody();
                            cStringBuilder.add('Response Received = '+respString);
                            Map<String,String> mapResponse = convertToMap(respString);
                            if(mapResponse.containsKey('error')) {
                                System.debug(cStringBuilder.getStringValue());
                                return 'An error occured.\nPlease contact your system administrator.';
                            }
                            /*if(mapResponse.get(LKConstant.APPLICATION_ID) != null && mapResponse.get(LKConstant.APPLICATION_ID) == '') {
                               mapResponse.put(LKConstant.APPLICATION_ID,String.valueOf(mapGlobal.get(appIdFieldKey)));
                            }*/ 
                            for(Integer i=0; i<asStringList.size(); i++) {
                                mapGlobal.put(asStringList.get(i),mapResponse.get(saveGloballyList.get(i)));
                            }
                            rec.responseMap = mapResponse;
                            rec.lkObjectId = mapResponse.get(getListfromString(mappObj.getResponseKey()).get(0));
                            if(saveIds != null && saveIds.equalsIgnoreCase('true')) {
                                coBorrIdMap.put(rec.sfObjectId,rec.lkObjectId);
                            }
                            for(Integer i=0;i<ofStringList.size();i++){    
                                List<LKRecord> lkRecList = XMLMappingAdapter.lkRecordsMap.get(ofStringList.get(i));
                                for(LKRecord lkRec : lkRecList) {
                                    lkRec.sObjectRef.put(toStringList.get(i),mapResponse.get(saveStringList.get(i)));
                                }            
                            }
                            rec.isProcessed = true;
                        }else
                            cStringBuilder.add('Response Received = '+resp+' null');
                    }
                }
                cStringBuilder.add('CoBorrower Id Map => '+coBorrIdMap);
                
            }
            updateToSalesforce();
            System.debug(cStringBuilder.getStringValue());
            return 'Your application has been processed successfully';
        }catch(Exception e) {
            cStringBuilder.add('\n\nException Occured');
            cStringBuilder.add(e.getTypeName());            
            cStringBuilder.add(e.getMessage());
            cStringBuilder.add(e.getStackTraceString());
            System.debug(cStringBuilder.getStringValue());
            return 'An error occured.\nPlease contact your system administrator!';
        }
    }
    
    public static HttpResponse sendRequest(String jsonBody, String url, String methodType) {
        if(jsonBody != null && url != null && methodType!=null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setHeader('Content-Type','application/json');
            req.setHeader('Content-length', '0');       
            req.setTimeout(LKConstant.TIME_OUT_VALUE);
            req.setMethod(methodType);
            req.setBody(jsonBody);
            Http http = new Http();          
            HttpResponse res = http.send(req);
            return res;
        }
        return null;
    }
    
    /***** method to put data which is common in every request *****/
    public static void putCommonData(Map<String,Object> requestBodyMap) {
        requestBodyMap.put('broker_id',brokerId);
        requestBodyMap.put('username',objLKCst.Username__c);
        requestBodyMap.put('auth',objLKCst.API_Key__c);
        requestBodyMap.put('sequence',objLKCst.Sequence__c);
    }
    
    
    public static void updateToSalesforce() {
        system.debug('================================== updateToSalesforce ======================================');
        Map<String,SObject> sObjectIDMap = new Map<String,SObject>();
        set<String> lkRecordMapSet = XMLMappingAdapter.lkRecordsMap.keyset();
        List<LKRecord> lrrecordList  = new List<LKRecord>();
    
        for(String lkRecordMapSetValue : lkRecordMapSet ){
           lrrecordList.addAll(XMLMappingAdapter.lkRecordsMap.get(lkRecordMapSetValue));
        } 
        Set<SObject> sObjectList = new Set<SObject>();
        for(LKRecord rec : lrrecordList) {
            XMLMapping xmlMappingOb = rec.getMappingObject();
            String fieldToUpdate = rec.getFieldToUpdate(); 
            String responseKey = xmlMappingOb.getResponseKey();
            if(rec.fieldToUpdate2 != null && rec.fieldToUpdate2.length() > 0 && rec.lkObjectId2 != null) {
                rec.sObjectRef.put(rec.fieldToUpdate2,rec.lkObjectId2);
            }
            if(fieldToUpdate!=null && responseKey!=null) {                            
                if(fieldToUpdate.contains('.')) {
                    List<Object> sObjectAndFieldName =  getSObjectAndFieldName(fieldToUpdate,rec.sObjectRef);
                    if(sObjectAndFieldName!=null) {
                        SObject relatedSObj =(SObject) sObjectAndFieldName.get(0);
                        if(relatedSObj != null) {
                            relatedSObj.put((String)sObjectAndFieldName.get(1),rec.responseMap.get(responseKey));
                            if(!sObjectIDMap.containsKey((String)relatedSObj.get('id'))) {
                                sObjectIDMap.put((String)relatedSObj.get('id'),relatedSObj);
                                sObjectList.add(relatedSObj);
                            }else {
                                SObject sobj = sObjectIDMap.get((String)relatedSObj.get('id'));
                                sobj.put((String)sObjectAndFieldName.get(1),rec.responseMap.get(responseKey));
                            }
                        }
                    }
                }else {
                    System.debug('sObject -> '+rec.sObjectRef.get('id'));
                    rec.sObjectRef.put(fieldToUpdate,rec.responseMap.get(responseKey));
                    if(!sObjectIDMap.containsKey((String)rec.sObjectRef.get('id'))) {
                        sObjectIDMap.put((String)rec.sObjectRef.get('id'),rec.sObjectRef);
                        sObjectList.add(rec.sObjectRef);
                    }else {
                        SObject sobj = sObjectIDMap.get((String)rec.sObjectRef.get('id'));
                        sobj.put(fieldToUpdate,rec.responseMap.get(responseKey));
                    }
                }
            }
            else {
                System.debug('ObjectS => '+rec.sObjectRef.get('id'));
                if(!sObjectIDMap.containsKey((String)rec.sObjectRef.get('id'))) {
                    sObjectList.add(rec.sObjectRef);
                    sObjectIDMap.put((String)rec.sObjectRef.get('id'),rec.sObjectRef);
                }
            }
        }
        String soIds = '';
        for(SObject so : sObjectList) {
            String s = (String)so.get('id');
            soIds = soIds+s+'\n';
        }
        System.debug('Sobjects in list === \n'+soids);
        List<SObject> lstSObject = new List<SObject>(sObjectList);
        update lstSObject;
    }
    
    public static void putAppId(Map<String,Object> reqBodyMap, String appId) {
        reqBodyMap.put(LKConstant.APPLICATION_ID,appId);
    }
    
    public static String getJsonString(Map<String,Object> requestBodyMap) {
        return JSON.serialize(requestBodyMap);
    }
    
    public static Map<String,String> convertToMap(String jsonStr) {
        Map<String,String> respMap = (Map<String,String>)Json.deserialize(jsonStr,Map<String,String>.class);
        return respMap;
    }
    
    private static List<String> getListFromString(String value){
      if(value!=null){
          return value.split(',');
      }
      return null;
    }
    
    private static List<Object> getSObjectAndFieldName(String fieldName, Sobject rec) {
        System.debug('-------------------- getSObjectAndFieldName ----------------------');
        system.debug('fieldName ========= '+fieldName);
        list<Object> objList = new List<Object>();
        List<String> fieldPartsList = new List<String>();
        fieldPartsList  = fieldName.split('\\.');
        System.debug('Separated Strings === '+fieldPartsList);
        String finalFieldName = '';
        for(Integer i=0;i < fieldPartsList.size();i++) {
            if(fieldPartsList.size() == i +1){
              finalFieldName = fieldPartsList.get(i);
            }else{
               rec = rec.getSObject(fieldPartsList.get(i));
            }     
        }
        System.debug('SOBJECT ===> '+rec+' , FieldName ===> '+finalFieldName);
        objList.add(rec);
        objList.add(finalFieldName);
        return objList;
    }
}