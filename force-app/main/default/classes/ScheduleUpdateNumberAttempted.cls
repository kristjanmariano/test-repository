/** 
* @FileName: ScheduleUpdateNumberAttempted
* @Description: 
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification 
* 1.0 3/9/18 RDAVID Created class
**/ 

global class ScheduleUpdateNumberAttempted implements Schedulable{

	public static String Sched = '0 00 00 * * ?';  //Every Day at Midnight 
	   
	global static String ScheduleUpdateNumberAttempted() {
		ScheduleUpdateNumberAttempted SJ = new ScheduleUpdateNumberAttempted(); 
	        return System.schedule('Update Number Attempted', Sched, SJ);
	}
	
	global void execute(SchedulableContext sc) {
		
		BatchUpdateNumberAttempted BatchJob = new BatchUpdateNumberAttempted();
		ID batchprocessid = Database.executeBatch(BatchJob,50);			
	}
	
	
}