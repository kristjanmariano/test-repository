/** 
* @FileName: RoleAddressGateway
* @Description: Provides finder methods for accessing data in the Role Address object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/03/18 RDAVID Created class
* 1.1 7/01/18 JBACULOD Commented methods no longer being used, added populateLocation
* 1.2 17/07/19 RDAVID - extra/task25349502-AutomateApplicationFieldInRoleAddress - Automate population of Application_Number lookup based on Role.
**/ 

public without sharing class RoleAddressGateway{

	//Populate Location (before Insert / Before Update)
	public static void populateLocation(Role_Address__c oldRoleAddress, Role_Address__c roleAddress, map <Id, Address__c> addressMap, boolean isInsert){
		if (isInsert){
			if(addressMap.containsKey(roleAddress.Address__c)){
				roleAddress.Location__c = addressMap.get(roleAddress.Address__c).Location__c;
			}
		}
		else{ //isUpdate
			if(roleAddress.Address__c != oldRoleAddress.Address__c && addressMap.containsKey(roleAddress.Address__c)){
				roleAddress.Location__c = addressMap.get(roleAddress.Address__c).Location__c;
			}
		}
	}
	/**
	* @Description: autoPopulateApplicationNumber - extra/task25349502-AutomateApplicationFieldInRoleAddress - Automate population of Application_Number lookup based on Role.
	* @Arguments:	Map<Id,Role__c> roleMap, List<Role_Address__c> roleAddressList
	* @Returns:		n/a
	*/
	public static void autoPopulateApplicationNumber(Map<Id,Role__c> roleMap, List<Role_Address__c> roleAddressList){
		for(Role_Address__c roleAdd : roleAddressList){
			if(roleMap.containsKey(roleAdd.Role__c)){
				roleAdd.Application_Number__c = roleMap.get(roleAdd.Role__c).Application__c;
			}
		}
	}

	/**
	* @Description: Returns a boolean if Role is populated
	* @Arguments:	Role_Address__c roleAddress - trigger.new
	* @Returns:		isRolePopulated
	*/

	/* public static Boolean isRolePopulated(Role_Address__c roleAddress){
		if(roleAddress.Role__c != NULL){
			return true;
		}return false;
	} */

	/**
	* @Description: Returns a boolean if Account is populated
	* @Arguments:	Role_Address__c roleAddress - trigger.new
	* @Returns:		isAccountPopulated
	*/

	/*public static Boolean isAccountPopulated(Role_Address__c roleAddress){
		if(roleAddress.Account__c != NULL){
			return true;
		}return false;
	} */

	/**
	* @Description: Returns a map of Parent Role__c
	* @Arguments:	Set<Id> roleIdSet, Set<Id> roleAddressIdSet
	* @Returns:		roleMap
	*/

	/*public static Map<Id,Role__c> queryRoleMap(Set<Id> roleIdSet, Set<Id> roleAddressIdSet){
		Map<Id,Role__c> roleMap = new Map<Id,Role__c>();

		if(!roleIdSet.isEmpty()){
			roleMap = new Map<Id,Role__c>([SELECT Id,(SELECT Id,Active_Address__c FROM Role_Addresses__r WHERE Id NOT In: roleAddressIdSet AND Active_Address__c =: CommonConstants.ADDRESS_CURRENT) FROM Role__c WHERE Id IN: roleIdSet]);
		}
		return roleMap;
	} */

	/**
	* @Description: Returns a map of Role_Address__c
	* @Arguments:	Map<Id,Role__c> roleMap
	* @Returns:		roleAddressToToggleMap
	*/

	/*public static Map<Id,Role_Address__c> getRoleAddressToToggleMap (Map<Id,Role__c> roleMap){
		Map<Id,Role_Address__c> roleAddressToToggleMap = new Map<Id,Role_Address__c>();
		if(!roleMap.isEmpty()){
			for(Role__c role : roleMap.values()){
				if(role.Role_Addresses__r != NULL && role.Role_Addresses__r.size() > 0){
					for(Role_Address__c roleAdd : role.Role_Addresses__r){
						roleAdd.Active_Address__c = CommonConstants.ADDRESS_PREVIOUS;
						roleAddressToToggleMap.put(roleAdd.Id,roleAdd);
					}
				}
			}
		}
		return roleAddressToToggleMap;
	}  */

	/**
	* @Description: Returns a Set of Role Ids with Current Role Address
	* @Arguments:	Set<Id> roleIds
	* @Returns:		roleWithoutCurrentRoleAddressSet
	*/

	/*public static Set<Id> getRoleWithoutCurrentRoleAddressIdSet(Set<Id> roleIds){
		Set<Id> roleWithoutCurrentRoleAddressSet = new Set<Id>();
		for(Role__c role : [SELECT Id, (SELECT Id,Active_Address__c FROM Role_Addresses__r WHERE Active_Address__c =: CommonConstants.ADDRESS_CURRENT) FROM Role__c WHERE Id IN: roleIds]){
			if(role.Role_Addresses__r.isEmpty()){
				roleWithoutCurrentRoleAddressSet.add(role.Id);
			}
		}
		return roleWithoutCurrentRoleAddressSet;
	} */
}