/** 
* @FileName: NVMCallSummaryGateway
* @Description: Provides finder methods for accessing data in the NVMCallSummary object.
* @Copyright: Positive (c) 2019 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 20/05/19 JBACULOD Created Class
**/
public without sharing class NVMCallSummaryGateway {

    public static void pullCallPurpose(Map <string,string> nvmCSForCallPurposeUpdateMap, NVMStatsSF__NVM_Call_Summary__c nvmcs){

        if (nvmCSForCallPurposeUpdateMap.containskey(nvmcs.NVMStatsSF__CallGuid__c)){
            nvmcs.Call_Purpose__c = nvmCSForCallPurposeUpdateMap.get(nvmcs.NVMStatsSF__CallGuid__c);
        }

    }

}