/** 
* @FileName: ApplicationTriggerTest
* @Description: test class of ApplicationTrigger, ApplicationHandler, ApplicationGateway
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/05/18 JBACULOD Created, testSyncApp2Case
**/ 
@isTest
private class ApplicationTriggerTest {
    
    @testsetup static void setup(){
        
        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = false,
                Enable_Case_Auto_Create_Application__c = false,
            	Enable_Application_Trigger__c = true,
            	Enable_Application_Sync_App_2_Case__c = false
        );
        insert trigSet;
        
        //Create test data for Case
        list <Case> cselist = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF,5);
        insert cselist;

        //Create test data for Application
        list <Application__c> applist = new list <Application__c>();
        for (Integer i=0; i<5; i++){
        	applist.addAll(TestDataFactory.createGenericApplication(CommonConstants.APP_RT_COMM_C, cselist[i].Id, 1));
        }
        insert applist;

        //Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

        //Create Test Role 
		List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, cselist[0].Id, accList[0].Id, appList[0].Id,  5);
		Database.insert(roleList);
        
        
    }
    
    static testmethod void testSyncApp2Case(){
        
        Trigger_Settings1__c trigSet = [Select Id, Enable_Triggers__c, Enable_Application_Trigger__c, Enable_Application_Sync_App_2_Case__c From Trigger_Settings1__c];
        trigSet.Enable_Triggers__c = true;
        trigSet.Enable_Application_Trigger__c = true;
        trigSet.Enable_Application_Sync_App_2_Case__c = true;
        trigSet.Enable_Application_Primary_Applicants__c = true;
        update trigSet;
        
        //Retrieve test data for Account
        list <Account> acclist = [Select Id From Account];
        //Retrieve test data for Case
        list <Case> cselist = [Select Id, Application_Record_Type__c From Case];
        //Retrieve test data for Application
        list <Application__c> applist = [Select Id, RecordTypeId, Name, Primary_Applicant__c, Primary_Contact_for_Application__c, Lead_12_Months_Address__c, Lead_12_Months_Income__c, Number_of_Applicants__c, Inconsistent_Application_Details__c, Other_Active_Cases_for_Applicants__c, Non_Borrowing_Spouse__c, Primary_Contact_Preferred_Call_Time__c, Comb_Total_Surplus__c, Case__c From Application__c];
        
        Test.startTest();
        
            Id upAppRT = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get(CommonConstants.APP_RT_CONS_I).getRecordTypeId();
            for (Application__c app : applist){
                app.Primary_Contact_Name_MC__c = acclist[0].Id;
                app.Primary_Contact_Email_MC__c = 'test@email.com';
                app.Co_Applicant_Email_MC__c = 'test2@email.com';
                app.Primary_Contact_Phone_MC__c = '123456';
                app.Co_Applicant_Name_MC__c = acclist[0].Id;
                app.Co_Applicant_Phone_MC__c = '123456';
                app.RecordTypeId = upAppRT;
                app.Lead_12_Months_Address__c = true;
                app.Lead_12_Months_Income__c  = true;
                app.Primary_Applicant__c  = 'Test';
                app.Non_Borrowing_Spouse__c = true;
                app.Other_Active_Cases_for_Applicants__c = true;
                app.Primary_Contact_for_Application__c  = 'test contact';
                app.Primary_Contact_Preferred_Call_Time__c = '8:30 am';
                app.Inconsistent_Application_Details__c = true;
                app.Number_of_Applicants__c = 1;
                app.Comb_Centrelink_Income__c = 5000;
                app.Comb_Accommodation_Expenses__c = 2000;
                app.Net_Profit_After_Tax__c =1;
            }
            update applist; 
            
            list <Case> upCselist = [Select Id, Application_Record_Type__c From Case];
        	//Verify that Applicant Type was updated due to Applicant RT change
            for (Case cse : upCselist){
            	system.assertEquals(cse.Application_Record_Type__c, CommonConstants.APP_RT_CONS_I);
            }
        
        	delete applist; //cover delete event in ApplicationHandler
        
        Test.stopTest();
        
    }
    

}