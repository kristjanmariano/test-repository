/* 

Author:     Steven Herod, Feb 2014
Purpose:    Backs the Veda Search page retrieving the base information used to prepopulate the search screen
            All Veda API work is deferred to VedaAPI.cls
History:    09/07/2017 - Updated, added validation for Credit Check to allow check just once. (Jesfer Baculod - Positive Group)
            09/08/2017 - set redirecting of page to VedaIndividualPagePrintable once Attachment gets created
            09/20/2017 - Added System Administrator OS on validation when generating CreditReport
            10/17/2017 - Added Mortgage on validation when generating CreditReport (Mortgage can do multiple veda credit check)
*/
public with sharing class VedaIndividualCC_NM {

    
    public boolean saveResultToContact { get; set;}
    public VedaDTO.IndividualResponse response { get; set; }
    public VedaDTO.IndividualRequest request { get; set; }
    public boolean errorMsg;
    public String errorTextMsg { get; set;}
    public Contact contact { get; set;}
    public Lead leadObj { get; set;}
    public Account acctObj { get; set;}
    public Opportunity oppObj { get; set;}
    public Applicant_2__c app2Obj {get; set; }
    public String sObjectType { get; set;}
    private User curUsr {get;set;}
    public string testresponse {get;set;}
    
    public Id idOfObjToAttach { get; set;}
    
    public Id attachmentId { get; set;}

    public VedaIndividualCC_NM(ApexPages.StandardController std) {
        
        errorMsg = false;
        errorTextMsg = '';

        //Retrieve current user details
        Id usrCurID = UserInfo.getUserId();
        curUsr = [Select Id, Name, Profile.Name From User Where Id = : usrCurID];

        //Retreiving the base contact details
        sObjectType = String.valueOf(std.getRecord().getSObjectType());
        VedaDTO.Individual individual;
        idOfObjToAttach = std.getRecord().id;   

        if(sObjectType  == 'Role__c'){          
            Id accId = [SELECT Id,Account__c FROM Role__c WHERE Id =: std.getRecord().id].Account__c;

            acctObj = new Account();
            acctObj = [ SELECT a.Website, a.Type, a.State__c, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, 
                            a.ShippingCity,  a.Salutation, a.RecordTypeId, a.Postcode__c, a.Phone, a.PersonMobilePhone, a.PersonLastCUUpdateDate, 
                            a.PersonLastCURequestDate, a.PersonEmailBouncedReason, a.PersonEmailBouncedDate, a.PersonEmail, a.PersonContactId, a.LastName, 
                            a.IsPersonAccount, a.Id, a.Gender__c, a.FirstName, a.Fax, 
                            a.Drivers_Licence_Number__c,  a.Description,  a.Date_of_Birth__c, a.Customer_Comments__c, 
                            a.Current_Street_Number__c, a.Current_Street_Name__c, a.Current_Street_Type__c, a.Current_State__c, a.Current_PostCode__c, a.Current_Country__c, 
                            a.Current_Suburb__c, a.Account_PersonMobilePhone__c,a.PersonBirthdate, Address__r.Street_Number__c, Address__r.Street_Name__c, 
                            Address__r.Street_Type__c, Address__r.Suburb__c, Address__r.State__c, Address__r.Postcode__c, Address__r.Country__c, Address__r.State_Acr__c
                        FROM Account a WHERE a.id = :accId]; 

            if(acctObj != null){ 
                individual = new VedaDTO.Individual(acctObj);   
                    
                if(acctObj.PersonBirthdate != null){
                    individual.dateOfBirth = DateTime.newInstance(acctObj.PersonBirthdate,Time.newInstance(0,0,0,0)).format('yyyy-MM-dd');
                }

                individual.currentAddress = new VedaDTO.Address();
                individual.currentAddress.type = 'residential-current';
                individual.currentAddress.postcode = (acctObj.Address__c != NULL && acctObj.Address__r.Postcode__c != NULL) ? Integer.valueOf(acctObj.Address__r.Postcode__c) : null;
                individual.currentAddress.suburb = (acctObj.Address__c != NULL && acctObj.Address__r.Suburb__c != NULL) ? acctObj.Address__r.Suburb__c : null;
                // currentAddress.state = acct.Address__r.State__c;
                individual.currentAddress.country = (acctObj.Address__c != NULL && acctObj.Address__r.Country__c != NULL) ? acctObj.Address__r.Country__c : null;
                individual.currentAddress.streetNumber = (acctObj.Address__c != NULL && acctObj.Address__r.Street_Number__c != NULL) ? acctObj.Address__r.Street_Number__c : null;
                individual.currentAddress.streetName = (acctObj.Address__c != NULL && acctObj.Address__r.Street_Name__c != NULL) ? acctObj.Address__r.Street_Name__c : null;
                individual.currentAddress.streetType = (acctObj.Address__c != NULL && acctObj.Address__r.Street_Type__c != NULL) ? acctObj.Address__r.Street_Type__c : null;
                individual.currentAddress.State = (acctObj.Address__c != NULL && acctObj.Address__r.State_Acr__c != NULL) ? acctObj.Address__r.State_Acr__c : null;   
            }
            
        }

        request = new VedaDTO.IndividualRequest();
        request = request.newAuthorisedAgentConsumerPlusRequest(CustomSettingUtil.getVedaConfigValue('AccessCode'),
                                                                CustomSettingUtil.getVedaConfigValue('AccessPassword'),
                                                                CustomSettingUtil.getVedaConfigValue('ClientReference'),individual);
                                                                //'P3le4s5o6e', 'P2le1s8o3e', '9934', individual);      
        request.mode = CustomSettingUtil.getVedaConfigValue('Request_Mode'); //'test';
        request.version = CustomSettingUtil.getVedaConfigValue('Request_Version'); //'1.0';
        request.carrier = CustomSettingUtil.getVedaConfigValue('Request_Carrier'); //'06';
        request.subscriberIdentifier = CustomSettingUtil.getVedaConfigValue('Request_SubscriberIdentifier'); //'AYFK9999';
        request.security = CustomSettingUtil.getVedaConfigValue('Request_Security'); //'TS'
        //request.individual.currentAddress = new VedaDTO.Address();  
    } 

    
    public PageReference generateCreditReport() {
        
        try{
            errorMsg = false;
            errorTextMsg = '';

            //check current user if not a Sys Admin, Sys Admin OS, Team Leader, HL Team Leader, Mortgage
            // if (!String.valueof(curUsr.Profile.Name).contains(Label.Profile_Name_System_Administrator) && curUsr.Profile.Name != Label.Profile_Name_Team_Leader 
            //     && curUsr.Profile.Name != Label.Profile_Name_HL_Team_Leader && curUsr.Profile.Name != Label.Profile_Name_Mortgage){
            //     //Check for existing CCResponse attachment on current record
            //     list <Attachment> exAttlist = [Select Id, ParentId, Name From Attachment Where ParentId = : idOfObjToAttach];
            //     for (Attachment att : exAttlist){
            //         if (att.Name.contains('-CC.pdf')){
            //             errorTextMsg = Label.Veda_Credit_Check_Attachment_Error;
            //             break;
            //         }
            //     }
            //     //Contains error before generating the Credit Report
            //     if (errorTextMsg != ''){
            //         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,errorTextMsg));
            //         return null;
            //     }
            // }

            Blob xmlBlob;
            if (!Test.isRunningTest()){
                response = VedaAPI.individualProductCall(request);
                xmlBlob = Blob.valueOf(response.xmlResponse);
            }
            else { 
                xmlBlob = Blob.valueOf(testresponse);      
            }
            Attachment xmlFile = new Attachment(parentId = idOfObjToAttach, name= 'CCResponse-' + DateTime.now().format()+ '.xml', body = xmlBlob);
            insert xmlFile;
            
            attachmentId = xmlFile.id;

            //Redirect to VedaIndividualPagePrintable vf page to convert created XML file to PDF
            PageReference pref = new PageReference('/apex/VedaIndividualPagePrintableNM?attachID='+attachmentId+'&objAttId='+idOfObjToAttach+'&objType='+sObjectType);
            pref.setRedirect(true);
            return pref;
                    
        }catch(Exception e){
            errorMsg = true;
            errorTextMsg = e.getMessage();
            System.debug('errorTextMsg : '+errorTextMsg );
            System.debug('errorMsg : '+errorMsg );
            return null;
        }
    }
    
    public PageReference back() {
        return new PageReference('/'+idOfObjToAttach);
    }
    
    public boolean getErrorMsg() {
        return errorMsg;
    }
}