/*
	@Description: class containing the invocable method that can be used on Lead / Opp flow definitions - resolves NVM Edit Session issue
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/20/2017 - Created
		-	9/22/2017 - Updated
		-	9/26/2017 - Ordered Pending Action by descending Name to push latest update on a related record if record got multiple Pending Actions
		-	12/11/2017 - Updated due to limitException issue 
		-	12/20/2017 - Rex David - [LIG-743 SF - Query Bug Fix] Added "ALL ROWS" in Task query
*/
public class PendingAction_UpdateNVMRelatedFields {
	
	//private static Integer flow_update_loopCount = 0;

	@InvocableMethod(label='Lead/Opp NVM Pending Action' description='execute all Pending Action for Lead/Opp due to NVM Edit Session issue')
	public static void leadOppPendingAction (list <ID> leadoppIDs) {

		//system.debug('@@PendingAction_UpdateNVMRelatedFields.flow_update_loopCount:'+PendingAction_UpdateNVMRelatedFields.flow_update_loopCount);

		//Execute only once for every record update
		//if (PendingAction_UpdateNVMRelatedFields.flow_update_loopCount == 0){
			system.debug('@@leadoppIDs:'+leadoppIDs);
			//Get all pendings actions 
			list <Pending_Action__c> palist = [Select Id, Related_Record_Id__c, Target_Object__c, CW_Call_End_Time__c From Pending_Action__c Where Related_Record_Id__c in : leadoppIDs AND Function_Name__c = 'updateNVMRelatedFields' ORDER BY Name DESC];
			system.debug('@@palist:'+palist);
			map <Id, Pending_Action__c> paMap = new map <id, Pending_Action__c>();
			set <Id> filteredRelIDs = new set <Id>();
			for (Pending_Action__c pa : palist){
				if (!paMap.containskey(pa.Related_Record_Id__c)){
					paMap.put(pa.Related_Record_Id__c, pa);
					filteredRelIDs.add(pa.Related_Record_Id__c);
				}
			}
			//Update only to related Record if a Pending Action was found
			if (palist.size() > 0){
				//Execute pending actions for Leads
				list <Lead> upLdlist = [Select Id, Calls_Made__c, Last_Call_Time_Ended__c From Lead Where Id in : filteredRelIDs];
				if (upLdlist.size() > 0){
					map <Id, list <Task>> leadTskMap = new map <id, list <Task>>();
					for (Task tsk : [Select Id, WhoId, CallType From Task Where CallType='Outbound' and Status='Completed' and WhoId in : filteredRelIDs ALL ROWS]){
						if (!leadTskMap.containsKey(tsk.WhoId)){
							leadTskMap.put(tsk.WhoId, new list <Task>());
						}
						leadTskMap.get(tsk.WhoId).add(tsk); 
					}

					for (Lead ld : upLdlist){
						//Update Calls Made (Running User = Current User)
						if (leadTskMap.containskey(ld.Id)){
							ld.Calls_Made__c = leadTskMap.get(ld.Id).size();
						}
						//Update Last Call Time Ended (Running User = Current User)
						if (paMap.containskey(ld.Id)){
							ld.Last_Call_Time_Ended__c = paMap.get(ld.Id).CW_Call_End_Time__c;
						}

					}
					update upLdlist;
					//PendingAction_UpdateNVMRelatedFields.flow_update_loopCount++;
				}


				//Execute pending actions for Opportunities
				list <Opportunity> upOpplist = [Select Id, Calls_Made__c, Last_Call_Time_Ended__c From Opportunity Where Id in : filteredRelIDs];
				system.debug('@@upOppList:'+upOpplist.size()+upOpplist);
				if (upOppList.size() > 0){
					map <Id, list <Task>> oppTskMap = new map <id, list <Task>>();
					for (Task tsk : [Select Id, WhatId, CallType From Task Where CallType='Outbound' and Status='Completed' and WhatId in : filteredRelIDs ALL ROWS]){
						if (!oppTskMap.containsKey(tsk.WhatId)){
							oppTskMap.put(tsk.WhatId, new list <Task>());
						}
						oppTskMap.get(tsk.WhatId).add(tsk);
					}

					for (Opportunity opp : upOpplist){
						//Update Calls Made (Running User = Current User)
						if (oppTskMap.containskey(opp.Id)){
							opp.Calls_Made__c = oppTskMap.get(opp.Id).size();
						}
						//Update Last Call Time Ended (Running User = Current User)
						if (paMap.containskey(opp.Id)){
							opp.Last_Call_Time_Ended__c = paMap.get(opp.Id).CW_Call_End_Time__c;
						}
					}
					update upOpplist;
					//PendingAction_UpdateNVMRelatedFields.flow_update_loopCount++;
				}

				//Delete executed Pending Actions (Requeried to ensure Pending Action gets deleted only once)
				list <Pending_Action__c> del_palist = [Select Id, Related_Record_Id__c, Target_Object__c, CW_Call_End_Time__c From Pending_Action__c Where Related_Record_Id__c in : filteredRelIDs AND Function_Name__c = 'updateNVMRelatedFields'];
				if (del_palist.size() > 0) delete del_palist;
			}

		//}
	}


}