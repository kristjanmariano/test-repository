/** 
* @FileName: SectorGateway
* @Description: Provides finder methods for accessing data in the Sector object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 10/04/19 RDAVID-task24148707 Created Class 
* 1.1 3/05/19 RDAVID-task24399056 : Populate the Email_Warning_Recipient__c and Email_Warning_Recipient_POC__c of the Sector record
* 1.2 9/05/19 RDAVID-task24399056 : Added Logic to apply Different time threshold for Consumer and Commercial Initial Review 
* 1.3 16/05/19 RDAVID-task24399056 : Updated Logic to send the Initial Review Email Warnings to Email_Warning_Recipient_POC__c of SLA Sector Metadata. as per Mel and Chris
**/ 
public class SectorGateway {
    /** 
    * @Description: Provides finder methods for accessing data in the Sector object.
    * @Created: Rexie Aaron David
    * @Params: BusinessHours busiHours, Datetime startDateTime, Datetime endDateTime
    * @Returns: String in this format ([X]Days & [X]Hours & [X]Minutes)
    * @Modification Log =============================================================== 
    * Ver Date Author Modification --- ---- ------ -------------
    * 1.0 10/04/19 RDAVID-task24148707 : logic to get time spent on based on two DateTime fields excluded hours outside Business Hours 
    **/ 
    public static String getTimeSpentInBusinessHours (BusinessHours busiHours, Datetime startDateTime, Datetime endDateTime){
        Long minutes = ((BusinessHours.diff (busiHours.id, startDateTime, endDateTime)) / 1000) / 60;
        Long days = minutes / 60 / 24 ;
        Long hours = (minutes - days * 60 * 24) / 60 ;
        Long mins = minutes - days * 60 * 24 - hours * 60 ;
        return days+ (days <= 1 ?' Day ' : ' Days ')+hours+(hours <= 1 ?' Hour ' : ' Hours ')+mins+(mins <= 1 ?' Minute ' : ' Minutes ');
    }

    /** 
    * @Description: Provides finder methods for accessing data in the Sector object.
    * @Created: Rexie Aaron David
    * @Params: Sector__c sector, Map<String,SLA_Sector__mdt> slaSectorMDTMap, Map<String,Schema.RecordTypeInfo> sectorRTMapByName
    * @Modification Log =============================================================== 
    * Ver Date Author Modification --- ---- ------ -------------
    * 1.0 10/04/19 RDAVID-task24148707 : logic to set Sector RT
    **/ 
    public static void setSectorRecordtype (Sector__c sector, Map<String,SLA_Sector__mdt> slaSectorMDTMap, Map<String,Schema.RecordTypeInfo> sectorRTMapByName){
        
        if(slaSectorMDTMap.containsKey(sector.Sector1__c) && !String.IsBlank(slaSectorMDTMap.get(sector.Sector1__c).Sector_RecordType__c) && sectorRTMapByName.containsKey(slaSectorMDTMap.get(sector.Sector1__c).Sector_RecordType__c)){
            //Set Sector SLA RT
            sector.RecordTypeId = sectorRTMapByName.get(slaSectorMDTMap.get(sector.Sector1__c).Sector_RecordType__c).getRecordTypeId();
        }
        else{//If Sector is not 
            if(sector.Case_Number__c != NULL && sector.SR_Number__c == NULL) sector.RecordTypeId = sectorRTMapByName.get('Case Sector').getRecordTypeId();
            else if(sector.Case_Number__c != NULL && sector.SR_Number__c != NULL || sector.Case_Number__c == NULL && sector.SR_Number__c != NULL) sector.RecordTypeId = sectorRTMapByName.get('SR Sector').getRecordTypeId();
        }
        system.debug('Sector Map ---> '+sector.RecordTypeId + sector.Sector1__c);
        
    }

    public static String getSLASectorMetadataQueryString (){
        String selectStr = 'SELECT Id,Additional_Rules__c,Related_to_Object__c,Related_to_Object_RT__c,Related_to_Partition__c,Sector_Entry_Event__c,Sector_Entry_Event_Criteria__c,Sector_Exit_Event__c,Sector_Exit_Event_Criteria__c,Sector_Name__c,Sector_Recordtype__c,SLA_Initial_Warning_Time_MM__c,SLA_Interval_MM__c,SLA_Red_Icon_Threshold_MM__c,SLA_Target_Time_MM__c,SLA_Yellow_Icon_Threshold_MM__c,Alert_Recipient_Email__c,Alert_Recipient_Name__c,Email_Warning_Recipient_POC__c,Is_Active__c,Email_Recipient_Fields__c,Email_Recipient_Fields1__c ';
        String fromStr = 'FROM SLA_Sector__mdt ';
        String whereStr = 'WHERE Is_Active__c = TRUE';
        return selectStr + fromStr + whereStr;
    }

    /** 
    * @Description: Populate the Alert_Recipient_Name__c and Alert_Recipient_Email__c of the Sector record
    * @Created: Rexie Aaron David
    * @Params: Sector__c sector
    * @Modification Log =============================================================== 
    * Ver Date Author Modification --- ---- ------ -------------
    * 1.0 12/04/19 RDAVID-task24148707 : logic to Populate the Alert_Recipient_Name__c and Alert_Recipient_Email__c of the Sector record
    * 1.1 6/05/19 RDAVID-task24399056 : this logic is deprecated and commented in SectorHandler and replaced by setRecipients method
    **/ 

    // public static void setParentOwnerFields (List<Sector__c> sectorList,  Map<Id,Schema.RecordTypeInfo> sectorRTMapById, Map<String,SLA_Sector__mdt> slaSectorMDTMap){
    //     Set<Id> parentIds = new Set<Id>();
    //     List<Sector__c> SRSectorList = new List<Sector__c>();
    //     List<Sector__c> sectorFinalList = new List<Sector__c>();
    //     Map<Id,Case> caseMap = new Map<Id,Case>();
    //     Map<Id,Support_Request__c> srMap = new Map<Id,Support_Request__c>();
    //     Boolean queryCase = false, querySector = false;
    //     String userIdPrefix = '005';
        
    //     for(Sector__c sector : sectorList){
    //         if(sectorRTMapById.containsKey(sector.RecordTypeId)){
    //             //if(sectorRTMapById.get(sector.RecordTypeId).getName().containsIgnorecase('Case SLA')){ //Check if Sector is SLA
    //             if(slaSectorMDTMap.containsKey(sector.Sector1__c) && slaSectorMDTMap.get(sector.Sector1__c).Sector_Recordtype__c.containsIgnorecase('Case SLA')){
    //                 if(sector.Case_Number__c != NULL){
    //                     System.debug('sector.Case_Number__c --> '+sector.Case_Number__c);
    //                     parentIds.add(sector.Case_Number__c);
    //                     sectorFinalList.add(sector);
    //                     queryCase = true;
    //                 }
    //             }
    //             // else if(sectorRTMapById.get(sector.RecordTypeId).getName().containsIgnorecase('SR SLA')){
    //             else if(slaSectorMDTMap.containsKey(sector.Sector1__c) && slaSectorMDTMap.get(sector.Sector1__c).Sector_Recordtype__c.containsIgnorecase('SR SLA')){
    //                 System.debug('sector.SR_Number__c --> '+sector.SR_Number__c);
    //                 parentIds.add(sector.SR_Number__c);
    //                 //SRSectorList.add(sector);//for populating Alert_Recipient_Name__c and Alert_Recipient_Email__c of the SR SLA Sectors
    //                 sectorFinalList.add(sector);
    //                 querySector = true;
    //             }
    //         }
    //     }
        
    //     if(queryCase) caseMap = new Map<Id,Case>([SELECT Id, CaseNumber, OwnerId, Owner.Email, Owner.Name, Settlement_Officer__r.Name, Settlement_Officer__c, Settlement_Officer__r.Email, Submitter__r.Name, Submitter__r.Email FROM Case WHERE Id IN: parentIds]); //Query Cases
    //     if(querySector) srMap = new Map<Id,Support_Request__c>([SELECT Id, Name, OwnerId, Owner.Email, Owner.Name, Status__c, Stage__c FROM Support_Request__c WHERE Id IN: parentIds]); //Query SRs
        
    //     // System.debug('sectorFinalList --> '+sectorFinalList);
    //     // System.debug('srMap --> '+srMap);
    //     // System.debug('queryCase --> '+queryCase);
    //     // System.debug('parentIds --> '+parentIds);
        
    //     for(Sector__c sec : sectorFinalList){
    //         //For Sectors related to the Case record
    //         // System.debug('@@@ caseMap --> ' + caseMap.get(sec.Case_Number__c));
    //         // System.debug('@@@ Case Owner Name --> ' + caseMap.get(sec.Case_Number__c).Owner.Name + ' @@@ Case Owner Email --> '+ caseMap.get(sec.Case_Number__c).Owner.Email);
    //         // System.debug('SR SLA 0 --> '+slaSectorMDTMap.containsKey(sec.Sector1__c));
    //         if(sec.Case_Number__c != NULL && sec.SR_Number__c == NULL && caseMap.containsKey(sec.Case_Number__c)  && String.valueOf(caseMap.get(sec.Case_Number__c).OwnerId).startsWithIgnoreCase(userIdPrefix)) {
    //             sec.Alert_Recipient_Name__c = caseMap.get(sec.Case_Number__c).Owner.Name;
    //             sec.Alert_Recipient_Email__c = caseMap.get(sec.Case_Number__c).Owner.Email;
    //             // system.debug('Parent Owner Name 1'+caseMap.get(sec.Case_Number__c).Owner.Name);
    //         }
    //         //For Sectors related to Support Request record 
            
    //         else if(sec.Case_Number__c != NULL && sec.SR_Number__c != NULL && srMap.containsKey(sec.SR_Number__c) && slaSectorMDTMap.containsKey(sec.Sector1__c)){ //&& String.valueOf(srMap.get(sec.SR_Number__c).OwnerId).startsWithIgnoreCase(userIdPrefix)
    //             //If the SRs are New , Assuming it's assigned to "Unassigned" Queue
    //             // System.debug('SR SLA 1 --> '+sec);
    //             // System.debug('SR SLA 1 srMap --> '+srMap);
    //             if(srMap.get(sec.SR_Number__c).Stage__c == 'New' && srMap.get(sec.SR_Number__c).Status__c == 'New'){
    //                 if(!String.IsBlank(slaSectorMDTMap.get(sec.Sector1__c).Alert_Recipient_Name__c))
    //                     sec.Alert_Recipient_Name__c = slaSectorMDTMap.get(sec.Sector1__c).Alert_Recipient_Name__c;//srMap.get(sec.SR_Number__c).Owner.Name;
    //                 if(!String.IsBlank(slaSectorMDTMap.get(sec.Sector1__c).Alert_Recipient_Email__c))
    //                     sec.Alert_Recipient_Email__c = slaSectorMDTMap.get(sec.Sector1__c).Alert_Recipient_Email__c;//srMap.get(sec.SR_Number__c).Owner.Email;
    //             }
    //             //Else If the SRs are In Progress and Assigned to Owner
    //             // else {
    //             //     sec.Alert_Recipient_Name__c = srMap.get(sec.SR_Number__c).Owner.Name;//slaSectorMDTMap.get(sec.Sector1__c).Alert_Recipient_Name__c;
    //             //     sec.Alert_Recipient_Name__c = srMap.get(sec.SR_Number__c).Owner.Email;//slaSectorMDTMap.get(sec.Sector1__c).Alert_Recipient_Name__c;
    //             // }
    //             // system.debug('Parent Owner Name 2'+srMap.get(sec.SR_Number__c).Owner.Name);
    //         }

    //         // RDAVID 30/04/2019: Logic to send the NOD - SLA - Sent for Settlement to the Case Settlement Officer per Melissa Caruso
    //         if(sec.Sector1__c == 'NOD - SLA - Sent for Settlement' && caseMap.containsKey(sec.Case_Number__c)) {
    //             system.debug('Logic to send the NOD - SLA - Sent for Settlement to the Case Settlement Officer');
    //             if(caseMap.get(sec.Case_Number__c).Settlement_Officer__c != NULL){
    //                 sec.Alert_Recipient_Name__c = caseMap.get(sec.Case_Number__c).Settlement_Officer__r.Name;
    //                 sec.Alert_Recipient_Email__c = caseMap.get(sec.Case_Number__c).Settlement_Officer__r.Email;
    //             }
    //         } 
    //         system.debug('sector ------> '+sec.Sector1__c + 'recordtype ------> '+sectorRTMapById.get(sec.RecordTypeId).getName() + ' owner --> '+ sec.Alert_Recipient_Name__c + 'SR - '  + sec.SR_Number__c  + 'CASE - '  + sec.Case_Number__c);
    //     }
    // }

    /** 
    * @Description: Populate the Email_Warning_Recipient__c and Email_Warning_Recipient_POC__c lookups of the Sector record
    * @Created: Rexie Aaron David
    * @Params: Sector__c sector
    * @Modification Log =============================================================== 
    * Ver Date Author Modification --- ---- ------ -------------
    * 1.0 3/05/19 RDAVID-task24399056 : Populate the Email_Warning_Recipient__c and Email_Warning_Recipient_POC__c of the Sector record
    **/

    //sector should be Queried with the relationship fields referenced in this method
    public static void setRecipients (SObject sectorSObject, Case cse, Support_Request__c sReq , Map<String,SLA_Sector__mdt> slaSectorMDTMap, Map<String,Id> slaTypeUserMap){
        SObject cseInstance = cse;
        //Needs a map of SLA Type (key) AND Email Warning Recipient (POC) User record as Value
        Sector__c sector = (Sector__c) sectorSObject;
        SLA_Sector__mdt slaMeta = (slaSectorMDTMap.containsKey(sector.Sector1__c)) ? slaSectorMDTMap.get(sector.Sector1__c) : null;
        
        if(slaSectorMDTMap.containsKey(sector.Sector1__c) && slaMeta != NULL){
            //Logic for Nodifi SUPPORT REQUEST SLA 
            if(slaSectorMDTMap.get(sector.Sector1__c).Related_to_Object__c == 'Support Request' && sector.SR_Number__c != NULL && sReq != NULL){
                if(slaTypeUserMap.containsKey(sector.Sector1__c)){
                    if(sReq.Status__c == 'New'){ //Set the POC when the SR is New
                        sectorSObject.put('Email_Warning_Recipient_POC__c',slaTypeUserMap.get(sector.Sector1__c));
                    }
                    else if(sReq.Status__c == 'In Progress' && !String.IsBlank(slaMeta.Email_Recipient_Fields1__c)){
                        List<String> userFields =  slaMeta.Email_Recipient_Fields1__c.split('\n');
                        //Designed for NOD - SLA - Initial Review
                        if(userFields.size() > 1){ 
                            sectorSObject.put('Email_Warning_Recipient__c',sReq.OwnerId);
                            if(sReq.Application_Type__c.containsIgnorecase('Consumer')){
                                sectorSObject.put('Email_Warning_Recipient_POC__c',slaTypeUserMap.get(sector.Sector1__c));
                            }
                            else if(sReq.Application_Type__c.containsIgnorecase('Commercial')){
                                sectorSObject.put('Email_Warning_Recipient_POC__c',slaTypeUserMap.get(sector.Sector1__c));  //1.3 16/05/19 RDAVID-task24399056
                            }
                        }
                        else if(userFields.size() == 1){ //Designed for other Nodifi SUPPORT REQUEST SLA with single field in 'Email Recipient Fields' Single field should be Case field
                            Id alertRecipient = (Id)cseInstance.get(userFields[0]);
                            if(alertRecipient != NULL){
                                sectorSObject.put('Email_Warning_Recipient_POC__c',slaTypeUserMap.get(sector.Sector1__c));
                                sectorSObject.put('Email_Warning_Recipient__c',alertRecipient); //Dynamic Assignment of Email Recipient based on SLA_Sector__mdt.Email_Recipient_Fields1__c
                            }
                        }   
                    }
                    // }
                }
            }
            //Logic for Nodifi Case SLA 
            else if(slaSectorMDTMap.get(sector.Sector1__c).Related_to_Object__c == 'Case' && sector.SR_Number__c == NULL && sReq == NULL){
                if(slaTypeUserMap.containsKey(sector.Sector1__c)){
                    List<String> userFields =  slaMeta.Email_Recipient_Fields1__c.split('\n');
                    if(userFields.size() == 1){ 
                        sectorSObject.put('Email_Warning_Recipient_POC__c',slaTypeUserMap.get(sector.Sector1__c));
                        Id alertRecipient = (Id)cseInstance.get(userFields[0]);
                        if(alertRecipient != NULL){
                            sectorSObject.put('Email_Warning_Recipient__c',alertRecipient); //Dynamic Assignment of Email Recipient based on SLA_Sector__mdt.Email_Recipient_Fields1__c
                        }
                    }   
                }
            }
        }
    }

    public static Map<String,Id> getSlaTypeUserMap (Map<String,String> slaTypeUserStringMap){
        Map<String,Id> slaTypeUserMap = new Map<String,Id>();
        Map<String,Id> userNameIdMap = new Map<String,Id>();
        for(User u : [SELECT Id,Name FROM User WHERE Name IN: slaTypeUserStringMap.values() AND IsActive = TRUE]){
            userNameIdMap.put(u.Name,u.Id);
        }
        // if(!userNameIdMap.isEmpty()){
        for(String slaType : slaTypeUserStringMap.keySet()){
            if(userNameIdMap.containsKey(slaTypeUserStringMap.get(slaType))){
                slaTypeUserMap.put(slaType,userNameIdMap.get(slaTypeUserStringMap.get(slaType)));
            }
            else {
                slaTypeUserMap.put(slaType,null);
            }
        }
        // }
        return slaTypeUserMap;
    }

    /** 
    * @Description: Added Logic to apply Different time threshold for Consumer and Commercial Initial Review 
    * @Created: Rexie Aaron David
    * @Params: Sector__c sector
    * @Modification Log =============================================================== 
    * Ver Date Author Modification --- ---- ------ -------------
    * 1.0 9/05/19 RDAVID-task24399056 : Added Logic to apply Different time threshold for Consumer and Commercial Initial Review 
    **/
    
    public static void setConsumerInitialReviewTimeToAppReview(Sector__c sector,BusinessHours busiHours, SLA_Sector__mdt appReviewCustomMetadata, Map<Id,Support_Request__c> srMap, String calledByClass, Integer convertToMinToMS, Datetime warningdate){
        String CONSUMER = 'Consumer';
        //Called in Sector Trigger handler
        if(calledByClass ==  'SectorHandler'){
            if(srMap.get(sector.SR_Number__c) != NULL && sector.Sector1__c.containsIgnoreCase(CommonConstants.SECTOR1_INIT_REV) && srMap.get(sector.SR_Number__c).Case_Number__c != NULL && srMap.get(sector.SR_Number__c).Case_Number__r.RecordTypeId != NULL && srMap.get(sector.SR_Number__c).Case_Number__r.RecordType.Name.containsIgnoreCase(CONSUMER)){
                System.debug('*** Sector is an Initial Review - '+srMap.get(sector.SR_Number__c).Case_Number__r.RecordType.Name);
                Integer slaInMilliseconds = Integer.valueOf(appReviewCustomMetadata.SLA_Target_Time_MM__c) * convertToMinToMS;
                Integer slaInitialWarningInMilliseconds = Integer.valueOf(appReviewCustomMetadata.SLA_Initial_Warning_Time_MM__c) * convertToMinToMS;
                sector.SLA_Due_Date__c = BusinessHours.addGmt (busiHours.id, sector.Sector_Entry__c, slaInMilliseconds); //Populate Due Date considering Business Hours
                sector.SLA_Active__c = 'Yes'; //Set the SLA_Active__c to Yes
                sector.SLA_Warning_Time__c = BusinessHours.addGmt(busiHours.id, sector.Sector_Entry__c, slaInitialWarningInMilliseconds); 
                sector.SLA_Target_Time_MM__c = Integer.valueOf(appReviewCustomMetadata.SLA_Target_Time_MM__c);
            }
        }
        //Called in ProcessBuilder Apex action
        else if (calledByClass == 'SectorSLANextWarning'){
            Integer sectorInterval = Integer.valueOf(appReviewCustomMetadata.SLA_Interval_MM__c);
            system.debug('@@sectorInterval:'+sectorInterval);
            sector.SLA_Active__c = 'Yes';
            if (sector.Sector_Exit__c == NULL){ 
                sector.SLA_Warning_Time__c = BusinessHours.add(busiHours.Id, warningdate, sectorInterval*60000); //Set succeeding warning of current SLA (add 15 minutes)
                system.debug('@@sector.SLA_Warning_Time__c:'+sector.SLA_Warning_Time__c);
                sector.SLA_Time_MM__c = Decimal.valueof(( BusinessHours.diff(busiHours.Id, sector.Sector_Entry__c, warningdate ) / 1000) / 60 ) + 1;  //returns SLA Age in minutes
                if(sector.SLA_Time_MM__c >= Integer.valueOf(appReviewCustomMetadata.SLA_Yellow_Icon_Threshold_MM__c) && sector.SLA_Time_MM__c < Integer.valueOf(appReviewCustomMetadata.SLA_Red_Icon_Threshold_MM__c)) 
                    sector.SLA_Warning_Status__c = 'Yellow SLA Warning Sent';
                else if(sector.SLA_Time_MM__c >= Integer.valueOf(appReviewCustomMetadata.SLA_Red_Icon_Threshold_MM__c)) 
                    sector.SLA_Warning_Status__c = 'Red SLA Warning Sent';
                system.debug('@@slaWarningB');
            }
        }
    }
}