/*
	@Description: class for calculating SLA Age of Initial Review 
	@Author: Jesfer Baculod - Positive Group
	@History:
		- 10/25/2017 - Created
	 	- 15/05/2019 - RDAVID - extra/task24564956-OLDSLACleanUp - Comment this class
*/
public class Case_UpdateSLAWarningInitialReview {
	
	@InvocableMethod(label='SLA Next Warning Time - Initial' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void caseSLAWarningTimeIR (list <ID> cseIDs) {
		/*RDAVID - extra/task24564956-OLDSLACleanUp -
		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Cases to update
		list <Case> cselist = [Select Id, 
								SLA_Time_Initial_Review_Start__c, SLA_Warning_Time_Initial_Review__c, SLA_Started_Count_Initial_Review__c, SLA_Completed_Count_Initial_Review__c, SLA_Active_Initial_Review__c, SLA_Time_Initial_Review_mm__c
								From Case Where Id in : cseIDs];

		Datetime warningdate; 
		for (Case cse : cselist){
			warningdate = cse.SLA_Warning_Time_Initial_Review__c;
			cse.SLA_Warning_Time_Initial_Review__c = null;
			cse.SLA_Active_Initial_Review__c = null;
		}
		update cselist; //force update to retrigger SLA Warning
		system.debug('@@warningdate:'+warningdate);

		for (Case cse : cselist){
			cse.SLA_Active_Initial_Review__c = 'Yes';
			if (cse.SLA_Started_Count_Initial_Review__c != cse.SLA_Completed_Count_Initial_Review__c){
                cse.SLA_Warning_Time_Initial_Review__c = BusinessHours.add(bh.Id, warningdate, 900000); //Set succeeding warning of current SLA (add 15 minutes)
                DateTime warningtimeIR = cse.SLA_Warning_Time_Initial_Review__c;
				cse.SLA_Warning_Time_Initial_Review__c = Datetime.newInstance(warningtimeIR.year(), warningtimeIR.month(), warningtimeIR.day(), warningtimeIR.hour(), warningtimeIR.minute(), 0);
				cse.SLA_Time_Initial_Review_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, cse.SLA_Time_Initial_Review_Start__c, warningdate ) / 1000) / 60 ); //returns SLA Age in minutes
				if (cse.SLA_Time_Initial_Review_mm__c == 14) cse.SLA_Time_Initial_Review_mm__c = 15; //workaround for business hours difference of SLA
				if (cse.SLA_Time_Initial_Review_mm__c == 29) cse.SLA_Time_Initial_Review_mm__c = 30; //workaround for business hours difference of SLA
				system.debug('@@slaWarningA');
			}
		}

		update cselist;
	*/
	}
	
}