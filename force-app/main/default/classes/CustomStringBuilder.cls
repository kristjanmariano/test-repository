/**** To append string to existing string ****/
/**** As stringBuilder class is not available in salesforce ****/
public virtual class CustomStringBuilder
{
    protected String stringValue;
    
    public CustomStringBuilder() {}
    
    /*public CustomStringBuilder(List<String> values)
    {
        add(values);
    }*/
    
    /*public virtual void add(List<String> values)
    {
        for(String value : values)
            add(value);
            add('\n');
    }*/

    public virtual void add(String value)
    {
        stringValue = ( stringValue==null ? value : stringValue + value +'\n');
    }

    public virtual String getStringValue()
    {
        return stringValue;
    }
}