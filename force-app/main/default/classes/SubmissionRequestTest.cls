@isTest
public class SubmissionRequestTest {
    
    @isTest static void subTest(){

    // Create Account
        Account acc = new Account();
        acc.FirstName = 'Test';
        acc.LastName = 'Test';        
        acc.PersonMobilePhone = '12345';
        acc.PersonEmail = 'test@test.com';
        acc.Primary_Country_of_Citizenship__c='India';
        acc.Additional_Previous_Address_Information__c ='Greece';
        acc.Current_Street_Number__c = '1';
        acc.Current_Street_Name__c='Smith';
        acc.Current_Street_Type__c='Street';
        acc.Current_Suburb__c='Brunswick';
        acc.Current_State__c='Victoria';
        acc.Current_Postcode__c=1234;
        acc.Current_Country__c='Australia';
        acc.Residential_Status__c = 'Mortgage';
        acc.Medicare_Number__c = '12345';
        insert acc;
        
        acc.Primary_Country_of_Citizenship__c ='Australia';
        update acc;
        
    //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.name = 'Test';
        opp.stageName = 'open';
        opp.Application_Type__c = 'Joint Application';
        opp.Loan_Type2__c = '';
        opp.Loan_Product__c = 'Consumer Loan';
        opp.Loan_Amount__c=null;
        opp.Bankruptcy__c='No';
        opp.Length_of_time_in_employment__c='20';
        opp.Available_Income__c=1000;
        opp.Income_Frequency__c='monthly';
        opp.closeDate = date.today();
        opp.AccountId=acc.Id;
                
        opp.Lender__c = 'Liberty';
        opp.Payment_Frequency__c='monthly';
        opp.Previous_Bankrupt__c = 'Yes';
        opp.Net_Trade_In__c = 0;
        opp.Available_Deposit__c = 100;
        opp.Partner_Pay_Slip_Obtained__c = true;
        opp.Trade_In_Description__c ='';
        opp.All_Other__c = 10;
        opp.Entertainment__c = 10;
        opp.Utilities_Rates__c = 10;
        opp.Education__c = 10;
        opp.Phone_TV_Internet__c = 10;
        opp.Insurance__c = 10;
        opp.Child_Expenses__c = 10;
        opp.Petrol_Public_Transport__c = 10;
        opp.Groceries_Food__c = 10;
        insert opp;

        List<Submission_Request__c> subList = new List<Submission_Request__c>();
        Submission_Request__c subObj = new Submission_Request__c();
        subObj.Purchase_Amount__c = 100;
        subObj.Opportunity__c=opp.Id;
        subObj.Account_FirstName__c = 'Test';
        subObj.Account_LastName__c='test';
        subObj.Additional_Previous_Address_Information__c='Lucknow';
        subObj.Previous_Bankrupt__c = 'Yes';
        subObj.Loan_Product__c = 'Consumer Loan';
        
        subList.add(subObj);        
        insert subList;
            
        Employee__c emp = new Employee__c (Name = 'Abc', Contact_Phone__c = '1234534', 
        Employment_Duration__c = '10', 
        Current_Employment__c = 'Yes',  Account__c =acc.id);
        insert emp;
        
        Assets__c asst = new Assets__c (Asset_Type__c = 'Test', Value__c = 1000, Account__c = acc.Id);
        insert asst;
        
        Liability__c lib = new Liability__c(Monthly_Payment__c=100, Account__c =acc.Id);
        insert lib;
         
        Reference__c ref = new Reference__c(Account__c = acc.Id );
        
        insert ref;
        
        Applicant_2__c appliObj=new Applicant_2__c(First_Name__c='ads', Last_Name__c='qwewq', Title__c='mr', 
        Gender__c='male', Date_of_Birth__c=date.today()-1000, Permanent_Resident_Status__c='yes', 
        Marital_Status__c='single', Account__c=acc.id);
        insert appliObj;
        
        Employee__c emp2 = new Employee__c (Name = 'Abc', Contact_Phone__c = '1234534', 
        Employment_Duration__c = '10', 
        Current_Employment__c = 'Yes',  Applicant2__c =appliObj.id);
        
        insert emp2;
        apexpages.hasMessages();
        
        ApexPages.StandardController sub = new ApexPages.StandardController(opp);
        SubmissionRequest subReq = new SubmissionRequest(sub);

        subReq.preApproval=false;
        subReq.lenderRating='None';
        subReq.getlenderRatings();
        subReq.requestedWritingRate='';
        subReq.notesForSupport='';
        subReq.notesForLender='';
        subReq.grossAnnualIncome=100;
        subReq.latitudePrivacyAccepted=false;
        subReq.doYouKnowVendorDealerName = 'Yes';
        subReq.empDescription = 'Current';
        subReq.getknowDealerNames();
        subReq.haveChecked3MonthsOfStatements = false;
        subReq.haveCheckedIncomeOnPayslips = false;
        subReq.haveSignedCreditGuideAndQuote = false;
        subReq.haveSignedCreditGuideAndQuote = false;
        subReq.haveSignedPrivacyConsent = false;
        subReq.positiveCalcInFile = false;
        subReq.whySpouseNotOnLoan = '';
        subReq.servicingCalcExpMatchLiabilities = false;
        subReq.suppDocsCombined = false;
        subReq.errorMessages = false;
                
        subReq.OpportunityId();
        subReq.readyforSub();
        subReq.saveUpdates();
        subReq.submit();
   
    }
    @isTest static void subTest1(){
    
        // Create Account
        Account acc = new Account();
        acc.FirstName = 'Test';
        acc.LastName = 'Test';        
        acc.PersonMobilePhone = '12345';
        acc.PersonEmail = 'test@test.com';
        acc.Primary_Country_of_Citizenship__c='Australia';
        acc.Additional_Previous_Address_Information__c ='Greece';
        acc.Current_Street_Number__c = '1';
        acc.Current_Street_Name__c='Smith';
        acc.Current_Street_Type__c='Street';
        acc.Current_Suburb__c='Brunswick';
        acc.Current_State__c='Victoria';
        acc.Current_Postcode__c=1234;
        acc.Current_Country__c='Australia';
        acc.Residential_Status__c = 'Mortgage';
        acc.Medicare_Number__c = '12345';
        insert acc;
        
    //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.name = 'Test';
        opp.stageName = 'open';
        opp.Application_Type__c = 'Joint Application';
        opp.Loan_Type2__c = '';
        opp.Loan_Product__c = 'Consumer Loan';
        opp.Loan_Amount__c=null;
        opp.Bankruptcy__c='No';
        opp.Length_of_time_in_employment__c='20';
        opp.Available_Income__c=1000;
        opp.Income_Frequency__c='monthly';
        opp.closeDate = date.today();
        opp.AccountId=acc.Id;
        opp.Payment_Frequency__c='monthly';
        opp.Lender__c ='Pepper';
        opp.Previous_Bankrupt__c= 'Yes';
        opp.Net_Trade_In__c = 0;
        opp.Available_Deposit__c = 100;
        opp.Partner_Pay_Slip_Obtained__c = true;
        opp.Trade_In_Description__c ='';
        insert opp;
        
        List<Submission_Request__c> subList = new List<Submission_Request__c>();
        Submission_Request__c subObj = new Submission_Request__c();
        subObj.Purchase_Amount__c = 100;
        subObj.Opportunity__c=opp.Id;
        subObj.Account_FirstName__c = 'Test';
        subObj.Account_LastName__c='test';
        subObj.Additional_Previous_Address_Information__c='Lucknow';
        subObj.Previous_Bankrupt__c = 'Yes';
        subObj.Loan_Product__c = 'Consumer Loan';
        
        subList.add(subObj);        
        insert subList;

        ApexPages.StandardController sub = new ApexPages.StandardController(opp);
        SubmissionRequest subReq = new SubmissionRequest(sub);
        subReq.lenderRating='AA';
        subReq.getlenderRatings();
                
    }
    @isTest static void subTest2(){
    
        // Create Account
        Account acc = new Account();
        acc.FirstName = 'Test';
        acc.LastName = 'Test';        
        acc.PersonMobilePhone = '12345';
        acc.PersonEmail = 'test@test.com';
        acc.Primary_Country_of_Citizenship__c='Australia';
        acc.Additional_Previous_Address_Information__c ='Greece';
        acc.Current_Street_Number__c = '1';
        acc.Current_Street_Name__c='Smith';
        acc.Current_Street_Type__c='Street';
        acc.Current_Suburb__c='Brunswick';
        acc.Current_State__c='Victoria';
        acc.Current_Postcode__c=1234;
        acc.Current_Country__c='Australia';
        acc.Residential_Status__c = 'Mortgage';
        acc.Medicare_Number__c = '12345';
        insert acc;
        
    //Create Opportunity
        Opportunity opp = new Opportunity();
        opp.name = 'Test';
        opp.stageName = 'open';
        opp.Application_Type__c = 'Joint Application';
        opp.Loan_Type2__c = '';
        opp.Loan_Product__c = 'Consumer Loan';
        opp.Loan_Amount__c=null;
        opp.Bankruptcy__c='No';
        opp.Length_of_time_in_employment__c='20';
        opp.Available_Income__c=1000;
        opp.Income_Frequency__c='monthly';
        opp.closeDate = date.today();
        opp.AccountId=acc.Id;
        opp.Payment_Frequency__c='monthly';
        opp.Lender__c ='E-Motors';
        opp.Previous_Bankrupt__c= 'Yes';
        opp.Net_Trade_In__c = 0;
        opp.Available_Deposit__c = 100;
        opp.Partner_Pay_Slip_Obtained__c = true;
        opp.Trade_In_Description__c ='';
        insert opp;
        
        List<Submission_Request__c> subList = new List<Submission_Request__c>();
        Submission_Request__c subObj = new Submission_Request__c();
        subObj.Purchase_Amount__c = 100;
        subObj.Opportunity__c=opp.Id;
        subObj.Account_FirstName__c = 'Test';
        subObj.Account_LastName__c='test';
        subObj.Additional_Previous_Address_Information__c='Lucknow';
        subObj.Previous_Bankrupt__c = 'No';
        subObj.Loan_Product__c = 'Consumer Loan';
        
        subList.add(subObj);        
        insert subList;
        
        ApexPages.StandardController sub = new ApexPages.StandardController(opp);
        SubmissionRequest subReq = new SubmissionRequest(sub);
        subReq.lenderRating='AAA';
        subReq.getlenderRatings();
        
    }
}