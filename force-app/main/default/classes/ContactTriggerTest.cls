@isTest
public class ContactTriggerTest {
    @testsetup static void setup(){
        Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Application_Sync_App_2_Case__c = true;
		triggerSettings.Enable_Application_Trigger__c= true;
		triggerSettings.Enable_Asset_Trigger__c= true;
		triggerSettings.Enable_Case_Auto_Create_Application__c= true;
		triggerSettings.Enable_Case_Scenario_Trigger__c= true;
		triggerSettings.Enable_Case_SCN_calculateSLATime__c= true;
		triggerSettings.Enable_Case_SCN_recordStageTimestamps__c= true;
		triggerSettings.Enable_Case_Trigger__c= true;
		triggerSettings.Enable_Expense_Trigger__c= true;
		triggerSettings.Enable_FO_Auto_Create_FO_Share__c= true;
		triggerSettings.Enable_FOShare_Rollup_to_Role__c= true;
		triggerSettings.Enable_FOShare_Trigger__c= true;
		triggerSettings.Enable_Income_Trigger__c= true;
		triggerSettings.Enable_Lender_Automation_Call_Lender__c= true;
		triggerSettings.Enable_Liability_Trigger__c= true;
		triggerSettings.Enable_Role_Address_Populate_Address__c= true;
		triggerSettings.Enable_Role_Address_Toggle_Current__c= true;
		triggerSettings.Enable_Role_Address_Trigger__c= true;
		triggerSettings.Enable_Role_Auto_Populate_Case_App__c= true;
		triggerSettings.Enable_Role_Rollup_to_Application__c= true;
		triggerSettings.Enable_Role_To_Account_Propagation__c= true;
		triggerSettings.Enable_Role_Toggle_Primary_Applicant__c= true;
		triggerSettings.Enable_Role_Trigger__c= true;
		triggerSettings.Enable_Triggers__c= true;
        triggerSettings.Enable_Account_Sync_Name_to_Role__c=true;
        triggerSettings.Enable_Contact_Trigger__c =true;
		triggerSettings.Enable_Partner_Sync_Account_Contact__c=true;
		upsert triggerSettings Trigger_Settings1__c.Id;
    }

    public static testMethod void testContact() {
        
        try{
        Contact c = new Contact();
        Opportunity opp = new Opportunity();
        opp.Name = 'test';
        opp.StageName ='Open';
        opp.CloseDate = date.today();
        insert opp;
        
        Contact co = new Contact();
        co.FirstName ='Test';
        co.LastName = 'Test';
        co.Opportunity__c = opp.Id;
        insert co;
        
        opp.contact__c= co.id;
        update opp;
        }catch(exception e){
        }
    }

    public static testMethod void testPartnerSync() {
        try{
            Referral_Company__c connective = new Referral_Company__c(Name = 'Connective', Company_Type__c = 'Mortgage Aggregator', Rating__c = 'Committed');
            insert connective;
            Referral_Company__c refCompany = new Referral_Company__c(Name = 'P1551512', Company_Type__c = 'Mortgage Broker', Rating__c = 'Cold', Related_Site__c = connective.Id);
            insert refCompany;
            Id connectiveBrokerRTId = SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Connective_Broker').getRecordTypeId();
            Contact co = new Contact(Role__c = 'Broker', RecordTypeId = connectiveBrokerRTId, Referral_Company__c = refCompany.Id);
            co.FirstName ='Test';
            co.LastName = 'Test';
            insert co;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            co.FirstName ='Test1';
            update co;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            delete co;
        }
        catch(exception e){
        }
    }
}