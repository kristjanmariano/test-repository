/** 
* @FileName: ApplicationHandler
* @Description: Trigger Handler for the Case SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/05/18 JBACULOD Created Class, Sync Application to Case on Record Type change 
* 1.1 5/06/18 JBACULOD Modified Sync Application to Case, included additional fields to sync
* 1.2 7/25/18 JBACULOD get all Applicant Roles populating Primary Applicant field
* 1.3 6/11/19 JBACULOD Added Retry and Record Locking FOR UPDATE on Case
**/ 
public without sharing class ApplicationHandler implements ITrigger {
    
    private static Map<Id,Schema.RecordTypeInfo> appRTMapById = Schema.SObjectType.Application__c.getRecordTypeInfosById();
    private Map <id, Case> CaseToSyncMap = new map <id, Case>();
    private Map <id, list <Role__c>> appRolesByAppMap = new map <id, list <Role__c>>();
    
    public ApplicationHandler() {}
    
    /** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){

        if (Trigger.isUpdate){
            if (TriggerFactory.trigSet.Enable_Application_Primary_Applicants__c){
                //Get all Roles on each Application
                for (Role__c role : [Select Id, Name, Application__c, RecordTypeId, RecordType.Name, Account_Name__c From Role__c Where Application__c in : trigger.new]){
                    if (!appRolesByAppMap.containskey(role.Application__c)) appRolesByAppMap.put(role.Application__c, new list <Role__c>());
                    appRolesByAppMap.get(role.Application__c).add(role);
                }
            }
        }

    }
    
    public void bulkAfter(){

        if (Trigger.isUpdate){
            if (TriggerFactory.trigSet.Enable_Application_Sync_App_2_Case__c){
                //Syncs Application Data to Case
                CaseToSyncMap = ApplicationGateway.SyncApp2Case(trigger.OldMap, trigger.new, appRTMapById);
            }
        }

    }
    
    public void beforeInsert(SObject so){
    }
    
    public void beforeUpdate(SObject oldSo, SObject so){
        Application__c oldapp = (Application__c) oldSo;
        Application__c app = (Application__c) so;

        if (!appRolesByAppMap.isEmpty()){
            //Populates Primary Applicant with all Applicant Roles under each Application
            ApplicationGateway.populatePrimaryApplicant(oldapp, app, appRolesByAppMap);
        }
    }
    
    public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){

	}
	
	public void afterUpdate(SObject oldSo, SObject so){
        
	}
	
	public void afterDelete(SObject so){
	}
    
    /** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
        
        //Sync Application to Case
        system.debug('@@CaseToSyncMap:'+CaseToSyncMap);
        if (CaseToSyncMap.size() > 0){
            try{
                list <Case> CasesLockForUpdate = [Select Id From Case Where Id in : CaseToSyncMap.keySet() FOR UPDATE]; //JBACU 6/11/19
                Database.update(CaseToSyncMap.values());
            }
            catch(Exception e){
                Database.update(CaseToSyncMap.values()); //Retry due to UNABLE_TO_LOCK_ROW issue
            }
        }
    }

}