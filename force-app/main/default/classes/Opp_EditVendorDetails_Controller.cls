/*
    @author: Jesfer Baculod - Positive Group
    @history: 07/24/17 - Created
    		  07/26/17 - Private Sale Vendor Contact accepts Individual Person Account Contact
    		  07/28/17 - Added Company Type filter on searchReferralCompany due to Partner Referral Companies
              08/02/17 - Updated Modified Referral Company, Key Person Contact Create/Search Screens, Additional Indicators
              08/07/17 - Added Duplicate check on creating Referral Company
              11/07/17 - Updated Registed ABN Handling
    @description: controller extension class of Opp_EditVendorDetails page.
*/
public class Opp_EditVendorDetails_Controller {

	public string opp_DV {get;set;}
    public string opp_DC {get;set;}
    public string opp_VC {get;set;}

    public string opp_DV_id {get;set;}
    public string opp_DC_id {get;set;}
    public string opp_VC_id {get;set;}
    public string opp_s_ID {get;set;} //gen search lookup ID

    public string errmsg {get;set;}
    public string dupeoption {get;set;}
    public string nullparam {get;set;}

    public Opportunity opp {get;set;}
    public Referral_Company__c refcomp {get;set;}
    public ABN__c inputABN {get;set;}
    public ABN__c currentABN {get;set;}
    public Referral_Company__c exrefcomp {get;set;}
    public ABN__c exABN {get;set;}
    public Contact bcon {get;set;} //Business Contact
    public Contact icon {get;set;} //Individual Contact
    public Account iacc {get;set;} //Individual Person Account

    public boolean isCreateABN {get;set;}
    public boolean showRefCompCre {get;set;}
    public boolean showBConCre {get;set;}
    public boolean showIconCre {get;set;}
    public boolean showRefCompS {get;set;}
    public boolean showBConS {get;set;}
    public boolean showIConS {get;set;}
    public boolean noRefComp {get;set;}
    public boolean saveSuccess {get;set;}
    public boolean saveSuccessBCon {get;set;}
    private boolean withExistingVD {get;set;}
    public boolean showDuplicateRC {get;set;}

    private static final string CON_RT_BUSINESS = Label.Contact_Business_RT; //Business
    private static final string CON_RT_INDIVIDUAL = Label.Contact_Individual_RT;

    private static final string SALE_TYPE_PRIVATE = Label.Opp_Sale_Type_Private; //'Private';
    private static final string SALE_TYPE_DEALER = Label.Opp_Sale_Type_Dealer; //'Dealer';

    private static final string ERR_MSG_1 = Label.Case_Edit_Vendor_Details_Error_Message_1;  //Dealer Sale Vendor & Dealer Contact Person are required for Dealer Sale Type
    private static final string ERR_MSG_2 = Label.Case_Edit_Vendor_Details_Error_Message_2;  //Dealer Sale Vendor is required for Dealer Sale Type
    private static final string ERR_MSG_3 = Label.Case_Edit_Vendor_Details_Error_Message_3;  //Dealer Contact Person is required for Dealer Sale Type
    private static final string ERR_MSG_4 = Label.Case_Edit_Vendor_Details_Error_Message_4;  //Private Sale Vendor Contact is required for Private Sale Type


    private Id rt_c_b {get;set;} //record type variable for Business Contact
    private Id rt_c_i {get;set;} //record type variable for Invidividual Person Account
    private Id rt_c_ic {get;set;} //record type variable for Individual Contact

    public integer searchMode {get;set;} //on which field does the user is doing the search

    //pagination of Dealer Sale Vendor - Referral Company
    private string RCquery {get;set;}
    public Integer RC_size {get;set;} 
    public Integer RC_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConRC { 
        get {
            if(setConRC == null) {                
                setConRC = new ApexPages.StandardSetController(Database.getQueryLocator(RCquery));
                setConRC.setPageSize(RC_size);  
                RC_noOfRecords = setConRC.getResultSize();
            }            
            return setConRC;
        }
        set;
    }

    //pagination of Dealer Contact Person - Business Contact
    private string BConquery {get;set;}
    public Integer BCon_size {get;set;} 
    public Integer Bcon_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConBcon {
        get {
            if(setConBcon == null) {                
                setConBcon = new ApexPages.StandardSetController(Database.getQueryLocator(BConquery));
                system.debug('@@setConBCon:'+setConBCon);
                system.debug('@@BCon_size:'+BCon_size);
                if (BCon_size == null) BCon_size = 5;
                setConBcon.setPageSize(BCon_size);  
                Bcon_noOfRecords = setConBcon.getResultSize();
            }            
            return setConBcon;
        }
        set;
    }

    //pagination of Private Sale Vendor Contact - Individual Account
    private string IConquery {get;set;}
    public Integer ICon_size {get;set;} 
    public Integer ICon_noOfRecords {get; set;} 
    public ApexPages.StandardSetController setConIcon { 
        get {
            if(setConIcon == null) {                
                setConIcon = new ApexPages.StandardSetController(Database.getQueryLocator(Iconquery));
                setConIcon.setPageSize(Icon_size);  
                Icon_noOfRecords = setConIcon.getResultSize();
            }            
            return setConIcon;
        }
        set;
    }

    public Opp_EditVendorDetails_Controller(ApexPages.StandardController stdController) {
        errmsg = opp_DV = opp_DC = opp_VC = '';
        showDuplicateRC = withExistingVD = noRefComp = saveSuccess = saveSuccessBcon = false; 
        searchMode = 1;  //default to Dealer Sale Vendor search
        Icon_size = BCon_size = RC_size = 5; RCquery = BConquery = Iconquery = ''; 
        showRefCompCre = showBConCre = showIconCre = showRefCompS = showBConS = showIConS = false;
        refcomp = new Referral_Company__c(OwnerId = UserInfo.getUserId());
        inputABN = new ABN__c();
        rt_c_b = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_BUSINESS).getRecordTypeId();
        rt_c_i = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CON_RT_INDIVIDUAL).getRecordTypeId();
        rt_c_ic = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(CON_RT_INDIVIDUAL).getRecordTypeId();
        bcon = new Contact(RecordTypeId = rt_c_b);
        iacc = new Account(RecordTypeId = rt_c_i);
        icon = new Contact(); //(RecordTypeId = rt_c_ic);
        this.opp = (Opportunity)stdController.getRecord();
        this.opp = [Select Id, Name, Sale_Type__c, StageName,
                    Dealer_Contact__c, Dealer_Contact__r.Name, Dealer_Vendor__c, Dealer_Vendor__r.Name, Vendor_Contact__c, Vendor_Contact__r.Name, //new Vendor fields
                    Dealer_Name__c, Dealer_Contact_Person__c, Dealer_Mobile__c, Dealer_Phone__c, Dealer_Address__c, Dealer_Email__c //old Vendor fields
                    From Opportunity Where Id = : stdController.getId()];

        //Retrieve existing Vendor Details
        if (opp.Sale_Type__c == SALE_TYPE_PRIVATE){ //search for all Individual Contacts
        	if (opp.Vendor_Contact__c != null){
	            opp_VC = opp.Vendor_Contact__r.Name;
	            opp_VC_id = opp.Vendor_Contact__c;
	        } 
            searchMode = 3;
            showIconS = true;
            showRefCompS = false;
            withExistingVD = true;
            searchIndividualContact();
            withExistingVD = false;
        }
        else if (opp.Sale_Type__c == SALE_TYPE_DEALER){ //search for all Referral Companies 
        	if (opp.Dealer_Vendor__c != null){
	            opp_DV = opp.Dealer_Vendor__r.Name;
	            opp_DV_id = opp.Dealer_Vendor__c; 
	            //searchReferralCompany();
	            //showRefCompS = true;
	        }
	        if (opp.Dealer_Contact__c != null){
	            opp_DC = opp.Dealer_Contact__r.Name;
	            opp_DC_id = opp.Dealer_Contact__c;
	        }
            searchMode = 1;
            showRefCompS = true;
            withExistingVD = true;
            searchReferralCompany();
            withExistingVD = false;
        }


    }

    public void retrieveCurrentABN(){
        currentABN = [Select Id, Name, Registered_ABN__c From ABN__c Where Id =: refComp.Legal_Entity_Name__c];
        system.debug('@@currentABN:'+currentABN);
    }


    //Changes the size of pagination
    public PageReference refreshPageSize() {
         if (searchMode == 1 ) setConRC.setPageSize(RC_size); //Searches in Referral Company
         else if (searchMode == 2) setConBcon.setPageSize(BCon_size); //Searches in Business Contact
         else if (searchMode == 3) setConIcon.setPageSize(ICon_size); //Searches in Individual Person Account Contact
         return null;
    }

    //Sets Id on selected record
    public void assignLookupID(){
        if (searchMode == 1) opp_DV_id = opp_s_ID;
        if (searchMode == 2) opp_DC_id = opp_s_ID;
        if (searchMode == 3) opp_VC_id = opp_s_ID;
        showRefCompCre = false;
        showBConCre = false;
        showIconCre = false;
    }

    //Clear all inputted data on Create Screens
    public void clearFormfields(){
        system.debug('@@opp_DV_id:'+opp_DV_id);
        refcomp = null;
        bcon = null; Icon = null;
        iAcc = null;
        saveSuccess = false;
        saveSuccessBcon = false;
        currentABN = new ABN__c();
        inputABN = new ABN__c();
        isCreateABN = false;
        exrefcomp = new Referral_Company__c();
        exABN  = new ABN__c();
        refcomp = new Referral_Company__c(OwnerId = UserInfo.getUserId());
        bcon = new Contact(RecordTypeId = rt_c_b);
        if (opp_DV_id != null && opp_DV_id != '') bcon.Referral_Company__c = opp_DV_id; //assign selected Referral Company
        icon = new Contact(); //(RecordTypeId = rt_c_ic);
        Iacc = new Account(RecordTypeId = rt_c_I);
    }

    public List<Referral_Company__c> getsearchRefComplist(){
         return (List<Referral_Company__c>) setConRC.getRecords();
    }

    public List<Contact> getsearchBConlist(){
         return (List<Contact>) setConBcon.getRecords();
    }

    public List<Contact> getsearchIconlist(){
         return (List<Contact>) setConIcon.getRecords();
    }


    //method for searching Dealer Sale Vendor - Referral Company
    public PageReference searchReferralCompany(){
        if (!withExistingVD) opp_DV_Id = null;
        setConRC = null; saveSuccess = false;
        string srchvar = '%' + opp_DV + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        RCquery = 'Select Id, Name, Address_Line_1__c, Company_Type__c, State__c, Dealer_Type__c, Company_Website__c, Primary_Contact_s_Name__c, Primary_Contact_s_Mobile__c, Last_Contact_Date__c From Referral_Company__c Where Name like \'' + srchvar + '\' AND Company_Type__c != \'Mortgage Broker\' Order by Name ASC';
        getsearchRefComplist();
        setConRC.setpageNumber(1);
        return null;
    }

    //method for searching Dealer Contact Person - Business Contact
    public PageReference searchBusinessContact(){
        opp_DC_Id = null;
        setConBcon = null; saveSuccess = false;
        if (opp_DV_id == '' || opp_DV_id == null){
            noRefComp = true;
        }
        else{ 
            noRefComp = false;
            bcon.Referral_Company__c = opp_DV_id; //assign selected Referral Company
        }
        system.debug('@@opp_DV_id:'+opp_DV_id);
        system.debug('@@noRefComp:'+noRefComp);
        string srchvar = '%' + opp_DC + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        Bconquery = 'Select Id, Name, FirstName, LastName, MobilePhone, Email, RecordTypeId, RecordType.Name, Referral_Company__c, CreatedDate From Contact Where Name like \'' + srchvar + '\' AND Referral_Company__c = : opp_DV_id AND RecordTypeId = : rt_c_b Order by Name ASC';
        getsearchBConlist();
        setConBCon.setpageNumber(1);
        return null;
    }

    //method for searching Primary Seller Vendor - Individual Account
    public PageReference searchIndividualContact(){
        if (!withExistingVD) opp_VC_Id = null;
        setConIcon = null; saveSuccess = false;
        string srchvar = '%' + opp_VC + '%';
        srchvar = String.escapeSingleQuotes(srchvar);
        Iconquery = 'Select Id, Name, FirstName, LastName, MobilePhone, Email, Street_Number__c, Street_Name__c, Street_Type__c, Suburb__c, State__c, Postcode__c, Country__c, RecordTypeId, RecordType.Name, HomePhone, CreatedDate From Contact Where Name like \'' + srchvar + '\' AND RecordTypeId = null Order By Name ASC limit 1000'; //: rt_c_ic Order by Name ASC limit 9999';
        getsearchIconlist();
        setConIcon.setpageNumber(1);
        return null;
    }


    //method for assigning existing Referral Company
    public void assignExistingReferralCompany(){
            opp_DV = exrefcomp.Name;
            opp_DV_id = exrefcomp.Id;
            saveSuccess = true;
            showRefCompCre = false;
            showBConCre = true;
            showDuplicateRC = false;
    }

    //method for saving New Dealer Sale Vendor
    public void saveReferralCompany(){
        Savepoint sp = Database.setSavepoint();
        try{
            system.debug('@@inputABN:'+inputABN);
            if (isCreateABN){
                insert inputABN;
                refcomp.Legal_Entity_Name__c = inputABN.Id;
            }
            else refcomp.Legal_Entity_Name__c = currentABN.Id;

            insert refcomp; 
            //Assign created Referral Company to Sale Dealer Vendor
            opp.Dealer_Vendor__c = refcomp.Id;
            opp_DV = refcomp.Name;
            opp_DV_id = refcomp.Id;
            refcomp = new Referral_Company__c(); 
            bcon = new Contact(RecordTypeId = rt_c_b);
            if (opp_DV_id != null && opp_DV_id != '') bcon.Referral_Company__c = opp_DV_id; //assign selected Referral Company
            saveSuccess = true;
            showRefCompCre = false;
            showBConCre = true;
            dupeoption = '0';
        }
        catch(Exception e){
            Database.rollback( sp );
            string rname = '';
            if (e.getMessage().contains('DUPLICATES_DETECTED') || e.getMessage().contains('DUPLICATE_VALUE') ){
                system.debug('@@fullmessage:'+e.getMessage());
                if (e.getMessage().contains('Referral Company already exists')){
                    //if (!isCreateABN){
                        exrefcomp = new Referral_Company__c();
                        exrefcomp = [Select Id, Name, Address_Line_1__c, Company_Type__c, State__c, Dealer_Type__c, Company_Website__c, Primary_Contact_s_Name__c, Primary_Contact_s_Mobile__c, Last_Contact_Date__c, Registered_ABN_F__c
                                From Referral_Company__c Where Name = : refcomp.Name limit 1];
                        rname = exrefComp.Name;
                        //Referral Company already exists + RC Name + Click link to select existing Referral Company
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Referral_Company_Msg_1) + rName + String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Referral_Company_Msg_2) ));
                        dupeoption = '1';
                        inputABN = new ABN__c();
                        system.debug('@@duperefcomplevel');
                    //}
                }
                if (e.getMessage().contains('Registered_ABN__c duplicates') || e.getMessage().contains('Legal Entity Name already exists')){
                    list <ABN__c> exABNlist = [Select Id, Name, Registered_ABN__c From ABN__c Where (Registered_ABN__c = : inputABN.Registered_ABN__c OR Name = : inputABN.Name) ];
                    if (exABNlist .size() > 0){ 
                        exABN = exABNlist[0];
                        rname = exABN.Name;
                        isCreateABN = false;
                        inputABN = new ABN__c();
                        //Legal Entity already exists + ABN Name + Click link to select existing Legal Entity
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Legal_Entity_Msg_1) + rName + String.escapeSingleQuotes(Label.Edit_Vendor_Details_Duplicate_Legal_Entity_Msg_2) ));
                        dupeoption = '2';
                        system.debug('@@dupeABNlevel');
                    }
                }
                showDuplicateRC = true;
            }
            saveSuccess = false;
        }
    }

    //method for saving New Dealer Contact Person
    public void saveBusinessContact(){
        Savepoint sp = Database.setSavepoint();
        try{
            insert bcon; 
            //Assign created Business Contact
            opp.Dealer_Contact__c = bcon.Id;
            string confname = '';
            if (bcon.FirstName != null) confname = bcon.FirstName + ' ';
            opp_DC = confname + bcon.LastName;
            opp_DC_id = bcon.Id;
            //bcon = new Contact();
            saveSuccess = true;
            saveSuccessBcon = true;
            system.debug('@@saveSuccessBCon:'+saveSuccessBCon);
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
            saveSuccessBcon = false;
        }
    }

    //method for saving New Primary Sale Vendor Contact
    public void saveIndividualContact(){
        Savepoint sp = Database.setSavepoint();
        try{
        	iacc.LastName = icon.LastName;
        	iacc.Firstname = icon.FirstName;
            insert iacc; 

            //Retrieve Individual Contact equivalent from Individual Person Account
            Contact upicon = [Select Id, FirstName, LastName From Contact Where AccountId = : iacc.Id];
            system.debug('@@upicon:'+upicon);

            //Assign created Individual Contact
            opp.Vendor_Contact__c = upicon.Id;
            string confname = '';
            if (upicon.FirstName != null) confname = upicon.FirstName + ' ';
            opp_VC = confname + upicon.LastName;
            opp_VC_id = upicon.Id;
            //i = new Contact();
            saveSuccess = true;
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
        }
    }

    //method for updating Opportunity with Vendor Details
    public PageReference saveOpp(){
        Savepoint sp = Database.setSavepoint();
        errmsg = '';
        try{    
            system.debug('@@opp_DV_id:'+opp_DV_id);
            if (opp_DV_id != '') opp.Dealer_Vendor__c = opp_DV_id; //assign Referral Company ID
            else opp.Dealer_Vendor__c = null;
            if (opp_DC_id != '') opp.Dealer_Contact__c = opp_DC_id; //assign Business Contact ID
            else opp.Dealer_Contact__c = null;
            if (opp_VC_id != '') opp.Vendor_Contact__c = opp_VC_id; //assign Individual Contact ID
            else opp.Vendor_Contact__c = null; 
            if (opp.Sale_Type__c == SALE_TYPE_DEALER){
                //Validate Dealer fields
                if (opp.Dealer_Vendor__c == null && opp.Dealer_Contact__c == null){ 
                    errmsg = ERR_MSG_1;
                    return null;
                }
                else if (opp.Dealer_Vendor__c == null && opp.Dealer_Contact__c != null){
                    errmsg = ERR_MSG_2;
                    return null;
                }
                else if (opp.Dealer_Vendor__c != null && opp.Dealer_Contact__c == null){
                    errmsg = ERR_MSG_3;
                    return null;
                }
            }
            else if (opp.Sale_Type__c == SALE_TYPE_PRIVATE){
                //Validate Private Sale fields
                if (opp.Vendor_Contact__c == null){
                    errmsg = ERR_MSG_4;
                    return null;
                }
            }
            update opp;
            saveSuccess = true;
            //return new PageReference('/'+ opp.Id);
            return null;
        }
        catch(Exception e){
            Database.rollback( sp );
            saveSuccess = false;
            errmsg = e.getMessage();
            return null;
        }
    }



}