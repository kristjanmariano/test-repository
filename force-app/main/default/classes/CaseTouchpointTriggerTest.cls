/** 
* @FileName: CaseTouchpointTriggerTest
* @Description: test class for CaseTouchpointHandler
* @Copyright: Positive (c) 2019
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 03/27/19 JBACULOD Added testBizibleFBLeadUTM
**/ 
@isTest
public class CaseTouchpointTriggerTest {
    @testSetup static void setupData() {

		//Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Case_TP_Trigger__c = true;
        triggerSettings.Enable_Case_TP_Map_Delacon_Lead__c = true;
		upsert triggerSettings Trigger_Settings1__c.Id;

        //Create Test Lead
        Lead delaconLead = new Lead(
            LastName = 'Delacon Test Lead',
            DELAPLA__PLA_Caller_Phone_Number__c = '0812345678',
            DELAPLA__PLA_Dealer_Reference__c = '111111 - Equipment Finance - Google Adwords',
            DELAPLA__PLA_Website_Landing_Page__c = 'https://positivelendingsolutions.com.au',
            DELAPLA__PLA_City__c = 'ADELAIDE',
            DELAPLA__PLA_Device_Used__c = 'iPad',
            DELAPLA__PLA_Keywords_Searched__c  = 'test',
            LeadSource = 'Delacon',
            Email = 'test@delaconlead.com'
        );
        insert delaconLead;

		//Create Test Account
		Account acc = new Account(FirstName = 'FName', LastName = 'LName');
        acc.RecordTypeId = [Select Id From RecordType Where Name = : CommonConstants.PACC_RT_I].Id;
        acc.PersonMobilePhone = '0812345678';
        acc.From_Positive_Path__c = true;
        acc.PersonEmail = 'test@delaconlead.com';
		Database.insert(acc);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        caselist[0].Primary_Contact_Name_MC__c = acc.Id;
        caselist[0].Primary_Contact_Phone_MC__c = acc.PersonMobilePhone;
        update caselist[0];
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, acc.Id, appList[0].Id,  1);
		Database.insert(roleList);

	}

    static testMethod void testBizibleDelaconLead(){

        list <Account> acclist = [Select Id, From_Positive_Path__c, PersonMobilePhone, PersonContactId From Account];
        list <Case> cselist = [Select Id, CaseNumber, Primary_Contact_Name_MC__c From Case Where Primary_Contact_Name_MC__c != null];

        //Create Test Frontend Submission
        list <Frontend_Submission__c> felist = new list <Frontend_Submission__c>();
        Frontend_Submission__c fe = new Frontend_Submission__c(
                FB_Utm_Source__c = 'facebook',
                Case__c = cselist[0].Id
            );  
        felist.add(fe);
        insert felist;
        
        bizible2__Bizible_Person__c biziPerson = new bizible2__Bizible_Person__c(	bizible2__Contact__c = acclist[0].PersonContactId,
                                                                                    bizible2__UniqueId__c = 'Person_'+acclist[0].PersonContactId,
                                                                                    Name = 'test@test.com');
        Database.insert(biziPerson);

        bizible2__Bizible_Touchpoint__c bz2TP = new bizible2__Bizible_Touchpoint__c(
            bizible2__Account__c = cselist[0].Primary_Contact_Name_MC__c,
            bizible2__Bizible_Person__c = biziPerson.Id,
            bizible2__Ad_Campaign_Name__c = 'Delacon Campaign'
        );
        insert bz2TP;

        Test.StartTest();

            Case_Touch_Point__c ctp = new Case_Touch_Point__c(
                Bizible_Touchpoint__c = bz2TP.Id,
                Case__c = cselist[0].Id,
                Latest_Touchpoint__c = true
            );
            insert ctp;

            bizible2__Bizible_Touchpoint__c upBzTP = [Select Id, bizible2__Ad_Campaign_Name__c, bizible2__Keyword_Text__c,  bizible2__Platform__c, bizible2__Geo_City__c From bizible2__Bizible_Touchpoint__c Where Id = : bz2TP.Id];
            //Verify Bizible Touchpoint's updated field values same with matching Delacon Lead
            system.assertEquals(upBzTP.bizible2__Ad_Campaign_Name__c, 'Equipment Finance');
            //system.assertEquals(upBzTP.bizible2__Touchpoint_Source__c, 'Google Adwords');
            system.assertEquals(upBzTp.bizible2__Platform__c, 'iPad');
            system.assertEquals(upBzTp.bizible2__Keyword_Text__c, 'test');
            system.assertEquals(upBzTP.bizible2__Geo_City__c, 'ADELAIDE');

            //Verify matching Delacon Lead is deleted
            list <Lead> delaconLeadlist = [Select Id From Lead Where LeadSource = 'delacon'];
            system.assert(delaconLeadlist.isEmpty());

            //Cover other CTP trigger events
            update ctp;
            delete ctp;

            
        Test.StopTest();

    }

}