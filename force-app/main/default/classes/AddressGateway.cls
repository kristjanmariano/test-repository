/** 
* @FileName: AddressGateway
* @Description: Provides finder methods for accessing data in the Address object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 15/06/18 RDAVID Created class
* 1.1 07/07/18 JBACULOD transferred logic for populateAddressWithLocation; added populateFoundLocation
* 1.2 09/06/19 JBACULOD added null check for populateAddressWithLocation
**/ 

public without sharing class AddressGateway{

	public static void populateFoundLocation(Address__c address, Map <string, Address__c> checkLocMap, list <Location__c> loclist){

		string LOC_UNKNOWN = 'UNKNOWN';
		string LOC_OVERSEAS = 'OVERSEAS';
		Id locUId; boolean locfound = false;
		map<string,location__c> locMap = new map<string,Location__c>();

		for(Location__c loc : loclist){
			if(!String.IsBlank(loc.Postcode__c) && !String.IsBlank(loc.Suburb_Town__c)){
				locMap.put(loc.Postcode__c.toUpperCase()+'-'+loc.Suburb_Town__c.toUpperCase(),loc);
			}
			else{
				if(loc.Name.containsIgnoreCase(LOC_UNKNOWN))locMap.put(LOC_UNKNOWN,loc);
				else if(loc.Name.containsIgnoreCase(LOC_OVERSEAS))locMap.put(LOC_OVERSEAS,loc);
			}
		}

		string key = address.Postcode__c + '-' + address.Suburb__c;
		key = key.toUpperCase();

		if(locMap.containsKey(key)){
			Location__c loc = locMap.get(key);
			address.Location__c = loc.Id;
			address.Postcode__c = loc.Postcode__c;// 23/07/2018 RDAVID Fix To Populate fields from Location to Address
			address.Suburb__c = loc.Suburb_Town__c;
			address.State__c = loc.State__c;
			address.State_Acr__c = loc.State_Acr__c;
			address.Country__c = loc.Country__c;
		}
		else if(!locMap.containsKey(key)){
			if (address.State__c == 'Outside Australia'){
				address.Location__c = locMap.get(LOC_OVERSEAS).Id;
			}
			else address.Location__c = locMap.get(LOC_UNKNOWN).Id;
		}
	}

	public static void populateAddressWithLocation(Address__c address, Map<Id,Location__c> locMap, Map<Id,Id> addLocIdMap, boolean isUpdate){
		if (!isUpdate){ //On Insert
			if(!locMap.isEmpty() && locMap.containsKey(address.Location__c)){
				address.Location__c = locMap.get(address.Location__c).Id;
				address.Postcode__c = locMap.get(address.Location__c).Postcode__c;
				address.Suburb__c = locMap.get(address.Location__c).Suburb_Town__c;
				address.State__c = locMap.get(address.Location__c).State__c;
				address.State_Acr__c = locMap.get(address.Location__c).State_Acr__c;
				address.Country__c = locMap.get(address.Location__c).Country__c;				
			}
		}
		else{ //On Update
			if(!locMap.isEmpty() && addLocIdMap.containsKey(address.Id)){
				if (locMap.containskey(address.Location__c)){ //6/09/2019 JBACU 
					address.Location__c = locMap.get(address.Location__c).Id;
					address.Postcode__c = locMap.get(address.Location__c).Postcode__c;
					address.Suburb__c = locMap.get(address.Location__c).Suburb_Town__c;
					address.State__c = locMap.get(address.Location__c).State__c;
					address.State_Acr__c = locMap.get(address.Location__c).State_Acr__c;
					address.Country__c = locMap.get(address.Location__c).Country__c;
				}
			}
		}

	}

}