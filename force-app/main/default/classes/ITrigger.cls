/** 
* @FileName: ITrigger
* @Description : Interface containing methods Trigger Handlers must implement to enforce best practice and bulkification of triggers.
* @Source : 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* */ 
public interface ITrigger {

	/** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	void bulkBefore();
	
	/** 
	* @FileName: bulkAfter
	* @Description: This method is called prior to execution of an AFTER trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	void bulkAfter();

	/** 
	* @FileName: beforeInsert
	* @Description: This method is called iteratively for each record to be inserted during a BEFORE trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
	**/ 
	void beforeInsert(SObject so);

	/** 
	* @FileName: beforeUpdate
	* @Description: This method is called iteratively for each record to be updated during a BEFORE
	**/ 
	void beforeUpdate(SObject oldSo, SObject so);

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	void beforeDelete(SObject so);

	/** 
	* @FileName: afterInsert
	* @Description: This method is called iteratively for each record inserted during an AFTER trigger. Always put field validation in the 'After' methods in case another trigger has modified any values. The record is 'read only' by this point.
	**/ 
	void afterInsert(SObject so);

	/** 
	* @FileName: afterUpdate
	* @Description: This method is called iteratively for each record updated during an AFTER
	**/ 
	void afterUpdate(SObject oldSo, SObject so);

	/** 
	* @FileName: afterDelete
	* @Description: This method is called iteratively for each record deleted during an AFTER trigger.
	**/ 
	void afterDelete(SObject so);
	
	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records.
	**/ 
	void andFinally();
}