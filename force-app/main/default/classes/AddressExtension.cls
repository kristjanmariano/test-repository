/*
    @author: Jesfer Baculod - Positive Group
    @history: 07/08/18 - Created
    @description: extension class of NewAddressOverride for creating new Address
*/
public class AddressExtension {
    
    public boolean hasErrors {get;set;}
    public Address__c addr {get;set;}

    public AddressExtension(ApexPages.StandardController stdController) {
        hasErrors = false;
        this.addr = (Address__c )stdController.getRecord();
        if (addr.Id == null) addr = new Address__c();
    }

    public PageReference populateAddresswithLocation(){
        if (addr.Location__c != null){
            //Retrieve selected Location
            list <Location__c> loclist = [Select Id, Suburb_Town__c, Postcode__c, State__c, State_ACR__c, Country__c From Location__c Where Id = : addr.Location__c AND Name != 'UNKNOWN' limit 1];
            if (!loclist.isEmpty()){
                //Populate Address fields with Location
                addr.Suburb__c = loclist[0].Suburb_Town__c;
                addr.Postcode__c = loclist[0].Postcode__c;
                addr.State__c = loclist[0].State__c;
                addr.State_ACR__c = loclist[0].State_ACR__c;
                addr.Country__c = loclist[0].Country__c;
            }
        }
        return null;
    }

    public boolean validateAddress(){
        boolean passed = true;
        integer errctr = 0;
        if (addr.Location__c == null){
            addr.Location__c.addError('Location is required');
            errctr++;
        }
        if (errctr > 0) passed = false;
        return passed;
    }

    public pageReference addrSave(){
        Savepoint sp = Database.setSavepoint();
        try{
            if (validateAddress()){
                upsert addr;
                return new PageReference('/'+ addr.Id);
            }
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );  
        }
        return null;
    }

}