/** 
* @FileName: SkillsBasedRouting
* @Description: Global class used in Process Builder for Skill-based Routing Omni-channel
* @Copyright: Positive (c) 2019 
* @author: Rexie Aaron David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 7/05/19 RDAVID-extra/task24399056-InitialReviewSRandSLA - Created Class copied from SF Documentation and modified to work with Initial Review SLA Omni-channel
**/ 

Global class SkillsBasedRouting {
    @InvocableMethod
    public static void routeUsingSkills(List<String> supportRequests) {
        List<Support_Request__c> srObjects = [SELECT Id, Case_Number__r.Recordtype.Name FROM Support_Request__c WHERE Id in :supportRequests];
        for (Support_Request__c srObj : srObjects) {
            try {
                // Create a PendingServiceRouting to initiate routing
                createPendingServiceRouting(srObj);
            } catch(exception e) {
                System.debug('ERROR:' + e.getStackTraceString());
                throw e;
            }
        }
    }
    
    static void createPendingServiceRouting(Support_Request__c srObj) {
        // Create a new SkillsBased PendingServiceRouting
        PendingServiceRouting psrObj = new PendingServiceRouting(
            CapacityWeight = 10,
            IsReadyForRouting = FALSE,
            RoutingModel  = 'MostAvailable',
            RoutingPriority = 1,
            RoutingType = 'SkillsBased',
            ServiceChannelId = getChannelId(),
            WorkItemId = srObj.Id,
            PushTimeout = 0
        );
        insert psrObj;
        psrObj = [select id, IsReadyForRouting from PendingServiceRouting where id = : psrObj.id];
        
        // Now add SkillRequirement(s) to the PendingServiceRouting
        SkillRequirement skReq = new SkillRequirement(
            RelatedRecordId = psrObj.id,
            SkillId = getSkillId(srObj.Case_Number__r.Recordtype.Name),
            SkillLevel = 0
        );
        insert skReq;
        
        // Update PendingServiceRouting as IsReadyForRouting
        psrObj.IsReadyForRouting = TRUE;
        update psrObj; 
    }
    
    static String getChannelId() {
        ServiceChannel channel = [Select Id From ServiceChannel Where RelatedEntity = 'Support_Request__c'];
        return channel.Id;
    }
    
    static String getSkillId(String caseRT) {
        String skillName;// = 'English';
        System.debug('caseRT --> '+caseRT);
        if (caseRT != null) {
            if (caseRT.containsIgnoreCase('NOD: Commercial')) {
                skillName = 'NOD_Commercial_Initial_Review';
            } else if (caseRT.containsIgnoreCase('NOD: Consumer')) {
                skillName = 'NOD_Consumer_Initial_Review';
            }
        }
        System.debug('skillName --> '+skillName);
        Skill skill = [Select Id From Skill Where DeveloperName = :skillName];
        return skill.Id;
    }
}