/** 
* @FileName: CaseTriggerTest
* @Description: stores record creation on each object which will be used on setup in unit tests
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 8/14/17 JBACULOD Updated, restructured test class for Old Model's CaseTrigger
* 1.1 2/19/18 JBACULOD Moved unit tests to CaseTrigger0Test. 
*                      [SD-19], Added unit test for Auto Create Application in Case
* 1.2 3/27/18 RDAVID Created unit test for Case Scenario SLAs (testCaseScenarioSLAOne, testCaseScenarioSLATwo, testCaseScenarioSLAThree)
* 1.3 4/04/18 JBACULOD Modified testautocreateApplication - test for setting default Application Record Type
* 1.4 5/11/18 JBACULOD Added testNodifiSLACalculation, testRecordStageTimeStamps, testAutomationCallLender
* 2.1 10/05/18 RDAVID - extra/MergeCaseTrigger - Merge recordStatusStageFunnels and extra/DialerAutomationUpdates (Jesfer and Rex Changes) in UAT to deploy to Production.
* 2.2 03/06/19 JBACULOD - added test method for SR Management Review, Removed old tests for test method
**/ 
@isTest
public class CaseTriggerTest {

    @testsetup static void setup(){
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = true,
                Enable_Case_Create_KAR_Sales_Opp__c = true,
                Enable_Case_Lookup_Fields__c = true,
                Enable_Case_Update_Commission_LI__c = true);
        insert trigSet;

        PLS_Bucket_Settings__c  plsbucketset = new PLS_Bucket_Settings__c(KAR_Bucket_Queue_ID__c = UserInfo.getUserId());
        insert plsbucketset;

        TestDataFactory.Case2FOSetupTemplate();
    }

    static testMethod void testSectorAutomation (){
        Test.startTest();
            Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = true,
                Enable_Case_Log_Sectors__c = true,
                Enable_Sector_Trigger__c = true);
            insert trigSet;
            List<Case> csList = [SELECT Id,Status,Stage__c,Partition__c,Channel__c,RecordType.Name,(SELECT Id FROM Sectors__r) FROM Case];
            Delete csList;
            csList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONSUMER_SCENARIO,5); //n: Commercial Scenario
            for(Case caseIns : csList){
                //caseIns.Status = CommonConstants.CASE_STATUS_SCENARIO;
                caseIns.Stage__c = CommonConstants.SCEN_STAGE_NEW;
                csList[0].Stage__c = 'Open';
            }
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            insert csList;

            csList = [SELECT Id,Status,Stage__c,Partition__c,Channel__c,RecordType.Name,(SELECT Id FROM Sectors__r) FROM Case];

            System.assertEquals(csList[0].Sectors__r.size(),2);
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            csList[0].Stage__c = 'Attempted Contact';
            csList[0].Attempted_Contact_Status__c = 'Attempted 1';
            Database.update(csList[0]);

            List<Sector__c> secList = [SELECT Id, Case_Number__c, Sector1__c FROM Sector__c WHERE Case_Number__c =: csList[0].Id AND Sector1__c LIKE '%Stage - Attempted Contact%'];        
            System.assertEquals(secList.size(),1);

            csList[0].Attempted_Contact_Status__c = 'Attempted 2';
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Database.update(csList[0]);

            csList[0].Status = 'Review';
            csList[0].Stage__c = 'Application Review';
            csList[0].Attempted_Contact_Status__c = null;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Database.update(csList[0]);
            secList = [SELECT Id, Case_Number__c, Sector1__c FROM Sector__c WHERE Case_Number__c =: csList[0].Id AND Sector1__c LIKE '%Status - Review%'];        
            System.assertEquals(secList.size(),1);
            System.debug('@@@ testSectorAutomation - csList - '+csList.size());
        Test.stopTest();
    }

    static testMethod void testSectorAutomationOff (){
        Test.startTest();
            List<Case> csList = [SELECT Id,Status,Stage__c,Partition__c,Channel__c,RecordType.Name,(SELECT Id FROM Sectors__r) FROM Case];

            System.assertEquals(csList[0].Sectors__r.size(),0);
            Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = true,
                Enable_Case_Log_Sectors__c = true,
                Enable_Sector_Trigger__c = true);
            insert trigSet;

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            csList[0].Stage__c = 'Attempted Contact';
            csList[0].Attempted_Contact_Status__c = 'Attempted 1';
            Database.update(csList[0]);
            List<Sector__c> secList = [SELECT Id, Case_Number__c, Sector1__c FROM Sector__c WHERE Case_Number__c =: csList[0].Id AND Sector1__c LIKE '%Stage - Attempted Contact%'];        
            System.assertEquals(secList.size(),1);

            csList[0].Status = 'Review';
            csList[0].Stage__c = 'Application Review';
            csList[0].Attempted_Contact_Status__c = null;

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Database.update(csList[0]);
            secList = [SELECT Id, Case_Number__c, Sector1__c FROM Sector__c WHERE Case_Number__c =: csList[0].Id AND Sector1__c LIKE '%Status - Review%'];        
            System.assertEquals(secList.size(),1);
            System.debug('@@@ testSectorAutomation - csList - '+csList.size());
        Test.stopTest();
    }

    static testMethod void testSectorAutomationOffTwo (){
        Test.startTest();
            List<Case> csList = [SELECT Id,Status,Stage__c,Partition__c,Channel__c,RecordType.Name,(SELECT Id FROM Sectors__r) FROM Case];

            System.assertEquals(csList[0].Sectors__r.size(),0);
            Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = true,
                Enable_Case_Log_Sectors__c = true,
                Enable_Sector_Trigger__c = true);
            insert trigSet;

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            csList[0].Status = 'Review';
            csList[0].Stage__c = 'Application Review';
            Database.update(csList[0]);
            List<Sector__c> secList = [SELECT Id, Case_Number__c, Sector1__c FROM Sector__c WHERE Case_Number__c =: csList[0].Id AND Sector1__c LIKE '%Status - Review%'];        
            System.assertEquals(secList.size(),1);
        Test.stopTest();
    }

    static testmethod void testautocreateApplication(){

        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = true,
                Enable_Case_Auto_Create_Application__c = true
        );
        insert trigset;

        Test.startTest();

            list <Case> cselist = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_COMM_AF, 20); //n: Commercial - Asset Finance
            for (Case cse : cselist){
                cse.Application_Record_Type__c = CommonConstants.APP_RT_CONS_I; //Consumer - Individual
            }
            insert cselist;

            //Verify Applications were created on every Case
            list <Application__c> applist = [Select Id, Case__c, RecordType.Name From Application__c Where Case__c in : cselist];
            system.assertEquals(cselist.size(),applist.size());
            //Verify that Record Types were updated base on Case Appicant Type
            for (Application__c app : applist){
                system.assertEquals(app.RecordType.Name, CommonConstants.APP_RT_CONS_I); 
            }

            //Verify Applications were linked to Cases
            for (Case cse : [Select Id, Application_Name__c From Case Where Id in : cselist]){
                system.assert(cse.Application_Name__c != null);
            }

            //Execute to cover delete event in CaseHandler
            delete cselist;

        Test.stopTest();
    }

    static testmethod void testrecordTimeStampsStatusStageFunnels(){

        BusinessHours bh = [select id from BusinessHours where IsDefault=true]; //Retrive Business Hours from system
        //Retrieve created test data for Case
        Case cse = [Select Id, CaseNumber, Status, Stage__c, Base_Rate__c, Loan_Rate__c, Loan_Reference__c, Loan_Term__c, Application_Fee__c, Regular_Monthly_Payment__c, Brokerage_inc_gst__c,
                    Required_Documents__c, Application_Validated__c, Sales_Support_Comment__c, Status_Funnel__c, Stage_Funnel__c,
                    Application_Type__c, Lender__c, Asset_Type__c, Credit_History__c, Loan_Product__c, Previous_Bankrupt__c, Finance_For_Business_Purposes__c, Changes_in_circumstance__c,
                    New_or_Used__c, Sale_Type__c, Approval_Date__c, Requested_Payment_Frequency__c, Comments_for_Broker__c, Required_Loan_Amount__c
                    From Case limit 1];

        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Case_Trigger__c = true,
            Enable_Case_Status_Stage_Funnels__c = true,
            Enable_Case_NVM_Timezone_based_Routing__c = true
            );
        insert trigset;

        Test.startTest();

            cse.Status = CommonConstants.CASE_STATUS_NEW;
            cse.Stage__c = CommonConstants.CASE_STAGE_OPEN;
            update cse;

            cse.Stage__c = CommonConstants.CASE_STAGE_OA;
            update cse;

            cse.Stage__c = CommonConstants.CASE_STAGE_ATTEMPTED;
            update cse;

            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            cse.Status = 'Review';
            cse.Stage__c = CommonConstants.CASE_STAGE_APP_REVIEW;
            update cse;

       Test.stopTest();        
    } 

     static testmethod void testGenerateTrustPilotLink(){

         Case cse = [Select Id, CaseNumber, Stage__c, Status From Case limit 1];
         Application__c app = [Select Id, Case__c From Application__c Where Case__c = : cse.Id limit 1];
         Role__c role = [Select Id, Account__c, Account__r.PersonEmail, TrustPilot_Unique_Link__c, Case__c From Role__c Where Case__c = : cse.Id limit 1];

         Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Case_Trigger__c = true,
                Enable_Case_generate_TrustPilot_Link__c = true,
                Enable_Role_Trigger__c = true,
                Enable_Role_Rollup_to_Application__c = true,
                Enable_Application_Trigger__c = true,
                Enable_Application_Sync_App_2_Case__c = true
            );
        insert trigset;

        Test.startTest();
            //Update Case Status to Approved
            cse.Status = CommonConstants.CASE_STATUS_APPROVED;
            cse.Stage__c = CommonConstants.CASE_STAGE_APWCOND;
            update cse;
        Test.stopTest();

     }

    static testmethod void testCreateKARSalesOpp(){

        list <Case> cselist0 = [Select Id, RecordTypeId, Primary_Contact_Name_MC__c, CaseNumber, Stage__c, Status From Case limit 2];
        list <Account> acclist = [Select Id, Name From Account];

        Id curMotorVehiclePART = Schema.SObjectType.Purchased_Asset__c.getRecordTypeInfosByName().get('Motor Vehicle').getRecordTypeId();
        Purchased_Asset__c pa = new Purchased_Asset__c(
            RecordTypeId = curMotorVehiclePART,
            Asset_Type__c = 'Motor Vehicle',
            Condition__c = 'New',
            Case__c = cselist0[0].Id
        );
        insert pa;

        //Retrieve Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Case_Trigger__c = true,
            Enable_Case_Create_KAR_Sales_Opp__c = true,
            Enable_Case_Lookup_Fields__c = true);
        insert trigSet;

        Test.StartTest();
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            for (Case cse : cselist0){
                cse.Primary_Contact_Name_MC__c = acclist[1].Id;
                cse.Status = 'Submitted';
                cse.Stage__c = 'Submitted to Lender';
            }
            update cselist0;
            for (Case cse : cselist0){
                system.debug('@@111');
                system.assertEquals(cse.Primary_Contact_Name_MC__c, acclist[1].Id);
            }

        Test.StopTest();

        list <Case> cselist = [Select Id, Parent_Case__r.KAR_Opportunity_Created__c, Parent_Case__c From Case Where Parent_Case__c = : cselist0[0].Id];
        system.assertEquals(cselist.size() > 0, true);
        system.assertEquals(cselist[0].Parent_Case__r.KAR_Opportunity_Created__c, true);
    }

    static testmethod void testCreateSRManagerReview(){
        //Retrieve Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Case_Trigger__c = true,
            Enable_Case_Create_SR_Manager_Review__c = true
        );
        insert trigSet;

        PLS_Bucket_Settings__c  plsbucketset = new PLS_Bucket_Settings__c(SR_Manager_Review_Queue_ID__c = UserInfo.getUserId());
        insert plsbucketset;

        Id cseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId(); //POS: Consumer - Asset Finance
        Case cse = new Case(
                Status = 'New',
                Stage__c = 'Open',
                RecordTypeId = cseRTId,
                Partition__c = 'Positive',
                Channel__c = 'PLS',
                Status_Funnel__c = 'Review'
            );        
        insert cse;

        Test.startTest();
            cse.Status = 'Closed';
            cse.Stage__c = 'Lost';
            update cse;
        Test.stopTest();

    }

    static testmethod void testCreatePOSPHLOpp(){       
        //Retrieve Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Case_Trigger__c = true,
            Enable_Case_Create_KAR_Sales_Opp__c = true,
            Enable_Case_Create_POS_PHL_Opp__c = true,
            Enable_Case_Lookup_Fields__c = true);
        insert trigSet;
        BusinessHours bh = [select id from businesshours where IsDefault=true];
        General_Settings1__c genSettings = new General_Settings1__c(
            Case_POS_PHL_Opportuntiy_RT_Id__c = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: PHL Opportunity').getRecordTypeId(),
            Default_Business_Hours_Id__c = bh.Id
        );
        insert genSettings;
        Set<String> allowedPCaseRTForPOSPHLOpp = new Set<String>{'POS: Commercial - Asset Finance','POS: Commercial - Business Funding', 'POS: Consumer - Asset Finance', 'POS: Consumer - Personal Loan'};
        list <Case> cselist0 = [Select Id, RecordTypeId, Primary_Contact_Name_MC__c, CaseNumber, Stage__c, Status From Case limit 2];
        list <Account> acclist = [Select Id, Name From Account];
        System.assertNotEquals(cselist0.size(),0);
        Test.StartTest();
            for (Case cse : cselist0){
                cse.Is_Veda_Credit_Checked__c = true;
                cse.Has_Owner_Occupied__c = true;
                cse.Status_Funnel__c = 'New;Review';
                cse.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: Consumer - Personal Loan').getRecordTypeId();
            }
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update cselist0;
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            Set<Id> PHLOppcases = new Set<Id>();
            for(Case cse : [SELECT Id FROM Case WHERE Parent_Case__c IN: cselist0 AND RecordType.Name =: 'POS: PHL Opportunity']){
                PHLOppcases.add(cse.Id);
            }
            System.assertNotEquals(PHLOppcases.size(),0);
            for (Case cse : cselist0){
                cse.Priority = 'High';
                cse.Stage__c = 'Lost';
                cse.Status = 'Closed';
            }
            update cselist0;
        Test.StopTest();
    }

    static testmethod void testSMSAutomation(){

        //Retrieve Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Case_Trigger__c = true,
            Enable_Case_SMS_Alerts__c = true
        );
        insert trigSet;

        Id cseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId(); //POS: Consumer - Asset Finance
        Case cse = new Case(
                Status = 'New',
                Stage__c = 'Open',
                RecordTypeId = cseRTId,
                Partition__c = 'Positive',
                Channel__c = 'PLS'
            );        
        insert cse;

        Test.startTest();
            cse.Stage__c = 'Attempted Contact';
            cse.Attempted_Contact_Status__c = 'Attempted 1';
            update cse;
        Test.stopTest();
    }

    static testmethod void testReferredOutByOwnerSetToUserOnClose(){

        //Retrieve Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Case_Trigger__c = true,
            Enable_Case_Referred_Out_By_Owner__c = true,
            Enable_Case_Set_User_as_Owner_on_Close__c = true
        );
        insert trigSet;

        PLS_Bucket_Settings__c  plsbucketset = new PLS_Bucket_Settings__c(M3_Queue_ID__c = [Select Id From Group Where Name = 'M3'].Id);
        insert plsbucketset;

        Id cseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_P_CONS_AF).getRecordTypeId(); //POS: Consumer - Asset Finance
        Case cse = new Case(
                Status = 'New',
                Stage__c = 'Open',
                RecordTypeId = cseRTId,
                Partition__c = 'Positive',
                Channel__c = 'PLS'
            );        
        insert cse;

        Test.startTest();
            cse.OwnerId = plsbucketset.M3_Queue_ID__c;
            update cse;
        Test.stopTest();

    }
    //4.3 5/08/2019 RDAVID - tasks/25563890
    static testmethod void testUpdateCLI(){       
        //Retrieve Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
            Enable_Triggers__c = true,
            Enable_Case_Trigger__c = true,
            Enable_Case_Create_KAR_Sales_Opp__c = true,
            Enable_Case_Create_POS_PHL_Opp__c = true,
            Enable_Case_Lookup_Fields__c = true,
            Enable_Case_Update_Commission_LI__c = true);
        insert trigSet;
        BusinessHours bh = [select id from businesshours where IsDefault=true];
        General_Settings1__c genSettings = new General_Settings1__c(
            Case_POS_PHL_Opportuntiy_RT_Id__c = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: PHL Opportunity').getRecordTypeId(),
            Default_Business_Hours_Id__c = bh.Id
        );
        insert genSettings;
        list <Case> cselist0 = [Select Id, RecordTypeId, Primary_Contact_Name_MC__c, CaseNumber, Stage__c, Status From Case limit 2];
        System.assertNotEquals(cselist0.size(),0);
        List<CommissionCT__c> commList = TestDataFactory.createCommission(1, 'NM - Asset Finance - Commission', cselist0[0].Id);
        insert commList;
        update commList;
        List<Commission_Line_Items__c> commLineList = TestDataFactory.createCommissionLine(1,'Asset Finance - Finance Commission', commList[0].Id);
        insert commLineList;
        Test.StartTest();
            cselist0[0].Stage__c = 'Won';
            cselist0[0].Status = 'Closed';
            TriggerFactory.isProcessedMap = new map <Id, boolean>();
            update cselist0;
        Test.StopTest();
    }
}