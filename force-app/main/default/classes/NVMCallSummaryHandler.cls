/** 
* @FileName: NVMCallSummaryHandler
* @Description: Trigger Handler for the Case_Touch_point__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2019
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 20/05/19 JBACULOD Created Trigger handler
* 1.1 17/06/19 RDAVID extra/task24928331-BugNVMCallSummaryTask - Added Logic to update the Task Call Purpose field when NVM updates the TaskExists field to True and populates the TaskID.
**/ 
public without sharing class NVMCallSummaryHandler implements ITrigger{

    private map <string,string> nvmCSForCallPurposeUpdateMap = new map <string,string>();

    public void bulkBefore(){

        if (TriggerFactory.trigSet.Enable_NVM_CS_Pull_Call_Purpose__c){
            if (Trigger.isBefore){
                if (Trigger.isInsert || Trigger.isUpdate){
                    set <string> CallGUIDset = new set <string>();
                    Map<Id,NVMStatsSF__NVM_Call_Summary__c> nvmcsOldMap = (Map<Id,NVMStatsSF__NVM_Call_Summary__c>) Trigger.oldMap;
                    for (NVMStatsSF__NVM_Call_Summary__c nvmcs : (List <NVMStatsSF__NVM_Call_Summary__c>) Trigger.new ){
                        if (nvmcs.NVMStatsSF__CallGuid__c != null){
                            CallGUIDset.add(nvmcs.NVMStatsSF__CallGuid__c);
                        }
                        //1.1 17/06/19 RDAVID extra/task24928331-BugNVMCallSummaryTask
                        if(Trigger.isUpdate && !nvmcsOldMap.isEmpty() 
                            && nvmcsOldMap.containskey(nvmcs.Id) && nvmcs.NVMStatsSF__TaskID__c != nvmcsOldMap.get(nvmcs.Id).NVMStatsSF__TaskID__c
                            && !String.isBlank(nvmcs.NVMStatsSF__TaskID__c)){
                            CallGUIDset.add(nvmcs.NVMStatsSF__CallGuid__c);
                        }
                    }
                    for (Task tsk : [Select Id, CallObject, Call_Purpose__c From Task Where CallObject in : CallGUIDSet]){
                        if (!nvmCSForCallPurposeUpdateMap.containskey(tsk.CallObject)){
                            nvmCSForCallPurposeUpdateMap.put(tsk.CallObject, tsk.Call_Purpose__c);
                        }
                    }
                }
            }

        }


    }

    public void bulkAfter(){
    }

    public void beforeInsert(Sobject so){        
        NVMStatsSF__NVM_Call_Summary__c nvmcs = (NVMStatsSF__NVM_Call_Summary__c)so;
        if (nvmCSForCallPurposeUpdateMap.size() > 0){
            NVMCAllSummaryGateway.pullCallPurpose(nvmCSForCallPurposeUpdateMap, nvmcs);
        }

    }

    public void beforeUpdate(Sobject oldSo, Sobject so){
        NVMStatsSF__NVM_Call_Summary__c nvmcs = (NVMStatsSF__NVM_Call_Summary__c)so;
        if (nvmCSForCallPurposeUpdateMap.size() > 0){
            NVMCAllSummaryGateway.pullCallPurpose(nvmCSForCallPurposeUpdateMap, nvmcs);
        }
    }

    public void beforeDelete(Sobject so){

    }

    public void afterInsert(Sobject so){ 
    }

    public void afterUpdate(Sobject oldSo, Sobject so){

    }

    public void afterDelete(Sobject so){

    }

    public void andFinally(){
    }

}