@isTest  
private class TestUpdateCallsMadeTrigger { 

    static testMethod void LeadTest(){
    //create a lead
    Lead newLead = new Lead();
    newLead.LastName = 'Test';
    newLead.email = 'test@test.com';
    newLead.calls_made__c = 1;
    newLead.LeadSource = 'Other';
    newLead.Loan_Product__c = 'Consumer Loan';
    newLead.Status='desiredNewLeadStatus';
    newLead.IsConverted=FALSE;
    newLead.First_Call_Made_Time__c=System.Now();
    insert newLead;
    
    // Create a Task  
    Task newTask = new Task(CallType='Outbound', Status='Completed', NVMContactWorld__CW_Call_Start_Time__c=System.Now(), WhoId=newLead.Id,subject='SMS: Sent');  
    test.startTest();  
    insert newTask;  
    test.stopTest();
      
    newTask.subject='SMS: Received';
    newTask.Description='fdsfsaf';
    newTask.callType = 'outbound';
    update newTask;
        
    // Verify that the # Open Tasks is correct  
    newLead = [select Calls_Made__c from Lead where Id=:newLead.Id ];  
    System.assertEquals(1,newLead.Calls_Made__c);  
    delete newTask;
    undelete newTask;
  
    Task t = new Task();
    insert t;
    Outbound_SMS_History__c createSms = new Outbound_SMS_History__c(OutboundSms_Lead__c = t.whoId, OutboundSms_Opportunity__c = t.whatId, OutboundSms_SMSText__c = t.Description, OutboundSms_StatusMessage__c = t.status, OutboundSms_Name__c = t.who.Name, OutboundSms_Sent_By__c = userInfo.getUserId() );
    Try{
    insert createSms;
    }Catch(Exception ex){
    
    }
    
    Task t2 = new Task(subject = 'SMS: Sent', calltype = 'Outbound', status = 'Completed', whatId = createSms.Id);    
    insert t2;
    
    t2.Subject = 'SMS: Received';
    update t2;

    Inbound_SMS_History__c incomingSms = new Inbound_SMS_History__c(InboundSms_Lead__c = t.whoId, InboundSms_Opportunity__c = t.whatId, InboundSms_SMS_Text__c= 'test', InboundSms_Sent_By__c = t.CreatedBy.Name, InboundSms_SentStatus__c = t.status );
    insert incomingSms;

       
    //Verify that the open tasks are correct
//    newlead=[SELECT calls_Made__c from lead where Id =:newlead.Id];
 
      
    } 
/*
    static testMethod void LeadTest(){  
        // Create a Lead  
        Lead newLead = new Lead(LastName='Test', Email='testing@testing0.com',Calls_Made__c = 1,LeadSource = 'Other',Loan_Product__c = 'Consumer Loan');  
        insert newLead;  
        
        // Create a Task  
        Task newTask = new Task(CallType='Outbound', Status='Completed', WhoId=newLead.Id);  
        test.startTest();  
        insert newTask;  
        test.stopTest();  
        
        // Verify that the # Open Tasks is correct  
        newLead = [select Calls_Made__c from Lead where Id=:newLead.Id ];  
        System.assertEquals(1,newLead.Calls_Made__c);  
    }  
*/    
    static testMethod void AccountTest(){  
        // Create an Account  
        Account newAccount = new Account(LastName='Test',Calls_Made__c = 1);  
        insert newAccount;  
        
        // Create a Task  
        Task newTask = new Task(CallType='Outbound', Status='Completed', WhatId=newAccount.Id);  
      
        // Verify that the # Open Tasks is correct  
        newAccount = [select Calls_Made__c from Account where Id=:newAccount.Id ];  
        System.assertEquals(1,newAccount.Calls_Made__c); 
        
     
        
        // test opportunity
        Opportunity newOpportunity = new Opportunity(Name='Test', Calls_Made__c = 1, StageName='Application Forms Sent', CloseDate=System.today().addDays(4),AccountId=newAccount.Id);  
        insert newOpportunity;  
        
        // Create an opportunity Task  
        Task newTaskk = new Task(CallType='Outbound', Status='Completed', WhatId=newOpportunity.Id, OwnerId = userInfo.getUserId()); 
        test.startTest();  
        insert newTask; 
        insert newTaskk;  
        test.stopTest();  
        
         
        // Verify that the # Open Tasks are correct  
        newOpportunity = [select Calls_Made__c from Opportunity where Id=:newOpportunity.Id ];  
        System.assertEquals(1,newOpportunity.Calls_Made__c);  
        
       }  
    
    static testMethod void CompletedTest(){  
        // Create a Lead  
        Lead newLead = new Lead(LastName='Test', Email='testing@testing0.com',Calls_Made__c = 1,LeadSource = 'Other',Loan_Product__c = 'Consumer Loan'); 
        insert newLead;  
        
        // Create a Completed Task  
        Task newTask = new Task(CallType='Outbound', Status='Completed', WhoId=newLead.Id); 
        test.startTest();  
        insert newTask;  
        test.stopTest();  
        
        // Verify that the # Open Tasks is empty  
        newLead = [select Calls_Made__c from Lead where Id=:newLead.Id ];  
        System.assertEquals(1,newLead.Calls_Made__c);  
    }  
    
    static testMethod void BatchTestLead(){  
        // Create a couple Leads, small batch test
        List<Lead> newLeads = new List<Lead>();  
        for (Integer i = 0; i < 4; i++) {  
            newLeads.add(new Lead(LastName='Test'+i, Email='testing@testing'+i+'.com',Calls_Made__c = 1,LeadSource = 'Other',Loan_Product__c = 'Consumer Loan'));  
        }  
        insert newLeads;  
        
        // Create a task for each one  
        List<Task> newTasks = new List<Task>();  
        for(Lead l : newLeads){  
            newTasks.add(new Task(CallType='Outbound', Status='Completed', WhoId=l.Id));  
        }  
    
        // Insert the tasks  
        test.startTest();  
        insert newTasks;  
        test.stopTest(); 
        
    
        update newTasks;        
    }  
    
    static testMethod void BatchTestAccount(){  
        
        // Create a couple Leads, small batch test
        List<Account> newAccounts = new List<Account>();  
        for (Integer i = 0; i < 4; i++) {  
            newAccounts.add(new Account(LastName='Test'+i,Calls_Made__c = 0));  
        }  
        insert newAccounts;  
        
        // Create a task for each one  
        List<Task> newTaskss = new List<Task>();  
        for(Account l : newAccounts){  
            newTaskss.add(new Task(CallType='Outbound', Status='Completed', WhatId=l.Id));  
        }  
    
        // Insert the tasks  
        test.startTest();  
        insert newTaskss;  
        test.stopTest();  
     
    }     
  
}