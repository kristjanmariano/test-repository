/**
    Test the VedaIndividualPrintableCC controller 
 */
@isTest
private class VedaIndividualPrintableCCTest {

    static testMethod void testVedaIndividualPrintableCCController() {
        
        Account acct = TestHelper.createPersonAccount();
        Attachment attach = TestHelper.createAttachmentForPersonAccount(acct);
        Opportunity opp = TestHelper.createPersonAccountOpportunity(acct);
        Attachment oppAttachment = TestHelper.createAttachmentForPersonAcctOpportunity(opp);
        
         Lead ld = TestHelper.createLead();
         Attachment ldattach = TestHelper.createAttachmentForLead(ld);
        
        Test.startTest();
        
        PageReference pageRef = Page.VedaIndividualPagePrintable;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('attachID', attach.id);
        ApexPages.currentPage().getParameters().put('objAttId', acct.id);
         ApexPages.currentPage().getParameters().put('objType', 'Account');        
        
        VedaIndividualPrintableCC custController = new VedaIndividualPrintableCC();
       
        
        custController.getSavedPDFFile();
        custController.savePdfFile();
        
        pageRef = Page.VedaIndividualPagePrintable;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('attachID', oppAttachment.id);
        ApexPages.currentPage().getParameters().put('objAttId', opp.id);
         ApexPages.currentPage().getParameters().put('objType', 'Opportunity');        
        
        custController = new VedaIndividualPrintableCC();
        
        pageRef = Page.VedaIndividualPagePrintable;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('attachID', ldattach.id);
        ApexPages.currentPage().getParameters().put('objAttId', ld.id);
         ApexPages.currentPage().getParameters().put('objType', 'Lead');        
        
        custController = new VedaIndividualPrintableCC();
       
        
        Test.stopTest(); 
    }
    
    
    static testmethod void testLeadConversion(){
        
        Test.startTest();
        Lead ld = TestHelper.createLead();
        Attachment ldattach = TestHelper.createAttachmentForLead(ld);
        try{
        Database.LeadConvert lc = new database.LeadConvert();
        
        lc.setLeadId(ld.id);

        LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);

        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
        }catch (Exception e){
        
        }
        Test.stopTest(); 
    }

}