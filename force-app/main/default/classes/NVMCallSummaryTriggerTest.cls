/** 
* @FileName: RoleCloneControllerTest
* @Description: Test class for NVMCallSummaryHandler, NVMCallSummaryGateway
* @Copyright: Positive (c) 2019
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 20/04/19 JBACULOD Created class
**/ 
@isTest
private class NVMCallSummaryTriggerTest {

    @testSetup static void setupData(){

        //Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
        triggerSettings.Enable_Triggers__c = true;
        triggerSettings.Enable_NVM_Call_Summary_Trigger__c = true;
        triggerSettings.Enable_NVM_CS_Pull_Call_Purpose__c = true;
        upsert triggerSettings Trigger_Settings1__c.Id;

        //Create test data for Task
        list <Task> tsklist = new list <Task>();
        for (Integer i=0; i<10; i++){
            Task tsk = new Task(
                CallObject = '0-' + i,
                Subject = 'Test Call'+i,
                Call_Purpose__c = 'Attempted Contact',
                Status = 'Completed',
                Priority = 'Normal'
            );
            tsklist.add(tsk);
        }
        insert tsklist;

    }

    static testmethod void testPulLCallPurpose(){

        list <Task> tsklist = [Select Id, Call_Purpose__c From Task];
        Test.starttest();
            list <NVMStatsSF__NVM_Call_Summary__c> nvmcslist = new list <NVMStatsSF__NVM_Call_Summary__c>();
            for (Integer i=0; i<10; i++){
                NVMStatsSF__NVM_Call_Summary__c nvmcs = new NVMStatsSF__NVM_Call_Summary__c(
                    NVMStatsSF__CallGuid__c = '0-' + i
                );
                nvmcslist.add(nvmcs);
            }
            insert nvmcslist;

            for (NVMStatsSF__NVM_Call_Summary__c upnvmcs : [Select Id, Call_Purpose__c From NVMStatsSF__NVM_Call_Summary__c Where Id = : nvmcslist]){
                //Verify Call Purpose has been pulled from matching Task
                system.assertEquals(upnvmcs.Call_Purpose__c, 'Attempted Contact');
            }

            //Cover update, delete 
            update nvmcslist;
            delete nvmcslist;

        Test.stopTest();


    }    

}