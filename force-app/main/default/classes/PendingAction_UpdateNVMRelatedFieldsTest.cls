/*
    @Description: test class of PendingAction_UpdateNVMRelatedFields
    @Author: Jesfer Baculod - Positive Group
    @History:	09/22/2017
*/ 
@isTest
private class PendingAction_UpdateNVMRelatedFieldsTest {

	@testsetup static void setup(){

		//Create test data for Lead
		Lead ld = new Lead(
				LastName = 'test',
				Status = 'Open'
			);
		insert ld;

		//Create test data for Opp
		Opportunity opp = new Opportunity(name = 'Test', stageName = 'open', closeDate = date.today(), loan_type2__c = 'car' );
    	insert opp;


	}

	static testmethod void testLeadOppPendingAction(){

		//Retrieve Lead
		Lead ld = [Select Id, Last_Call_Time_Ended__c, Calls_Made__c From Lead];
		//Retrieve Opportunity
		Opportunity opp = [Select Id, Last_Call_Time_Ended__c, Calls_Made__c From Opportunity];

		Test.startTest();

			
			//Create test data for Lead Task
			Task tsk_ld = new Task(
					WhoId = ld.Id,
					CallType = 'Outbound',
					Status = 'Completed',
					Subject = 'Outbound Call from XXXX'
				);
			insert tsk_ld;
			
			//Create Pending Action (this is automatically created when Outbound Call task was created)
			Pending_Action__c pa_ld = new Pending_Action__c(
					CW_Call_End_Time__c = System.now(),
					Related_Record_ID__c = ld.Id,
					Target_Object__c = 'Lead',
					Function_name__c = 'updateNVMRelatedFields'
				);
			insert pa_ld;
			string paldID = pa_ld.Id;

			//Execute PendingAction_UpdateNVMRelatedFields (this is automatically checked/executed for every Lead update)
			list <ID> ldIDlist = new list <ID>();
			ldIDlist.add(ld.Id);
			PendingAction_UpdateNVMRelatedFields.leadOppPendingAction(ldIDlist);

			//Verify that Lead was updated
			list <Lead> upLead = [Select Id, Last_Call_Time_Ended__c, Calls_Made__c From Lead];
			system.assertEquals(upLead[0].Last_Call_Time_Ended__c, pa_ld.CW_Call_End_Time__c);
			system.assertEquals(upLead[0].Calls_Made__c, 1);
			//Verify that Pending Action was deleted
			list <Pending_Action__c> palist = [Select Id From Pending_Action__c Where Id = : paldID];
			system.assertEquals(palist.size(),0);

			//Create test data for Opp task
			Task tsk_opp = new Task(
					WhatId = opp.Id,
					CallType = 'Outbound',
					Status = 'Completed',
					Subject = 'Outbound Call from XXXX'
				);
			insert tsk_opp;

			//Create Pending Action (this is automatically created when Outbound Call task was created)
			Pending_Action__c pa_opp = new Pending_Action__c(
					CW_Call_End_Time__c = System.now(),
					Related_Record_ID__c = opp.Id,
					Target_Object__c = 'Opportunity',
					Function_name__c = 'updateNVMRelatedFields'
				);
			insert pa_opp;
			string paoppID = pa_opp.Id;

			//Execute PendingAction_UpdateNVMRelatedFields (this is automatically checked/executed for every Lead update)
			list <ID> oppIDlist = new list <ID>();
			oppIDlist.add(opp.Id);
			PendingAction_UpdateNVMRelatedFields.leadOppPendingAction(oppIDlist);

			//Verify that Opportunity was updated
			list <Opportunity> upOpp = [Select Id, Last_Call_Time_Ended__c, Calls_Made__c From Opportunity];
			system.assertEquals(upOpp[0].Last_Call_Time_Ended__c, pa_opp.CW_Call_End_Time__c);
			system.assertEquals(upLead[0].Calls_Made__c, 1);
			//Verify that Pending Action was deleted
			list <Pending_Action__c> palist2 = [Select Id From Pending_Action__c Where Id = : paoppID];
			system.assertEquals(palist2.size(),0);


		Test.stopTest();

	}


	static testmethod void testPendingActionPushUpdateSchedule(){

		//Retrieve Lead
		Lead ld = [Select Id, Last_Call_Time_Ended__c, Calls_Made__c From Lead];

		//Create test data for Lead Task
		Task tsk_ld = new Task(
				WhoId = ld.Id,
				CallType = 'Outbound',
				Status = 'Completed',
				Subject = 'Outbound Call from XXXX'
			);
		insert tsk_ld;

		//Create Pending Action (this is automatically created when Outbound Call task was created)
		Pending_Action__c pa_ld = new Pending_Action__c(
				CW_Call_End_Time__c = System.now(),
				Related_Record_ID__c = ld.Id,
				Target_Object__c = 'Lead',
				Function_name__c = 'updateNVMRelatedFields'
			);
		insert pa_ld;

		Test.startTest();

			PendingAction_PushUpdates_Schedulable papus = new PendingAction_PushUpdates_Schedulable();
			String sch = '0 0 23 * * ?'; 
			String jobID = system.schedule('Test Pending Action - updateNVMRelatedFields', sch, papus);

			list <CronTrigger> ctlist = [SELECT Id, CronJobDetailId, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType, NextFireTime, PreviousFireTime FROM CronTrigger Where Id = : jobID limit 1];
			system.debug('@@crontrigger:'+ctlist.size()+ctlist);
		Test.stopTest();

	}


}