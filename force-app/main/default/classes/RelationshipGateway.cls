/** 
* @FileName: RelationshipGateway
* @Description: Gateway class of RelationshipTrigger
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 6/29/18 JBACULOD Created class
* 1.1 7/26/18 JBACULOD Added populateAssocRelationshipsInAcc
* 2.0 6/06/19 JBACULOD Made transaction for updating Account as Qeuaeable 
* 2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync  - Added logic to propagate fields from Relationship to Person Account 
                                                                    - Added logic to set the Nodifi access picklist fields to No Access once the EndDate is populated
**/ 
public class RelationshipGateway{

    private static string formattedAssociatedRelationships(map <id, list <Relationship__c>> masterAssocAccMap){
        string assocRelationships = ''; //'<ul><b><h1>TEST</h1></b><li>test1</li><li>test2</li></ul>';
        map <string, list <Relationship__c>> relAssocsDispMap = new map <string, list <Relationship__c>>();
        for (Id masterAccID : masterAssocAccMap.keySet()){
            for (Relationship__c rel : masterAssocAccMap.get(masterAccID)){
                if (rel.Relationship_Type__c != null){
                    if (!relAssocsDispMap.containskey(rel.Relationship_Type__c)) relAssocsDispMap.put(rel.Relationship_Type__c, new list <Relationship__c>());
                    relAssocsDispMap.get(rel.Relationship_Type__c).add(rel);
                }
            }
        }
        system.debug('@@relAssocsDispMap:'+relAssocsDispMap);
        for (String relType : relAssocsDispMap.keySet()){
            string tempAssocRelGroup;
            string relTypeP = relType + 's';
            if (relType == 'Primary Beneficiary') relTypeP = 'Primary Beneficiaries';
            else if (relType == 'Spouse') relTypeP = 'Spouse';
            string header = '<b><h1>' + relTypeP.toUpperCase() + '</h1></b>';
            string listassocs = '';
            system.debug('@@relAssocsDispMap.get(relType):'+relAssocsDispMap.get(relType));
            for (Relationship__c assocs : relAssocsDispMap.get(relType)){
                listassocs+= '<li id=\'' + assocs.Associated_Account__c + '\'>' + assocs.Associated_Account__r.Name;
                if (assocs.Start_Date__c != null) listassocs+= ' since ' + DateTime.newInstance(assocs.Start_Date__c.year(), assocs.Start_Date__c.month(), assocs.Start_Date__c.day()).format('dd/MM/YYYY');
                listassocs+= '</li>';
            }
            tempAssocRelGroup = header + listassocs;
            assocRelationships+= '<ul>' + tempAssocRelGroup + '</ul>';
        }
        system.debug('@@assocRelationships:'+assocRelationships);
        return assocRelationships;
    }

    public static list <Account> populateAssocRelationshipsInAcc(map<id,SObject> oldRelMap, map <id, SObject> newRelMap, string operation){
        list <Account> accToUpdatelist = new list <Account>();
        map <Id, list<Relationship__c>> masterAssocAccMap = new map <id, list<Relationship__c>>();
        set <Id> masterAccIds = new set <id>();
        if (operation != 'Delete'){
            for (Sobject sobj : newRelMap.values()){
                if ( (ID) sobj.get('Master_Account__c') != null){
                    masterAccIds.add( (ID) sobj.get('Master_Account__c') );
                }
            }
        }
        else{
            for (Sobject sobj : oldRelMap.values()){
                if ( (ID) sobj.get('Master_Account__c') != null){
                    masterAccIds.add( (ID) sobj.get('Master_Account__c') );
                }
            }
        }
        if (masterAccIds.size() > 0){
            for (Relationship__c rel : [Select Id, Start_Date__c, End_Date__c, Relationship_Type__c, RecordTypeId, RecordType.Name, Master_Account__c, Associated_Account__c, Associated_Account_Type__c, Associated_Account__r.Name, Master_Account__r.Name, Master_Account__r.Associated_Relationships__c From Relationship__c Where Master_Account__c in : masterAccIds Order By Relationship_Type__c ASC]){
                if(!masterAssocAccMap.containskey(rel.Master_Account__c)) masterAssocAccMap.put(rel.Master_Account__c, new list <Relationship__c>());
                masterAssocAccMap.get(rel.Master_Account__c).add(rel);
            }
            system.debug('@@masterAssocAccMap:'+masterAssocAccMap);
            if (!masterAssocAccMap.isEmpty()){
                for (Id mkey : masterAssocAccMap.keySet()){
                    Boolean updateAcc = false;
                    Account upAcc = new Account(Id = mkey);
                    if (masterAssocAccMap.get(mkey).size() > 0){
                        if (operation == 'Insert'){
                            //Adds new Associated Relationship on Account
                            upAcc.Associated_Relationships__c = formattedAssociatedRelationships(masterAssocAccMap);
                            updateAcc = true;
                        }
                        else if (operation == 'Update'){
                            /*system.debug('@@mkey:'+mkey);
                            system.debug('@@Old masteracc:'+oldRelMap.get(mkey).get('Master_Account__c'));
                            system.debug('@@New masteracc:'+newRelMap.get(mkey).get('Master_Account__c'));
                            system.debug('@@Old Start_Date__c:'+oldRelMap.get(mkey).get('Start_Date__c'));
                            system.debug('@@New Start_Date__c:'+newRelMap.get(mkey).get('Start_Date__c'));
                            if (oldRelMap.get(mkey).get('Master_Account__c') != newRelMap.get(mkey).get('Master_Account__c') ||
                                oldRelMap.get(mkey).get('Associated_Account__c') != newRelMap.get(mkey).get('Associated_Account__c') ||
                                oldRelMap.get(mkey).get('Start_Date__c') != newRelMap.get(mkey).get('Start_Date__c')
                            ){ */
                                //Updates previously added Associated Relationship on Account
                                upAcc.Associated_Relationships__c = formattedAssociatedRelationships(masterAssocAccMap);
                                updateAcc = true;
                            //}
                        }
                        else if (operation == 'Delete'){
                            //Removes previously added Associated Relationship on Account
                            upAcc.Associated_Relationships__c = formattedAssociatedRelationships(masterAssocAccMap);
                            updateAcc = true;
                        }
                    }
                    else{
                        //No Relationships found for Master Account
                        upAcc.Associated_Relationships__c = null;
                        updateAcc = true;
                    }
                    if (updateAcc) accToUpdatelist.add(upAcc);
                }
            }

        }
        return accToUpdatelist;
    }

    public static list <Relationship__c> linkSpouseRelationship(list <Relationship__c> newRelationships, string operation){
        system.debug('@@operation:'+operation);
        string relQuery = 'Select Id, Start_Date__c, End_Date__c, Spouse_Relationship__c, Relationship_Type__c, RecordTypeId, RecordType.Name, Master_Account__c, Associated_Account__c From Relationship__c';
        list <Relationship__c> exRelationships = database.query(relQuery + ' Where Id in : newRelationships AND RecordType.Name = \'Spouse\'');
        system.debug('@@exRelationships:'+exRelationships);
        list <Relationship__c> returningRelationship = new list <Relationship__c>(); 
        for (Relationship__c spouseM : exRelationships){
            if (operation == 'Insert'){
                Relationship__c spouseF = new Relationship__c(
                    RecordTypeId = spouseM.RecordTypeId,
                    Master_Account__c = spouseM.Associated_Account__c,
                    Associated_Account__c = spouseM.Master_Account__c,
                    Start_Date__c = spouseM.Start_Date__c,
                    End_Date__c = spouseM.End_Date__c,
                    Relationship_Type__c = spouseM.Relationship_Type__c,
                    Spouse_Relationship__c = spouseM.Id //Link Spouse Relationship
                );
                returningRelationship.add(spouseF);
            }
            else if (operation == 'Update'){
                if (spouseM.Spouse_Relationship__c != null){
                    Relationship__c spouseF = new Relationship__c(
                        Id = spouseM.Spouse_Relationship__c,
                        Start_Date__c = spouseM.Start_Date__c,
                        End_Date__c = spouseM.End_Date__c,
                        Master_Account__c = spouseM.Associated_Account__c,
                        Associated_Account__c = spouseM.Master_Account__c,
                        Relationship_Type__c = spouseM.Relationship_Type__c
                    );
                    returningRelationship.add(spouseF);
                }
            }
            else if (operation == 'Delete'){
                if (spouseM.Spouse_Relationship__c != null){
                    returningRelationship.add(
                        new Relationship__c(Id = spouseM.Spouse_Relationship__c)
                    );
                }
            }
        }
        system.debug('@@returningRelationship:'+returningRelationship);
        return returningRelationship;
    }

    //2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync
    public static list <Account> propagateRelToAcc(map<id,SObject> oldRelMap, map <id, SObject> newRelMap, string operation){
        list <Account> accToUpdatelist = new list <Account>();
        map <Id, list<Relationship__c>> masterAssocAccMap = new map <id, list<Relationship__c>>();
        set <Id> assAccIds = new set <id>();
        if (operation != 'Delete'){
            for (Sobject sobj : newRelMap.values()){
                if ( (ID) sobj.get('Associated_Account__c') != null && (Boolean) sobj.get('Active__c')){
                    assAccIds.add( (ID) sobj.get('Associated_Account__c') );
                }
            }
        }
        if (assAccIds.size() > 0){
            Map<Id,Account>  accMap = new Map<Id,Account> ([SELECT Id,Rel_Mobile_Phone__c,Rel_Other_Phone__c,Rel_Email__c FROM Account WHERE Id IN: assAccIds]);
            for(Relationship__c rel : (List<Relationship__c>)newRelMap.values()){
                if(accMap.containskey(rel.Associated_Account__c)){
                    if(operation == 'Insert'){
                        system.debug('RDAVID extra/task25055605-RelationshipToAccountSync - '+operation + ' Acc - '+accMap.get(rel.Associated_Account__c));
                        if(updateInAcc(accMap.get(rel.Associated_Account__c), rel)){
                            system.debug('RDAVID extra/task25055605-RelationshipToAccountSync - updateInAcc is TRUE');
                            accMap.get(rel.Associated_Account__c).Rel_Mobile_Phone__c = rel.Contact_Mobile1__c;
                            accMap.get(rel.Associated_Account__c).Rel_Other_Phone__c = rel.Contact_Other_Phone1__c;
                            accMap.get(rel.Associated_Account__c).Rel_Email__c = rel.Contact_Email1__c;
                            accToUpdatelist.add(accMap.get(rel.Associated_Account__c));
                        }    
                    }
                    else if(operation == 'Update'){
                        Relationship__c oldRel = (Relationship__c)oldRelMap.get(rel.Id);
                        if(updateInRel(oldRel, rel)){
                            system.debug('RDAVID extra/task25055605-RelationshipToAccountSync - '+operation + ' Acc - '+accMap.get(rel.Associated_Account__c));
                            if(updateInAcc(accMap.get(rel.Associated_Account__c), rel) ){
                                system.debug('RDAVID extra/task25055605-RelationshipToAccountSync - Update - updateInAcc is TRUE');
                                accMap.get(rel.Associated_Account__c).Rel_Mobile_Phone__c = rel.Contact_Mobile1__c;
                                accMap.get(rel.Associated_Account__c).Rel_Other_Phone__c = rel.Contact_Other_Phone1__c;
                                accMap.get(rel.Associated_Account__c).Rel_Email__c = rel.Contact_Email1__c;
                                accToUpdatelist.add(accMap.get(rel.Associated_Account__c));
                            }
                        }
                    }   
                }
            }
           
        }
        return accToUpdatelist;
    }

    //2.1 17/07/19 RDAVID extra/task25055605-RelationshipToAccountSync - logic to pull values from updated Account in Relationship
    public static void pullValuesFromAccount(Map<Id,SObject> oldRelMap, List<SObject> relList){
        set <Id> assAccIds = new set <id>();
        Map<Id,Relationship__c> oldRelMapObj = (Map<Id,Relationship__c>)oldRelMap;
        List<Relationship__c> relListObj = (List<Relationship__c>)relList;
        for(Relationship__c rel : relListObj){
            if(oldRelMapObj.get(rel.Id).Associated_Account__c != rel.Associated_Account__c){
                assAccIds.add(rel.Associated_Account__c);
            }
            if(oldRelMapObj.get(rel.Id).End_Date__c != rel.End_Date__c && rel.End_Date__c != NULL && rel.End_Date__c <= System.today()){
                rel.Nodifi_Tenancy__c = 'No Access';
                rel.Nodifi_User_Role__c = '';
            }
        }
        if (assAccIds.size() > 0){
            Map<Id,Account>  accMap = new Map<Id,Account> ([SELECT Id,Rel_Mobile_Phone__c,Rel_Other_Phone__c,Rel_Email__c FROM Account WHERE Id IN: assAccIds]);
            for(Relationship__c rel : relListObj){
                if(accMap.containsKey(rel.Associated_Account__c)){
                    rel.Contact_Mobile1__c = accMap.get(rel.Associated_Account__c).Rel_Mobile_Phone__c;
                    rel.Contact_Other_Phone1__c = accMap.get(rel.Associated_Account__c).Rel_Other_Phone__c;
                    rel.Contact_Email1__c = accMap.get(rel.Associated_Account__c).Rel_Email__c;
                }
            }
        }
    }

    public static Boolean updateInAcc (Account acc, Relationship__c rel){
        if( acc.Rel_Mobile_Phone__c != rel.Contact_Mobile1__c ||
            acc.Rel_Other_Phone__c != rel.Contact_Other_Phone1__c ||
            acc.Rel_Email__c != rel.Contact_Email1__c ){
                return true;
        }
        else return false;
    }

    public static Boolean updateInRel (Relationship__c oldRel, Relationship__c rel){
        if( oldRel.Contact_Mobile1__c != rel.Contact_Mobile1__c ||
            oldRel.Contact_Other_Phone1__c != rel.Contact_Other_Phone1__c ||
            oldRel.Contact_Email1__c != rel.Contact_Email1__c){
                return true;
        }
        else return false;
    }
}