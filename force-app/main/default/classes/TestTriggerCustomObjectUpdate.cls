//This is a test case for a situation where a lead will be converted.  The developer must explicitly call the convert lead
//method to simulate the user action.
@isTest
Public class TestTriggerCustomObjectUpdate {
        static testMethod void TestReferralUpdate() {
        
        Test.startTest();
        // Insert the Lead
        List<Lead> leads = new List<Lead>();
        Lead leadt = new Lead (FirstName ='fname', LastName ='test', Email ='adfg@asdrfg.com',Living_Expenses_Customer__c = 1000.00, 
        Living_Expenses_currency__c = 1000.00, Expenses_Considered__c = 2000, Available_Income__c = 5000.00, Rent__c = 1000.00, LeadSource ='Other',
        Loan_Product__c = 'Consumer Loan', Dependant_Expenses__c = 1000, Calls_Made__c = 1, CreatedTime__c = '12:00 AM', CreatedTime24__c = '07:00');
        
        insert leadt;
        leadt.Expenses_Considered__c = 3000;
        leadt.Living_Expenses_Customer__c = 2000;
        leadt.CreatedTime__c = 'PM';
        leadt.CreatedTime24__c = '07:00';
        update leadt;

        Test.stopTest();   
        
        // Insert the custom object Record 
        Applicant_2__c Qualification_Criteria  = new Applicant_2__c (Lead__c = leadt.Id, Name = 'test', First_Name__c = 'first', Middle_Name__c = 'middle', Last_Name__c = 'last', Drivers_Licence_Number__c = '12345',
                                                                    Current_Street_Number__c = '123', Current_Street_Name__c = 'test', Current_Street_Type__c = 'street', Current_Suburb__c = 'Prospect',
                                                                    Current_state__c = 'Victoria', Current_Postcode__c = 3000, Current_Country__c = 'Australia', Mobile__c = '12345');
        insert Qualification_Criteria ; 
        Assets__c Qualification_Criteria2  = new Assets__c (Lead__c = leadt.Id, Value__c = 1000);
        insert Qualification_Criteria2 ; 
        Employee__c Qualification_Criteria3  = new Employee__c (Lead__c = leadt.Id, Contact_Phone__c = '1300123456');
        insert Qualification_Criteria3 ; 
        Liability__c Qualification_Criteria4  = new Liability__c (Lead__c = leadt.Id, Monthly_Payment__c = 150);
        insert Qualification_Criteria4 ; 
        Reference__c Qualification_Criteria5  = new Reference__c (Lead__c = leadt.Id, Phone__c = '0871231234');
        insert Qualification_Criteria5 ;        
        Form_Submissions__c Qualification_Criteria6 = new Form_Submissions__c (Lead__c = leadt.Id, Email__c = 'test@test.com');
        insert Qualification_Criteria6;    

        //Convert the Lead
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leadt.Id);
        LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        try{
        Database.LeadConvertResult lcr = Database.convertLead(lc);    
        }catch(Exception e){
        
        
        }
        
        //Requery for the referral record to see if it is updated
        Applicant_2__c ref_upd = [select Account__c from Applicant_2__c where Lead__c = :leadt.Id];
        Assets__c ref_upd2 = [select Account__c from Assets__c where Lead__c = :leadt.Id];
        Employee__c ref_upd3 = [select Account__c from Employee__c where Lead__c = :leadt.Id];
        Liability__c ref_upd4 = [select Account__c from Liability__c where Lead__c = :leadt.Id];
        Reference__c ref_upd5 = [select Account__c from Reference__c where Lead__c = :leadt.Id];
/*
        //Check that the test passed
        System.assertEquals(ref_upd.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteria.Lead__c].ConvertedAccountId);  
        System.assertEquals(ref_upd2.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteria2.Lead__c].ConvertedAccountId);  
        System.assertEquals(ref_upd3.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteria3.Lead__c].ConvertedAccountId);  
        System.assertEquals(ref_upd4.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteria4.Lead__c].ConvertedAccountId);  
        System.assertEquals(ref_upd5.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteria5.Lead__c].ConvertedAccountId);     
*/
        //Test if no opty is created
        string NoOpty = 'Y';        
        if (NoOpty =='Y'){
            Lead leadto = new Lead (FirstName ='fnameo', LastName ='testo', Email ='emailo@emailo.com', Living_Expenses_Customer__c = 1000.00, 
            Living_Expenses_currency__c = 1000.00,Available_Income__c = 5000.00, Rent__c = 1000.00, LeadSource ='Other',
            Loan_Product__c = 'Consumer Loan', Dependant_Expenses__c = 1000, Calls_Made__c = 1);
            
            insert leadto;
            
            // Insert the custom object record 
            Applicant_2__c Qualification_Criteriao = new Applicant_2__c (Lead__c = leadto.Id, Name = 'test', First_Name__c = 'first', Middle_Name__c = 'middle', Last_Name__c = 'last', Drivers_Licence_Number__c = '12345',
                                                                        Current_Street_Number__c = '123', Current_Street_Name__c = 'test', Current_Street_Type__c = 'street', Current_Suburb__c = 'Prospect',
                                                                        Current_state__c = 'Victoria', Current_Postcode__c = 3000, Current_Country__c = 'Australia', Mobile__c = '12345');
            insert Qualification_Criteriao;
            Assets__c Qualification_Criteriao2 = new Assets__c (Lead__c = leadto.Id, Value__c = 1000);
            insert Qualification_Criteriao2;
            Employee__c Qualification_Criteriao3 = new Employee__c (Lead__c = leadto.Id, Contact_Phone__c = '1300123456');
            insert Qualification_Criteriao3;
            Liability__c Qualification_Criteriao4 = new Liability__c (Lead__c = leadto.Id, Monthly_Payment__c = 150);
            insert Qualification_Criteriao4;
            Reference__c Qualification_Criteriao5 = new Reference__c (Lead__c = leadto.Id, Phone__c = '0871231234');
            insert Qualification_Criteriao5;


            Database.LeadConvert lco = new database.LeadConvert();
            lco.setLeadId(leadto.Id);
            lco.isDoNotCreateOpportunity();
            lco.setDoNotCreateOpportunity(true);
            LeadStatus convertStatuso = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
            lco.setConvertedStatus(convertStatuso.MasterLabel);
            Try{
            Database.LeadConvertResult lcro = Database.convertLead(lco); 
            }
            Catch(Exception e){
            }
            Applicant_2__c ref_updo = [select Account__c from Applicant_2__c where Lead__c = :leadto.Id];
            Assets__c ref_updo2 = [select Account__c from Assets__c where Lead__c = :leadto.Id];
            Employee__c ref_updo3 = [select Account__c from Employee__c where Lead__c = :leadto.Id];
            Liability__c ref_updo4 = [select Account__c from Liability__c where Lead__c = :leadto.Id];
            Reference__c ref_updo5 = [select Account__c from Reference__c where Lead__c = :leadto.Id];
/*
            //Check that the test passed
            System.assertEquals(ref_updo.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteriao.Lead__c].ConvertedAccountId);
            System.assertEquals(ref_updo2.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteriao2.Lead__c].ConvertedAccountId);
            System.assertEquals(ref_updo3.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteriao3.Lead__c].ConvertedAccountId);
            System.assertEquals(ref_updo4.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteriao4.Lead__c].ConvertedAccountId);
            System.assertEquals(ref_updo5.Account__c,[Select ConvertedAccountId From Lead Where Id = :Qualification_Criteriao5.Lead__c].ConvertedAccountId);
*/
        }  
        
    }

    static testMethod void testBulkUpdate() {
        List<Lead> leads = new List<Lead>();  
             
            test.StartTest();
            Lead l = new Lead (FirstName ='bulk', LastName ='Test', Email ='emailo@emailo.com', Living_Expenses_Customer__c = 1000.00, 
            Living_Expenses_currency__c = 1000.00,Available_Income__c = 5000.00, Rent__c = 1000.00, 
            LeadSource ='Other', Loan_Product__c = 'Consumer Loan', Dependant_Expenses__c = 1000, Calls_Made__c = 1, CreatedTime__c = '12:00 AM', CreatedTime24__c = '08:00');
            insert l;
 
             // Insert the custom Record 
            Applicant_2__c r = new Applicant_2__c (Lead__c = l.Id, Name = 'test', First_Name__c = 'first', Middle_Name__c = 'middle', Last_Name__c = 'last', Drivers_Licence_Number__c = '12345', 
                                                   Current_Street_Number__c = '123', Current_Street_Name__c = 'test', Current_Street_Type__c = 'street', Current_Suburb__c = 'Prospect',
                                                   Current_state__c = 'Victoria', Current_Postcode__c = 3000, Current_Country__c = 'Australia', Mobile__c = '12345');
            insert r;
            Assets__c r2 = new Assets__c (Lead__c = l.Id, Value__c = 1000);
            insert r2;
            Employee__c r3 = new Employee__c (Lead__c = l.Id, Contact_Phone__c = '1300123456');
            insert r3;
            Liability__c r4 = new Liability__c (Lead__c = l.Id, Monthly_Payment__c = 150);
            insert r4;
            Reference__c r5 = new Reference__c (Lead__c = l.Id, Phone__c = '0871231234');
            insert r5;
        	Form_Submissions__c r6 = new Form_Submissions__c (Lead__c = l.Id, Email__c = 'test@test.com');
        	insert r6; 
        
            Test.stopTest(); 
            //Convert the Lead
            try{
            Database.LeadConvert lcb = new database.LeadConvert();

            lcb.setLeadId(l.Id);
            LeadStatus convertStatusb = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
            lcb.setConvertedStatus(convertStatusb.MasterLabel);
            Database.LeadConvertResult lcrb = Database.convertLead(lcb);
            
            }catch(exception e){
            
            }
        }

                
}