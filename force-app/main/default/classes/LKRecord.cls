/*** Represents a record for loankit ***/
/*** Holds the request and response data of a record ***/
public class LKRecord {
    public String sfObjectId;  // ID of salesforce object
    public String lkObjectId;  // Loankit Id in response for an object
    public String lkObjectId2;
    public String lkObjectId3;
    public String lkObjectId4;
    public SObject sObjectRef; // sobject of sf record 
    public Map<String,String> responseMap;  // loan kit response 
    XMLMapping mappingObject;   // xml node configuration
    public boolean isProcessed = false;
    public String applicantRefId;
    public Map<String,Object> lkValuesMap = new Map<String,Object>(); // loan kit key and salesforce values
    String fieldToUpdate ='';
    public String fieldToUpdate2 = '';
    public String fieldToUpdate3 = '';
    public String fieldToUpdate4 = '';
    public String applicantLKLink;
    public boolean checkForUpdate = false;
    public boolean sendToLK = true;
    
    public void setFieldToUpdate(String field) {
        fieldToUpdate = field;
    }
    public String getFieldToUpdate() {
        if(fieldToUpdate == null || fieldToUpdate.length() == 0) {
            return mappingObject.getFieldToUpdate();
        }
        return fieldToUpdate;
    }
    public void setMappingObject(XMLMapping nodeObj) {
        mappingObject = nodeObj;
    }
    public XMLMapping getMappingObject() {
        return mappingObject;
    }
    public void setLkObjectId(String id) {
        lkObjectId = id;
    }
    public String getLkObjectId() {
        return lkObjectId;
    }
    public String getMappingName() {
        return mappingObject.getLKObjectName();
    }
}