/*
    @Description: test class of LiabilityTrigger, LiabilityTriggerHandler
    @Author: Raoul Lake, Jesfer Baculod - Positive Group
    @History:
        09/11/17 - Updated, restructured test class, added testSLACalculations test method
*/
@isTest
private class LiabilityTriggerTest {

	 private static final string ACC_PRIVATE_RT = Label.Account_Private_RT; //Private
	 private static final string CON_KEYPERSON_RT = Label.Contact_Key_Person_RT; //Key Person
	 private static final string LIAB_TYP_MORT_LOAN = Label.Liability_Type_Mortgage_Loan; //Mortgage Loan
	 private static final string LIAB_TYP_CC = Label.Liability_Type_Credit_Card; //Credit Card
	 private static final string LIAB_TYP_ULOAN = Label.Liability_Type_Unsecured_Loan; //Unsecured Loan
	 private static final string LIAB_TYP_OUT_TAXATION = Label.Liability_Type_Outstanding_Taxation; //Outstanding Taxation

	 @testsetup static void setup(){
        
        List<Id> AccoutnRecIds = new List<ID>();
            for(RecordType rec : [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' AND Name != : ACC_PRIVATE_RT]){
                AccoutnRecIds.add(rec.Id);
            }


        //Create test data for Account
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.RecordtypeId = AccoutnRecIds[0];
		insert acc;

		//Create test data for Contact
		Id ContactRecordTypeId = [select id from RecordType where Name =: CON_KEYPERSON_RT AND SobjectType='Contact'].Id;
        Contact Cnt = new Contact(LastName = 'Lname', RecordtypeId = ContactRecordTypeId, Accountid = acc.Id);
		insert cnt ;

	 }

	 static testmethod void rollupLiabilitiesTest(){

	 	Account acc = [Select Id, Name From Account];
	 	Contact con = [Select Id, Name From Contact];

	 	Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_Liability_Trigger__c = true,
                Enable_Liability_rollupLiabilities__c = true
            );
        insert trigset;

	 	Test.startTest();
	        
	        Liability__c liab_acc = new Liability__c(
	        		Account__c = acc.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_MORT_LOAN
	        	);
	        insert liab_acc;
	        
	        Liability__c liab_acc2 = new Liability__c(
	        		Account__c = acc.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_CC
	        	);
	        insert liab_acc2;

	        Liability__c liab_acc3 = new Liability__c(
	        		Account__c = acc.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_ULOAN
	        	);
	        insert liab_acc3;

	        liab_acc3.Monthly_Payment__c = 3.0;
	        update liab_acc3;

	        delete liab_acc3;

	        Liability__c liab_acc4 = new Liability__c(
	        		Account__c = acc.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_OUT_TAXATION
	        	);
	        insert liab_acc4;


	        Liability__c liab_con = new Liability__c(
	        		Key_Person_Name__c = con.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_MORT_LOAN
	        	);
	        insert liab_con;

	        Liability__c liab_con2 = new Liability__c(
	        		Key_Person_Name__c = con.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_CC
	        	);
	        insert liab_con2;

	        Liability__c liab_con3 = new Liability__c(
	        		Key_Person_Name__c = con.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_ULOAN
	        	);
	        insert liab_con3;

	        Liability__c liab_con4 = new Liability__c(
	        		Key_Person_Name__c = con.id,
	        		Monthly_Payment__c = 2.0,
	        		Type1__c = LIAB_TYP_OUT_TAXATION
	        	);
	        insert liab_con4;

	        Liability__c liab_not = new Liability__c(
	        		Account__c = acc.Id,
	        		Key_Person_Name__c = con.Id
	        	);
	        insert liab_not;

	 	Test.stopTest();

	 }
}