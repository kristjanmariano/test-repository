/*
	@Description: Utility Class for Preferred Call Time Feature
	@Author: Rexie David - Positive Group
	@History:
		-	10/09/2019 - extra/task25953355-NODFlowV3SFAutomations - Reusable class for handling Preferred Call Time
*/
public class TimeUtils {
    public static Map<String,BusinessHours> stateBusinessHoursMap = new Map<String,BusinessHours>();
    public static List<BusinessHours> busiHoursList = new List<BusinessHours>();
    public static BusinessHours busiHours = new BusinessHours();
    public static Map<String,List<Boolean>> stateBusinessDayMap = new Map<String,List<Boolean>>();
    public static Map<String,List<DateTime>> stateBusinessStartTimeMap = new Map<String,List<DateTime>>();
	public static Map<String,List<DateTime>> stateBusinessEndTimeMap = new Map<String,List<DateTime>>();
	public static Integer i = Math.mod(Math.abs(date.newInstance(2013, 1, 6).daysBetween(System.today())),7);
	public static Date myDateTomorrow = System.today().addDays(1);

	private static DateTime currentStartDateInCaseTZ = DateTime.newInstance(Date.today(), Time.newInstance(9, 0, 0, 0));//.addMilliseconds(offset) 5pm
	private static DateTime currentEndDateInCaseTZ = DateTime.newInstance(Date.today(), Time.newInstance(17, 0, 0, 0));//.addMilliseconds(offset)	7pm
	
	public static Boolean isCurrentDayBusinessDayInCaseBH = false;
	

	public static void TimeUtils(){
		// System.debug('TimeUtils constructor');
		if(stateBusinessHoursMap.isEmpty()){	
			// System.debug('TimeUtils constructor Query HERE');	
			if(busiHoursList.size() == 0) busiHoursList = [select Id,IsDefault,Name,IsActive,TimeZoneSidKey,SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime from BusinessHours where IsActive = true];
			busiHours = getDefaultBusinessHours(busiHoursList);
		} 
	}

	/**
	* @Description: Returns a converted DateTime Based on the specified TimeZoneIdString ('Australia/Perth') and Time parameters
	* @Arguments:	String clientTimeZoneIdString (e.g. 'Australia/Perth'), Time preferredCallTimeOnClientTZ (e.g. '2:00PM')
	* @Returns:		local date time of client's preferred call time.
	*/
    public static DateTime getLocalClientPreferredTime (String clientTimeZoneIdString, Time preferredCallTimeOnClientTZ){
        // This is the Date and Time in the users TimeZone
        String customerDateTimeString = String.valueOf(System.today())+' '+preferredCallTimeOnClientTZ;
        DateTime customerDateTime = DateTime.valueof(customerDateTimeString);
        TimeZone customerTimeZone = TimeZone.getTimeZone(clientTimeZoneIdString);//'Australia/Perth';
        TimeZone userTimeZone = UserInfo.getTimezone();
        Integer offsetToCustomersTimeZone = customerTimeZone.getOffset(customerDateTime);
        // integer offsetuserTimeZone = UserInfo.getTimezone().getOffset(customerDateTime);
        Integer offsetuserTimeZoneNow = userTimeZone.getOffset(Datetime.now());
        Integer timeDiffInSecs = ((offsetuserTimeZoneNow - offsetToCustomersTimeZone)/1000);
        DateTime localDateTime = customerDateTime.addSeconds(timeDiffInSecs);

        System.debug('Client Time = ' + clientTimeZoneIdString + String.valueOf(customerDateTime.format()));
        System.debug('Agent Time = ' + userTimeZone.getID() + String.valueOf(localDateTime.format()));
        return localDateTime;
    }

    /**
	* @Description: Returns a Map of State (eg."SA") and BusinessHour record - Invoked in FE Sub Trigger
	* @Arguments:	n/a
	* @Returns:		Returns a Map of State (eg."SA") and BusinessHour record
	*/
    public static Map<String,BusinessHours> getStateBusinessHoursMap(){
        Map<String,BusinessHours> stateBusinessHoursMap = new Map<String,BusinessHours>();

        for(BusinessHours bh : [SELECT Id,IsDefault,Name,IsActive,TimeZoneSidKey from BusinessHours where IsActive = true]){
            stateBusinessHoursMap.put(bh.Name,bh);
        }
        return stateBusinessHoursMap;
    } 

    // public static void initBusinessHoursVariables(){
	// 	if(stateBusinessHoursMap.isEmpty()){		
	// 		if(busiHoursList.size() == 0) busiHoursList = [select Id,IsDefault,Name,IsActive,TimeZoneSidKey,SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime, SundayEndTime, MondayEndTime,TuesdayEndTime, WednesdayEndTime, ThursdayEndTime, FridayEndTime,SaturdayEndTime from BusinessHours where IsActive = true];
	// 		busiHours = getDefaultBusinessHours(busiHoursList);
	// 	} 
	// }

    public static BusinessHours getDefaultBusinessHours(List<BusinessHours> busiHoursList){
		// System.debug('TimeUtils getDefaultBusinessHours busiHoursList --> '+busiHoursList.size());	
		BusinessHours defaultBH = new BusinessHours();
	
		for(BusinessHours bh : busiHoursList){
			if(bh.IsDefault) defaultBH = bh;
			stateBusinessHoursMap.put(bh.Name,bh);
			
			List<Boolean> businessDay = new Boolean[7];
			List<DateTime> startHours = new DateTime [7];
    		List<DateTime> endHours = new DateTime [7];

			businessDay[0] = (bh.SundayStartTime != null);
			businessDay[1] = (bh.MondayStartTime != null);
			businessDay[2] = (bh.TuesdayStartTime != null);
			businessDay[3] = (bh.WednesdayStartTime != null);
			businessDay[4] = (bh.ThursdayStartTime != null);
			businessDay[5] = (bh.FridayStartTime != null);
			businessDay[6] = (bh.SaturdayStartTime != null);
			
			startHours[0] = (bh.SundayStartTime != null) ? DateTime.newInstance(Date.today(),bh.SundayStartTime) : null;
			startHours[1] = (bh.MondayStartTime != null) ? DateTime.newInstance(Date.today(),bh.MondayStartTime) : null;
			startHours[2] = (bh.TuesdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.TuesdayStartTime) : null;
			startHours[3] = (bh.WednesdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.WednesdayStartTime) : null;
			startHours[4] = (bh.ThursdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.ThursdayStartTime) : null;
			startHours[5] = (bh.FridayStartTime != null) ? DateTime.newInstance(Date.today(),bh.FridayStartTime) : null;
			startHours[6] = (bh.SaturdayStartTime != null) ? DateTime.newInstance(Date.today(),bh.SaturdayStartTime) : null;

			endHours[0] = (bh.SundayEndTime != null) ? DateTime.newInstance(Date.today(),bh.SundayEndTime) : null;
			endHours[1] = (bh.MondayEndTime != null) ? DateTime.newInstance(Date.today(),bh.MondayEndTime) : null;
			endHours[2] = (bh.TuesdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.TuesdayEndTime) : null;
			endHours[3] = (bh.WednesdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.WednesdayEndTime) : null;
			endHours[4] = (bh.ThursdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.ThursdayEndTime) : null;
			endHours[5] = (bh.FridayEndTime != null) ? DateTime.newInstance(Date.today(),bh.FridayEndTime) : null;
			endHours[6] = (bh.SaturdayEndTime != null) ? DateTime.newInstance(Date.today(),bh.SaturdayEndTime) : null;
			
			stateBusinessDayMap.put(bh.Name,businessDay);
			stateBusinessStartTimeMap.put(bh.Name,startHours);
			stateBusinessEndTimeMap.put(bh.Name,endHours);
			//break;
		}
		return defaultBH;
	}

	/**
	* @Description: Returns a converted DateTime Based on the respected AU State Timezone
	* @Arguments:	String auState (e.g. 'WA'), Datetime preferredCallTimeLocal (e.g. '24/09/2019 7:30 PM')
	* @Returns:		local date time of client's preferred call time RESPECTING TIMEZONE BUSINESS HOURS OF CLIENT'S AUSTRALIAN STATE
	*/
	public static Boolean callNow = true;
	public static Datetime setTimezoneBasedReadyToDialTime (String auState, Datetime preferredCallTimeLocal){
		Datetime readyToDialTime;
		if(preferredCallTimeLocal == NULL) preferredCallTimeLocal = system.now();
		System.debug('preferredCallTimeLocal'+preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa'));
		if(stateBusinessHoursMap.isEmpty()){	
			TimeUtils();
		}
		
		Timezone recStateTZ = Timezone.getTimeZone(String.valueOf(stateBusinessHoursMap.get(auState).TimeZoneSidKey));
		Timezone currUserTZ = UserInfo.getTimeZone();

		if(!Test.isRunningTest()){
			isCurrentDayBusinessDayInCaseBH = stateBusinessDayMap.get(auState)[i];
		}
		


		Datetime nextStartofSA = BusinessHours.nextStartDate(stateBusinessHoursMap.get('SA').Id, preferredCallTimeLocal);//.addHours(1)); // Get next Start Date of Case Business Hour
	
		Date nextStartDate = date.newinstance(nextStartofSA.year(), nextStartofSA.month(), nextStartofSA.day());

		Integer timeDiffInSecs = (currUserTZ.getOffset(preferredCallTimeLocal) - recStateTZ.getOffset(preferredCallTimeLocal))/1000;
		
		DateTime businessDayStartDateTimeInGMTinSA = (stateBusinessStartTimeMap.get('SA')[i] != NULL) ? stateBusinessStartTimeMap.get('SA')[i] : null; 
		DateTime businessDayEndDateTimeInGMTinSA = (stateBusinessEndTimeMap.get('SA')[i] != NULL) ? stateBusinessEndTimeMap.get('SA')[i] : null; 

		
		DateTime businessDayStartDateTimeInGMTbasedonState = (stateBusinessStartTimeMap.get(auState)[i] != NULL) ? stateBusinessStartTimeMap.get(auState)[i].addSeconds(timeDiffInSecs) : null; 
		DateTime businessDayEndDateTimeInGMTbasedonState = (stateBusinessEndTimeMap.get(auState)[i] != NULL) ? stateBusinessEndTimeMap.get(auState)[i].addSeconds(timeDiffInSecs) : null;

		DateTime nextStartDateOfState;

		
		if(i == 6){
			nextStartDateOfState = stateBusinessStartTimeMap.get(auState)[0].addSeconds(timeDiffInSecs);
		}else{
			nextStartDateOfState = stateBusinessStartTimeMap.get(auState)[i+1].addDays(1).addSeconds(timeDiffInSecs);
		}
		
		DateTime convertedLocalTime = Datetime.valueOf(preferredCallTimeLocal);
		System.debug('converted local time'+ convertedLocalTime);
		System.debug('--------------------------');
		if(isCurrentDayBusinessDayInCaseBH){
			if(preferredCallTimeLocal >= businessDayStartDateTimeInGMTinSA && preferredCallTimeLocal <= businessDayEndDateTimeInGMTinSA){
				System.debug('Today is '+ preferredCallTimeLocal.format('E') +' and current time ('+ preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)) + ' -- ' + currUserTZ + ') is within business hours ('+businessDayStartDateTimeInGMTinSA.format('h:mm:ss aaa') + ' To '+ businessDayEndDateTimeInGMTinSA.format('h:mm:ss aaa') + ')');
				System.debug('setTimezoneBasedReadyToDialTime --> Ready_To_Dial_Time__c --> '+ preferredCallTimeLocal.format('E'));
				System.debug('setTimezoneBasedReadyToDialTime --> Ready_To_Dial_Time__c in '+ currUserTZ + ' --> '+ preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)));
				readyToDialTime = preferredCallTimeLocal;
			}
			else if(preferredCallTimeLocal < businessDayStartDateTimeInGMTinSA && preferredCallTimeLocal  < businessDayEndDateTimeInGMTinSA){
				System.debug('Today is '+ preferredCallTimeLocal.format('E') +' and current time ('+ preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)) + ' -- ' + currUserTZ + ') is before business hours ('+businessDayStartDateTimeInGMTinSA.format('h:mm:ss aaa') + ' To '+ businessDayEndDateTimeInGMTinSA.format('h:mm:ss aaa') + ')');
				System.debug('setTimezoneBasedReadyToDialTime --> Ready_To_Dial_Time__c --> '+ businessDayStartDateTimeInGMTinSA.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)));
				System.debug('converted'+ preferredCallTimeLocal.format('E', String.valueOf(recStateTZ)));
				
				readyToDialTime = businessDayStartDateTimeInGMTbasedonState;
				callNow = false;
			}
			else if(preferredCallTimeLocal > businessDayStartDateTimeInGMTinSA && preferredCallTimeLocal > businessDayEndDateTimeInGMTinSA){
				System.debug('Today is '+ preferredCallTimeLocal.format('E') +' and current time ('+ preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)) + ' -- ' + currUserTZ + ') is after business hours ('+businessDayStartDateTimeInGMTinSA.format('h:mm:ss aaa') + ' To '+ businessDayEndDateTimeInGMTinSA.format('h:mm:ss aaa') + ')');
				System.debug('myDateTomorrow is '+myDateTomorrow.format());
				if(myDateTomorrow <= nextStartDate){				
					System.debug('Set Ready_To_Dial_Time__c to Next Business Start Date ('+ nextStartofSA.format() +')');
					
					System.debug('setTimezoneBasedReadyToDialTime --> Ready_To_Dial_Time__c --> in ' + recStateTZ + ' -- > ' + nextStartofSA.format('dd/MM/yyyy h:mm aaa', String.valueOf(recStateTZ)));	
					System.debug('setTimezoneBasedReadyToDialTime --> Ready_To_Dial_Time__c --> in ' + currUserTZ + ' -- > ' + nextStartofSA.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)));
					
					readyToDialTime = nextStartDateOfState;
					callNow = false;
				}
			}
		}
		else{
			if(preferredCallTimeLocal >= currentStartDateInCaseTZ && preferredCallTimeLocal  <= currentEndDateInCaseTZ){
				System.debug('Today is '+ preferredCallTimeLocal.format('E') +' and current time ('+ preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)) + ' -- ' + currUserTZ + ') is within Holiday business hours ('+currentStartDateInCaseTZ.format('h:mm:ss') + ' To '+ currentEndDateInCaseTZ.format('h:mm:ss') + ')');			
				readyToDialTime = preferredCallTimeLocal;
			}
			else if(preferredCallTimeLocal  < currentStartDateInCaseTZ && preferredCallTimeLocal  < currentEndDateInCaseTZ){	
				System.debug('Today is '+ preferredCallTimeLocal.format('E') +' and current time ('+ preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)) + ' -- ' + currUserTZ + ') is before Holiday business hours ('+currentStartDateInCaseTZ.format('h:mm:ss') + ' To '+ currentEndDateInCaseTZ.format('h:mm:ss') + ')');	
				readyToDialTime = currentStartDateInCaseTZ;		
				callNow = false;
			}
			else if(preferredCallTimeLocal  > currentStartDateInCaseTZ && preferredCallTimeLocal  > currentEndDateInCaseTZ){
				System.debug('Today is '+ preferredCallTimeLocal.format('E') +' and current time ('+ preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)) + ' -- ' + currUserTZ + ') is after Holiday business hours ('+currentStartDateInCaseTZ.format('h:mm:ss') + ' To '+ currentEndDateInCaseTZ.format('h:mm:ss') + ')');
				System.debug('myDateTomorrow is '+myDateTomorrow);
				System.debug('nextStartDate is '+nextStartDate);
				System.debug('nextStartofSA is '+nextStartofSA);
				if(myDateTomorrow <= nextStartDate){
					readyToDialTime = nextStartDateOfState;
					callNow = false;
				}
			}
		}

		System.debug('local time converted based on state' + preferredCallTimeLocal.format('dd/MM/yyyy h:mm aaa', String.valueOf(recStateTZ)));
		System.debug('currentStartDateInCaseTZ' + currentStartDateInCaseTZ);
		System.debug('currentEndDateInCaseTZ' + currentEndDateInCaseTZ);
		System.debug('currentStartDateInCaseTZ' + currentStartDateInCaseTZ.format('dd/MM/yyyy h:mm aaa', String.valueOf(recStateTZ)));
		System.debug('currentEndDateInCaseTZ' + currentEndDateInCaseTZ.format('dd/MM/yyyy h:mm aaa', String.valueOf(recStateTZ)));
		System.debug('setTimezoneBasedReadyToDialTime --> businessDayStartDateTimeInGMTinSA in recStateTZ'+ recStateTZ +' --> ' + businessDayStartDateTimeInGMTinSA.format('dd/MM/yyyy h:mm aaa', String.valueOf(recStateTZ)));
		System.debug('setTimezoneBasedReadyToDialTime --> businessDayStartDateTimeInGMTinSA in currUserTZ'+ currUserTZ +' --> ' + businessDayStartDateTimeInGMTinSA.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)));
		System.debug('setTimezoneBasedReadyToDialTime --> businessDayEndDateTimeInGMTinSA in recStateTZ'+ recStateTZ +' --> ' + businessDayEndDateTimeInGMTinSA.format('dd/MM/yyyy h:mm aaa', String.valueOf(recStateTZ)));
		System.debug('setTimezoneBasedReadyToDialTime --> businessDayEndDateTimeInGMTinSA in currUserTZ'+ currUserTZ +' --> ' + businessDayEndDateTimeInGMTinSA.format('dd/MM/yyyy h:mm aaa', String.valueOf(currUserTZ)));

		return readyToDialTime;
	}
}