/*
    @author: Jesfer Baculod - Positive Group
    @history:  09/27/17 - Created
               10/11/17 - Updated
    @description: controller extension class of Box_PGHome and Box_Case_CustomControls VF pages
*/
public class Box_PGExtension {

     public string sJWT {get;set;}
     public string authparams {get;set;}
     public string con_status {get;set;}
     public string resMSG {get;set;}
     public string access_token {get;set;}
     public string app_access_token {get;set;}
     public string app_access_token_cuser {get;set;}
     public string app_access_token_madmin {get;set;}
     public string boxauthtype {get;set;}
     public string urlhome {get;set;}
     public string urlcurpage {get;set;}
     public string box_curfolder {get;set;}
     public string box_curfolderName {get;set;}
     private string box_curFolderOwnerID {get;set;}
     public string box_curUserID {get;set;}
     public string usermode {get;set;}
     public integer box_curFilesCount {get;set;}
     public integer box_curFoldersCount {get;set;}
     public map <string,string> BoxCurFolderNameMap {get;set;} //key - Box Folder ID, value - Box Folder Name
     public map <string,string> BoxCurFolderIDMap {get;set;} //key - Box Folder ID, value - Box Folder Name
     public map <string,string> BoxCurFolderFilesMap {get;set;}
     private map <string,boolean> BoxFolderAdminEditAccessMap {get;set;}
     public string sharedLink {get;set;}
     private string box_mainUserID {get;set;}
     public list <fileWrapper> fileWrList {get;set;}
     public map <string, folderWrapper> folderWrMap {get;set;}
     public string saveMSG {get;set;}
     private Box_Controls_Settings__c boxconset {get;set;}
     public boolean isLoaded {get;set;}
     public id recID {get;set;} //Record ID
     private Sobject sobj {get;set;}
     private string recName {get;set;}
     private string box_recfolderID_base {get;set;}

     public Box_PGExtension(ApexPages.StandardController con) { //Access Box Control from Record Detail
          boxconset = Box_Controls_Settings__c.getOrgDefaults();
          recID = con.getId();
          box_curfolderName = box_curfolder = '';
          if (recID.getSobjectType() == Schema.Case.SObjectType){ //Case Record
               sobj = (Case) database.query('Select Id, CaseNumber From Case Where Id = : recID');
               recName = String.valueof(sobj.get('CaseNumber'));
               system.debug('@@box_curfolderName:'+box_curfolderName);
               box_recfolderID_base = boxconset.Box_SF_Case_Folder_ID__c; //look into Case folder
          }
          system.debug('@@recID:'+recID);
          extInit();
     }

     public void extInit(){
          sharedLink = '';
          box_curFoldersCount = box_curFilesCount = 0;
          isLoaded = false;
          boxauthtype = 'enterprise';
          saveMSG = usermode = box_curFolderOwnerID = app_access_token = app_access_token_cuser = app_access_token_madmin = access_token = resMSG = con_status = sJWT = authparams = '';
          box_mainUserID = boxconset.Box_Main_Admin_User_ID__c; //'2436439530'; //Box Main Admin User ID (info@pls)
          BoxCurFolderNameMap = new map <string,string>(); //1st value - Parent Folder, succeeding values - SubFolders
          BoxCurFolderIDMap = new map <string,string>();
          BoxFolderAdminEditAccessMap = new map <string,boolean>();
          fileWrList = new list <fileWrapper>();
          folderWrMap = new map <string, folderWrapper>();
     }


     public Box_PGExtension() { //Access Box Control from Home tab
          boxconset = Box_Controls_Settings__c.getOrgDefaults();
          box_curfolder = boxconset.Box_SF_Root_Folder_ID__c; //'0'; //default folder (PGA-SF)
          system.debug('@@box_curfolder:'+box_curfolder);
          box_curfolderName = boxconset.Box_SF_Root_Folder_Name__c; //'PGA SF';
          extInit();
     }

     public void BoxAuthentication(){

          HttpRequest req = new HttpRequest();
          req.setEndpoint('https://api.box.com/oauth2/token');
          req.setMethod('POST');
          req.setHeader('Content-Type','application/x-www-form-urlencoded');
          req.setTimeout(60 * 1000);
          req.setBody(authparams);
          system.debug('@@authparams:'+authparams);

          Http http = new Http();
          HttpResponse res = http.send(req);
          system.debug('@@res.body:'+res.getBody());
          con_status = res.getStatus();
          resMSG = res.getBody();
          if (res.getStatusCode() == 200){ //success
               JSONParser parser = JSON.createParser(res.getBody());  
               while (parser.nextToken() != null) {  
                    String fieldName = parser.getCurrentName();
                    if (fieldName == 'access_token'){
                         if (access_token != null && access_token != '') { 
                              if (app_access_token_madmin == ''){
                                   app_access_token_madmin = parser.getText(); //get Box Main Admin user app token
                                   usermode = 'mainadmin';
                              }
                              else{
                                   app_access_token_cuser = parser.getText(); //get current user app token
                                   usermode = 'user';
                              }
                         }
                         else access_token = parser.getText(); //enterprise token
                    }
                    parser.nextToken();
               }
          }

          if (boxauthtype == 'enterprise'){
               BoxGetCurrentUser();
          }
     }

     public void BoxGetCurrentUser(){

          HttpRequest req = new HttpRequest();
          req.setEndpoint('https://api.box.com/2.0/users?user_type=all&fields=id&offset=0&filter_term='+UserInfo.getUserEmail());
          req.setMethod('GET');
          req.setHeader('Content-Type','application/x-www-form-urlencoded');
          req.setHeader('Authorization', 'Bearer '+access_token);
          req.setTimeout(60 * 1000);

          Http http = new Http();
          HttpResponse res = http.send(req);
          con_status = res.getStatus();
          system.debug('@@res.getBody:'+res.getBody());
          resMSG = res.getBody();
          if (res.getStatusCode() == 200){ //success
               Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
               List<Map<String, Object>> myMaps = new List<Map<String, Object>>();
               List<Object> myMapObjects = (List<Object>) meta.get('entries');
               for (Object obj : myMapObjects) {
                   myMaps.add((Map<String, Object>)obj);
               }
               for(Map<String, Object> sMap : myMaps){
                    if (sMap.containsKey('id')) box_curUserID = String.valueof(sMap.get('id')); //get Current SF User's BOX UserID
               }
          }
     }

     public void BoxGetCurrentRecordFolder(){
          isLoaded = false;
          HttpRequest req = new HttpRequest();
          req.setEndpoint('https://api.box.com/2.0/folders/' + box_recfolderID_base + '/items?limit=1000&fields=type,id,name&direction=DESC');
          req.setMethod('GET');
          req.setHeader('Content-Type','application/x-www-form-urlencoded');
          req.setHeader('Authorization', 'Bearer '+app_access_token_cuser);
          req.setTimeout(60 * 1000);

          system.debug('@@box_recfolderID_base:'+box_recfolderID_base);

          Http http = new Http();
          HttpResponse res = http.send(req);
          system.debug('@@res.body:'+res.getBody());
          resMSG = res.getBody();
          con_status = res.getStatus();

          if (res.getStatusCode() == 200){ //success
               Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
               List<Map<String, Object>> myMaps = new List<Map<String, Object>>();
               List<Object> myMapObjects = (List<Object>) meta.get('entries');
               for (Object obj : myMapObjects) {
                   myMaps.add((Map<String, Object>)obj);
               }
               for(Map<String, Object> sMap : myMaps){
                    if (sMap.containsKey('type')){
                         if (String.valueof(sMap.get('type')) == 'folder'){
                              if (String.valueof(sMap.get('name')) == recName){
                                   box_curfolder = String.valueof(sMap.get('id')); //get Current SF Record Box FOlder ID
                                   break;
                              }
                         }
                    } 
               }
          }
          box_curfolderName = recName;
          system.debug('@@box_curfolder:'+box_curfolder);
          system.debug('@@box_curfolderName:'+box_curfolderName);
          BoxGetCurrentFolder();

     }

     public void BoxGetCurrentFolder(){
          isLoaded = false;
          if (usermode == 'mainadmin'){
               app_access_token = app_access_token_madmin;
          }
          else {
               app_access_token = app_access_token_cuser;
          }
          system.debug('@@box_curfolder:'+box_curfolder);
          system.debug('@@usermode:'+usermode);

          HttpRequest req = new HttpRequest();
          req.setEndpoint('https://api.box.com/2.0/folders/' + box_curfolder); 
          req.setMethod('GET');
          req.setHeader('Content-Type','application/x-www-form-urlencoded');
          req.setHeader('Authorization', 'Bearer '+app_access_token);
          req.setTimeout(60 * 1000);

          Http http = new Http();
          HttpResponse res = http.send(req);
          system.debug('@@res.body:'+res.getBody());
          resMSG = res.getBody();
          con_status = res.getStatus();
          if (res.getStatusCode() == 200){ //success
               BoxCurFolderNameMap = new map <string,string>();
               BoxCurFolderIDMap = new map <string,string>();
               BoxCurFolderFilesMap = new map <string,string>();
               box_curfolderName = '';
               Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());

               box_curfolderName = String.valueof(meta.get('name')); //Current Folder Name
               Map<String, Object> myMapOwnerObj = (Map<String, Object>) meta.get('owned_by');
               box_curFolderOwnerID = String.valueof(myMapOwnerObj.get('id')); //get Folder Owner ID

               folderWrapper fWrcurrent = new folderWrapper(box_curfolderName,box_curfolder,'','','','','','');
               folderWrMap.put(box_curfolder,fWrcurrent);

               //Get Parent Folders of Current Folder
               List<Map<String, Object>> myMaps = new List<Map<String, Object>>();
               Map<String, Object> myMapObjects = (Map<String, Object>) meta.get('path_collection');
               List<Object> myMapObjectsChild = (List<Object>) myMapObjects.get('entries');
               for (Object obj : myMapObjectsChild){
                    myMaps.add((Map<String, Object>)obj);
               }
               integer parentctr = myMaps.size();
               string parentdot = '';
               for (integer i=0;i < parentctr; i++){
                    parentdot = parentdot + '.';
               }
               for(Map<String, Object> sMap : myMaps){
                    string currfolID;
                    folderWrapper fWr = new folderWrapper('','',box_curfolderName, box_curfolder,'','','','');
                    if (sMap.containsKey('id')){
                         string pvalue = String.valueof(sMap.get('id'));
                         currfolID = pvalue;
                         if (!BoxCurFolderIDMap.containskey(pvalue)){
                              BoxCurFolderIDMap.put(pvalue,pvalue); //get Folder IDs
                              fWr.folderID = pvalue;
                         }
                    }
                    if (sMap.containsKey('name')){
                         string pvalue = String.valueof(sMap.get('name'));
                         if (parentctr != 0 ) parentdot = parentdot.subString(0,parentctr);
                         if (!BoxCurFolderNameMap.containskey(currfolID)){
                              BoxCurFolderNameMap.put(currfolID,parentdot + pvalue); //get Folder Name
                               fWr.folderName = pvalue;
                         }
                         parentctr--;
                    }
                    folderWrMap.put(currfolID,fWr);
               }
               if (folderWrMap.containskey(box_curfolder)){
                    if (box_curFolderOwnerID == box_curUserID) folderWrMap.get(box_curfolder).ownedByCurUser = true; //Folder is Owned by Current User
               }

               //Get all SubFolders of Current Folder
               myMapObjects = (Map<String, Object>) meta.get('item_collection');
               List<Map<String, Object>> myMapsChild = new List<Map<String, Object>>();
               List<Object> myMapObjectsGrandChild = (List<Object>) myMapObjects.get('entries');
               for (Object obj : myMapObjectsGrandChild){
                    myMapsChild.add((Map<String, Object>)obj);
               }
               for(Map<String, Object> sMap : myMapsChild){
                    string currfolID;
                    boolean isFolder = false;
                    if (sMap.containsKey('type')){
                         string pvalue = String.valueof(sMap.get('type'));
                         if (pvalue == 'folder') isFolder = true;
                    }
                    if (isFolder){ //Display only Folders
                         folderWrapper fWr = new folderWrapper('','',box_curfolderName, box_curfolder,'','','','');
                         if (sMap.containsKey('id')){
                              string pvalue = String.valueof(sMap.get('id'));
                              currfolID = pvalue;
                              if (!BoxCurFolderIDMap.containskey(pvalue)){
                                   BoxCurFolderIDMap.put(pvalue,pvalue); //get Folder IDs
                                   fWr.folderID = pvalue;
                              }
                         }
                         if (sMap.containsKey('name')){
                              string pvalue = String.valueof(sMap.get('name'));
                              if (!BoxCurFolderNameMap.containskey(currfolID)){
                                   BoxCurFolderNameMap.put(currfolID,pvalue); //get Folder Name
                                   fWr.folderName = pvalue;
                              }
                         }
                         folderWrMap.put(fWr.folderID,fWr);
                    }
                    else{ //Display Files
                         if (sMap.containsKey('id')){
                              string pvalue = String.valueof(sMap.get('id'));
                              currfolID = pvalue; //ID
                              if (!BoxCurFolderFilesMap.containskey(pvalue)){
                                   BoxCurFolderFilesMap.put(pvalue,''); //get Folder IDs
                              }
                         }
                         if (sMap.containsKey('name')){
                              string pvalue = String.valueof(sMap.get('name')); //Name
                              BoxCurFolderFilesMap.put(currfolID,pvalue); //get File Name
                         } 
                    }
               }
               box_curFoldersCount = BoxCurFolderNameMap.size();
               box_curFilesCount = BoxCurFolderFilesMap.size();
          }
          isLoaded = true; //Displays the panel conten 
          system.debug('@@BoxCurFolderNameMap:'+BoxCurFolderNameMap);
          system.debug('@@BoxCurFolderIDMap:'+BoxCurFolderIDMap);
          system.debug('@@BoxCurFolderFilesMap:'+BoxCurFolderFilesMap);
          system.debug('@@folderWrMap:'+folderWrMap);

     }

     public void EditFolder(){
          BoxGetCurrentFolder(); //recall Current Folder to refresh list
          //use Main Admin App Token
          //- if main admin has no access add folder collaboration EDITOR to folder
          userMode = 'mainadmin';
          if (!BoxFolderAdminEditAccessMap.containskey(box_curfolder)){ //check if Main Admin already has access to current folder
               HttpRequest req = new HttpRequest();
               req.setEndpoint('https://api.box.com/2.0/folders/'+ box_curfolder + '/collaborations');
               req.setMethod('GET');
               req.setHeader('Content-Type','application/x-www-form-urlencoded');
               req.setHeader('Authorization', 'Bearer '+app_access_token); //Box Main Admin User App Token
               req.setTimeout(60 * 1000);

               Http http = new Http();
               HttpResponse res = http.send(req);
               system.debug('@@res.body:'+res.getBody());
               con_status = res.getStatus();
               resMSG = res.getBody();
               if (res.getStatusCode() == 200){ //success
                    system.debug('@@1');
                    addFolderCollabToMainAdmin(app_access_token, resMSG);
               }
          }
     }

     private void addFolderCollabToMainAdmin(string apptoken, string resBody){

          Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(resBody);
          if (String.valueof(meta.get('total_count')) == '0'){ //Check current Folder collaboration, folder is a private folder of Current User
               //Create folder collaboration for Main Admin
               HttpRequest reqCollab = new HttpRequest();
               reqCollab.setEndpoint('https://api.box.com/2.0/collaborations?notify=false');
               reqCollab.setMethod('POST');
               reqCollab.setHeader('Content-Type','application/x-www-form-urlencoded');
               reqCollab.setHeader('Authorization', 'Bearer '+apptoken); //Passed App Token from main Admin / User
               reqCollab.setTimeout(60 * 1000);
               string reqBody = '{"item": { "id": "'+ box_curfolder + '", "type": "folder"}, "accessible_by": { "id": "' + box_mainUserID + '", "type": "user" }, "role": "editor"}'; //JSON request
               reqCollab.setBody(reqBody);

               Http httpCollab = new Http();
               HttpResponse resCollab = httpCollab.send(reqCollab);
               system.debug('@@resCollab.body:'+resCollab.getBody());
               con_status = resCollab.getStatus();
               resMSG = resCollab.getBody();
               if (resCollab.getStatusCode() == 200){
                    BoxFolderAdminEditAccessMap.put(box_curfolder,true);
               }
               else BoxFolderAdminEditAccessMap.put(box_curfolder,false);
          }
          else{
               BoxFolderAdminEditAccessMap.put(box_curfolder,true);
          }

     }

     public void EditFiles(){
          //use Main Admin App Token
          //- if main admin has no access add folder collaboration EDITOR to folder
          userMode = 'mainadmin';
          if (!BoxFolderAdminEditAccessMap.containskey(box_curfolder)){ //check if Main Admin already has access to current folder
               HttpRequest req = new HttpRequest();
               req.setEndpoint('https://api.box.com/2.0/folders/'+ box_curfolder + '/collaborations');
               req.setMethod('GET');
               req.setHeader('Content-Type','application/x-www-form-urlencoded');
               req.setHeader('Authorization', 'Bearer '+app_access_token); //Box Main Admin User App Token
               req.setTimeout(60 * 1000);

               Http http = new Http();
               HttpResponse res = http.send(req);
               system.debug('@@res.body:'+res.getBody());
               con_status = res.getStatus();
               resMSG = res.getBody();
               if (res.getStatusCode() == 200){ //success
                    system.debug('@@2');
                    addFolderCollabToMainAdmin(app_access_token_madmin, resMSG);
               }
          }

          fileWrlist = new list <fileWrapper>();
          for (String s : BoxCurFolderFilesMap.keySet()){ //iterate to File IDs
               string fname = String.valueOf(BoxCurFolderFilesMap.get(s));
               fileWrapper fwr = new fileWrapper(
                         fname.subString(0,fname.lastIndexOf('.')), //File Name (don't include extension)
                         s, //File  Ids
                         box_curfolderName, //Folder Name
                         box_curfolder //Folder ID
                    );
               fwr.fileType = fname.substring(fname.lastIndexOf('.'),fname.length());
               fileWrList.add(fwr);
          }

     }

     public void EditCancel(){
          saveMSG = '';
          userMode = 'user';
          BoxGetCurrentFolder();
     }

     public void saveChangesEditFolder(){
          //https://api.box.com/2.0/folders/folder_id
          saveMSG = '';
          boolean withErrors = false;
          for (string folderMapKey : folderWrMap.keySet()){
               if (folderWrMap.get(folderMapKey).isUpdated){ //File Name was updated | File Folder was updated
                    if (box_curFolderOwnerID == box_curUserID) app_access_token = app_access_token_cuser;
                    else app_access_token = app_access_token_madmin;
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint('https://api.box.com/2.0/folders/' + folderMapKey);
                    req.setMethod('PUT');
                    req.setHeader('Content-Type','application/x-www-form-urlencoded');
                    req.setHeader('Authorization', 'Bearer '+app_access_token); //Box Main Admin User App Token
                    req.setTimeout(60 * 1000);
                    string reqBody = '{"name":"'+ folderWrMap.get(folderMapKey).folderName + '"}';
                    if (folderWrMap.get(folderMapKey).ownedByCurUser){ //Private Folder
                         if (folderWrMap.get(folderMapKey).pfolderID != ''){
                              reqBody = '{"name":"'+ folderWrMap.get(folderMapKey).folderName + '", "parent":{ "id" : "' + folderWrMap.get(folderMapKey).pfolderID + '" }}';
                         }
                    }
                    system.debug('@@reqBody:'+reqBody);
                    req.setBody(reqBody);

                    Http http = new Http();
                    HttpResponse res = http.send(req);
                    system.debug('@@res.body:'+res.getBody());
                    con_status = res.getStatus();
                    resMSG = res.getBody();
                    if (res.getStatusCode() == 200){ //success
                         withErrors = false;
                    }
                    else {
                         withErrors = true;
                         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, resMSG));
                    }
                    break;
               } 
          }
          if (!withErrors){
               saveMSG = Label.Box_Nav_Panel_Folder_Changes_Saved; //'Changes have been made on the folder.';
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, saveMSG));
               usermode = 'user'; //switch mode back to current user
               BoxGetCurrentFolder(); //recall Current Folder to refresh list
          }

     }

     public void saveChangesEditFiles(){
          //limit of 100 API Calls in 1 transaction
          //https://api.box.com/2.0/files/file_id
          saveMSG = '';
          boolean withErrors = false;
          for (fileWrapper fwr : fileWrlist){
               if (fwr.isSelected){ //File Name was updated | File Folder was updated
                    HttpRequest req = new HttpRequest();
                    req.setEndpoint('https://api.box.com/2.0/files/'+ fwr.fileID);
                    req.setMethod('PUT');
                    req.setHeader('Content-Type','application/x-www-form-urlencoded');
                    req.setHeader('Authorization', 'Bearer '+app_access_token_madmin); //Box Main Admin User App Token
                    req.setTimeout(60 * 1000);
                    string reqBody = '{"name":"'+ fwr.fileName + fwr.fileType + '", "parent":{ "id" : "' + fwr.filefolderID + '" }}';
                    system.debug('@@reqBody:'+reqBody);
                    req.setBody(reqBody);

                    Http http = new Http();
                    HttpResponse res = http.send(req);
                    system.debug('@@res.body:'+res.getBody());
                    con_status = res.getStatus();
                    resMSG = res.getBody();
                    if (res.getStatusCode() == 200){ //success
                         withErrors = false;
                    }
                    else {
                         withErrors = true;
                         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, resMSG));
                    }
               } 
          }
          if (!withErrors){
               saveMSG = Label.Box_Nav_Panel_File_Changes_Saved; //'Changes have been made on the files.';
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, saveMSG));
               usermode = 'user'; //switch mode back to current user
               BoxGetCurrentFolder(); //recall Current Folder to refresh list
          }


     }

     public class folderWrapper{ //folder selection
          public string folderName {get;set;} //folder name [sub folders]
          public string folderID {get;set;} //folder Id
          public string pfolderName {get;set;} //parent folder name [current folder]
          public string pfolderID {get;set;} //parent folder ID
          public string gfolderName {get;set;} //grand parent folder name [parent of current]
          public string gfolderID {get;set;}
          public string upfolderName {get;set;} //ultimate parent folder name
          public string upfolderID {get;set;} //ultimate parent folder ID
          public boolean isUpdated {get;set;}
          public boolean ownedByCurUser {get;set;}
          public integer totalFileCount {get;set;}
          public integer totalFolderCount {get;set;}
          public folderWrapper(string fname, string fID, string pfName, string pfID, string gfName, string gfID, string upfName, string upFID){
               folderName = fname;
               folderID = fID;
               pfolderName = pfName;
               pfolderID = pfID;
               gfolderName = gfName;
               gfolderID = gfID;
               upfolderName = upfName;
               upfolderID = upFID;
               isUpdated = false;
               ownedByCurUser = false;
               totalFolderCount = totalFileCount = 0;
          }
     }

     public class fileWrapper{
          public string filename {get;set;}    
          public string fileID {get;set;}
          public string filefolderName {get;set;}
          public string filefolderID {get;set;}
          public string fileType {get;set;}
          public boolean isSelected {get;set;}
          public fileWrapper(string fname, string fID, string ffName, string ffID){
               fileName = fname;
               fileID = fID;
               filefolderName = ffName;
               filefolderID = ffID;
               isSelected = false;
          }

     }

}