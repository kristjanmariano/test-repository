@isTest
Public class OutgoingsTriggerTest {

    public static testMethod void outgoingsTest(){
    
    Opportunity opp = new Opportunity (Name = 'Test', StageName = 'Open', CloseDate = date.today(), Outgoing_Amount2__c = 1000.0 );
    insert opp;
    
    Outgoings__c og = new Outgoings__c(Name = 'Test', Outgoing_Amount__c = 100, Opportunity__c = opp.Id, Deduct_from_Broker__c = true );
    insert og;
    
    og.Outgoing_Amount__c = 200;
    update og;
    delete og;
    
    Outgoings__c og2 = new Outgoings__c(Name = 'Test', Outgoing_Amount__c = null, Opportunity__c = opp.Id, Deduct_from_Broker__c = true );
    insert og2;
    update og2;
        
    Opportunity opp2 = new Opportunity (Name = 'Test', StageName = 'Open', CloseDate = date.today(), Outgoing_Amount2__c = 1000.0 );
    insert opp2;
    
    opp2.Outgoing_Amount2__c = 2000.0;
    update opp2;
    
    }
}