@isTest
public class CommissionLineTriggerTest {
        @testsetup static void setup(){
        //Turn On Trigger 
		Trigger_Settings1__c triggerSettings = Trigger_Settings1__c.getOrgDefaults();
		triggerSettings.Enable_Triggers__c = true;
		triggerSettings.Enable_Case_Trigger__c = true;
		triggerSettings.Enable_Role_Trigger__c = true;
        triggerSettings.Enable_Income_Trigger__c = true;
        triggerSettings.Enable_Front_End_Submission_Trigger__c = true;
		triggerSettings.Enable_Frontend_Sub_SetNVMContact__c = true;
        triggerSettings.Enable_CommissionCT_Trigger__c = true;
        triggerSettings.Enable_CommissionCT_GetCaseLender__c = true;
        triggerSettings.Enable_Commission_Line_Trigger__c = true;
        triggerSettings.Enable_CommissionLine_GetCommission__c = true;
		upsert triggerSettings Trigger_Settings1__c.Id;
        TestDataFactory.Case2FOSetupTemplate();
	}

    static testmethod void testFinance(){
        Test.StartTest();
            List<Case> caseList = [SELECT Id,NVMConnect__NextContactTime__c,Lender1__c FROM Case LIMIT 1];
            // System.assertNotEquals(caseList[0].Lender1__c,null);
            List<Partner__c> partnerList = TestDataFactory.createPartner(1, 'Lender Division', null);
            insert partnerList;
            caseList[0].Lender1__c = partnerList[0].Id;
            update caseList[0];
            System.assertEquals(caseList[0].Lender1__c,partnerList[0].Id);

            List<CommissionCT__c> commList = TestDataFactory.createCommission(1, 'NM - Asset Finance - Commission', caseList[0].Id);
            insert commList;
            update commList;
            List<Commission_Line_Items__c> commLineList = TestDataFactory.createCommissionLine(1,'Asset Finance - Finance Commission', commList[0].Id);
            insert commLineList;
            update commLineList;
            commLineList = [SELECT Id, h_CommissionCT_Finance__c, h_CommissionCT_Insurance__c FROM Commission_Line_Items__c WHERE Id =: commLineList[0].Id];
            System.assertEquals(commList[0].Id,commLineList[0].h_CommissionCT_Finance__c);
            System.assertEquals(NULL,commLineList[0].h_CommissionCT_Insurance__c);
            delete commList;
        Test.StopTest();
    }

    static testmethod void testInsurance(){
        Test.StartTest();
            List<Case> caseList = [SELECT Id,NVMConnect__NextContactTime__c,Lender1__c FROM Case LIMIT 1];
            List<Partner__c> partnerList = TestDataFactory.createPartner(1, 'Lender Division', null);
            insert partnerList;
            caseList[0].Lender1__c = partnerList[0].Id;
            update caseList[0];
            System.assertEquals(caseList[0].Lender1__c,partnerList[0].Id);

            List<CommissionCT__c> commList = TestDataFactory.createCommission(1, 'NM - Asset Finance - Commission', caseList[0].Id);
            insert commList;

            List<Commission_Line_Items__c> commLineList = TestDataFactory.createCommissionLine(1,'Asset Finance - Insurance Commission', commList[0].Id);
            insert commLineList;
            commLineList = [SELECT Id, h_CommissionCT_Finance__c, h_CommissionCT_Insurance__c FROM Commission_Line_Items__c WHERE Id =: commLineList[0].Id];
            System.assertEquals(NULL,commLineList[0].h_CommissionCT_Finance__c);
            System.assertEquals(commList[0].Id,commLineList[0].h_CommissionCT_Insurance__c);
            commLineList[0].h_CommissionCT_Insurance__c = null;
            update commLineList[0];
            delete commLineList;
            delete commList;
        Test.StopTest();
    }
}