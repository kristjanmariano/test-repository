/** 
* @FileName: FOShareGateway
* @Description: Provides finder methods for accessing data in the FOShare object.
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification 
* 1.0 4/23/18 RDAVID Created class, Added getRoleIds
* 1.1 3/13/19 JBACULOD Added retry due to Unable_to_lock_row issue
* 1.2 22/07/19 RDAVID: extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
**/ 

public without sharing class FOShareGateway implements Queueable{

	public list <Role__c> roleToUpdate;
	public FOShareGateway(list <Role__c> roleToUpdate){
		this.roleToUpdate = roleToUpdate;
	}

	public void execute(QueueableContext context){
		system.debug('@@roleToUpdate:'+roleToUpdate);
		if (roleToUpdate.size() > 0){ 
			try{
				update roleToUpdate;
			}
			catch (Exception e){
				update roleToUpdate; //Retry due to Unable_to_lock_Row issue
			}
		}
    }
	
	/**
	* @Description: RDAVID extra/task25350275-SeparateCommercialConsumerIncomeOnApplicationRole
	* @Arguments:	
	* @Returns:		void
	*/
	public static void setRoleLookup(List<FO_Share__c> newIncFoShareList, Map<Id,Schema.RecordTypeInfo> foShareRTIDMap){
		
		for(FO_Share__c incFoS : newIncFoShareList){
			if(incFoS.RecordTypeId != NULL && foShareRTIDMap.get(incFoS.RecordTypeId).getName() == 'Income'){
				if(incFoS.FO_Record_Type__c == 'Business Income' || incFoS.FO_Record_Type__c == 'Self-Employed'){
					incFoS.Role_Commercial_Income__c = incFoS.Role__c;
					incFoS.Role_Consumer_Income__c = null;
				}
				else{
					incFoS.Role_Consumer_Income__c = incFoS.Role__c;
					incFoS.Role_Commercial_Income__c = null;
				}
			}
		}
	}
}