/*
	@Description: This invocable class is being used in Process Builders below (NEW Version for extra/task25953355-NODFlowV3SFAutomations)
                    - Case Assignment (NM - PLS)
                    - Frontend Submission - On Create
                    - Support Request - Call Request to NVM
                ** Created this v2 to avoid conflicts on the scheduled actions of old Process Builder versions.
	@Author: Rexie David - Positive Group
	@History:
        -   12/09/2019 - CREATED Case_UpdateLFPFlowv2 because there are scheduled process on the Case_UpdateLFPFlow - extra/task25953355-NODFlowV3SFAutomations - Updates this will now be invoked in Support Request - Call Request to NVM (Nodifi) Process Builder after the creation of SR Call Request
*/
public class Case_UpdateLFPFlowv2 implements Queueable{

    private static final Trigger_Settings1__c trigSet = Trigger_Settings1__c.getInstance(UserInfo.getUserId());
    private static final String CONSUMERASSETQUEUE = 'Consumer Asset';
    private static final String OPENCASESTAGE = 'Open';
    private static final String WOPACASESTAGE = 'WOPA';
    private static final String NOD_FULLAPP = 'nod-fullapp';
    public List<Case> caseListToUpdate = new List<Case>();
    public static List<Case> caseList = new List<Case>();

    public Case_UpdateLFPFlowv2(List<Case> caseListToUpdate){
        this.caseListToUpdate = caseListToUpdate;
    }

    public void execute (QueueableContext context){
        System.debug('Case_UpdateLFPFlowv2 execute');
        if (caseListToUpdate.size() > 0){
            System.debug('Case_UpdateLFPFlowv2 execute caseListToUpdate '+ caseListToUpdate.size());
            Database.update(caseListToUpdate);
        }
    } 

    /** 
	* @FileName: updateLFPCase
	* @Description: RDAVID extra/task25953355-NODFlowV3SFAutomations 
                    Invocable method that Updates the Case to:
                    - OwnerId to Consumer Asset
                    - Stage__c to Open
                    - Deactivate_Client_Access_to_Full_App__c to TRUE
                    - Lead_Bucket__c = Consumer Asset
                    - PLS_Initial_Team_Queue__c = Consumer Asset
    * @Arguments:	list <Id> caseIDs
	* @Returns:		n/a

	**/ 
    @InvocableMethod(label='Update LFP Case v2' description='Update Case Stage from WOPA to NEW, Assign Case to Consumer_Motor_Team Queue, Deactivate Client Access to Full App')
	public static void updateLFPCase (list <Id> caseIDs) {
        if(caseIDs.size() > 0) caseList = [SELECT Id, State_Code__c, Stage__c, Latest_Frontend_Submission__c, Partition__c, (SELECT Id,Positive_Form_Type__c,Preferred_Call_Time__c,Preferred_Call_Date_Time_Local__c FROM Frontend_Submissions__r) FROM Case WHERE ID IN: caseIDs];
        if(caseList.size() > 0){
            Group consumerMotorQueue = [SELECT Id FROM Group WHERE Name =: CONSUMERASSETQUEUE LIMIT 1];
            List<Case> caseListToUpd = new List<Case>();

            for(Case cse : caseList){
                Boolean hasNOFullAppFE = CaseGateway.checkIfCaseHasNOFullAppFE(cse);
                Frontend_Submission__c reqCallFESub = new Frontend_Submission__c();
                if(hasNOFullAppFE){
                    if(cse.Stage__c == WOPACASESTAGE){
                        cse.OwnerId = consumerMotorQueue.Id; //Assign to LFP Flow Queue
                        cse.Stage__c = OPENCASESTAGE; //Set stage to New
                        cse.Deactivate_Client_Access_to_Full_App__c = TRUE;
                        cse.Lead_Bucket__c = CONSUMERASSETQUEUE;  //25/07/2019 - extra/tasks25398773-PLSCaseAssignmentsJulyUpdates //16/07/2019 - extra/task25317965-FixOnLFPFlow
                        cse.PLS_Initial_Team_Queue__c = CONSUMERASSETQUEUE; //25/07/2019 - extra/tasks25398773-PLSCaseAssignmentsJulyUpdates //16/07/2019 - extra/task25317965-FixOnLFPFlow
                        //Populate Case Preferred_Call_Time1__c AND Preferred_Call_Date_Time_Local__c
                        reqCallFESub = CaseGateway.getRequestCallFE(cse);
                        if(cse.Latest_Frontend_Submission__c == FrontEndSubmissionHandler.NOD_REQUEST_CALL && reqCallFESub.Id != NULL){
                            cse.Preferred_Call_Time1__c = reqCallFESub.Preferred_Call_Time__c;
                            cse.Preferred_Call_Date_Time_Local__c = reqCallFESub.Preferred_Call_Date_Time_Local__c;
                        } 
                        caseListToUpd.add(cse);
                    }
                }
            }
            if(caseListToUpd.size() > 0){
                System.debug('Case_UpdateLFPFlowv2 updateLFPCase caseListToUpd '+caseListToUpd.size());
                Case_UpdateLFPFlowv2 caseUpdateLFPFlow = new Case_UpdateLFPFlowv2(caseListToUpd);
                ID jobID = System.enqueueJob(caseUpdateLFPFlow);       
            }
        }
    }
}