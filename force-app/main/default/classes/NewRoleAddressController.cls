/**
 * @File Name          : NewRoleAddressController.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 05/09/2019, 2:21:20 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/09/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
 * 1.1    3/12/2019    rexie.david@positivelendingsolutions.com.au  extra/task26984298-EditRoleAddress - Updates to not allow Location with "FACEBOOK" to be saved as Full Address
**/
public class NewRoleAddressController {

    public Role__c role {get;set;}
    public Role_Address__c raddr {get;set;}
    public Address__c addr {get;set;}
    private ID curID {get;set;}
    public list <roleAddressWrapper> roleAddrWRlist {get;set;}
    public roleAddressWrapper raddrWR {get;set;}
    public boolean hasErrors {get;set;}
    public boolean showRoleAdddressEdit {get;set;}
    public boolean showAddressError {get;set;}
    public string curRoleAddrID {get;set;}
    private Id FullAddRT = Schema.SObjectType.Role_Address__c.getRecordTypeInfosByName().get('Full Address').getRecordTypeId();	
    private Id PostAddRT = Schema.SObjectType.Role_Address__c.getRecordTypeInfosByName().get('Postcode Only').getRecordTypeId();	

    public NewRoleAddressController(){
        
        showAddressError = hasErrors = false;
        roleAddrWrlist = new list <roleAddressWrapper>();
        addr = new Address__c();
        curID = Apexpages.currentpage().getparameters().get('id');
        system.debug('@@curID:'+curID);
        showRoleAdddressEdit = true;

        if (curID != null){
            role = [Select Id, Name, RecordTypeId, RecordType.Name From Role__c Where Id = : curID];
            raddr = new Role_Address__c( Role__c = curID, RecordTypeId = FullAddRT, Active_Address__c = 'Current');
            raddrWR = new roleAddressWrapper(raddr);
            //Retrieve existing Role Address in Role
            retrieveExistingRoleAddress();
            if (roleAddrWrlist.isEmpty()) showRoleAdddressEdit = true;
        }
        else {
            raddr = new Role_Address__c( RecordTypeId = FullAddRT, Active_Address__c = 'Current');
            raddrWR = new roleAddressWrapper(raddr);
        }

    }
    
    private void retrieveExistingRoleAddress(){
        roleAddrWrlist = new list <roleAddressWrapper>();
        list <Role_Address__c> roleAddrlist = [Select Id, Name, Start_Date__c, End_Date__c, RecordTypeId, RecordType.Name, Residential_Situation__c, Role__c, Location__c,
                                               Active_Address__c, Joint_Address__c, Full_Address__c, Time_at_Address_years__c, Address__c 
                                               From Role_Address__c Where Role__c =: curID Order By Start_Date__c ASC];
        for (Role_Address__c exraddr : roleAddrlist){
            roleAddressWrapper exraddrWr = new roleAddressWrapper(exraddr);
            if (exraddr.RecordType.Name == 'Postcode Only') exraddrWr.isPostCodeOnly = true;
            exraddrWr.rectypeName = exraddr.RecordType.Name;
            roleAddrWrlist.add(exraddrWr);
        }
    }

    public pageReference transformtoFullAddress(){
        raddrWr.raddr.RecordTypeId = FullAddRT;
        raddrWR.isPostCodeOnly = false;
        raddrWR.isChangedToFull = true;
        if(!Test.isRunningTest()) raddrWr.raddr.RecordType.Name = 'Full Address';
        raddrWr.rectypeName = 'Full Address';
        addr.Location__c = raddrWR.raddr.Location__c;
        populateAddresswithLocation();
        return null;
    }

    public pageReference saveRoleAddress(){
        Savepoint sp = Database.setSavepoint();
        try{
            if (raddrWr.raddr.Address__c == null){
                raddrWr.raddr.Address__c.addError('Address is required');
                hasErrors = true;
                return null;
            }
            if (raddrWR.raddr.Start_Date__c == null){
                raddrWR.raddr.Start_Date__c.addError('Start Date is required');
                hasErrors = true;
                return null;
            }
            if (raddrWR.raddr.Id == null) raddrWR.raddr.Role__c = role.Id;
            upsert raddrWR.raddr;
            hasErrors = false;

            if (roleAddrWrlist.size() > 0){ 
                retrieveExistingRoleAddress();
                showRoleAdddressEdit = false;
            }
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage() ));
            hasErrors = true;
            Database.rollback( sp );  
        }
        return null;
    }

    public PageReference editRoleAddress(){
        for (roleAddressWrapper exraddrWr : roleAddrWRlist){
            if (curRoleAddrID == exraddrWr.raddr.Id){
                raddrWr = exraddrWr;
                showRoleAdddressEdit = true;
                break;
            }
        }
        return null;
    }

    public PageReference newRoleAddress(){
        raddr = new Role_Address__c( Role__c = curID, RecordTypeId = FullAddRT, Active_Address__c = 'Current');
        raddrWR = new roleAddressWrapper(raddr);
        showRoleAdddressEdit = true;
        return null;
    }

    public PageReference showNewAddress(){
        addr = new Address__c();
        if (raddrWr.raddr.Location__c != null) addr.Location__c = raddrWr.raddr.Location__c;
        return null;
    }

    public PageReference populateAddresswithLocation(){
        if (addr.Location__c != null){
            //Retrieve selected Location
            list <Location__c> loclist = [Select Id, Suburb_Town__c, Postcode__c, State__c, State_ACR__c, Country__c From Location__c Where Id = : addr.Location__c AND Name != 'UNKNOWN' limit 1];
            if (!loclist.isEmpty()){
                //Populate Address fields with Location
                addr.Suburb__c = loclist[0].Suburb_Town__c;
                addr.Postcode__c = loclist[0].Postcode__c;
                addr.State__c = loclist[0].State__c;
                addr.State_ACR__c = loclist[0].State_ACR__c;
                addr.Country__c = loclist[0].Country__c;
            }
        }
        return null;
    }

    public boolean validateAddress(){
        boolean passed = true;
        integer errctr = 0;
        if (addr.Street_Number__c == '' || addr.Street_Number__c == null){
            addr.Street_Number__c.addError('Street Number is required');
            errctr++;
        }
        if (addr.Street_Name__c  == '' || addr.Street_Name__c  == null ){
            addr.Street_Name__c.addError('Street Name is required');
            errctr++;
        }
        if (addr.Street_Type__c == '' || addr.Street_Type__c == null ){ 
            addr.Street_Type__c.addError('Street Type is required');
            errctr++;
        }
        if (addr.Location__c == null){
            addr.Location__c.addError('Location is required');
            errctr++;
        }
        if (errctr > 0) passed = false;
        return passed;
    }

    private boolean checkDuplicateAddress(){
        boolean dupeAddrfound = false;
        list <Address__c> exdupeAddresses = [ SELECT Id, Name, Location__c, Suburb__c, Street_Name__c, Street_Type__c, Street_Number__c FROM Address__c WHERE Location__c =: addr.Location__c 
                            AND Street_Name__c =: addr.Street_Name__c AND Street_Number__c =: addr.Street_Number__c AND Street_Type__c =: addr.Street_Type__c];
        if (!exdupeAddresses.isEmpty()){
            for (Address__c exaddr : exdupeAddresses){
                string addrmatchKey = exaddr.Location__c + '-' + exaddr.Street_Name__c + '-' + exaddr.Street_Number__c + '-' + exaddr.Street_Type__c;
                string enteredAddress = addr.Location__c + '-' + addr.Street_Name__c + '-' + addr.Street_Number__c + '-' + addr.Street_Type__c;
                if(addr.Location__c != NULL && String.IsBlank(addr.Street_Name__c) && String.IsBlank(addr.Street_Number__c) && String.IsBlank(addr.Street_Type__c)){
                    dupeAddrfound = false;
                    break;
                }
                else{
                    if (addrmatchKey == enteredAddress){
                        raddrWr.raddr.Address__c = exaddr.Id; //Populate Role Address with found Address
                        dupeAddrfound = true;
                        break;
                    }
                }

            }
        }
        return dupeAddrfound;
    }

    public pageReference saveAssignNewAddress(){
        Savepoint sp = Database.setSavepoint();
        try{
            if (validateAddress()){
                if (checkDuplicateAddress()){
                }
                else {
                    insert addr;
                    raddrWR.raddr.Address__c = addr.Id;
                }
                showAddressError = false;
                return null;
            }
            else showAddressError = true;
        }
        //RDAVID 3/12/2019 - extra/task26984298-EditRoleAddress
        catch(Exception e){ 
            showAddressError = true;
            hasErrors = true;
            System.debug(e.getMessage());
            String errMsg;
            String locName = [SELECT Id,Name FROM Location__c WHERE Id =: addr.Location__c LIMIT 1].Name;
            if(e.getMessage().containsIgnoreCase('Invalid Location')){
                errMsg = 'Invalid Location '+ (!String.isBlank(locName) ? locName : '') + '. Please select a valid Location';
            }
            else {
                errMsg = e.getLineNumber()+'-'+ e.getMessage();
            }
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errMsg));
            Database.rollback( sp );  
        }
        return null;
    }

    public pageReference racancel(){
        showRoleAdddressEdit = false;
        return null;
    }
    
    public pageReference popUpCancel(){
        System.debug('raddrWR.raddr.RecordTypeId'+raddrWR.raddr.RecordTypeId);
        System.debug('PostAddRT'+PostAddRT);
        if(raddrWR.isChangedToFull){
            raddrWR.raddr.RecordTypeId = PostAddRT;
            raddrWR.isPostCodeOnly = true;
            raddrWr.rectypeName = 'Postcode Only';
            if(!Test.isRunningTest()) raddrWr.raddr.RecordType.Name = 'Postcode Only';
            raddrWR.isChangedToFull = false;
        }
		return null;        
    }

    public class roleAddressWrapper{
        public string rectypeName {get;set;}
        public Role_Address__c raddr {get;set;}
        public Boolean isSelected {get;set;}
        public Boolean isPostCodeOnly {get;set;}
        public Boolean isChangedToFull {get;set;}
        public roleAddressWrapper(Role_Address__c raddr1){
            rectypeName = 'Full Address';
            raddr = raddr1;
            isPostCodeOnly = isSelected = isChangedToFull = false;
        }
    }


}