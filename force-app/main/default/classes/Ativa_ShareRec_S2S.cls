//AT_NN 14-6-17
//This class creates the PartnerNetwordRecordConnection record that is needed for Salesforce 2 Salesforce
//This class is invoked from a cusom button called 'Submit_to_PCS'

global class Ativa_ShareRec_S2S
{
    webservice static void Mtd_ShareRec_S2S(String VSRId)
    {        
        List<PartnerNetworkConnection> li_PNetConn = [Select Id from PartnerNetworkConnection where ConnectionStatus = 'Accepted'];
        
        
        //List<VS_Referral__c> li_VSR = [Select id, Stage_History__c from VS_Referral__c where Id =: VSRId ];
        
        for(PartnerNetworkConnection PNetConn : li_PNetConn){
             
            PartnerNetworkRecordConnection newConnection =
                new PartnerNetworkRecordConnection(
                    ConnectionId = PNetConn.Id,
                    LocalRecordId = VSRId,
                    SendClosedTasks = false,
                    SendOpenTasks = false,
                    SendEmails = false);
                    insert newConnection;
        }

        recordtype RecTypeID = [Select Id from recordtype where sobjecttype = 'VS_Referral__c' and name = 'VS Referral - Locked'];
        
        List<VS_Referral__c> li_VSRUpdate = new List<VS_Referral__c>();                                 
        
        List<VS_Referral__c> li_VSR = [Select Id,
                                       RecordTypeId 
                                  from VS_Referral__c
                                 where Id =: VSRId];
                                 
        
        for(VS_Referral__c VSR : li_VSR){
            VSR.RecordTypeId = RecTypeID.Id;
            li_VSRUpdate.add(VSR);
        }
        
        Update li_VSRUpdate;
        
        
    }
}