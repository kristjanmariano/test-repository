@isTest
private class Box_AutoCreateRecordFolderCCTest {
	
	private static string PROFILE_SYSADMIN = Label.Profile_Name_System_Administrator; //System Administrator
	private static string PROFILE_LT = Label.Profile_Name_Lightning_Team; //Lightning Team

	@testsetup static void setup(){

		//create test data for Sys Admin
        ID pId_sysadmin = [Select Id, Name From Profile Where Name = : PROFILE_SYSADMIN ].Id;
        User sysadminUsr = new User(
                UserName = 'sysadmin@casetriggertest.com.uat',
                LastName = 'TestSysAdminCASE',
                Email = 'sysadmin@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tSACSE',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                ProfileId =  pId_sysadmin
            );
        insert sysadminUsr;

		//create test data for Lightning team User
        ID roleID = [Select Id, Name From UserRole Where Name = : PROFILE_LT ].Id;
        ID pId_lt = [Select Id, Name From Profile Where Name = : PROFILE_LT ].Id;
        User ltuser = new User(
                UserName = 'testlt@casetriggertest.com.uat',
                LastName = 'TestLT',
                Email = 'testlt@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tlt',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                UserRoleId = roleID,
                ProfileId =  pId_lt
            );
        insert ltuser;

        //create test data for Group
        Group grp = new Group(
        		Name = 'Box Lightning Team',
        		DeveloperName = 'Box_Lightning_Team_Test'
        	);
        insert grp;
        //create test data for GroupMember
        GroupMember gmember = new GroupMember(
        		GroupId = grp.Id,
        		UserOrGroupId = ltuser.Id
        	);
        insert gmember;


		System.runAs(sysAdminUsr){

			//create test data for Box Group Permission Settings
			list <Box_Group_Permission_Settings__c> boxgroupSet = new list <Box_Group_Permission_Settings__c>();
			boxgroupSet.add(
				new Box_Group_Permission_Settings__c(
					Name = 'Lightning-Cases',
					Box_Group_Name__c = 'Lightning',
					SF_Group_Name__c = 'Box_Lightning_Team_Test',
					Folder__c = 'Cases',
					Permission__c = 'VIEWERUPLOADER'
				)
			);
			insert boxgroupset;

			//create test data for Account
	        Account acc = new Account();
	        acc.Name = 'test Account';
	        insert acc;
	        
			//create test data for Case
	        Case cs = new Case(
	        		Status = 'New',
                	Stage__c = 'Open',
                	AccountId = acc.Id
	        	);
	        insert cs;
    	}

	}

	static testmethod void testBoxAutoCreateRecordFolder(){

		Case cse = [Select Id, CaseNumber From Case];
		User curusr = [Select Id, UserRoleId From User Where UserName = 'testlt@casetriggertest.com.uat'];
		list <GroupMember> gmembers = [Select Id, GroupId, Group.DeveloperName, UserOrGroupId From GroupMember];

		Test.startTest();

			system.runAs(curusr){

				ApexPages.StandardController st0 = new ApexPages.StandardController(cse);
				Box_AutoCreateRecordFolderCC boxautoCC = new Box_AutoCreateRecordFolderCC(st0);
				boxautoCC.test_objRecordFolderID = '37272335804';
				boxautoCC.test_collabType = box.Toolkit.CollaborationType.PREVIEWERUPLOADER;
				boxautoCC.autocreateBoxFolder();
			}

		Test.stopTest();

	}

	static testmethod void testBoxErrors(){

		Case cse = [Select Id, CaseNumber From Case];
		User curusr = [Select Id, UserRoleId From User Where UserName = 'testlt@casetriggertest.com.uat'];

		Test.startTest();

			system.runAs(curusr){

				ApexPages.StandardController st0 = new ApexPages.StandardController(cse);
				Box_AutoCreateRecordFolderCC boxautoCC = new Box_AutoCreateRecordFolderCC(st0);
				boxautoCC.autocreateBoxFolder();

				ApexPages.StandardController st1 = new ApexPages.StandardController(cse);
				Box_AutoCreateRecordFolderCC boxautoCC1 = new Box_AutoCreateRecordFolderCC(st1);
				boxautoCC1.test_objRecordFolderID = '37272335804';
				boxautoCC1.autocreateBoxFolder();
			}

		Test.stopTest();

		//Verify that page contains error messages
		system.assertEquals(ApexPages.hasMessages(),true);

	}
	
}