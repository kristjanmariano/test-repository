public with sharing class CustomSettingUtil {
	
	public static String getVedaConfigValue(String configName ){
		VedaConfig__c cfg = VedaConfig__c.getInstance(configName);			
		return cfg!= null ? cfg.Value__c : '';
	}
	
	public static boolean getVedaConfigBooleanValue(String configName ){
		VedaConfig__c cfg = VedaConfig__c.getInstance(configName);			
		return cfg!= null ? cfg.BooleanValue__c : false;
	}
	
	@isTest static void testGetVedaConfigValue() {
		String val = CustomSettingUtil.getVedaConfigValue('testing');
		System.assertEquals('', val);		
	}
	
	@isTest static void testGetVedaConfigBooleanValue() {
		boolean val = CustomSettingUtil.getVedaConfigBooleanValue('testing');
		System.assertEquals(false, val);		
	}
	

}