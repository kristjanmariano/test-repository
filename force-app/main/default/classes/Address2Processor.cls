public class Address2Processor extends DefaultProcessor {
    Map<String,String> abbreviations = new Map<String,String>{'Australian Capital Territory'=>'ACT','New South Wales'=>'NSW','Northern Territory'=>'NT','Queensland'=>'QLD','South Australia'=>'SA','Tasmania'=>'TAS',
                                        'Victoria'=>'VIC','Western Australia'=>'WA','Other (Outside Australia)'=>'Other'};
    CustomStringBuilder cStringBuilder = LKIntegration.cStringBuilder;
    public override boolean process() {
        if(!super.process())
            return false;
        List<LKRecord> lKRecordsList = XMLMappingAdapter.lkRecordsMap.get('Address2');
        //XMLMapping mappObj = XMLMappingAdapter.xmlNodesMap.get('Address2');
        cStringBuilder.add('Address 2 Processor');
        Integer i = 0;
        List<LkRecord> tempLKRecord = new List<LkRecord>(); 
        for(LKRecord rec : lKRecordsList) {
            i = i + 1;
            if(!rec.isProcessed) {
               String applicantSfId = rec.sfObjectId;
               String applicantLKId = LKIntegration.coBorrIdMap.get(applicantSfId);
               String postSettleCity = (String)rec.lkValuesMap.get('Post_Settle_Suburb__c');
               if(null != postSettleCity){
                   Map<String,String> requestBody = new Map<String,String>();
                   LKRecord postSettleLKRec= new LKRecord();
                   postSettleLKRec.sfObjectId = rec.sfObjectId;
                   postSettleLKRec.sObjectRef = rec.sObjectRef;
                   postSettleLKRec.setMappingObject(rec.getMappingObject()) ;
                   XMLMapping mappObj = postSettleLKRec.getMappingObject();
                   Map<String,Object> lkValuesMap =  new Map<String,Object>();
                   String streetName = (String)rec.lkValuesMap.get('Post_Settle_Street_Name__c');
                   String streetNum = (String)rec.lkValuesMap.get('Post_Settle_Street_Number__c');
                   String nonStandardAdd = '';
                   if(streetName != null) {
                       nonStandardAdd = nonStandardAdd + streetName ;
                   }
                   if(streetNum != null) {
                       nonStandardAdd = nonStandardAdd+' , '+streetNum ;
                   }
                   requestBody.put('street',streetName );   
                   requestBody.put('street_number',streetNum);
                   String stateVal = (String)rec.lkValuesMap.get('Post_Settle_State__c');
                   if(stateVal != null && stateVal!='null'){
                       requestBody.put('state',abbreviations.get(stateVal));
                   }
                   requestBody.put('postcode',(String)rec.lkValuesMap.get('Post_Settle_Postcode__c'));
                   requestBody.put('country',(String)rec.lkValuesMap.get('Post_Settle_Country__c'));
                   requestBody.put('non_standard_address_line',nonStandardAdd);  
                   String addStandard = (String)rec.lkValuesMap.get('Post_Settle_Address_Type__c');
                   
                   if(addStandard!= null) {
                       addStandard= addStandard.replaceAll( '\\s+', '');
                       if(addStandard.equalsIgnoreCase('Non-Standard'))
                           addStandard = 'NonStandard';
                   }
                   //cStringBuilder.add('applicant 2222222222222222222');
                   requestBody.put('address_standard',addStandard);
                   requestBody.put('address_type','PostSettlement Residential');
                   requestBody.put('street_type',(String)rec.lkValuesMap.get('Post_Settle_Street_Type__c'));
                   requestBody.put('city',(String)rec.lkValuesMap.get('Post_Settle_Suburb__c'));
                   postSettleLKRec.lkValuesMap  = lkValuesMap;
                   postSettleLKRec.setFieldToUpdate('Loankit_PostSettle_Address_Id__c');
                   
                   LKIntegration.putAppId(requestBody,String.valueOf(LKIntegration.mapGlobal.get(LKConstant.APPLICATION_ID)));
                   LKIntegration.putCommonData(requestBody);
                if(mappObj != null) {
                    //cStringBuilder.add('mappObj for 222222222222222222222222');
                    String url = mappObj.getEndPointUrl(false,null);
                    url = url+applicantLKId +'?serializer=JSON';
                    cStringBuilder.add('post address url ==== '+url);
                    //url = url+LKIntegration.mapGlobal.get('contact_id')+'?serializer=JSON';
                    String requestMethod = mappObj.getReqMethod(false);
                    String jsonString = LKIntegration.getJsonString(requestBody);
                    cStringBuilder.add('post address json ========= '+jsonString);
                    HttpResponse resp = LKIntegration.sendRequest(jsonString,url,requestMethod);
                    if(resp!=null) {
                        String respString = resp.getBody();
                        Map<String,String> mapResponse = LKIntegration.convertToMap(respString);
                        if(!mapResponse.containsKey('error')) {
                            String addressId = mapResponse.get('address_id');
                            postSettleLKRec.lkObjectId = addressId;
                        }
                        cStringBuilder.add('post address response ====== '+mapResponse);
                    }
                    tempLKRecord.add(postSettleLKRec);
                }
                
              }  
                String previousCity = (String)rec.lkValuesMap.get('Previous_Suburb__c');
               if(null != previousCity){
                   Map<String,String> requestBody = new Map<String,String>();
                   LKRecord previousLKRec = new LKRecord();
                   previousLKRec.sfObjectId = rec.sfObjectId ;               
                   previousLKRec.sObjectRef = rec.sObjectRef;
                   previousLKRec.setMappingObject(rec.getMappingObject()) ; 
                   XMLMapping mappObj = previousLKRec.getMappingObject();         
                   Map<String,Object> lkValuesMap =  new Map<String,Object>();                 
                   lkValuesMap.put('building_name',rec.lkValuesMap.get('Previous_Address__c'));
                   String streetName = (String)rec.lkValuesMap.get('Previous_Street_Name__c');
                   String streetNum = (String)rec.lkValuesMap.get('Previous_Street_Number__c');
                   String nonStandardAdd = '';
                   if(streetName != null) {
                       nonStandardAdd = nonStandardAdd + streetName ;
                   }
                   if(streetNum != null) {
                       nonStandardAdd = nonStandardAdd+' , '+streetNum ;
                   }
                   requestBody.put('street',streetName );   
                   requestBody.put('street_number',streetNum );
                   requestBody.put('non_standard_address_line',nonStandardAdd); 
                    requestBody.put('city',(String)rec.lkValuesMap.get('Previous_City__c'));   
                    String stateVal = (String)rec.lkValuesMap.get('Previous_State__c');
                    if(stateVal != null && stateVal!='null'){
                        requestBody.put('state',abbreviations.get(stateVal));
                    }
                    requestBody.put('postcode',(String)rec.lkValuesMap.get('Previous_Postcode__c'));
                    requestBody.put('country',(String)rec.lkValuesMap.get('Previous_Country__c'));  
                    requestBody.put('start_date',(String)rec.lkValuesMap.get('Previous_Address_Start_Date__c'));
                    requestBody.put('end_date',(String)rec.lkValuesMap.get('Previous_Address_End_Date__c'));
                    requestBody.put('address_type','Previous Residential');
                    String addStandard = (String)rec.lkValuesMap.get('Previous_Address_Type__c');
                    //System.debug('prev address type ========= '+addStandard);
                    if(addStandard!= null) {
                        addStandard= addStandard.replaceAll( '\\s+', '');
                        if(addStandard.equalsIgnoreCase('Non-Standard'))
                           addStandard = 'NonStandard';
                    }
                    //System.debug('prev address type ========= '+addStandard);
                    requestBody.put('address_standard',addStandard);  
                   previousLKRec.lkValuesMap  = lkValuesMap  ;
                   previousLKRec.setFieldToUpdate('Loankit_Previous_Address_Id__c');
                   
                  
                  LKIntegration.putAppId(requestBody,String.valueOf(LKIntegration.mapGlobal.get(LKConstant.APPLICATION_ID)));
                  LKIntegration.putCommonData(requestBody);
                if(mappObj != null) {
                    //cStringBuilder.add('mappObj for 222222222222222222222222');
                    String url = mappObj.getEndPointUrl(false,null);
                    url = url+applicantLKId +'?serializer=JSON';
                    cStringBuilder.add('previous address url =========== '+url);
                    //url = url+LKIntegration.mapGlobal.get('contact_id')+'?serializer=JSON';
                    String requestMethod = mappObj.getReqMethod(false);
                    String jsonString = LKIntegration.getJsonString(requestBody);
                    cStringBuilder.add('previous address json ========= '+jsonString);
                    HttpResponse resp = LKIntegration.sendRequest(jsonString,url,requestMethod);
                    if(resp!=null) {
                        String respString = resp.getBody();
                        Map<String,String> mapResponse = LKIntegration.convertToMap(respString);
                        if(!mapResponse.containsKey('error')) {
                            String addressId = mapResponse.get('address_id');
                            previousLKRec.lkObjectId = addressId;
                        }
                        cStringBuilder.add('prev address response ====== '+mapResponse);
                    }
                    tempLKRecord.add(previousLKRec);
                }
               }
               
               String currentCity = (String)rec.lkValuesMap.get('Current_Suburb__c');
               if(null != currentCity){
                   Map<String,String> requestBody = new Map<String,String>();
                   LKRecord currentLKRec = new LKRecord();
                   currentLKRec.sfObjectId = rec.sfObjectId;                  
                   currentLKRec.sObjectRef = rec.sObjectRef;
                   currentLKRec.setMappingObject(rec.getMappingObject()) ; 
                   XMLMapping mappObj = currentLKRec.getMappingObject();               
                   Map<String,Object> lkValuesMap =  new Map<String,Object>();                 
                    requestBody.put('street',(String)rec.lkValuesMap.get('Current_Street_Name__c'));
                    requestBody.put('street_number',(String)rec.lkValuesMap.get('Current_Street_Number__c'));   
                    requestBody.put('city',(String)rec.lkValuesMap.get('Current_Suburb__c'));  
                    String stateVal = (String)rec.lkValuesMap.get('Current_State__c');
                    if(stateVal != null && stateVal!='null'){
                        requestBody.put('state',abbreviations.get(stateVal));
                    } 
                    requestBody.put('postcode',(String)rec.lkValuesMap.get('Current_Postcode__c'));
                    requestBody.put('country',(String)rec.lkValuesMap.get('Current_Country__c'));   
                    String addStandard = (String)rec.lkValuesMap.get('Current_Address_Type__c');
                    //System.debug('curr address type ========= '+addStandard);
                    if(addStandard!= null) {
                        addStandard= addStandard.replaceAll( '\\s+', '');
                        if(addStandard.equalsIgnoreCase('Non-Standard'))
                           addStandard = 'NonStandard';
                    }
                    //System.debug('curr address type ========= '+addStandard);
                    requestBody.put('address_standard',addStandard);
                    String address = rec.lkValuesMap.get('Current_Street_Name__c') + ',' +rec.lkValuesMap.get('Current_Street_Number__c') ;             
                    requestBody.put('non_standard_address_line',address);   
                    requestBody.put('start_date',(String)rec.lkValuesMap.get('Current_Address_Start_Date__c'));
                    requestBody.put('address_type','PreSettlement Residential');   
                    requestBody.put('street_type',(String)rec.lkValuesMap.get('Current_Street_Type__c'));      
                    requestBody.put('street_number',(String)rec.lkValuesMap.get('Current_Street_Number__c')); 
                    currentLKRec.lkValuesMap  = lkValuesMap  ;
                    currentLKRec.setFieldToUpdate('Loankit_Current_Address_Id__c');
                    LKIntegration.putAppId(requestBody,String.valueOf(LKIntegration.mapGlobal.get(LKConstant.APPLICATION_ID)));
                    LKIntegration.putCommonData(requestBody);
                if(mappObj != null) {
                    //cStringBuilder.add('mappObj for 222222222222222222222222');
                    String url = mappObj.getEndPointUrl(false,null);
                    url = url+applicantLKId +'?serializer=JSON';
                    cStringBuilder.add('curr add url =========== '+url);
                    //url = url+LKIntegration.mapGlobal.get('contact_id')+'?serializer=JSON';
                    String requestMethod = mappObj.getReqMethod(false);
                    String jsonString = LKIntegration.getJsonString(requestBody);
                    cStringBuilder.add('curr address json ========= '+jsonString);
                    HttpResponse resp = LKIntegration.sendRequest(jsonString,url,requestMethod);
                    if(resp!=null) {
                        String respString = resp.getBody();
                        Map<String,String> mapResponse = LKIntegration.convertToMap(respString);
                        if(!mapResponse.containsKey('error')) {
                            String addressId = mapResponse.get('address_id');
                            currentLKRec.lkObjectId = addressId;
                        }
                        cStringBuilder.add('curr add response ====== '+mapResponse);
                    }
                    tempLKRecord.add(currentLKRec);
                }
               }
               List<LKRecord> appl2List = XMLMappingAdapter.lkRecordsMap.get('Applicant2');
                for(LKRecord appl2Rec : appl2List) {
                    if(appl2Rec.sfObjectId.equalsIgnoreCase(applicantSfId)) {
                        SObject sObj = appl2Rec.sObjectRef;
                        for(LKRecord tempRec : tempLKRecord) {
                            sObj.put(tempRec.getFieldToUpdate(),tempRec.lkObjectId);
                        }
                    }
                }       
            }
        }
        if(i > 0){
          lKRecordsList.remove(i-1);
         }
                  
         return true;
    }
}