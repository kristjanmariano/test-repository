/**
 * @File Name          : CaseOwnerAssignedBatch_Schedule.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 19/07/2019, 9:11:01 am
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    19/07/2019, 9:11:01 am   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
global class CaseOwnerAssignedBatch_Schedule implements Schedulable{
 
    global void execute(SchedulableContext sc) {
		Date currentDate = System.today();
		CaseOwnerAssignedBatch coab = new CaseOwnerAssignedBatch(); //allows partial success
		if(!Test.isRunningTest()) Database.executeBatch(coab,1); //limit batch update to 1 due to JBSystemFlow error
		else Database.executeBatch(coab);
	}

}