/*
    @Description: test class of ContentDocumentLinkTrigger, ContentDocumentLinkTriggerHandler
    @Author: Raoul Lake, Jesfer Baculod - Positive Group
    @History:
        11/10/2017 - Created
		07/01/2020 - Updated for NM - SFBAU-161
*/
@isTest
public class ContentDocumentLinkTriggerTest {

	private static string PROFILE_SYSADMIN = Label.Profile_Name_System_Administrator; //System Administrator

	@testsetup static void setup(){

		//create test data for Sys Admin
        ID pId_sysadmin = [Select Id, Name From Profile Where Name = : PROFILE_SYSADMIN ].Id;
        User sysadminUsr = new User(
                UserName = 'sysadmin@casetriggertest.com.uat',
                LastName = 'TestSysAdminCASE',
                Email = 'sysadmin@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tSACSE',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                ProfileId =  pId_sysadmin
            );
        insert sysadminUsr;

        System.runAs(sysAdminUsr){

        	//create test data for Leads
        	list <Lead> ldlist = new list <Lead>();
        	for (Integer i=0; i < 10 ; i++){
        		Lead ld = new Lead(
        			LastName = 'Test Lead'+i,
        			Status = 'Open');
        		ldlist.add(ld);
        	}
        	insert ldlist;

			list <Partner__c> prtnerlist = new list <Partner__c>();
			for (Integer i=0; i < 10; i++){
				Partner__c prt = new Partner__c(
					Name = 'Test Partner'+i
				);
				prtnerlist.add(prt);
			}
			insert prtnerlist;

        	/* //create test data for Account
            Account acc = new Account(
                    Name = 'test'
                );
            insert acc;

            //create test data for Opp
            list <Opportunity> opplist = new list <Opportunity>();
            for (Integer i=0; i < 10; i++){
            	Opportunity opp = new Opportunity(name = 'OppTest'+i, stageName = 'open', closeDate = date.today(), loan_type2__c = 'car' );
            	opplist.add(opp);
            }
            insert opplist; */

        }

	}

	static testmethod void testupdateLatestCommentFromNote(){

		list <Lead> ldlist = [Select Id, Name From Lead];
		list <Partner__c> prtnerlist = [Select Id, Name From Partner__c];
		//list <Opportunity> opplist = [Select Id, Name From Opportunity];

		/*Trigger_Settings__c trigSet = new Trigger_Settings__c(
                Enable_ContentDocumentLink_Trigger__c = true,
                Enable_CDLink_latestCommentFromNote__c = true
            );
        insert trigset; */

		Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
			Enable_Triggers__c = true,
			Enable_ContentDocumentLink_Trigger__c = true,
			Enable_CDLink_Latest_Comment_From_Note__c = true
		);
		insert trigSet;

		General_Settings1__c genSet = new General_Settings1__c(
			Object_Prefix_Partner__c = 'a48'
		);
		insert genSet;

        Test.startTest();

        	//Create test data for Notes
        	list <ContentVersion> cvlist = new list <ContentVersion>();
        	for (Integer i=0; i < 10; i++){
        		ContentVersion cv = new ContentVersion(
        			Title = 'LeadTestNote'+i,
        			VersionData = Blob.valueof('test body')
        		);
        		cv.PathonClient = note.Title + '.snote';
        		cvlist.add(cv);
        	}
        	for (Integer i=0; i < 10; i++){
        		ContentVersion cv = new ContentVersion(
        			Title = 'PartnerTestNote'+i,
        			VersionData = Blob.valueof('test body')
        		);
        		cv.PathonClient = note.Title + '.snote';
        		cvlist.add(cv);
        	} 
        	insert cvlist; 

        	//Retrieve create Notes
        	list <ContentVersion> inCVlist = [Select Id, Title, ContentDocumentId From ContentVersion Where Id in : cvlist];
        	map <Id,string> cdtitleMap = new map <id,string>();
        	for (ContentVersion cv : inCVlist){
        		cdtitleMap.put(cv.ContentDocumentId,cv.Title);
        	}

        	//Link Notes to Leads and Opps
        	list <ContentDocumentLink> cdllist = new list <ContentDocumentLink>();
        	integer ldctr = 0; integer partnerctr = 0;
        	for (Id cd : cdtitleMap.keySet()){
    			ContentDocumentLink cdl = new ContentDocumentLink(
    				ContentDocumentId = cd,
    				ShareType = 'I', //'V'
					Visibility = 'AllUsers'
    			);	
    			if (cdtitleMap.get(cd).startsWith('Lead')){
    				cdl.LinkedEntityId = ldlist[ldctr].Id;
    				ldctr++;
    			}
    			else if (cdtitleMap.get(cd).startsWith('Partner')){
    				cdl.LinkedEntityId = prtnerlist[partnerctr].Id;
    				partnerctr++;
    			} 
        		cdllist.add(cdl);
        	}
			system.debug('@@cdllist: '+cdllist);
        	insert cdllist;

			for (Lead ld : [Select Id, Latest_Comment__c From Lead]){
				system.assert(ld.Latest_Comment__c != null);
			}
			for (Partner__c prt : [Select Id, Latest_Comment__c From Partner__c]){
				system.assert(prt.Latest_Comment__c != null);
			}

			//Cover Update,Delete helper events
			update cdllist;
			delete cdllist;

        Test.stopTest();

	}
	
}