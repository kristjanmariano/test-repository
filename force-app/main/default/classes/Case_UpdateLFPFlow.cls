/*
	@Description: This invocable class is being used in Case Assignment (NM - PLS) Process Builder "LFP Flow" criteria
	@Author: Rexie David - Positive Group
	@History:
		-	3/07/2019 - extra/task25140782-UpdateCaseAfter48Hours - Created
        -   16/07/2019 - extra/task25317965-FixOnLFPFlow - Fix Error on Case - Email Alerts (NM - PLS)
        -   25/07/2019 - extra/tasks25398773-PLSCaseAssignmentsJulyUpdates - "Consumer Motor Team" updated to "Consumer Asset"
*/

public class Case_UpdateLFPFlow implements Queueable{
    private static final Trigger_Settings1__c trigSet = Trigger_Settings1__c.getInstance(UserInfo.getUserId());
    private Case caseToUpdate;
    public static Location__c testLocation;
    private static final String CONSUMERASSETQUEUE = 'Consumer Asset';

    public Case_UpdateLFPFlow(Case caseToUpdate){
        this.caseToUpdate = caseToUpdate;
    }

    public void execute (QueueableContext context){
        System.debug('UPDATE Case_UpdateLFPFlow *********');
        if (caseToUpdate != new Case()){
            Database.update(caseToUpdate);
        }
    } 

    @InvocableMethod(label='Update LFP Case' description='Change SF Case Stage from WOPA to NEW, Assign Case to Consumer_Motor_Team Queue, Tick NVM Routing')
	public static void updateLFPCase (list <String> caseIDs) {

        List<Case> csList = [SELECT Id, Status, Stage__c, BusinessHoursId, State_Code__c, Ready_For_Call__c, Ready_To_Dial_Time__c, RecordType.Name FROM Case WHERE Id IN: caseIDs AND Stage__c = 'WOPA'];
        System.debug('Number of WOPA Cases -> '+csList.size());
        if(csList.size() > 0){
            // System.debug(29);
            List<Frontend_Submission__c> feSubmissionList = [SELECT Id FROM Frontend_Submission__c WHERE Case__c =: csList[0].Id AND Positive_Form_Type__c = 'nod-fullapp'];
            if(feSubmissionList.size() == 0){ //This means that the application is not completed or full app is not submitted.
            // System.debug(32);
                Group consumerMotorQueue = [SELECT Id FROM Group WHERE Name =: CONSUMERASSETQUEUE LIMIT 1]; //25/07/2019 - extra/tasks25398773-PLSCaseAssignmentsJulyUpdates
                csList[0].OwnerId = consumerMotorQueue.Id; //Assign to LFP Flow Queue
                csList[0].Stage__c = 'Open'; //Set stage to New
                csList[0].Deactivate_Client_Access_to_Full_App__c = TRUE;
                csList[0].Lead_Bucket__c = CONSUMERASSETQUEUE;  //25/07/2019 - extra/tasks25398773-PLSCaseAssignmentsJulyUpdates //16/07/2019 - extra/task25317965-FixOnLFPFlow
                csList[0].PLS_Initial_Team_Queue__c = CONSUMERASSETQUEUE; //25/07/2019 - extra/tasks25398773-PLSCaseAssignmentsJulyUpdates //16/07/2019 - extra/task25317965-FixOnLFPFlow
                Boolean readyForCall = csList[0].Ready_For_Call__c;
                if(!csList[0].RecordType.Name.containsIgnoreCase('PWM') && trigSet.Enable_Case_NVM_Timezone_based_Routing__c){
                    // System.debug(39);
                    CaseGateway.initBusinessHoursVariables(); 
                    CaseGateway.processCaseOutsideBusinessHours(csList[0]);
                    if(readyForCall && csList[0].Ready_For_Call__c){//&& processFlowSettings.Enable_Case_NVM_Flow_Definitions__c){ //Handle NVM Routing when Case is Ready_for_Call__c before and after the update since Process Builder will only fire if there is a change in the Ready_for_Call__c.
                       
                        csList[0].NVMContactWorld__NVMRoutable__c = true;
                    }
                }
                // 2/07/2019 - extra/task25140648-BugFixAttemptedCallback - RDAVID
                Case_UpdateLFPFlow caseUpdateLFPFlow = new Case_UpdateLFPFlow(csList[0]);
                ID jobID = System.enqueueJob(caseUpdateLFPFlow);  
            }
        }
    }
}