/*
Author: Original: Jesfer Baculod (Positive Group)
History: 08/27/2018 Created
         10/02/2018 Added Extras field in Purchase and populate in Sourcing Case's Initial Asset Details once selected
         07/02/2019 RDAVID TeamworkId: 22915233 Branch:extra/CaseSourcing - Updated class to map Introducer/Introducer Support from the Parent Case
         24/07/2019 RDAVID extra/tasks25429652-PopulateLeadCreationMethod - Populate Lead_Creation_Method__c
Purpose: controller class of Case_VehicleSourcing for creating Sourcing Case
*/
public class Case_VehicleSourcingCC {

    private Case pcse {get;set;}
    public Case scse {get;set;}
    public Case crescse {get;set;}
    public string scseNumber {get;set;}
    public Purchased_Asset__c passet {get;set;}
    public parentCasePurchaseWrapper pcpWr {get;set;}
    private ID curID {get;set;}
    private static Id cseKARRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_KAR_VS).getRecordTypeId(); //KAR: Vehicle Sourcing

    public boolean hasErrors {get;set;}
    public boolean displaySourcing {get;set;}
    public boolean saveSuccess {get;set;}

    public Case_VehicleSourcingCC(){
        curID = Apexpages.currentpage().getparameters().get('id');
        hasErrors = saveSuccess = false; scseNumber = '';
        displaySourcing = true;
        if (curID != null){
            retrieveCase();
        }
        pcpWr = new parentCasePurchaseWrapper(pcse, new list <purchaseWrapper>());
        sourcingCaseDefaults();
        passet = new Purchased_Asset__c();
        if (pcse.Purchased_Assets__r.size() > 0){
            for (Purchased_Asset__c passet : pcse.Purchased_Assets__r){
                purchaseWrapper pwr = new purchaseWrapper(passet);
                if (pcse.Purchased_Assets__r.size() == 1){ 
                    pwr.isSelected = true;
                    scse.Estimate_Max_Purchase_Price__c = passet.Purchase_Price__c;
                }
                pcpWr.pWrlist.add(pwr);
            }
        }
        else{ //No Purchase
            displaySourcing = false;
        }
    }

    private void retrieveCase(){
        string cseQuery = 'Select Id, CaseNumber, RecordTypeId, RecordType.Name, Channel__c, Partition__c, Sale_Type__c, Lead_Source__c, Lender1__c, Lender1__r.Name, Loan_Term__c, Loan_Product__c, Primary_Contact_Name_MC__c, Loan_Type__c, Introducer1__c, Introducer1_Support__c, Introducer1_Email__c, Introducer1_Name_MC__c, Introducer1_Phone__c, Introducer1_Reference_Number__c, Introducer1_State__c, Introducer1_Support_Email__c, Introducer1_Support_Name_MC__c, Introducer1_Support_Phone__c, Introducer1_Relationship_Manager__c, Introducer1_Support_Relationship_Manager__c, ';
        cseQuery+= 'Referral_Company__c, Connective_Broker_lookup__c, Introducer_Support__c, '; //Nodifi OM field Support
        string purchaseQuery = 'Select Id, Name, RecordTypeId, RecordType.Name, Asset_Type__c, Asset_Sub_Type__c, Case__c, Purchase_Price__c, Cash_Deposit__c, Trade_in_Amount__c, Trade_In_Description__c, Payout_Amount__c, Payout_To__c, Required_Loan_Amount__c, Condition__c, Vehicle_VIN__c, Engine_Number__c, Registration_Number__c, Registration_State__c, Vehicle_NVIC__c, Build_year__c, Build_Date__c, Compliance_year__c, Compliance_Date__c, Vehicle_Odometer_Reading__c, Description__c, Year_of_Manufacture__c, ';
        purchaseQuery+= 'Dealer_Sale_Vendor__c, Dealer_Sale_Vendor__r.Name, Dealer_Contact_Person__c, Private_Seller__c, '; //Vendor/Private Seller Detail fields
        purchaseQuery+= 'Vehicle_Year__c, Vehicle_Make__c, Vehicle_Model__c, Vehicle_Variant__c, Vehicle_Transmission__c, Vehicle_Body_Type__c, Vehicle_Colour__c, Vehicle_Fuel_Type__c, Extras__c'; //Initial Asset Details' fields
        purchaseQuery+= ' From Purchased_Assets__r Order By Name ASC'; //Child Purchases
        purchaseQuery= '(' + purchaseQuery + ')';
        cseQuery+= purchaseQuery + ' From Case ';
        if (curID.getSObjectType() == Schema.Case.SobjectType){ //ID is from Case
            cseQuery+= 'Where Id = : curID limit 1';
        }
        pcse = Database.query(cseQuery);

    }

    private void sourcingCaseDefaults(){
        scse = new Case(
            RecordTypeId = cseKARRTId,
            Channel__c = 'Karlon',
            Partition__c = pcse.Partition__c,
            Referral_Company__c = pcse.Referral_Company__c,
            Parent_Case__c = pcse.Id,
            Primary_Contact_Name_MC__c = pcse.Primary_Contact_Name_MC__c,
            Loan_Term__c = pcse.Loan_Term__c,
            Loan_Product__c = pcse.Loan_Product__c,
            Loan_Type__c = pcse.Loan_Type__c,
            Lead_Source__c = pcse.Lead_Source__c,
            Sale_Type__c = pcse.Sale_Type__c,
            Introducer1__c = pcse.Introducer1__c,
            Introducer1_Email__c = pcse.Introducer1_Email__c,
            Introducer1_Name_MC__c = pcse.Introducer1_Name_MC__c,
            Introducer1_Phone__c = pcse.Introducer1_Phone__c,
            Introducer1_Reference_Number__c = pcse.Introducer1_Reference_Number__c,
            Introducer1_State__c = pcse.Introducer1_State__c,
            Introducer1_Support__c = pcse.Introducer1_Support__c,
            Introducer1_Support_Email__c = pcse.Introducer1_Support_Email__c,
            Introducer1_Support_Name_MC__c = pcse.Introducer1_Support_Name_MC__c,
            Introducer1_Support_Phone__c = pcse.Introducer1_Support_Phone__c,
            Introducer1_Relationship_Manager__c = pcse.Introducer1_Relationship_Manager__c,
            Introducer1_Support_Relationship_Manager__c = pcse.Introducer1_Support_Relationship_Manager__c,
            Status = 'New',
            Stage__c = 'Open'
        );
        if (pcse.Lender1__c != null) scse.Lender_Name__c = pcse.Lender1__r.Name;
    }

    private boolean validateSourcingCase(){
        boolean passed = true;
        if (scse.Submission_Comments_Karlon__c == null || scse.Submission_Comments_Karlon__c == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter Submission Comments'));
            hasErrors = true;
            passed = false;
        }
        if (scse.Estimate_Max_Purchase_Price__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter Max Purchase Price'));
            hasErrors = true;
            passed = false;
        }
        integer selCtr = 0;
        for (purchaseWrapper pwr : pcpWr.pWrlist){
            if (pwr.isSelected) selCtr++;
        }
        if (selCtr == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select Purchase for Sourcing'));
            hasErrors = true;
            passed = false;
        }
        return passed;
    }

    public pageReference saveSourcingCase(){
        Savepoint sp = Database.setSavepoint();
        try{
            hasErrors = false;
            if (validateSourcingCase()){
                for (purchaseWrapper pwr : pcpWr.pWrlist){
                    if (pwr.isSelected){
                        //Prepopulate Purchase for Sourcing Case
                        passet = pwr.purchase.clone(false,false,false,false);
                        //Populate Initial Asset Details in Sourcing Case from selected Purchase
                        if (passet.Vehicle_Year__c == null) scse.Year__c = passet.Year_of_Manufacture__c;
                        else scse.Year__c = passet.Vehicle_Year__c;
                        scse.Vehicle_Make__c = passet.Vehicle_Make__c;
                        scse.Vehicle_Model__c = passet.Vehicle_Model__c;
                        scse.Varient__c = passet.Vehicle_Variant__c;
                        scse.Vehicle_Transmission__c = passet.Vehicle_Transmission__c;
                        scse.Vehicle_Body_Type__c = passet.Vehicle_Body_Type__c;
                        scse.Vehicle_Colour__c = passet.Vehicle_Colour__c;
                        scse.Vehicle_Fuel_Type__c = passet.Vehicle_Fuel_Type__c;
                        scse.Purchase_Price__c = passet.Purchase_Price__c;
                        scse.Cash_Deposit__c = passet.Cash_Deposit__c;
                        scse.Trade_in_Amount__c = passet.Trade_In_Amount__c;
                        scse.Payout_Amount__c = passet.Payout_Amount__c;
                        scse.Extras_Sourcing__c = passet.Extras__c;
                        if (passet.Dealer_Sale_Vendor__c != null) {
                            scse.Winning_Dealer__c = passet.Dealer_Sale_Vendor__r.Name;
                        }
                    }
                }
                scse.Lead_Creation_Method__c = 'Manually Created'; //24/07/2019 RDAVID extra/tasks25429652-PopulateLeadCreationMethod
                //Create Sourcing Case from Parent Case
                insert scse;
                crescse = [Select Id, CaseNumber From Case Where Id = : scse.Id];
                passet.Case__c = scse.Id;
                //Create Purchase under Sourcing Case
                insert passet;
                saveSuccess = true;
                return null; //Redirection of page is being handled in javascript
                //return new PageReference('/'+scse.Id);
            }
        }
        catch(Exception e){
            saveSuccess = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getLineNumber()+'-'+ e.getMessage()));
            hasErrors = true;
            Database.rollback( sp );
        }
        return null;
    }

    public class purchaseWrapper{
        public boolean isSelected {get;set;}
        public Purchased_Asset__c purchase {get;set;}
        public purchaseWrapper(Purchased_Asset__c tpurchase){
            isSelected = false;
            purchase = tpurchase;
        }
    }

    public class parentCasePurchaseWrapper{
        public Case parentCse {get;set;}
        public list <purchaseWrapper> pWrlist {get;set;}
        public parentCasePurchaseWrapper(Case pCse, list <purchaseWrapper> tpwr){
            parentCse = pCse;
            pWrlist = tpWr;
        }
    }

}