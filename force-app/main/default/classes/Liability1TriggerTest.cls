/** 
* @FileName: Liability1TriggerTest
* @Description: Test class for Liability Trigger 
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/20/18 JBACULOD Created class, Added testLiabilitytoRoleRollup [SD-24], Modified hard coded record type labels with CommonConstants
* 2.0 4/25/18 JBACULOD Removed existing test setup and test methods, added testautoCreateFOShareLiability
**/ 
@isTest
private class  Liability1TriggerTest {
    
    @testsetup static void setup(){
        TestDataFactory.Case2FOSetupTemplate();
    }


    static testmethod void testautoCreateFOShareLiability(){

        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
                Enable_Triggers__c = true,
                Enable_Liability_Trigger__c = true,
                Enable_FO_Auto_Create_FO_Share__c = true,
                Enable_FO_Delete_FO_Share_on_FO_Delete__c = true
        );
        insert trigSet;

        //Retrieve Role Test data from Setup
        list <Role__c> rolelist = [Select Id, Case__c, Application__c From Role__c];
        string foshareRoleIDs = '';
        for (Integer i=0; i<10; i++){
            foShareRoleIds+= rolelist[i].Id + ';';
        }

        Test.startTest();

            //Create test data for Liability, autoCreating FO Share
            Id assRTId = Schema.SObjectType.Liability1__c.getRecordTypeInfosByName().get(CommonConstants.LIABILITY_RT_CC).getRecordTypeId(); //Cards & Credit
            list <Liability1__c> lialist = new list <Liability1__c>();
            for (Integer i=0; i<10; i++){
                Liability1__c ass = new Liability1__c(
                    h_Auto_Create_FO_Share__c = true,
                    RecordTypeId = assRTId,
                    Equal_Share_for_Roles__c = true,
                    h_FO_Share_Role_IDs__c = foShareRoleIDs,
                    Application1__c = rolelist[i].Application__c,
                    Amount_Owing__c = 1000
                );
                lialist.add(ass);
            }
            insert lialist;

            //Retrieve created FO Shares
            list <FO_Share__c> fosharelist = [Select Id, Case__c, Application_Liability__c, Monthly_Amount__c, Liability__c, Liability__r.Liability_Type__c, Liability__r.RecordType.Name, Type__c, FO_Record_Type__c, Role__c, Role__r.Application__c, Role_Share__c, Role_Share_Amount__c From FO_Share__c];
            for (FO_Share__c FOs : fosharelist){
                //Verify Liability Details were populated in FO Share
                system.assertEquals(FOs.Liability__r.RecordType.Name, FOs.FO_Record_Type__c);
                system.assertEquals(FOs.Liability__r.Liability_Type__c, FOs.Type__c);
                //Verify Liability, Case, Application were linked to FO Share
                system.assert(FOs.Liability__r != null);
                system.assert(FOs.Case__c != null);
                system.assert(FOs.Application_Liability__c != null);
                //Verify Equal Share was split to each Role in Application
                Decimal dval = FOs.Role_Share__c;
                dval = dval.setScale(2);
                Decimal drolesize = 10; //# of Roles assigned in FO
                drolesize = drolesize.setScale(2);
                system.assertEquals(dval, drolesize);
                //Verify Role Share Amount was calculated
                Decimal dmval = FOs.Role_Share_Amount__c;
                dmval = dmval.setScale(2);
                Decimal dsval = FOs.Monthly_Amount__c * (FOs.Role_Share__c / 100);
                dsval = dsval.setScale(2);
                system.assertEquals(dmval, dsval );
            }

            //Cover Asset update, delete trigger events
            update lialist;
            delete lialist;

        Test.stopTest();


    } 


}