/** 
* @FileName: CustomErrorHandlingEmailNotification
* @Description: CustomErrorHandlingEmailNotification - Class for Error Handling
* @Copyright: Positivve (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 23/10/2019 
**/ 

public class CustomErrorHandlingEmailNotification {
    public string toMail { get; set;}
    public string ccMail { get; set;}
    public string repMail { get; set;}
    public errorLogWrapper errLogWrap { get; set;}

    public CustomErrorHandlingEmailNotification(){
        
    }
    
	public void sendMail(){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {toMail};
        string[] cc = new string[] {ccMail};
        
        email.setToAddresses(to);
        if(ccMail!=null && ccMail != '')
	        email.setCcAddresses(cc);
        if(repmail!=null && repmail!= '')
        	email.setInReplyTo(repMail);
        
        email.setSubject('Error in ' + errLogWrap.className);
        
        email.setHtmlBody('Hello, <br/><br/>See below errors. <br/>' + getErrorLogs(errLogWrap.errorLogs) + '<br/><br/>Regards<br/> CustomErrorHandlingEmailNotification');
        
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){
            System.debug('CustomErrorHandlingEmailNotification Error : '+e.getLineNumber() + e.getMessage());
        }
        
        toMail = '';
        ccMail = '';
        repMail = '';
    }

    public string getErrorLogs(Set <String> errorLogs){
        String errLog = '';
        for(String str : errorLogs){
            errLog += '<br/>' + str;
        }
        return errLog;
    }

    public class errorLogWrapper {
        public String className {get; set;}
        public Set <String> errorLogs { get; set;}
        public errorLogWrapper(String cName, Set<String> errLogs) {
            className = cName;
            errorLogs = errLogs;
        }
    }
}