/** 
* @FileName: CaseTouchpointHandler
* @Description: Trigger Handler for the Case_Touch_point__c SObject. This class implements the ITrigger interface to help ensure the trigger code is bulkified and all in one place.
* @Source: 	http://developer.force.com/cookbook/recipe/trigger-pattern-for-tidy-streamlined-bulkified-triggers
* @Copyright: Positive (c) 2019
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 26/10 JBACULOD Created Trigger handler
* 1.1 01/04/19 JBACULOD Fixed bug issue
* 1.2 04/15/19 JBACULOD Updated field mapping for Bizible TP from Delacon Lead
* 1.3 07/04/19 JBACULOD Added Updating of Auto-Referred Out Cases to Close that are Waiting on TP
**/ 
public without sharing class CaseTouchpointHandler implements ITrigger {

    private map <string,boolean> CTPMap = new map <string,boolean>();
    private map <string,string> BTPMap = new map <string,string>(); //key CTP Id
    private map <string,Lead> DelaconLeadMap = new map <string, Lead>();
    private map <string, bizible2__Bizible_Touchpoint__c> BTPtoUpdateMap = new map <string, bizible2__Bizible_Touchpoint__c>();
    private set <Id> leadIDForDeleteSet = new set <Id>();
    private set <Id> autoReferredOutCaseIdSet = new set <Id>();

    public CaseTouchpointHandler(){

    }

    public void bulkBefore(){

    }

    public void bulkAfter(){

        if (TriggerFactory.trigset.Enable_Case_TP_Map_Delacon_Lead__c){
            if (Trigger.isInsert){
                list <Case_Touch_point__c> newctplist = Trigger.new;
                for (Case_Touch_point__c ctp : newctplist){
                    system.debug('@@Case_Primary_Contact_Phone__c: '+ctp.Case_Primary_Contact_Phone__c);
                    system.debug('@@Account_is_From_Positive_Path__c: '+ctp.Account_is_From_Positive_Path__c);
                    if (ctp.Case_Primary_Contact_Phone__c != '' && ctp.Account_is_From_Positive_Path__c){ //Only check for Case created from Positive Path
                        if (!CTPMap.containskey(ctp.Case_Primary_Contact_Phone__c)){
                            CTPMap.put(ctp.Case_Primary_Contact_Phone__c, false);
                            BTPMap.put(ctp.Id, ctp.Bizible_Touchpoint__c);
                        }
                    }
                }
                if (CTPMap.size() > 0){
                    list <Lead> delaconLeads = [Select Id, DELAPLA__PLA_Caller_Phone_Number__c, DELAPLA__PLA_Browser_Type__c, DELAPLA__PLA_City__c , DELAPLA__PLA_Dealer_Reference__c , DELAPLA__PLA_Device_Used__c , 
                                                DELAPLA__PLA_Keywords_Searched__c , DELAPLA__PLA_Referral_URL__c , DELAPLA__PLA_Search_Engine_Used__c , DELAPLA__PLA_State__c , DELAPLA__PLA_Website_Landing_Page__c 
                                                From Lead Where LeadSource = 'delacon' AND DELAPLA__PLA_Caller_Phone_Number__c in : CTPMap.keySet()];
                    for (Lead dlead : delaconLeads){
                        if (CTPMap.containskey(dlead.DELAPLA__PLA_Caller_Phone_Number__c)){
                            CTPMap.put(dlead.DELAPLA__PLA_Caller_Phone_Number__c, true);
                            DelaconLeadMap.put(dlead.DELAPLA__PLA_Caller_Phone_Number__c, dlead);
                        }
                    }
                }
            }
        }

    }

    public void beforeInsert(Sobject so){        
    }

    public void beforeUpdate(Sobject oldSo, Sobject so){

    }

    public void beforeDelete(Sobject so){

    }

    public void afterInsert(Sobject so){    
        Case_Touch_point__c ctp = (Case_Touch_point__c)so;
        if (CTPMap.size() > 0){
            if (ctp.Case_Primary_Contact_Phone__c != ''){
                if (CTPMap.containskey(ctp.Case_Primary_Contact_Phone__c)){
                    if (CTPMap.get(ctp.Case_Primary_Contact_Phone__c) && DelaconLeadMap.containskey(ctp.Case_Primary_Contact_Phone__c)){ //matching Delacon Lead found
                        //Map Delacon Lead info to Master Bizible TP
                        Lead delaconLead = DelaconLeadMap.get(ctp.Case_Primary_Contact_Phone__c);
                        bizible2__Bizible_Touchpoint__c bizTP = new bizible2__Bizible_Touchpoint__c(
                            Id = BTPMap.get(ctp.Id),
                            bizible2__Geo_City__c = delaconLead.DELAPLA__PLA_City__c,
                            bizible2__Landing_Page__c = delaconLead.DELAPLA__PLA_Website_Landing_Page__c,
                            bizible2__Medium__c = delaconLead.DELAPLA__PLA_Browser_Type__c,
                            bizible2__Platform__c = delaconLead.DELAPLA__PLA_Device_Used__c,
                            bizible2__Keyword_Text__c = delaconLead.DELAPLA__PLA_Keywords_Searched__c,
                            bizible2__Referrer_Page__c = delaconLead.DELAPLA__PLA_Referral_URL__c,
                            bizible2__Touchpoint_Source__c = delaconLead.DELAPLA__PLA_Search_Engine_Used__c,
                            bizible2__Geo_Region__c = delaconLead.DELAPLA__PLA_State__c 
                        );
                        if (delaconLead.DELAPLA__PLA_Dealer_Reference__c != null){
                            String delReference = delaconLead.DELAPLA__PLA_Dealer_Reference__c;
                            list <String> delReferenceSplit = delReference.split('-');
                            if (delReferenceSplit.size() == 3){ //e.g 111111 - Equipment Finance - Google Adwords
                                bizTP.bizible2__Ad_Campaign_Name__c = delReferenceSplit[1].trim();  //e.g get Equipment Finance
                                //bizTP.bizible2__Touchpoint_Source__c = delReferenceSplit[2].trim(); //e.g get Google Adwords (Removed due to updated 041219 Mapping)
                            }
                        }
                        if (bizTP.Id != null){ 
                            BTPtoUpdateMap.put(ctp.Id, bizTP); //To Update Bizible TP with Delacon Lead Call Tracking Data
                            leadIDForDeleteSet.add(delaconLead.Id);
                        }
                    }
                }
            }
        }

        system.debug('@@ctp.Case_Stage__c: '+ctp.Case_Stage__c);
        if (ctp.Case_Stage__c == 'Waiting On TP') autoReferredOutCaseIdSet.add(ctp.Case__c);
    }

    public void afterUpdate(Sobject oldSo, Sobject so){

    }

    public void afterDelete(Sobject so){

    }

    public void andFinally(){

        if (BTPtoUpdateMap.size() > 0){
            database.update(BTPtoUpdateMap.values());
            if (leadIDForDeleteSet.size() > 0) {
                list <Lead> delaconLeadForDelete = [Select Id From Lead Where Id in : leadIDForDeleteSet];
                delete delaconLeadForDelete; //delete matching Delacon Lead
            }
        }

        /*if (autoReferredOutCaseIdSet.size() > 0){ //JBACU 04/07/19 - Added Updating of Auto-Referred Out Cases to Close that are Waiting on TP
            list <Case> CaseRecordLockForUpdate = [Select Id, PLS_Initial_Team_Queue__c, Closed_Reason__c, Status, Stage__c, Lead_Loan_Amount__c From Case Where Id in : autoReferredOutCaseIdSet FOR UPDATE];
            for (Case cse : CaseRecordLockForUpdate){
                cse.Status = 'Closed';
                cse.Stage__c = 'Referred Out';

                if (cse.PLS_Initial_Team_Queue__c == 'PLS Initial Queue' && (cse.Lead_Loan_Amount__c <= 2000 || cse.Lead_Loan_Amount__c == 0 || String.isBlank(String.valueOf(cse.Lead_Loan_Amount__c)))) cse.Closed_Reason__c = 'Referred to Fair Go Finance';
                else if (cse.PLS_Initial_Team_Queue__c == 'PLS Initial Queue' && (cse.Lead_Loan_Amount__c >= 2001 || cse.Lead_Loan_Amount__c <= 5000)) cse.Closed_Reason__c = 'Referred to Money 3';

                if (cse.PLS_Initial_Team_Queue__c == 'M3' || cse.PLS_Initial_Team_Queue__c == 'PLS Initial Queue') cse.Closed_Reason__c = 'Referred to Money 3';
                else if (cse.PLS_Initial_Team_Queue__c == 'Overflow') cse.Closed_Reason__c = 'Referred to Overflow';
            }
            Database.update(CaseRecordLockForUpdate);
        }
        */


    }

}