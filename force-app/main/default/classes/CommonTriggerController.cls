/*
	@Description: class containing the common methods used in triggers
	@Author: Rex David - Positive Group
	@Date: 12/15/2017 
*/
public class CommonTriggerController {

	//Method for retrieving Pending_Action__c records
	public static List<Pending_Action__c> queryPendingActionList (Set<Id> relatedRecordIds, String functionName){

		return [	SELECT Id, Related_Record_Id__c, Target_Object__c, CW_Call_End_Time__c 
					FROM Pending_Action__c 
					WHERE Related_Record_Id__c IN : relatedRecordIds 
					AND Function_Name__c =: functionName 
					ORDER BY Name DESC];
	}

	
}