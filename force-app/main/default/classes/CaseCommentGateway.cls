/** 
* @FileName: CaseCommentGateway
* @Description: Provides finder methods for accessing data in the CaseComment object.
* @Copyright: Positive (c) 2019
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 4/1/19 RDAVID - extra/CaseCommentTrigger-NewCaseCommentNotification - Created Class for Notifying Case Owner when a new Case Comment was inserted in SF by Api Admin
* 1.1 3/2/19 JBACULOD - fixed latest comment time stamp display' 
* 1.2 7/0219 RDAVID TeamworkId: 22915233 Branch:extra/CaseSourcing - Updated logic to append "Car Sourcing Comment" on the Latest Comment Full field if Case VSR
                    TeamworkId: 22816684 Branch:extra/CaseSourcing - Added logic to append Created By User Name and Time stamp
* 1.3 6/03/19 RDAVID TeamworkId: 23671053 Branch:extra/CaseSourcing -  Added logic to fix Luke Caesar Issue hitting Latest Case Comment character limit
* 1.4 14/05/19 RDAVID TeamworkId: tasks/24385208 Branch:extra/tasks24385208-RollupChildCaseCommentToParentCase - Added logic to create Case Comment on Active Parent Case when a case is created from Child Case
* 1.5 10/07/19 RDAVID TeamworkId: extra/task25265924-NotifyParentCaseofCaseNodifi - Added Logic to include Nodifi Cases in Creating Case Comment for Parent Case of Case record.
**/ 

public class CaseCommentGateway {

    public static Id CASE_VEHICLESOURCING_RTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('KAR: Vehicle Sourcing').getRecordTypeId();
    /*public static void sendEmailToCaseOwner(CaseComment cc, Map<Id,Case> caseMap){
        Case parentCase = caseMap.get(cc.Id);

        List<String> sendTo = new List<String>();
        sendTo.add(parentCase.Owner.Email);
        if(parentCase.Settlement_Officer__c != NULL && parentCase.Settlement_Officer__r.Email != NULL) sendTo.add(parentCase.Settlement_Officer__r.Email);
        // sendTo.add(userInfo.getUserEmail());
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(sendTo);
            mail.setSubject('New Case Comment for Case '+ parentCase.CaseNumber);
            String body = 'Hi Team,';
            body += ' <br/> <br/> The introducer of Case '+ parentCase.CaseNumber + ' has left a new comment on the Case via the Nodifi Platform.';
            body += ' <br/> <br/> The comment is: <br/>'+ cc.CommentBody;
            body += ' <br/> <br/> Please action this as soon as possible and leave a new Case Comment confirming your action.';
            mail.setHtmlBody(body);
        
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        allmsg.add(mail);
        
        Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                + results[0].errors[0].message);
            }   
    } */

    /*public static void sendEmailToIntroducer(CaseComment cc, Case parentCase){

        List<String> sendTo = new list <String>();
        if (parentCase.Connective_Broker_lookup__c != null && parentCase.Connective_Broker_lookup__r.Email != null){ 
            sendTo.add(parentCase.Connective_Broker_lookup__r.Email);
            
            if (parentCase.Introducer_Support__c != null && parentCase.Introducer_Support__r.Email != null) sendTo.add(parentCase.Introducer_Support__r.Email);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(sendTo);
            mail.setOrgWideEmailAddressId('0D26F000000Gvf8');
            mail.setTemplateId('00Xp0000000Yfr6');
            mail.setTargetObjectId(parentCase.Connective_Broker_lookup__c);
            mail.setWhatId(parentCase.Id);

            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            allmsg.add(mail);
            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(allmsg);
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
            System.debug('The email failed to send: '
                + results[0].errors[0].message);
            }

        }

    } */

    public static Case updateLatestCommentonCase(CaseComment cc, Case parentCase){
        system.debug('@@parentCase:'+parentCase);
        DateTime now = DateTime.now();
        String curTime = now.format('dd/MM/yyyy hh:mm a'); //hh for 12-hour format, HH for 24-hour format
        string trimcom = '';
        if (String.valueof(cc.CommentBody).length() > 230){
            trimcom = String.valueof(cc.CommentBody).substring(0,230);
        }
        else trimcom = cc.CommentBody; 
        string stamp = '\n\n'+ 'Created By: '+ userInfo.getName() +'('+curTime+')';
        if(parentCase.RecordTypeId == CASE_VEHICLESOURCING_RTID){
            parentCase.Latest_Comment_Full__c = 'Car Sourcing Comment: '+cc.CommentBody+stamp; 
            trimcom = 'Car Sourcing Comment: '+trimcom;
        }
        else{
            parentCase.Latest_Comment_Full__c = cc.CommentBody+stamp; 
        }
        parentCase.Latest_Comment__c = curTime + ' : ' + trimcom;
        parentCase.Latest_Comment1__c = curTime + ' : ' + trimcom; //COPY
        // 1.3 6/03/19 RDAVID TeamworkId: 23671053
        if(!String.IsBlank(parentCase.Latest_Comment__c) && String.valueOf(parentCase.Latest_Comment__c).length() > 254){
            parentCase.Latest_Comment__c = parentCase.Latest_Comment__c.substring(0,250);
        }
        if(!String.IsBlank(parentCase.Latest_Comment1__c) && String.valueOf(parentCase.Latest_Comment1__c).length() > 254){
            parentCase.Latest_Comment1__c = parentCase.Latest_Comment1__c.substring(0,250);
        }
        // 1.3 6/03/19 RDAVID TeamworkId: 23671053 END
        parentCase.Latest_Comment_IsPublished__c = cc.IsPublished;
        return parentCase;
    }
    
    public static Map<Id,Case> getCaseMap (Map<Id,Id> caseCcIdMap, Map<Id,Id> parentCaseMap){
        Map<Id,Case> cseMap = new Map<Id,Case>();
        list <ID> casePCseIDs = caseCcIDMap.values(); 
        // 1.4 14/05/19 RDAVID TeamworkId: tasks/24385208  - Modified Query
        for (CaseComment cc : [Select Id, IsPublished, ParentId, Parent.Latest_Comment_Full__c, Parent.Latest_Comment_IsPublished__c, Parent.Latest_Comment1__c, Parent.Latest_Comment__c, Parent.RecordTypeId, Parent.Parent_Case__r.Partition__c, Parent.Parent_Case__r.Status,Parent.Parent_Case__c From CaseComment Where ParentId IN: casePCseIDs]){
            Case cse = new Case(
                Id = cc.ParentId,
                Latest_Comment_Full__c = cc.Parent.Latest_Comment_Full__c,
                Latest_Comment__c = cc.Parent.Latest_Comment__c,
                Latest_Comment1__c = cc.Parent.Latest_Comment1__c,
                Latest_Comment_IsPublished__c = cc.Parent.Latest_Comment_IsPublished__c,
                RecordTypeId = cc.Parent.RecordTypeId
            );
            cseMap.put(cc.Id, cse);
            // 1.4 14/05/19 RDAVID TeamworkId: tasks/24385208  - Added Logic/Method to determine if the Parent Case is valid.
            if(isValidForParentCase(cc)){
                parentCaseMap.put(cc.ParentId,cc.Parent.Parent_Case__c);
            }
        }
        //for(Case cse : [SELECT Id, Partition__c, Latest_Comment_Full__c, Latest_Comment_IsPublished__c, Owner.FirstName,Owner.Email,CaseNumber,Settlement_Officer__c, Settlement_Officer__r.Email, Connective_Broker_lookup__c, Connective_Broker_lookup__r.Email, Introducer_Support__c, Introducer_Support__r.Email  FROM Case WHERE Id IN: caseCcIdMap.keySet()]){
        //for(Case cse : [SELECT Id, Latest_Comment_Full__c, Latest_Comment_IsPublished__c, Latest_Comment1__c, Latest_Comment__c FROM Case WHERE Id IN: casePCseIDs]){
            //cseMap.put(caseCcIdMap.get(cse.Id),cse);
        //}
        return cseMap;
    }

    // 1.4 14/05/19 RDAVID TeamworkId: tasks/24385208 - Logic/Method to determine if the Parent Case is valid.
    public static Boolean isValidForParentCase (CaseComment cc){
        //1.5 10/07/19 RDAVID TeamworkId: extra/task25265924-NotifyParentCaseofCaseNodifi - Added Nodifi
        if(cc.Parent.Parent_Case__c != NULL && (cc.Parent.Parent_Case__r.Partition__c == 'Positive' || cc.Parent.Parent_Case__r.Partition__c == 'Nodifi') && cc.Parent.Parent_Case__r.Status != 'Closed'){
            return true;
        }
        return false;
    }
}