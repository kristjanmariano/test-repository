/*
	@Description: class containing the invocable method that creates Case Touchpoint Junction records.
	@Author: Rexie David - Positive Group
	@History:
		-	7/09/2018 - extra/BizibleTouchPointInvocable - Created
        -   19/09/2018 - extra/BizibleTouchPointInvocable - Fixed error in Process Builder added null checks
*/
public class Role_CaseTouchpointNewCase {

    @InvocableMethod(label='Create Case Touchpoint records' description='creates a new Case Touchpoint junction records.')
    
	public static void createNewCaseTouchpoint (list <String> rolecaseaccountId) {
        Savepoint sp = Database.setSavepoint();
        String[] rolecaseaccountIdArr = rolecaseaccountId[0].split(':');
        System.debug('rolecaseaccountIdArr = '+rolecaseaccountIdArr);
        
        Id roleId = !String.isBlank(rolecaseaccountIdArr[0]) ? rolecaseaccountIdArr[0]:null;
        Id caseId = !String.isBlank(rolecaseaccountIdArr[1]) ? rolecaseaccountIdArr[1]:null;
        Id accountId = !String.isBlank(rolecaseaccountIdArr[2]) ? rolecaseaccountIdArr[2]:null;

        if(roleId != NULL && caseId != NULL && accountId != NULL){
            try {
                Map<Id,Account> accountBizi = new Map<Id,Account>([SELECT Id, (SELECT Id, bizible2__Touchpoint_Date__c FROM bizible2__Bizible_Touchpoints__r ORDER BY bizible2__Touchpoint_Date__c ASC) FROM Account WHERE Id =: accountId]);

                List<Case_Touch_Point__c> caseTouchPointToInsert = new List<Case_Touch_Point__c>();

                if(accountBizi.get(accountId).bizible2__Bizible_Touchpoints__r != NULL && accountBizi.get(accountId).bizible2__Bizible_Touchpoints__r.size()>0){
        
                    for(bizible2__Bizible_Touchpoint__c biziCount : accountBizi.get(accountId).bizible2__Bizible_Touchpoints__r){
                        Case_Touch_Point__c ctp = new Case_Touch_Point__c(Bizible_Touchpoint__c = biziCount.Id, Case__c=caseId);
                                
                        if(accountBizi.get(accountId).bizible2__Bizible_Touchpoints__r.indexOf(biziCount)+1 == accountBizi.get(accountId).bizible2__Bizible_Touchpoints__r.size()){
                            System.debug('this is the latest '+biziCount.bizible2__Touchpoint_Date__c);     
                            ctp.Latest_Touchpoint__c = true;
                        }  
                        
                        caseTouchPointToInsert.add(ctp);
                    }
                }
                System.debug('caseTouchPointToInsert = '+caseTouchPointToInsert);
                Database.insert(caseTouchPointToInsert);  
            }
            catch(Exception e){
                Database.rollback( sp );  
            }
        }
	}
}