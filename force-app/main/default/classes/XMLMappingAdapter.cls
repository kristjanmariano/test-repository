/******* A utility class to read data from xml and store mapping in form of objects *******/
global class XMLMappingAdapter {
    private static Map<String,Dom.Document> xmlFileMap = new Map<String,Dom.Document>(); // xml file dom for both xml
    public static Map<String,XMLMapping> xmlNodesMap = new Map<String,XMLMapping>(); // Xml mapping data with object name as key
    public static Map<String,List<LKRecord>> lkRecordsMap = new Map<String,List<LKRecord>>(); // key as object name and list from salesforce query
    
    /********** Read XML File from static resource **********/
    public static String readMappingFromXML(String fileName) {
        String xmlString = '';
        StaticResource sResource = [Select Body From StaticResource Where Name =: fileName LIMIT 1];
        if(sResource != null && sResource.Body != null) {
            xmlString = sResource.Body.toString();
        }
        return xmlString;
    }
    
    /********* Parse string to Dom.Document object *********/
    public static Dom.Document parseStringToXmlDom(String xmlString) {
        Dom.Document doc = new Dom.Document();
        doc.load(xmlString);
        return doc;
    }
    
    /********* Get Document object for XML from file in static resource **********/
    public static Dom.Document getDomDocument(String fileName) {
        if(xmlFileMap.containsKey(fileName)) {
            return xmlFileMap.get(fileName);
        }else {
            String xmlString = readMappingFromXML(fileName);
            dom.Document doc = parseStringToXmlDom(xmlString);
            xmlFileMap.put(fileName,doc);
            return doc;
        }
    }
    
    public static void clearData() {
        if(xmlFileMap != null && xmlNodesMap != null) {
            xmlFileMap.clear();
            xmlNodesMap.clear();
        }
    }
    
    /******** Get the object of mapping class for the given node name ********/
    public static XMLMapping getNode(String nodeName) {
        return xmlNodesMap.get(nodeName);
    }
}