/*

    Purpose: Supports VF page for printable version of the Veda credit score.
    
*/

public without sharing class VedaIndividualPrintableCC_NM {
    
   public VedaDTO.IndividualResponse response { get; set; }   
   public boolean savedPDFFile;
      
   public Contact contact { get; set;}
   public Lead leadObj { get; set;}
   public Account acctObj { get; set;}
   public Opportunity oppObj { get; set;}
   public Applicant_2__c app2Obj {get; set; }
   
   public Id idOfObjToAttach { get; set;}
   public String sObjectType { get; set;}
   
   public Attachment xmlFile { get; set;}
   public Role__c role { get; set;}
    /*
        Search Attachments for a xml document associated which
        contains the VEDA advantage response, when you find it, populate the Credit
        report response object for the VF Page

    */   
   public VedaIndividualPrintableCC_NM()
    {
        savedPDFFile = false;
        xmlFile = [Select ParentId, OwnerId, Name, Id, Description, ContentType, 
                                  BodyLength, Body From Attachment where id =:ApexPages.currentPage().getParameters().get('attachID')] ; 
                                  
        idOfObjToAttach = ApexPages.currentPage().getParameters().get('objAttId');
        sObjectType = ApexPages.currentPage().getParameters().get('objType');
        system.debug('sObjectType' + sObjectType);   
        
        if(sObjectType == 'Role__c'){
            Id accId; 
            // if(sObjectType == 'Account'){
            //     accId = idOfObjToAttach;
            // } 
            if(sObjectType == 'Role__c'){
                role = [SELECT Id, Case__c, Role_Type__c, Case__r.CaseNumber, Account__c FROM Role__c WHERE Id =: idOfObjToAttach];
                accId = role.Account__c;
            } 
            System.debug('RDAVID = '+accId);
              acctObj = [Select a.Website, a.Type, a.State__c, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, 
                 a.ShippingCity,  a.Salutation, a.RecordTypeId, a.Postcode__c, a.Phone, a.PersonMobilePhone, a.PersonLastCUUpdateDate, 
                 a.PersonLastCURequestDate, a.PersonEmailBouncedReason, a.PersonEmailBouncedDate, a.PersonEmail, a.PersonContactId, a.LastName, 
                 a.IsPersonAccount, a.Id,a.Gender__c, a.FirstName, a.Fax, 
                 a.Drivers_Licence_Number__c,  a.Description,  a.Date_of_Birth__c, a.Customer_Comments__c, 
                 a.BillingStreet, a.BillingState, a.BillingPostalCode, a.BillingCountry, 
                 a.BillingCity, a.Account_PersonMobilePhone__c From Account a where a.id = :accId];                                 
        
        }                                 
        response = new VedaDTO.IndividualResponse().fromXml(xmlFile.body.toString()); 
    }
   
   public boolean getSavedPDFFile(){
        return savedPDFFile;
   }
   
   public PageReference savePdfFile(){
        System.debug('save the pdf file');
        savedPDFFile = true;
        Attachment a;
        Blob pdfBlob;
        
        if (Test.IsRunningTest())
            pdfBlob=Blob.valueOf('UNIT.TEST');      
        else
            pdfBlob=Page.VedaIndividualPagePrintableNM.getContentAsPDF();
         system.debug(CustomSettingUtil.getVedaConfigBooleanValue('SaveResultToContact') + 'test');    
        if(sObjectType == 'Role__c'){
            system.debug('RDAVID : Save Attachment to Account temporarily.');    
            a = new Attachment(parentId = acctObj.id, name=acctObj.firstname + ' ' +acctObj.lastName + '-Credit Check.pdf', body = pdfBlob);//DateTime.now().format()+
        }
        insert a;
        if(role.Id != NULL){
            if(sObjectType == 'Role__c') savePDFToBox(role.Case__r.CaseNumber, a.Id);
            role.h_Is_Veda_Credit_Checked__c = true;
            update role;
            if(role.Role_Type__c == 'Primary Applicant'){
            	Case cs = new Case(Id = role.Case__c, Is_Veda_Credit_Checked__c = true);
            	update cs;
            }	
        }  
        delete xmlFile;
        return null;
    }

    @future (callout=true)
    public static void savePDFToBox(String caseNumber, Id attachId){
        // Boolean isSuccess = false;
        system.debug('*caseNumber = '+caseNumber);
        system.debug('*attach = '+attachId);
        system.debug('*savePDFToBox');
        try{
            box__Folder_Meta__c boxRecord = [   SELECT Id,box__Folder_Append__c,box__Folder_Name__c,box__Folder_Id__c,box__Folder_Share_Link__c 
                                                FROM box__Folder_Meta__c 
                                                WHERE box__Folder_Name__c =: caseNumber];
            system.debug('*boxRecord = '+boxRecord);
            box.Toolkit boxToolkit = new box.Toolkit();
            system.debug('*boxToolkit');
            Attachment attachToBox=[select name,body,parentid from attachment where id=:attachId];
            boxtoolkit.createFileFromAttachment(attachToBox,null, boxRecord.box__Folder_Id__c,null);
            system.debug('*createFileFromAttachment');
            if(!Test.IsRunningTest()) boxToolkit.commitChanges();
            system.debug('*commitChanges');
            delete attachToBox;
        }
        catch(Exception e){
            System.debug('errorTextMsg : '+e.getMessage());
        }    
    }
}