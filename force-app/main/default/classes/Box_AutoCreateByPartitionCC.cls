/*
    @Description: controller class of Box_AutoCreateByPartition
    @Author: Jesfer Baculod - Positive Group
    @History:
        -   7/24/18 - Created, Logic based from Box_AutoCreateRecordFolderCC (old Box integration for Nodifi)
        -   8/07/18 - Added checking of existing Box Folder on current record
        -   8/20/18 - Removed redirecting of Box on get/create Box Case Folder (Due to Box Link)
        -   9/12/18 - Renabled redirecting of Box on get/create Box Case Folder (Removed Box Link on current page, will display Box Link on a separate page)
        -   11/01/18 - Fixed duplicate folder creation due to no checking of Box Folder ID
        -   11/27/18 - Removed Manual Collab on Folder, removed checking of Box Access
        -   12/07/19  - RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders - Updated folder structure for POS: Consumer - Property Finance
*/
public class Box_AutoCreateByPartitionCC {

    public Id curID {get;set;} //current Object ID
    public string objType {get;set;}
    public boolean noBoxAccess {get;set;}
    //public boolean BoxSuccess {get;set;}
    public Case cse {get;set;}
    public string test_objRecordFolderID {get;set;}
    private list <Box_Partition_Settings__mdt> boxpset {get;set;}

    public Box_AutoCreateByPartitionCC(ApexPages.StandardController con) {
        
        curID = con.getId();
        system.debug('@@curID:'+curID);
        noBoxAccess = false; //BoxSuccess = 
        //Retrieve Box Partition Settings
        boxpset = new list <Box_Partition_Settings__mdt>();
        //boxpset = [Select Id, Partition__c, Box_Folder_ID__c, Sub_Folders__c From Box_Partition_Settings__mdt];
        boxpset = [Select Id, Partition__c, Box_Folder_ID__c, Sub_Folders__c, Recordtype_Name__c From Box_Partition_Settings__mdt]; //25/06/19    RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders
        if (curID != null){ 
            if (curID.getSobjectType() == Case.SobjectType){ //ID is from Case
                //Retrieve Case
                cse = [Select Id, CaseNumber, Box_Folder_ID__c, Status, Stage__c, Partition__c, Channel__c, Recordtype.Name From Case Where Id = : curID];
            }
        }

    }

    public pageReference autocreateBoxFolder(){

        if (TriggerFactory.trigset.Enable_Case_Box_Partition_Auto_Create__c){

            // Instantiate the Toolkit object
            box.Toolkit boxToolkit = new box.Toolkit();
            if (cse != null){
                if (cse.Partition__c != null && cse.Partition__c != 'Partner'){
                    // Create a folder and associate it with the record; Return the folder ID if it's already associated on a Box folder
                    String objRecordFolderID;
                    if (cse.Box_Folder_ID__c == null){
                        if (boxToolkit.getFolderIdByRecordId(curID) == null){
                            //No created Box Folder found on current record        
                            system.debug('@@1');
                            objRecordFolderID = boxToolkit.createFolderForRecordId(curID, null, true); //Create Box folder
                        }
                        else {
                            system.debug('@@2');
                            objRecordFolderID = boxToolkit.getFolderIdByRecordId(curID); //Retrieve existing Box Folder
                        }
                    }
                    else objRecordFolderID = cse.Box_Folder_ID__c;
                    system.debug('@@objRecordFolderID: '+objRecordFolderID);
                    if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
                    system.debug('@@new item folder id: ' + objRecordFolderID);
                    if(objRecordFolderID == null){
                        system.debug('most recent error: ' + boxToolkit.mostRecentError);
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,boxToolkit.mostRecentError));
                        return null;
                    }
                    else{

                        for (Box_Partition_Settings__mdt bpset : boxpset){
                            system.debug('@@bpSet partition:'+bpSet.Partition__c);
                            if (cse.Partition__c == bpset.Partition__c && String.IsBlank(bpset.Recordtype_Name__c) && cse.Recordtype.Name != 'POS: Consumer - Property Finance' ||
                                cse.Partition__c == bpset.Partition__c && !String.IsBlank(bpset.Recordtype_Name__c) && cse.Recordtype.Name == bpset.Recordtype_Name__c){ //12/07/19    RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders
                                system.debug('@@cse'+cse.CaseNumber);
                                //Move Box folder base on Case Partition
                                boolean moveFolder = boxToolkit.moveFolder(objRecordFolderID, bpset.Box_Folder_ID__c,'');
                                system.debug('@@moveFolder:'+moveFolder);
                                string sfolder = bpset.Sub_Folders__c;
                                set <string> subfolders = new set <string>();
                                boolean breakfolder = false;
                                do {
                                    string mtype = sfolder.substringBetween('[',']');
                                    if (mtype == null) breakfolder = true;
                                    if (mtype != null) {
                                        subfolders.add(mtype);
                                        sfolder = sfolder.remove('['+mtype+']');
                                    }
                                }
                                while (!breakfolder);
                                for (string strfolderName : subfolders){
                                    if(strfolderName.containsIgnoreCase('>')){
                                        String[] arrTest = strfolderName.split('>'); 
                                        String childfolder = boxToolkit.createFolder(arrTest[0], objRecordFolderID, ''); 
                                        String subchildfolder = boxToolkit.createFolder(arrTest[1], childfolder, ''); 
                                        if(arrTest.size() == 3){ //12/07/2019   RDAVID extra/task24400704-POSConsumerPropertyAutoCreateBoxFolders
                                            String subgrandchildfolder = boxToolkit.createFolder(arrTest[2], subchildfolder, '');
                                        }
                                    }
                                    else{
                                        String childfolder = boxToolkit.createFolder(strfolderName, objRecordFolderID, ''); 
                                    }
                                }
                            }
                        }

                        if (cse.Box_Folder_ID__c == null){ //Store ID of Box Folder created for Case
                            cse.Box_Folder_ID__c = objRecordFolderID;
                            update cse;
                        }
                        system.debug('@@cse:'+cse);

                        // ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
                        boxToolkit.commitChanges();

                        //Redirect to object's BoxSection VF
                        PageReference pref = new PageReference('/apex/box__CaseBoxSection?id='+curID);
                        pref.setRedirect(true);
                        return pref; 
                        //BoxSuccess = true;
                        //return null;
                    }
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Box_NM_Partition_Error_1));
                    return null;
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Box_NM_Partition_Error_2));
                return null;
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Box Folder creation currently disabled. Please contact your system administrator'));
            return null;
        }
    }

}