/** 
* @FileName: HappyPathController
* @Description: controller class of HappyPath VF page
* @Copyright: Positive (c) 2018 
* @author: Jan Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 5/08/18 JBACULOD Created
**/ 
@isTest
private class HappyPathControllerTest {

    @testsetup static void setup(){
        //Create test data for (Case, Application, Role, Account)
        TestDataFactory.Case2FOSetupTemplate();

        //Create test data for Asset

    }

    static testmethod void testHappyPath(){

        //Retrieve created Test Data
        Case cse = [Select Id, CaseNumber From Case limit 1];
        Application__c app = [Select Id, Case__c From Application__c Where Case__c = : cse.Id];
        list <Account> acclist = [Select Id, Name From Account];
        list <Role__c> rolelist = [Select Id, Account_Name__c, Case__c, Application__c From Role__c];

        //Load HappyPath from Case
        PageReference pref_cse = Page.HappyPath;
        pref_cse.getParameters().put('id',cse.Id);
        Test.setCurrentPage(pref_cse);
        HappyPathController hpcon_cse = new HappyPathController();

        //Load HappyPath from Role
        PageReference pref_role = Page.HappyPath;
        pref_role.getParameters().put('id',rolelist[0].Id);
        Test.setCurrentPage(pref_role);
        HappyPathController hpcon_role = new HappyPathController();
 
        //Load HappyPath from Application
        PageReference pref_app = Page.HappyPath;
        pref_app.getParameters().put('id',app.Id);
        Test.setCurrentPage(pref_app);
        HappyPathController hpcon_app = new HappyPathController(); 

        hpcon_app.onEdit = false;
        hpcon_app.curPage = 'a1';
        hpcon_app.firstLoadFO = true;
        hpcon_app.loadAssetFO();
        hpcon_app.newAddressIn = 'Asset1__c';
        hpcon_app.showNewAddress();
        hpcon_app.saveAssignNewAddress(); //Error on Save
        hpcon_app.addr.Street_Number__c = '342343243';
        hpcon_app.addr.Street_Name__c = 'test';
        hpcon_app.addr.Street_Type__c = 'test';
        hpcon_app.addr.Suburb__c = 'suburb';
        hpcon_app.addr.Postcode__c = '3243232';
        hpcon_app.addr.Country__c = 'Australia';
        hpcon_app.addr.State__c = 'New South Wales';
        hpcon_app.saveAssignNewAddress();

        hpcon_app.changeRolein = 'ALE';
        hpcon_app.validateRoleSelected();

        hpcon_app.preContinue();
        hpcon_app.HPContinue();

        hpcon_app.saveAll();
        hpcon_app.fowr.ass.Asset_Value__c = 34324324;
        hpcon_app.fowr.allowAssocAsset = true;
        hpcon_app.fowr.defaultRole = false;
        hpcon_app.saveAll();


        hpcon_app.curPage = 'l1';
        hpcon_app.firstLoadFO = true;
        hpcon_app.loadLiabilityFO();
        hpcon_app.preContinue();
        hpcon_app.HPContinue();
        hpcon_app.fowr.allowAssocLiability = true;
        hpcon_app.saveAll();

        hpcon_app.curPage = 'i1';
        hpcon_app.firstLoadFO = true;
        hpcon_app.loadIncomeFO();
        hpcon_app.preContinue();
        hpcon_app.HPContinue();
        hpcon_app.newAddressIn = 'Income1__c';
        hpcon_app.showNewAddress();
        hpcon_app.saveAssignNewAddress(); //Error on Save
        hpcon_app.addr.Street_Number__c = '342343243';
        hpcon_app.addr.Street_Name__c = 'test';
        hpcon_app.addr.Street_Type__c = 'test';
        hpcon_app.addr.Suburb__c = 'suburb';
        hpcon_app.addr.Postcode__c = '3243232';
        hpcon_app.addr.Country__c = 'Australia';
        hpcon_app.addr.State__c = 'New South Wales';
        hpcon_app.saveAssignNewAddress();
        hpcon_app.fowr.allowAssocIncome = true;
        hpcon_app.fowr.defaultRole = false;
        hpcon_app.saveAll();

        hpcon_app.curPage = 'e1';
        hpcon_app.firstLoadFO = true;
        hpcon_app.loadExpenseFO();
        hpcon_app.preContinue();
        hpcon_app.HPContinue();
        hpcon_app.fowr.allowAssocExpense = true;
        hpcon_app.saveAll();

        hpcon_app.ValidateHappyPath();
        hpcon_app.switchToExpenseWithLiability();


        //hpcon_app.cancelFO();

        hpcon_app.fakeMethod();

    }

    /* static testmethod void testNewFO(){

        //Retrieve created Test Data
        Case cse = [Select Id, CaseNumber From Case limit 1];
        Application__c app = [Select Id, Case__c From Application__c Where Case__c = : cse.Id];
        list <Account> acclist = [Select Id, Name From Account];
        list <Role__c> rolelist = [Select Id, Account_Name__c, Case__c, Application__c From Role__c];

        //Load HappyPath from Application
        PageReference pref_app = Page.HappyPath;
        pref_app.getParameters().put('id',app.Id);
        Test.setCurrentPage(pref_app);
        HappyPathController hpcon_app = new HappyPathController();

        hpcon_app.app.Role_Share_Across_FOs__c = 'Consistent - Equal Shares';
        hpcon_app.createFoShares = true;
        hpcon_app.linkFOs = true;

        hpcon_app.curPage = hpcon_app.CP_ASSET;
        hpcon_app.loadAssetFO();
        hpcon_app.defaultFoType();
        hpcon_app.createAssociatedFO();
        hpcon_app.assWr.createAssocIncome = true;
        hpcon_app.assWr.ass.Asset_Value__c = 5000;
        hpcon_app.loadAssetFO();
        hpcon_app.saveHappyPath();
        hpcon_app.cancelFO();

        hpcon_app.curPage = hpcon_app.CP_LIABILITY;
        hpcon_app.loadLiabilityFO();
        hpcon_app.defaultFoType();
        hpcon_app.liaWr.createAssocExpense = true;
        hpcon_app.createAssociatedFO();
        hpcon_app.loadLiabilityFO();
        hpcon_app.saveHappyPath();
        hpcon_app.cancelFO();

        hpcon_app.curPage = hpcon_app.CP_INCOME;
        hpcon_app.loadIncomeFO();
        hpcon_app.defaultFoType();
        hpcon_app.incWr.createAssocAsset = true;
        hpcon_app.createAssociatedFO();
        hpcon_app.loadIncomeFO();
        hpcon_app.saveHappyPath();
        hpcon_app.cancelFO();

        hpcon_app.curPage = hpcon_app.CP_EXPENSE;
        hpcon_app.defaultFoType();
        hpcon_app.loadExpenseFO();
        hpcon_app.expWr.createAssocLiability = true;
        hpcon_app.createAssociatedFO();
        hpcon_app.loadExpenseFO();
        hpcon_app.saveHappyPath();
        hpcon_app.cancelFO(); 

    } */

}