/*
	@Description: class containing the invocable method that can be used on Callback for creating new Case from a Closed Case
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	8/02/2018 - Created
        -   10/02/18 - Updated - RDAVID  issuefix/CreateCaseFromCallbackIssue - Manually populate Primary Contact fields for the New Case
        -   06/11/19 - Updated - Added Lead purpose, Lead Loan Amount mapping
        -   9/07/2019 - Updated - RDAVID - extra/task25140648-BugFixAttemptedCallback - Populate Lead_Creation_Method__c = 'Callback Automation'
*/
public class Case_CallbackNewCase {
    
    private static final Process_Flow_Definition_Settings1__c processFlowSettings = Process_Flow_Definition_Settings1__c.getInstance(UserInfo.getUserId());
    private static final Trigger_Settings1__c trigSet = Trigger_Settings1__c.getInstance(UserInfo.getUserId());
    private static final String CASERTPWMInsuranceApp = 'PWM: Insurance App';
    @InvocableMethod(label='Create New Case on Callback' description='creates a brand new Case from a Closed Case once Callback Date/Time has been reached')
	public static void createNewCaseOnCallback (list <ID> caseIDs) {
        Savepoint sp = Database.setSavepoint();
        try {
            system.debug('@@caseIDs:'+caseIDs);
            //Retrieve Account - Role Field Mapping for prepopulating Role fields
            list <Account_To_Role_Field_Mapping__mdt> accToRoleMapping = [Select Id, Account_Field__c, Role_Field__c From Account_To_Role_Field_Mapping__mdt];
            //Retrieve Closed Case to copy and its Primary Contact details
            String cseQuery = '';
            String precseQuery = 'Select Id, Lead_Purpose__c, Lead_Loan_Amount__c, Status, Stage__c, On_Hold__c, RecordTypeId, RecordType.Name, Channel__c, Partition__c, Lead_Source__c, NVMContactWorld__NVMAccountOverride__c, NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedIn__c, NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedOut__c, NVMContactWorld__RoutePlanIdentifier__c, Primary_Contact_Name_MC__c, Primary_Contact_Location_MC__c, Primary_Contact_for_Application__c, Primary_Contact_Email_MC__c, Primary_Contact_Phone_MC__c,';
            String cseCommentsQuery = '( Select Id, CommentBody, IsPublished, ParentId From CaseComments Order BY CreatedDate DESC limit 1), ';
            String roleQuery = '( Select Id, RecordTypeId, Primary_Contact_for_Application__c, Account__c, Address__c, Location__c, Role_Type__c, ';
            for (Account_To_Role_Field_Mapping__mdt atrfm : accToRoleMapping){ //Add fields from Custom Metadata
                if (atrfm.Account_Field__c != 'x'){
                    string fp = atrfm.Account_Field__c;
                    roleQuery+= 'Account__r.' + fp + ',';
                }
            }
            roleQuery = roleQuery.subString(0,roleQuery.Length()-1); //trim last comma
            roleQuery+= ' From Roles__r Where Primary_Contact_for_Application__c = true)';
            String postcseQuery = ' From Case Where Id in : caseIDs';
            cseQuery = precseQuery + cseCommentsQuery + roleQuery + postcseQuery;
            list <Case> sourceCases = database.Query(cseQuery);
            system.debug('@@sourceCases:'+sourceCases);

            set <Id> roleIDs = new set <ID>();
            map <Id,Role__c> primContactRoleMap = new map <Id,Role__c>(); //key - Case ID
            map <Id, Case> oldnewCaseMap = new map <id,Case>(); //key - Case ID
            map <Id, CaseComment> oldCaseCommentsMap = new map <id,CaseComment>(); //key - Case ID
            if (sourceCases.size() > 0){
                for (Case oldCse : sourceCases){
                    if (oldCse.Status == 'Closed' && oldCse.Stage__c == 'Lost' && oldCse.On_Hold__c == 'Callback' && oldCse.Partition__c == 'Positive' && oldCse.RecordType.Name != CASERTPWMInsuranceApp){
                        Case cse = new Case(
                            Channel__c = oldCse.Channel__c,
                            Partition__c = oldCse.Partition__c,
                            RecordTypeId = oldCse.RecordTypeId,
                            Status = CommonConstants.CASE_STATUS_NEW, 
                            Stage__c = CommonConstants.CASE_STAGE_OPEN,
                            Lead_Source__c = 'PLS Database Callback',
                            Lead_Purpose__c = oldCse.Lead_Purpose__c,
                            Lead_Loan_Amount__c = oldCse.Lead_Loan_Amount__c,
                            NVMContactWorld__NVMAccountOverride__c = oldCse.NVMContactWorld__NVMAccountOverride__c,
                            NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedIn__c = oldCse.NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedIn__c,
                            NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedOut__c = oldCse.NVMContactWorld__NVMOverrideCaseOwnerTimeoutLoggedOut__c,
                            NVMContactWorld__RoutePlanIdentifier__c = oldCse.NVMContactWorld__RoutePlanIdentifier__c,
                            Primary_Contact_Name_MC__c = oldCse.Primary_Contact_Name_MC__c, //RDAVID 2/10/18 issuefix/CreateCaseFromCallbackIssue - Manually populate Primary Contact fields for the New Case
                            Primary_Contact_Location_MC__c = oldCse.Primary_Contact_Location_MC__c,
                            Primary_Contact_for_Application__c = oldCse.Primary_Contact_for_Application__c,
                            Primary_Contact_Email_MC__c = oldCse.Primary_Contact_Email_MC__c,
                            Primary_Contact_Phone_MC__c = oldCse.Primary_Contact_Phone_MC__c,
                            AccountId = oldCse.Primary_Contact_Name_MC__c,
                            Lead_Creation_Method__c = 'Callback Automation' //9/07/2019 - Updated - RDAVID - extra/task25140648-BugFixAttemptedCallback
                        );
                        /*if(!trigSet.Enable_Case_NVM_Timezone_based_Routing__c){
                            cse.NVMContactWorld__NVMRoutable__c = (processFlowSettings.Enable_Case_NVM_Flow_Definitions__c) ? true:false;
                        }*/
                        if (oldCse.CaseComments.size() > 0){
                            for (CaseComment cseComment : oldCse.CaseComments){
                                oldCaseCommentsMap.put(oldCse.Id, cseComment);
                            }
                        }
                        if (oldCse.Roles__r.size() > 0){
                            for (Role__c oldrole : oldCse.Roles__r){
                                primContactRoleMap.put(oldCse.Id, oldRole);
                                roleIDs.add(oldrole.Id);
                            }
                        }
                        oldNewCaseMap.put(oldCse.Id, cse);
                    }
                }
            }

            map <Id, list <Role_Address__c>> roleAddressMap = new map <Id, list <Role_Address__c>>();
            if (roleIDs.size() > 0){
                //Retrieve Role Addresses of Role in Closed Case
                for (Role_Address__c raddr : [Select Id, RecordTypeId, Active_Address__c, Address__c, Role__c, End_Date__c, Location__c, Residential_Situation__c, Start_Date__c From Role_Address__c Where Role__c in : roleIDs]){
                    if (!roleAddressMap.containskey(raddr.Role__c)) roleAddressMap.put(raddr.Role__c, new list <Role_Address__c>());
                    roleAddressMap.get(raddr.Role__c).add(raddr);
                }
            }

            map <Id, Role__c> oldnewRoleMap = new map <id,Role__c>(); //key - Role ID
            list <CaseComment> creCaseCommentList = new list <CaseComment>();
            if (oldNewCaseMap.size() > 0){ 
                insert oldNewCaseMap.values(); //Create brand new Case
                System.debug('New Case Size = '+oldNewCaseMap.values().size());
                System.debug('New Case ID = '+oldNewCaseMap.values()[0].Id);
                for (Id oldcseId : oldNewCaseMap.keySet()){
                    if (oldCaseCommentsMap.containskey(oldcseId)){
                        CaseComment oldCseComment = oldCaseCommentsMap.get(oldcseId);
                        CaseComment newCseComment = oldCseComment.clone(false,false,false,false);
                        newCseComment.ParentId = oldNewCaseMap.get(oldcseId).Id; //Assign Case Comment to the created Case
                        creCaseCommentList.add(newCseComment);
                    }
                    if (primContactRoleMap.containskey(oldcseId)){
                        Role__c oldrole = primContactRoleMap.get(oldcseId);
                        Role__c role = oldrole.clone(false,false,false,false); //Clone old Role
                        role.Case__c = oldNewCaseMap.get(oldcseId).Id; //Assign Role to the created Case
                        oldnewRoleMap.put(oldRole.Id, role);
                    }
                }
            }

            list <Role_Address__c> creRoleAddrList = new list <Role_Address__c>();
            if (oldnewRoleMap.size() > 0){ 
                insert oldnewRoleMap.values(); //Create Roles (Primary Contact Details)
                for (Id oldroleID : oldNewRoleMap.keySet()){
                    if (roleAddressMap.containskey(oldroleId)){
                        for (Role_Address__c oldraddr : roleAddressMap.get(oldroleId)){
                            Role_Address__c raddr = oldraddr.clone(false,false,false,false); //Clone old Role Address
                            raddr.Role__c = oldNewRoleMap.get(oldroleId).Id; //Assign Role Address to the created Role
                            creRoleAddrList.add(raddr);
                        }
                    }
                }
            }

            if (creCaseCommentList.size() > 0) insert creCaseCommentList; //Create Case Comment from Last Comment of Closed Case
            if (creRoleAddrList.size() > 0 ) insert creRoleAddrList; //Create Role Addresses of Primary Contact
        }
        Catch(Exception e){
            System.debug('Error Case_CallbackNewCase Catch Line 132: '+e.getMessage());
            Database.rollback( sp );  
        }

	}
}