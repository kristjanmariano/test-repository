/*
	@Description: LIG 615 Http Request to retrieve query in the given List View parameters
	@Author: Rexie David - Positive Group
	@History:
		-	12/18/17 - Created 
*/
global without sharing class CommonListViewRestResource {
	/*
		@Description: LIG 615 Method to retrive query in List view
		@Parameters: String sObjectAPIName - API Name of the sObject that contains the List View 
					 Id listViewId - Id of the list view
		@Return: String
	*/
	@RemoteAction
    global static String getListViewQueryString(String sObjectAPIName, Id listViewId) {
    	General_Settings__c generalSettings = General_Settings__c.getOrgDefaults();
    	String apiVersion = generalSettings.APIVersion__c;
    	HttpRequest req = new HttpRequest();
		req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+'/services/data/v'+apiVersion+'/sobjects/'+sObjectAPIName+'/listviews/'+listViewId+'/describe');
		req.setMethod('GET');
		req.setHeader('Authorization',  'Bearer ' + UserInfo.getSessionId());
		
		Http http = new Http();
		HTTPResponse res = http.send(req);
		System.debug('res -> '+res);
		System.debug('res.getBody() -> '+res.getBody());
		Map<String,Object> results = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
 		String queryString = JSON.serialize(results.get('query'));
		queryString = queryString.replaceAll('"','');
		System.debug('queryString -> '+queryString);	  
 		return queryString;
    }
}