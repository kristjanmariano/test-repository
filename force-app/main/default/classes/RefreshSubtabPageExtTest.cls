@isTest
public class RefreshSubtabPageExtTest {
    public static List<Account> accList = new List<Account>();
    public static List<Role__c> roleList = new List<Role__c>();

    @testSetup static void setupData() {
		//Create Test Account
		accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);
	}

    static testMethod void testRefreshSubtabVF() {
        Test.startTest();
            PageReference newSupportRequestPage = Page.NewSupportRequestPage;
            Case caseRec = [SELECT Id,Application_Name__c FROM Case LIMIT 1];
            caseRec.Status = 'Review';
            update caseRec;
            Test.setCurrentPage(newSupportRequestPage);
        	Test.setCurrentPageReference(newSupportRequestPage); 
    
            System.currentPageReference().getParameters().put('id', caseRec.Id);
            System.currentPageReference().getParameters().put('type', 'Submission');

            NewSupportRequestController newSupportRequestCtrl = new NewSupportRequestController();
            newSupportRequestCtrl.supportRequest.Notes_to_Support__c = 'Test Notes to Support.';
            newSupportRequestCtrl.supportRequest.Notes_to_Lender__c = 'Test Notes to Lender.';
            newSupportRequestCtrl.supportRequest.I_confirm_the_SF_Record_is_accurate__c=true;
            newSupportRequestCtrl.submit();

            List<Support_Request__c> supportRequestList = [SELECT Id, Status__c, Stage__c, Refresh_Case__c FROM Support_Request__c LIMIT 1];
            System.assertEquals(1, supportRequestList.size());
            supportRequestList[0].Status__c = 'Closed';
            Database.update (supportRequestList);

            ApexPages.StandardController sc = new ApexPages.StandardController(supportRequestList[0]);
            RefreshSubtabPageExt refreshPageExt = new RefreshSubtabPageExt(sc);
            
            PageReference pageRef = Page.RefreshSubtabPage;
            pageRef.getParameters().put('id', String.valueOf(supportRequestList[0].Id));
            Test.setCurrentPage(pageRef);

            refreshPageExt.updateSupporReq();
        Test.stopTest();
    }
}