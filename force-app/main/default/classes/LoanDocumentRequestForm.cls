/*
Author: Original: Srikanth Vivaram
        Last Modified: Jesfer Baculod (07/19/2017)
History: 07/18/2017 - Replaced error/info messages with Custom Labels, fixed some issues
         07/19/2017 - Include LPI Amount in LDR form - Requested by Tim Wells
                    - Clicking Submit will update Opp's Stage to 'Loan Docs Requested' - Requested by Jason Varischetti
                    - Added validation on Internal Customer Rating and Base Rate when Loan Type is not Personal Loan
         07/20/2017 - Added Product - Requested by Tim Wells
         08/01/2017 - Updated Vendor Details mapping
         08/15/2017 - Fixed Loan Amount Validation Issue
Purpose: This Visualforce page will create Loan Document Request Form and save it on Opportunity record.
*/

public with sharing class LoanDocumentRequestForm {

// Property Declaration

    public Opportunity oppObj {get;set;}
    public Id oppId {get;set;}
    public Id loanobjID {get;set;}
    //public Account accObj {get;set;}
    public Loan_Document_Request_Form__c loanObj {get;set;}
    public String notesToLender {get;set;}
    public String notesToSupport {get;set;}
    public Boolean payoutValidMin5Days {get;set;}
    public Boolean payoutLetterInFile {get;set;}
    public Boolean receivedInsuranceWaiver {get;set;}
    public String insurancePackage {get;set;}
    public String insurancePackageNotesCOMPULSORY {get;set;}
    public Boolean figuresAreCorrect {get;set;}
    public boolean vendorDetailsCorrect {get;set;}
    public Decimal brokerage {get;set;}
//    public Decimal applicationFee {get;set;}
    //public Decimal trail {get;set;}
    public String gapProvider {get;set;}
    public Decimal gapPremium {get;set;}
    public string gapProduct {get;set;}
    public String lpiCoverOption {get;set;}
    public String lpiCoverType {get; set;}
    public String ltiCoverOption {get; set;}
    public String ltiCoverType {get; set;}
    public Decimal lpiamount {get; set;}
    public String warrantyProvider {get;set;}
    public String warrantyProductName {get;set;}
    public String warrantyTerm {get;set;}
    public Decimal warrantyPremium {get;set;}
    private String str_opp_query {get;set;}
    
// Variable Declaration 
    public boolean saveSuccess {get;set;}
    public Boolean errorMessages;    

    private final string OPP_STAGE_LOANDOCSREQ = Label.Loan_Document_Request_Form_Set_Opp_Stage; //Loan Docs Requested

    //Constructor 
    public LoanDocumentRequestForm(ApexPages.StandardController con) {
        oppId = con.getRecord().Id;
        saveSuccess = false;
        errorMessages = true;
        oppObj = new Opportunity();

        //Get all fields from Opportunity
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Opportunity.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
        str_opp_query = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues){
           // Continue building your dynamic query string
           str_opp_query += s.getDescribe().getName() + ',';
        }
        //str_cse_query = str_cse_query.subString(0, str_cse_query.length() - 1); // Trim last comma
        str_opp_query+= ' Dealer_Vendor__r.Name, Dealer_Vendor__r.Business_Phone_Number__c, Dealer_Vendor__r.Address_Line_1__c, Dealer_Vendor__r.Suburb__c, Dealer_Vendor__r.Postcode__c, Dealer_Vendor__r.State__c, Dealer_Contact__r.Name, Dealer_Contact__r.Email, Dealer_Contact__r.MobilePhone, Vendor_Contact__r.Name, Vendor_Contact__r.Email, Vendor_Contact__r.MobilePhone, Vendor_Contact__r.Phone, Vendor_Contact__r.Street_Number__c, Vendor_Contact__r.Street_Type__c, Vendor_Contact__r.Postcode__c, Vendor_Contact__r.State__c, Vendor_Contact__r.Country__c, Vendor_Contact__r.Suburb__c';
        str_opp_query+= ' FROM Opportunity Where Id =: oppId';
        //Retrieve current Opportunity
        oppObj = database.query(str_opp_query);
        loanObj = new Loan_Document_Request_Form__c();
    }

    public PageReference refreshOppDetails(){
        //Mainly refershing Vendor Details and other opp data
        oppObj = database.query(str_opp_query);
        return null;
    }

    
    public List<SelectOption> getinsurancePackageSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from Insurance Package picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.Insurance_Package__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
  
    public List<SelectOption> getgapProviderSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from GAP Provide picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.GAP_Provider__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public List<SelectOption> getlipCoverOptionSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LPI Cover Option picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.LPI_Cover_Option__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public List<SelectOption> getlpiCoverTypeSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LPI Cover Type picklist\
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.LPI_Cover_Type__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }

    public List<SelectOption> getltiCoverOptionSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LTI Cover Option picklist\
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.LTI_Cover_Option__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    } 

    public List<SelectOption> getltiCoverTypeSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LTI Cover Type picklist\
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.LTI_Cover_Type__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    } 
    
    public List<SelectOption> getwarrantyProvidersp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from Warranty Provider Type picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.Warranty_Provider__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
    public List<SelectOption> getwarrantyTermSp(){
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('',''));
        //Retrieve picklist values from LPI Cover Type picklist
        Schema.DescribeFieldResult fieldResult = Loan_Document_Request_Form__c.Warranty_Term__c.getDescribe();
        List<Schema.PicklistEntry> pick_list_values = fieldResult.getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { 
          opt.add(new SelectOption(a.getValue(),a.getLabel()));
        }
        return opt;
    }
    
// public Loan_Document_Request_Form__c loanObj{get;set;}
    
    public PageReference validateLdr(){

        string curProfileId = UserInfo.getProfileId();
        
        //Validate these fields as these are needed when updating Stage to Loan Docs Requested
        if(oppObj.Loan_Type2__c != 'Personal Loan' && !curProfileId.contains(Label.Profile_ID_Used_Cars_Adelaide) && !curProfileId.contains(Label.Profile_ID_Sales_BDM)  ){             
            if(oppObj.Internal__c == null){ //Insurance Rating
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_34));
            }
            if(oppObj.Base_Rate__c == null){    
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_35));
            }
        }
        if (oppObj.Lender__c == 'ANZ'){
            if (oppObj.Residential_Reference_Completed__c == false){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_36 + ' on Opportunity.'));
            }
            if (oppObj.Employment_Reference_Completed__c == false){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_37 + ' on Opportunity.'));
            }
        }

        //********************************* Docs Request Checklist *****************************************

        if(oppObj.Approval_Conditions__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_1));
        }
        if(notesToLender == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_2)); 
        }
        if(notesToSupport == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_3));
        }
        if(receivedInsuranceWaiver == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_4));
        }
        if(insurancePackage == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_5));
        }
        if(insurancePackageNotesCOMPULSORY == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_6));
        }
        
        //********************************* Finance Details ************************************************
        
        if(oppObj.Payout__c >0 && oppObj.Payout_To__c == ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_7));
        }
        if(oppObj.Payout__c >1 && payoutValidMin5Days == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_8));
        }
        if(oppObj.Payout__c>1 && payoutLetterInFile == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_9));
        }
        if(oppObj.Lender__c == 'ANZ' && (LoanObj.Brokerage__c == null || LoanObj.Brokerage__c == 0 )){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_10));
        }
        if(oppObj.Lender__c == 'Liberty' || oppObj.Lender__c == 'Macquarie' || oppObj.Lender__c == 'Pepper'){
            if(oppObj.Trail__c == null || oppObj.Trail__c == 0 ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_11));
            }
        }
        if(oppObj.Loan_Rate__c == null || oppObj.Loan_Rate__c == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_12));
        }
        if(oppObj.Application_Fee__c == null || oppObj.Application_Fee__c == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_13));
        }
        if(figuresAreCorrect == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_14));
        }
        if(oppObj.loan_Type2__c == 'Truck Loan' && (oppObj.Gross_Vehicle_Mass__c == null || oppObj.Gross_Vehicle_Mass__c == 0)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_15));
        }
        if(oppObj.Loan_Type2__c != 'Personal Loan' && oppObj.Vehicle_Purchase_Price__c  != null){
            if (oppObj.Available_Deposit__c == null) oppObj.Available_Deposit__c = 0;
            if (oppObj.Net_Trade_In__c == null) oppObj.Net_Trade_In__c = 0;
            if (oppObj.Payout__c == null) oppObj.Payout__c = 0;
            oppObj.Loan_Amount__c = ((oppObj.Vehicle_Purchase_Price__c - oppObj.Available_Deposit__c - oppObj.Net_Trade_In__c) + (oppObj.Payout__c));
        }
        

        //****************************** Insurance Details *********************************

        if(gapProvider == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_16));
        }
        if(gapProvider != '-- no Cover --' && gapProvider != null){
            if(gapPremium == null || gapPremium == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_17));
            }
        }
        if(lpiCoverOption == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_18));
        }
        if(lpiCoverOption != '-- no Cover --' && lpiCoverOption != null){
            if(lpiCoverType == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_19));
            }
        }
        if(warrantyProvider == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_20));
        }
        if(warrantyProvider != '-- no Cover --' && warrantyProvider != null){
            if(warrantyProductName == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_21));
            }
        }
        if(warrantyProvider != '-- no Cover --' && warrantyProvider != null){
            if(warrantyTerm == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_22));
            }
        }
        if(warrantyProvider != '-- no Cover --' && warrantyProvider != null){
            if(warrantyPremium == null || warrantyPremium == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_23));
            }
        }

        //************************* Vendor Details ***********************************
        
        if(oppObj.Loan_Type2__c != 'Personal Loan'){
            if (vendorDetailsCorrect == false){
                ApexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_38));
            }
            /*
            if(oppObj.Dealer_Contact_Person__c == null){ //oppObj.Sale_Type__c == 'Dealer' && 
                ApexPages.addMessage(new apexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_24));
            }
            if(oppObj.Dealer_Name__c == null || oppObj.Dealer_Name__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_25));
            }
            if(oppObj.Dealer_Phone__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_28));
            } 
            if(oppObj.Dealer_Address__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_26));
            }
            if(oppObj.Dealer_Email__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_27));
            }
            */
        } 

        //********************************* Primary Asset Details Validations ***************************************    

        if(oppObj.Loan_Type2__c != 'Personal Loan'){
            if(oppObj.Vin__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_29));
            }
            if(oppObj.Vehicle_Type__c == 'Motor Vehicle' || oppObj.Vehicle_Type__c == 'Caravan' || oppObj.Vehicle_Type__c == 'Motor Cycle'){    
                System.debug('vinLength===========>'+oppObj.Vin__c.length());
                if( oppObj.Vin__c != null && oppObj.Vin__c.Length() <17){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'VIN length is'+' '+oppObj.Vin__c.Length()+','+' '+'It should be 17 characters long'));
                }
            }
            if(oppObj.Vehicle_Type__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_30));
            }
            if(oppObj.Year__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_31));
            }
            if(oppObj.Make__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_32));
            }
            if(oppObj.Model__c == ''){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Loan_Document_Request_Form_Err_Msg_33));
            }
        }

        //************************** Final check for Error Messages *******************************     
        
        if(!ApexPages.hasMessages()){
            errorMessages = ApexPages.hasMessages();
            System.debug('errorMessages==============>'+errorMessages);
            try{
                update oppObj;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.Loan_Document_Request_Form_Info_Msg_1));
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getLineNumber()+' '+e.getMessage()));   
            }
        }
        return null;
    }
        
    //Save Updates on Opportunity
    public PageReference saveUpdates(){
        Savepoint sp = Database.setSavepoint();
        try{
            update oppObj;    
        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getLineNumber()+ ' ' + e.getMessage()));
        }
        return null;
    }
       
    public PageReference submit(){
        Savepoint sp = Database.setSavepoint();
        try{
            if(errorMessages == true){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.Loan_Document_Request_Form_Info_Msg_2));
                return null;
            }else{ 
                //create a new Loan Document Request Form with Opportunity field mapping
                Loan_Document_Request_Form__c creloanObj = new Loan_Document_Request_Form__c(
                        Client_Name__c = oppObj.Id,
                        Application_Type__c = oppObj.Application_Type__c,
                        // Docs Request Checklist
                        Approval_Conditions_Notes__c = oppObj.Approval_Conditions__c,
                        Notes_to_Lender__c = notesToLender,
                        Notes_To_Support__c = notesToSupport,
                        Payout_Valid_min_5_days__c = payoutValidMin5Days,
                        Payout_Letter_in_File__c = payoutLetterInFile,
                        Received_Insurance_Waiver__c = receivedInsuranceWaiver,
                        Insurance_Package__c = insurancePackage,
                        Insurance_Package_Notes_COMPULSORY__c = insurancePackageNotesCOMPULSORY,
                        // Finance Details   
                        Vehicle_Price__c = oppObj.Vehicle_Purchase_Price__c,
                        Opp_Loan_Type__c = oppObj.Loan_Type2__c,
                        Loan_Term2__c = oppObj.Loan_Term__c,
                        Cash_Deposit__c = oppObj.Available_Deposit__c,
                        Interest_Rate__c = oppObj.Loan_Rate__c,
                        Balloon_N__c = oppObj.Balloon__c,
                        Brokerage__c = loanObj.Brokerage__c,
                        Trade_In_Amount__c = oppObj.Net_Trade_In__c,
                        App_Fee__c = oppObj.Application_Fee__c,
                        Trade_In_Description__c = oppObj.Trade_In_Description__c,
                        Trail_N__c = oppObj.Trail__c,
                        Payout__c = oppObj.Payout__c,
                        Payout_To__c = oppObj.Payout_To__c,
                        Required_Loan_Amount__c = oppObj.Loan_Amount__c,
                        Figures_are_correct__c = figuresAreCorrect,
                        // Insurance Details:
                        GAP_Provider__c = gapProvider,
                        GAP_Premium__c = gapPremium,
                        Product__c = LoanObj.Product__c, //added 7/20/17 requested by Tim Wells
                        LPI_Cover_Option__c = lpiCoverOption,
                        LPI_Cover_Type__c = lpiCoverType,
                        LPI_Amount__c = lpiamount, //added 7/19/17 requested by Tim Wells
                        LTI_Cover_Option__c = ltiCoverOption, //added 11/24/17 requested by Jordan Mutton
                        LTI_Cover_Type__c = ltiCoverType, //added 11/24/17 requested by Jordan Mutton
                        Warranty_Provider__c = warrantyProvider,
                        Warranty_Product_Name__c = warrantyProductName,
                        Warranty_Term__c = warrantyTerm,
                        Warranty_Premium__c = warrantyPremium,
                        // Vendor Details
                        Sale_Type__c = oppObj.Sale_Type__c,
                        Vendor_Phone__c = oppObj.Dealer_Vendor__r.Business_Phone_Number__c,
                        Dealer_Contact_Person__c = oppObj.Dealer_Contact__r.Name,
                        Vendor_Name__c = oppObj.Dealer_Vendor__r.Name,
                        Dealer_Contact_Mobile__c = oppObj.Dealer_Contact__r.MobilePhone,
                        Vendor_Address__c = oppObj.Dealer_Vendor__r.Address_Line_1__c + ' ' + oppObj.Dealer_Vendor__r.Suburb__c + ' ' + oppObj.Dealer_Vendor__r.Postcode__c + ' ' + oppObj.Dealer_Vendor__r.State__c + ' ' + oppObj.Dealer_Vendor__r.State__c,
                        Vendor_Email__c = oppObj.Dealer_Contact__r.Email,
                        /* Sale_Type__c = oppObj.Sale_Type__c,
                        Vendor_Phone__c = oppObj.Dealer_Phone__c,
                        Dealer_Contact_Person__c = oppObj.Dealer_Contact_Person__c,
                        Vendor_Name__c = oppObj.Dealer_Name__c,
                        Dealer_Contact_Mobile__c = oppObj.Dealer_Mobile__c,
                        Vendor_Address__c = oppObj.Dealer_Address__c,
                        Vendor_Email__c = oppObj.Dealer_Email__c, */
                        //Primary Asset Details
                        Asset_Type__c = oppObj.Vehicle_Type__c,
                        Transmission__c = oppObj.Transmission__c,
                        Year__c = oppObj.Year__c,
                        Body_Type__c = oppObj.Body_Type__c,
                        Make__c = oppObj.Make__c,
                        Colour__c = oppObj.Colour__c,
                        Model__c = oppObj.Model__c,
                        Fuel_Type__c = oppObj.Fuel_Type__c,
                        Variant__c = oppObj.Varient__c,
                        Odometer__c = oppObj.Odometer__c,
                        Vin__c = oppObj.Vin__c,
                        Date_First_Registered__c = oppObj.Date_First_Registered__c,
                        Engine_No__c = oppObj.Engine_No__c,
                        Vehicle_Registration_Number__c = oppObj.Vehicle_Registration_Number__c,
                        Gross_Vehicle_Mass__c = oppObj.Gross_Vehicle_Mass__c,
                        Vehicle_Registration_State__c = oppObj.Vehicle_Registration_State__c
                    );
                //loanObj.Loan_Type__c = oppObj.Loan_Product__c;
                if (oppObj.Sale_Type__c == 'Private'){
                    creloanObj.Vendor_Email__c = oppObj.Vendor_Contact__r.Email;
                    creloanObj.Dealer_contact_Person__c = oppObj.Vendor_Contact__r.Name;
                    creloanObj.Dealer_Contact_Mobile__c = oppObj.Vendor_Contact__r.MobilePhone;
                    creloanObj.Vendor_Phone__c = oppObj.Vendor_Contact__r.Phone;
                    creloanObj.Vendor_Name__c = oppObj.Vendor_Contact__r.Name;
                    creloanObj.Vendor_Address__c = oppObj.Vendor_Contact__r.Street_Number__c + ' ' + oppObj.Vendor_Contact__r.Street_Type__c + oppObj.Vendor_Contact__r.Suburb__c + ' ' + oppObj.Vendor_Contact__r.Postcode__c + ' ' + oppObj.Vendor_Contact__r.State__c + ' ' + oppObj.Vendor_Contact__r.Country__c;
                }
                insert creloanObj;     
                loanobjID = creloanObj.Id;
                system.debug('@@creloanObj:'+creloanObj);

                //Update Stage to Loan Docs Requested
                oppObj.StageName = OPP_STAGE_LOANDOCSREQ;
                update oppObj;
                saveSuccess = true;

                //redirect page to created LDR
                //return new PageReference('/'+creloanObj.Id);
                return null;
            }
        }
        catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getLineNumber()+ ' ' + e.getMessage()));
            return null;
        }

    }
    
    public PageReference oppOrtunityId(){
        return new PageReference('/'+oppId);
    }
    
}