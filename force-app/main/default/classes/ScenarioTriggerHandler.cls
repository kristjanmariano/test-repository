/*
	@Description: handler class of ScenarioTrigger
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	11/13/2017 - Created
		-	11/14/2017 - Updated, added calculation of SLA2 and SLA3
		-	11/24/2017 - Updated, due to issue with Trigger Settings
*/
public class ScenarioTriggerHandler {
	
	static Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

	public static boolean firstRun = true; //prevents trigger to run multiple times

	private static final string SCEN_STAGE_NEW = Label.Scenario_Stage_New; //New
	private static final string SCEN_STAGE_WINTRODUCER = Label.Scenario_Stage_With_Introducer; //With Introducer
	private static final string SCEN_STAGE_WLENDER = Label.Scenario_Stage_With_lender; //With Lender
	private static final string SCEN_STAGE_COMPLETE = Label.Scenario_Stage_Complete; //Complete
	private static final string SCEN_STAGE_LOST = Label.Scenario_Stage_Lost; //Lost

	//execute all Before Update event triggers
	public static void onBeforeInsert(list <Complex_Scenario__c> newScenlist){
		if(trigSet.Enable_Scenario_recordStageTimestamps__c) recordStageTimestamps(null,newScenlist);
		if(trigSet.Enable_Scenario_calculateSLATime__c) calculateSLATimeOnInsert(newScenlist);
	}


	//execute all Before Update event triggers
	public static void onBeforeUpdate(map<Id,Complex_Scenario__c> oldScenMap, map <Id,Complex_Scenario__c> newScenMap){
		list <Complex_Scenario__c> newScenList = newScenMap.values();
		if(trigSet.Enable_Scenario_recordStageTimestamps__c) recordStageTimestamps(oldScenMap,newScenList);
		if(trigSet.Enable_Scenario_calculateSLATime__c) calculateSLATime(oldScenMap,newScenList);
	}

	/* @Description: method for recording Time Stamps on Stage updated.
	   @Author: Jesfer Baculod - Positive Group
	*/
	private static void recordStageTimestamps(map<Id,Complex_Scenario__c> oldScenMap, list <Complex_Scenario__c> newScenlist){
		for (Complex_Scenario__c scen : newScenlist){ //Only update if timestamp field is empty
			boolean isChanged = false;
			if (oldScenMap != null){
				if (scen.Stage__c != oldScenMap.get(scen.Id).Stage__c) isChanged = true;
			}
			else isChanged = true; //isInsert
			if (isChanged){
				if (scen.Stage__c == SCEN_STAGE_NEW) scen.Stage_New_DateTime__c = system.now(); //New stage timestamp
				else if (scen.Stage__c == SCEN_STAGE_WINTRODUCER) scen.Stage_With_Introducer_DateTime__c = system.now(); //With Introducer stage timestamp
				else if (scen.Stage__c == SCEN_STAGE_WLENDER) scen.Stage_With_Lender_DateTime__c = system.now(); //With Lender stage timestamp
				else if (scen.Stage__c == SCEN_STAGE_COMPLETE) scen.Stage_Complete_DateTime__c = system.now(); //Complete stage timestamp
				else if (scen.Stage__c == SCEN_STAGE_LOST) scen.Stage_Lost_DateTime__c = system.now(); //Lost stage timestamp
			}
		}
	}

	private static void calculateSLATimeOnInsert(list <Complex_Scenario__c> newScenlist){

		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		for (Complex_Scenario__c scen : newScenlist){
			//Populate SLA - 1 Start Time and Start Count on Scenario create
			if (scen.Stage__c == SCEN_STAGE_NEW){
				//SLA 1
				scen.SLA_Time_1_Start__c = system.now(); 
				scen.SLA_Started_Count_1__c = 1;
				scen.SLA_Warning_Time_1__c = BusinessHours.add(bh.Id, scen.SLA_Time_1_Start__c, 5400000); //Set 1st Warning of current SLA (add 90 minutes)
				DateTime warningtimeIR = scen.SLA_Warning_Time_1__c;
				scen.SLA_Warning_Time_1__c = Datetime.newInstance(warningtimeIR.year(), warningtimeIR.month(), warningtimeIR.day(), warningtimeIR.hour(), warningtimeIR.minute(), 0);
				scen.SLA_Active_1__c = 'Yes'; //Set current SLA to Active                
                scen.SLA_Time_1_mm__c = 0; 
			}
		}


	}


	/* @Description: method for calculating, counting SLA Time, SLA Met and logging SLA History. Created in trigger to follow execution order on SLAs and include Business Hours/holidays
	   @Author: Jesfer Baculod - Positive Group
	*/
	private static void calculateSLATime(map<Id,Complex_Scenario__c> oldScenMap, list <Complex_Scenario__c> newScenList){

		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 
		//For logging timestamp on SLA History
		DateTime dnow = system.now();
		string strDnow = dnow.format();

		for (Complex_Scenario__c scen : newScenlist){

			//------------------------------------------------- SLA 1 ------------------------------------------------------------------------

			//SLA - Start - Scenario Actioned
			if (scen.Stage__c == SCEN_STAGE_NEW && oldScenMap.get(scen.Id).Stage__c != scen.Stage__c ) {
				if (oldScenMap.get(scen.Id).Stage_New_DateTime__c != scen.Stage_New_DateTime__c){ //Stage was changed to New
					if (scen.Stage_New_DateTime__c != null){	
						scen.SLA_Time_1_Start__c = scen.Stage_New_DateTime__c;
					}

				}
			}
			if (scen.SLA_Time_1_Start__c != null){
				if (oldScenMap.get(scen.Id).SLA_Time_1_Start__c != scen.SLA_Time_1_Start__c ){
					scen.SLA_Warning_Time_1__c = BusinessHours.add(bh.Id, scen.SLA_Time_1_Start__c, 5400000); //Set 1st warning of current SLA (add 90 minutes)
					DateTime warningtimeIR = scen.SLA_Warning_Time_1__c;
					scen.SLA_Warning_Time_1__c = Datetime.newInstance(warningtimeIR.year(), warningtimeIR.month(), warningtimeIR.day(), warningtimeIR.hour(), warningtimeIR.minute(), 0);
                    scen.SLA_Active_1__c = 'Yes'; //Set current SLA to Active
					scen.SLA_Time_1_End__c = null; //resets End time
					scen.SLA_Met_1__c = false; //resets Met check
					scen.SLA_Time_1_HH__c = null; //reset Hour Duration
					scen.SLA_Time_1_mm__c = 0; //reset SLA Age in minutes
					if (scen.SLA_Started_Count_1__c == null) scen.SLA_Started_Count_1__c = 0;
					scen.SLA_Started_Count_1__c+= 1;
					system.debug('@@0a');
				}
			}

			//SLA - End - Scenario Actioned
			if (oldScenMap.get(scen.Id).SLA_Time_1_Start__c == scen.SLA_Time_1_Start__c && scen.SLA_Time_1_Start__c != null){ //Start time should remain unchanged when on updating End Time

				if (oldScenMap.get(scen.Id).Stage_With_Introducer_DateTime__c != scen.Stage_With_Introducer_DateTime__c){ //Stage was changed to With Introducer
					if (scen.Stage_With_Introducer_DateTime__c != null) scen.SLA_Time_1_End__c = scen.Stage_With_Introducer_DateTime__c;
				}
				else if (oldScenMap.get(scen.Id).Stage_Complete_DateTime__c != scen.Stage_Complete_DateTime__c){ //Stage was changed to Complete
					if (scen.Stage_Complete_DateTime__c != null) scen.SLA_Time_1_End__c = scen.Stage_Complete_DateTime__c;
				}
				if (scen.SLA_Time_1_End__c != null && (scen.SLA_Started_Count_1__c > scen.SLA_Completed_Count_1__c || scen.SLA_Completed_Count_1__c == null) ){
					if (oldScenMap.get(scen.Id).SLA_Time_1_End__c != scen.SLA_Time_1_End__c){ //Scenario Actioned SLA End Time got changed
						if (scen.SLA_Completed_Count_1__c == null) scen.SLA_Completed_Count_1__c = 0;
						scen.SLA_Completed_Count_1__c+= 1;
                        scen.SLA_Active_1__c = 'No'; //Set current SLA to Inactive
						scen.SLA_Warning_Time_1__c = null;
					}
				}
				system.debug('@@0b');
			}

			//Calculate Scenario Actioned SLA Time in Hours
			if (scen.SLA_Time_1_Start__c  != null && scen.SLA_Time_1_End__c != null){
				if (oldScenMap.get(scen.Id).SLA_Completed_Count_1__c != scen.SLA_Completed_Count_1__c){ //Scenario Actioned Duration Time got changed
					scen.SLA_Time_1_HH__c = Decimal.valueof(BusinessHours.diff(bh.Id, scen.SLA_Time_1_Start__c, scen.SLA_Time_1_End__c)) / (1000*60*60);
					if (scen.SLA_Met_Count_1__c == null) scen.SLA_Met_Count_1__c = 0;
					if (scen.SLA_Time_1_HH__c <= 2){ //Start - End Time Duration <= 2 Hours
						scen.SLA_Met_1__c = true;
						//Count Met
						scen.SLA_Met_Count_1__c+= 1;
						//Update SLA History
						string newhistory = 'Scenario Actioned SLA Met. Time was ' + strDnow;
						if (scen.SLA_History__c != '' && scen.SLA_History__c != null) scen.SLA_History__c += '\n' + newhistory;
						else scen.SLA_History__c = newhistory;
					}
					else{ 
						scen.SLA_Met_1__c = false;
						//Update SLA History
						string newhistory = 'Scenario Actioned SLA Missed. Time was ' + strDnow;
						if (scen.SLA_History__c != '' && scen.SLA_History__c != null) scen.SLA_History__c += '\n' + newhistory;
						else scen.SLA_History__c = newhistory;
					}
					system.debug('@@0c');
				}
			}

			//------------------------------------------------- SLA 2 ------------------------------------------------------------------------

			//SLA - Start - With Introducer
			if (scen.Stage__c == SCEN_STAGE_WINTRODUCER && oldScenMap.get(scen.Id).Stage__c != scen.Stage__c ) {
				if (oldScenMap.get(scen.Id).Stage_With_Introducer_DateTime__c != scen.Stage_With_Introducer_DateTime__c){ //Stage was changed to With Introducer
					if (scen.Stage_With_Introducer_DateTime__c != null){	
						scen.SLA_Time_2_Start__c = scen.Stage_With_Introducer_DateTime__c;
					}

				}
			}
			if (scen.SLA_Time_2_Start__c != null){
				if (oldScenMap.get(scen.Id).SLA_Time_2_Start__c != scen.SLA_Time_2_Start__c ){
					scen.SLA_Warning_Time_2__c = BusinessHours.add(bh.Id, scen.SLA_Time_2_Start__c, 86400000); //Set 1st warning of current SLA (add 1440 minutes / 24 hours)
					DateTime warningtimeIR = scen.SLA_Warning_Time_2__c;
					scen.SLA_Warning_Time_2__c = Datetime.newInstance(warningtimeIR.year(), warningtimeIR.month(), warningtimeIR.day(), warningtimeIR.hour(), warningtimeIR.minute(), 0);
                    scen.SLA_Active_2__c = 'Yes'; //Set current SLA to Active
					scen.SLA_Time_2_End__c = null; //resets End time
					scen.SLA_Met_2__c = false; //resets Met check
					scen.SLA_Time_2_HH__c = null; //reset Hour Duration
					scen.SLA_Time_2_mm__c = 0; //reset SLA Age in minutes
					if (scen.SLA_Started_Count_2__c == null) scen.SLA_Started_Count_2__c = 0;
					scen.SLA_Started_Count_2__c+= 1;
					system.debug('@@0a');
				}
			}

			//SLA - End - With Introducer
			if (oldScenMap.get(scen.Id).SLA_Time_2_Start__c == scen.SLA_Time_2_Start__c && scen.SLA_Time_2_Start__c != null){ //Start time should remain unchanged when on updating End Time

				if (scen.Stage__c == SCEN_STAGE_LOST && oldScenMap.get(scen.Id).Stage__c != scen.Stage__c ) {
					if (scen.SLA_Completed_Count_2__c == null) scen.SLA_Completed_Count_2__c = 0;
					else scen.SLA_Completed_Count_2__c+= 1; 
                    scen.SLA_Active_2__c = 'No'; //Set current SLA to Inactive
					scen.SLA_Warning_Time_2__c = null;
				}

				if (oldScenMap.get(scen.Id).Stage_With_Lender_DateTime__c != scen.Stage_With_Lender_DateTime__c){ //Stage was changed to With Lender
					if (scen.Stage_With_Lender_DateTime__c != null) scen.SLA_Time_2_End__c = scen.Stage_With_Lender_DateTime__c;
				}
				else if (oldScenMap.get(scen.Id).Stage_Complete_DateTime__c != scen.Stage_Complete_DateTime__c){ //Stage was changed to Complete
					if (scen.Stage_Complete_DateTime__c != null) scen.SLA_Time_2_End__c = scen.Stage_Complete_DateTime__c;
				}
				if (scen.SLA_Time_2_End__c != null && (scen.SLA_Started_Count_2__c > scen.SLA_Completed_Count_2__c || scen.SLA_Completed_Count_2__c == null) ){
					if (oldScenMap.get(scen.Id).SLA_Time_2_End__c != scen.SLA_Time_2_End__c){ //Scenario Actioned SLA End Time got changed
						if (scen.SLA_Completed_Count_2__c == null) scen.SLA_Completed_Count_2__c = 0;
						scen.SLA_Completed_Count_2__c+= 1;
                        scen.SLA_Active_2__c = 'No'; //Set current SLA to Inactive
						scen.SLA_Warning_Time_2__c = null;
					}
				}
				system.debug('@@0b');
			}

			//Calculate With Introducer SLA Time in Hours
			if (scen.SLA_Time_2_Start__c  != null && scen.SLA_Time_2_End__c != null){
				if (oldScenMap.get(scen.Id).SLA_Completed_Count_2__c != scen.SLA_Completed_Count_2__c){ //With Introducer Duration Time got changed
					scen.SLA_Time_2_HH__c = Decimal.valueof(BusinessHours.diff(bh.Id, scen.SLA_Time_2_Start__c, scen.SLA_Time_2_End__c)) / (1000*60*60);
					if (scen.SLA_Met_Count_2__c == null) scen.SLA_Met_Count_2__c = 0;
					if (scen.SLA_Time_2_HH__c <= 76){ //Start - End Time Duration <= 76 Hours
						scen.SLA_Met_2__c = true;
						//Count Met
						scen.SLA_Met_Count_2__c+= 1;
						//Update SLA History
						string newhistory = 'With Introducer SLA Met. Time was ' + strDnow;
						if (scen.SLA_History__c != '' && scen.SLA_History__c != null) scen.SLA_History__c += '\n' + newhistory;
						else scen.SLA_History__c = newhistory;
					}
					else{ 
						scen.SLA_Met_2__c = false;
						//Update SLA History
						string newhistory = 'With Introducer SLA Missed. Time was ' + strDnow;
						if (scen.SLA_History__c != '' && scen.SLA_History__c != null) scen.SLA_History__c += '\n' + newhistory;
						else scen.SLA_History__c = newhistory;
					}
					system.debug('@@0c');
				}
			}

			//------------------------------------------------- SLA 3 ------------------------------------------------------------------------

			//SLA - Start - With Lender
			if (scen.Stage__c == SCEN_STAGE_WLENDER && oldScenMap.get(scen.Id).Stage__c != scen.Stage__c ) {
				if (oldScenMap.get(scen.Id).Stage_With_Lender_DateTime__c != scen.Stage_With_Lender_DateTime__c){ //Stage was changed to With Lender
					if (scen.Stage_With_Lender_DateTime__c != null){	
						scen.SLA_Time_3_Start__c = scen.Stage_With_Lender_DateTime__c;
					}

				}
			}
			if (scen.SLA_Time_3_Start__c != null){
				if (oldScenMap.get(scen.Id).SLA_Time_3_Start__c != scen.SLA_Time_3_Start__c ){
					scen.SLA_Warning_Time_3__c = BusinessHours.add(bh.Id, scen.SLA_Time_3_Start__c, 7200000); //Set 1st warning of current SLA (add 120 minutes)
					DateTime warningtimeIR = scen.SLA_Warning_Time_3__c;
					scen.SLA_Warning_Time_3__c = Datetime.newInstance(warningtimeIR.year(), warningtimeIR.month(), warningtimeIR.day(), warningtimeIR.hour(), warningtimeIR.minute(), 0);
                    scen.SLA_Active_3__c = 'Yes'; //Set current SLA to Active
					scen.SLA_Time_3_End__c = null; //resets End time
					scen.SLA_Met_3__c = false; //resets Met check
					scen.SLA_Time_3_HH__c = null; //reset Hour Duration
					scen.SLA_Time_3_mm__c = 0; //reset SLA Age in minutes
					if (scen.SLA_Started_Count_3__c == null) scen.SLA_Started_Count_3__c = 0;
					scen.SLA_Started_Count_3__c+= 1;
					system.debug('@@0a');
				}
			}

			//SLA - End - With Lender
			if (oldScenMap.get(scen.Id).SLA_Time_3_Start__c == scen.SLA_Time_3_Start__c && scen.SLA_Time_3_Start__c != null){ //Start time should remain unchanged when on updating End Time

				if (oldScenMap.get(scen.Id).Stage_With_Introducer_DateTime__c != scen.Stage_With_Introducer_DateTime__c){ //Stage was changed to With Introducer
					if (scen.Stage_With_Introducer_DateTime__c != null) scen.SLA_Time_3_End__c = scen.Stage_With_Introducer_DateTime__c;
				}
				else if (oldScenMap.get(scen.Id).Stage_Complete_DateTime__c != scen.Stage_Complete_DateTime__c){ //Stage was changed to Complete
					if (scen.Stage_Complete_DateTime__c != null) scen.SLA_Time_3_End__c = scen.Stage_Complete_DateTime__c;
				}
				if (scen.SLA_Time_3_End__c != null && (scen.SLA_Started_Count_3__c > scen.SLA_Completed_Count_3__c || scen.SLA_Completed_Count_3__c == null) ){
					if (oldScenMap.get(scen.Id).SLA_Time_3_End__c != scen.SLA_Time_3_End__c){ //Scenario Actioned SLA End Time got changed
						if (scen.SLA_Completed_Count_3__c == null) scen.SLA_Completed_Count_3__c = 0;
						scen.SLA_Completed_Count_3__c+= 1;
                        scen.SLA_Active_3__c = 'No'; //Set current SLA to Inactive
						scen.SLA_Warning_Time_3__c = null;
					}
				}
				system.debug('@@0b');
			}

			//Calculate With Lender SLA Time in Hours
			if (scen.SLA_Time_3_Start__c  != null && scen.SLA_Time_3_End__c != null){
				if (oldScenMap.get(scen.Id).SLA_Completed_Count_3__c != scen.SLA_Completed_Count_3__c){ //With Lender Duration Time got changed
					scen.SLA_Time_3_HH__c = Decimal.valueof(BusinessHours.diff(bh.Id, scen.SLA_Time_3_Start__c, scen.SLA_Time_3_End__c)) / (1000*60*60);
					if (scen.SLA_Met_Count_3__c == null) scen.SLA_Met_Count_3__c = 0;
					if (scen.SLA_Time_3_HH__c <= 4){ //Start - End Time Duration <= 4 Hours
						scen.SLA_Met_3__c = true;
						//Count Met
						scen.SLA_Met_Count_3__c+= 1;
						//Update SLA History
						string newhistory = 'With Lender SLA Met. Time was ' + strDnow;
						if (scen.SLA_History__c != '' && scen.SLA_History__c != null) scen.SLA_History__c += '\n' + newhistory;
						else scen.SLA_History__c = newhistory;
					}
					else{ 
						scen.SLA_Met_3__c = false;
						//Update SLA History
						string newhistory = 'With Lender SLA Missed. Time was ' + strDnow;
						if (scen.SLA_History__c != '' && scen.SLA_History__c != null) scen.SLA_History__c += '\n' + newhistory;
						else scen.SLA_History__c = newhistory;
					}
					system.debug('@@0c');
				}
			}

		}


	}




}