/** 
* @FileName: IncomeTriggerTest
* @Description: Test class for Asset Trigger 
* @Copyright: Positive (c) 2018 
* @author: Rex David
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 2/19/18 RDAVID Created class (setupData,testIncomeToRole1,testIncomeToRole2)
* 2.0 4/25/18 JBACULOD Removed existing test setup and test methods, added testautoCreateFOShareIncome
* 3.0 8/10/19 JBACULOD added testsetLeadIncomeFields
**/ 
@isTest 
public class IncomeTriggerTest{

	@testsetup static void setup(){
		TestDataFactory.Case2FOSetupTemplate();
	}


	static testmethod void testautoCreateFOShareIncome(){

		//Create test data for Trigger Settings
		Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
				Enable_Triggers__c = true,
				Enable_Income_Trigger__c = true,
				Enable_Income_Parent_Role_Updates__c = true,
				Enable_FO_Auto_Create_FO_Share__c = true,
				Enable_FO_Delete_FO_Share_on_FO_Delete__c = true
		);
		insert trigSet;

		//Retrieve Role Test data from Setup
		list <Role__c> rolelist = [Select Id, Case__c, Application__c, Role_Type__c From Role__c];
	
		Test.startTest();

			//Create test data for Income, autoCreating FO Share
			Id incRTId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get(CommonConstants.INCOME_RT_PE).getRecordTypeId(); //PAYG Employment
			list <Income1__c> inclist = new list <Income1__c>();
			for (Integer i=0; i<10; i++){
				Income1__c inc = new Income1__c(
					h_Auto_Create_FO_Share__c = true,
					RecordTypeId = incRTId,
					Equal_Share_for_Roles__c = false,
                    Application1__c = roleList[i].Application__c,
					Net_Standard_Pay__c  = 1000,
					Employment_Job_Status__c = 'Full-Time',
                    Role__c = rolelist[0].Id
				);
				if (i == 0){ 
					inc.Income_Situation__C = CommonConstants.INCOME_IS_CI;
				}
				inclist.add(inc);
			}
			insert inclist;

			//Retrieve created FO Shares
            list <FO_Share__c> fosharelist = [Select Id, Case__c, Application_Income__c, Monthly_Amount__c, Income__c, Income__r.Income_Type__c, Income__r.RecordType.Name, Type__c, FO_Record_Type__c, Role__c, Role__r.Application__c, Role_Share__c, Role_Share_Amount__c From FO_Share__c];
			for (FO_Share__c FOs : fosharelist){
				//Verify Income  Details were populated in FO SHare
				system.assertEquals(FOs.Income__r.RecordType.Name, FOs.FO_Record_Type__c);
				system.assertEquals(FOs.Income__r.Income_Type__c, FOs.Type__c);
				//Verify Income, Case, Application were linked to FO Share
				system.assert(FOs.Income__c != null);
                system.assert(FOs.Case__c != null);
                system.assert(FOs.Application_Income__c != null);
				//Verify Equal Share was split to each Role in Application
				Decimal dval = FOs.Role_Share__c;
				dval = dval.setScale(2);
				system.assertEquals(dval, 100);
				//Verify Role Share Amount was calculated
				Decimal dmval = FOs.Role_Share_Amount__c;
				dmval = dmval.setScale(2);
				Decimal dsval = FOs.Monthly_Amount__c * (FOs.Role_Share__c / 100);
				dsval = dsval.setScale(2);
				system.assertEquals(dmval, dsval );
			}

			//Cover Income update, delete trigger events
			update inclist;
			delete inclist;
        
        	//Cover TriggerException
            try { TriggerFactory.createHandler(Partner__c.sObjectType); }
        	catch (Exception e){ 
                TriggerException te = new TriggerException(); 
                te.Test();
            }

			ContinuousUtility cutil = new ContinuousUtility();
            cutil.fakeMethod();

		Test.stopTest();

	}

	static testmethod void testIncomeTrigger(){

		//Create test data for Trigger Settings
		Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
				Enable_Triggers__c = true,
				Enable_Income_Trigger__c = true,
				Enable_Income_Parent_Role_Updates__c = true,
				Enable_FO_Auto_Create_FO_Share__c = true,
				Enable_FO_Delete_FO_Share_on_FO_Delete__c = true
		);
		insert trigSet;

		//Retrieve Role Test data from Setup
		list <Role__c> rolelist = [Select Id, Case__c, Application__c From Role__c WHERE Account__r.IsPersonAccount =: TRUE];
		// System.assertEquals(rolelist.size(),1);
		rolelist[0].RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get(CommonConstants.ROLE_RT_APP_SOLE).getRecordTypeId();
		rolelist[1].RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get(CommonConstants.ROLE_RT_APP_COMP).getRecordTypeId();
		update rolelist;

		Test.startTest();

			//Create test data for Income, autoCreating FO Share
			list <Income1__c> inclist = new list <Income1__c>();
			Id incRTId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get(CommonConstants.INCOME_RT_SE).getRecordTypeId(); 
			for (Integer i=0; i<3; i++){
				Income1__c inc = new Income1__c(
					h_Auto_Create_FO_Share__c = true,
					RecordTypeId = incRTId,
					Equal_Share_for_Roles__c = false,
                    Application1__c = roleList[i].Application__c,
					Net_Monthly_Amount__c  = 1000,
                    Role__c = rolelist[0].Id,
					Income_Type__c = 'Self-Employed'

				);
				if (i == 0){ 
					inc.Income_Situation__C = CommonConstants.INCOME_IS_CI;
				}
				inclist.add(inc);
			}

			Id incRTId2 = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get(CommonConstants.INCOME_RT_BI).getRecordTypeId(); 
			for (Integer i=0; i<3; i++){
				Income1__c inc = new Income1__c(
					h_Auto_Create_FO_Share__c = true,
					RecordTypeId = incRTId2,
					Equal_Share_for_Roles__c = false,
                    Application1__c = roleList[i].Application__c,
					Net_Monthly_Amount__c  = 1000,
                    Role__c = rolelist[1].Id,
					Income_Type__c = CommonConstants.INCOME_RT_BI

				);
				if (i == 0){ 
					inc.Income_Situation__C = CommonConstants.INCOME_IS_CI;
				}
				inclist.add(inc);
			}
			insert inclist;

		Test.stopTest();
	}

	static testmethod void testsetLeadIncomeFields(){

		//Create test data for Trigger Settings
		Trigger_Settings1__c trigSet = new Trigger_Settings1__c(
				Enable_Triggers__c = true,
				Enable_Income_Trigger__c = true,
				Enable_Income_Populate_Lead_Fields__c = true
		);
		insert trigSet;

		//Retrieve test data for Role
		list <Role__c> rolelist = [Select Id, Application__c From Role__c];

		Test.startTest();
			//Test Not Gainfully Employed on Insert
			Income1__c inc1 = new Income1__c(
				RecordTypeId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get('Centrelink').getRecordTypeId(),
				Role__c = rolelist[0].Id,
				Application1__c = rolelist[0].Application__c,
				Income_Type__c = 'Age Pension',
				Income_Situation__c = 'Previous Income'
			);
			insert inc1;

			//Test PAYG Employment on Update
			inc1.RecordTypeId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get('PAYG Employment').getRecordTypeId();
			inc1.Income_Type__c = 'PAYG Employment';
			inc1.Income_Situation__c = 'Current Income';
			update inc1;

			//Test Self-Employed on Insert
			Income1__c inc2 = new Income1__c(
				RecordTypeId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get('Self-Employed').getRecordTypeId(),
				Role__c = rolelist[0].Id,
				Application1__c = rolelist[0].Application__c,
				Income_Type__c = 'Self-Employed',
				Income_Situation__c = 'Secondary Income'
			);
			insert inc2;

		Test.stopTest();

	}

}