/*
	@Description: class for calculating SLA Age of With Introducer (SLA-2) on Scenario
	@Author: Jesfer Baculod - Positive Group
	@History:
		- 11/14/2017 - Created
		- 11/15/2017 - Updated
*/
public class Scenario_UpdateSLAWarning_SLA2 {

	private static final string SCEN_STAGE_LOST = Label.Scenario_Stage_Lost; //Lost
	
	@InvocableMethod(label='SLA Next Warning Time - With Introducer' description='updates next Warning Time of an SLA whenever an SLA is not yet completed')
	public static void scenarioSLAWarningTimeWI (list <ID> scenIDs) {

		//Retrieve default Business Hours
		BusinessHours bh = [select Id from BusinessHours where IsDefault=true]; 

		//Retrieve Scenarios to update
		list <Complex_Scenario__c> scenlist = [Select Id, 
								SLA_Time_2_Start__c, SLA_Warning_Time_2__c, SLA_Started_Count_2__c, SLA_Completed_Count_2__c, SLA_Active_2__c, SLA_Time_2_mm__c, Stage__c, Lost_Reason__c
								From Complex_Scenario__c Where Id in : scenIds];
        
        Datetime warningdate; 
		for (Complex_Scenario__c scen : scenlist){
			warningdate = scen.SLA_Warning_Time_2__c;
			scen.SLA_Warning_Time_2__c = null;
			scen.SLA_Active_2__c = null;
		}
		update scenlist; //force update to retrigger SLA Warning
		system.debug('@@warningdate:'+warningdate);

		for (Complex_Scenario__c scen : scenlist){
            scen.SLA_Active_2__c = 'Yes';
            if (scen.SLA_Started_Count_2__c != scen.SLA_Completed_Count_2__c){ 
                scen.SLA_Warning_Time_2__c = BusinessHours.add(bh.Id, warningdate, 86400000); //Set succeeding warning of current SLA (add 1440 minutes / 24 hours)
                DateTime warningtimeAR = scen.SLA_Warning_Time_2__c;
				scen.SLA_Warning_Time_2__c = Datetime.newInstance(warningtimeAR.year(), warningtimeAR.month(), warningtimeAR.day(), warningtimeAR.hour(), warningtimeAR.minute(), 0);
                scen.SLA_Time_2_mm__c = Decimal.valueof(( BusinessHours.diff(bh.Id, scen.SLA_Time_2_Start__c, warningdate ) / 1000) / 60 );  //returns SLA Age in minutes
                if (scen.SLA_Time_2_mm__c == 1439) scen.SLA_Time_2_mm__c = 1440; //workaround for business hours difference of SLA
				if (scen.SLA_Time_2_mm__c == 2879) scen.SLA_Time_2_mm__c = 2880; //workaround for business hours difference of SLA
				if (scen.SLA_Time_2_mm__c == 4319) scen.SLA_Time_2_mm__c = 4320; //workaround for business hours difference of SLA
				//if (scen.SLA_Time_2_mm__c >= 4320){ //change Stage to Lost and Lost Reason to Went Cold
				/*if (scen.SLA_Time_2_mm__c >= 15){ //change Stage to Lost and Lost Reason to Went Cold
					scen.Stage__c = SCEN_STAGE_LOST;
					scen.Lost_Reason__c = 'Went Cold';
					scen.SLA_Active_2__c = 'No'; //Set current SLA to Inactive
					scen.SLA_Completed_Count_2__c+= 1;
					scen.SLA_Warning_Time_2__c = null;
				} */
				if (scen.SLA_Time_2_mm__c >= 4320){
                	scen.SLA_Warning_Time_2__c = BusinessHours.add(bh.Id, warningdate, 60000); //add 1 minute to update Stage to Lost once SLA elapsed >72 Hours
                }
                system.debug('@@slaWarningB');
            }
		}

		update scenlist;

	}


}