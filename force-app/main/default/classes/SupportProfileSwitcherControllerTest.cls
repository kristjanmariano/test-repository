/*
	@Description: testing auto create of Box Folders on Partner records
	@Author: Rexie David - Positive Group
	@History:
		-	4/04/19 - Created
*/
@isTest
public class SupportProfileSwitcherControllerTest {
    static testmethod void testMethodOne(){
        Test.startTest();
            List<User> userList = SupportProfileSwitcherController.getUsers('Roge');
            System.assert(userList.size() != 0);
            Map<String,Support_Profile_Switcher_Setting__mdt> spSettingMetaMap = SupportProfileSwitcherController.getSPSettingMetaMap();
            System.assert(spSettingMetaMap.isEmpty() != true);
            String isSwitchUserProfile =  SupportProfileSwitcherController.switchUserProfile(userList[0], 'Switch to NOD Support');
            System.assert(isSwitchUserProfile.containsIgnoreCase('true'));
            String isSwitchUserProfileFalse =  SupportProfileSwitcherController.switchUserProfile(userList[0], 'Switch');
            System.assert(isSwitchUserProfileFalse.containsIgnoreCase('false'));
        Test.stopTest();
    }
}