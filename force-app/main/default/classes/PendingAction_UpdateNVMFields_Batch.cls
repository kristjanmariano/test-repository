/*
	@Description: batchable class for pushing updates from Pending Actions
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	9/25/2017 - Created
		- 	9/26/2017 - Fixed Minute and Second values must be between 0 and 59 error, Batch is now scheduled to run once per day / daily
*/
global class PendingAction_UpdateNVMFields_Batch implements Database.Batchable<sObject> {

	global Database.QueryLocator start(Database.BatchableContext bc) {
		string query = 'Select Id, Target_Object__c, Related_Record_Id__c From Pending_Action__c';
		return Database.getQueryLocator(query);
    }

    //execute method for batch
    global void execute(Database.BatchableContext bc, List<Pending_Action__c> scope) {
		//Execute UpdateNVMRelatedFields
		list <ID> sobjIDs = new list <Id>();
		for (Pending_Action__c pa : scope){
			sobjIDs.add(pa.Related_Record_ID__c);
		}
		system.debug('@@sobjIDs:'+sobjIDs);
		PendingAction_UpdateNVMRelatedFields.leadOppPendingAction(sobjIDs);
    }

    global void finish(Database.BatchableContext bc) {

    	/*//Retrieve current running scheduled job
		string cjbName = 'Pending Action - updateNVMRelatedFields'; //'Job-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
		if (Test.isRunningTest()) cjbName = 'Test Pending Action - updateNVMRelatedFields';
		list <CronTrigger> ctlist = [SELECT Id, CronJobDetailId, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType, NextFireTime, PreviousFireTime FROM CronTrigger Where CronJobDetail.Name =: cjbName limit 1];
		for (CronTrigger ct : ctlist){
			system.abortJob(ct.Id);
		}

		//Run another scheduled job for 'Pending Action - updateNVMRelatedFields';
		String day = string.valueOf(system.now().day());
		String month = string.valueOf(system.now().month());
		String hour = string.valueOf(system.now().hour());
		String minute = string.valueOf(system.now().minute() + 10); //10 minutes
		if (Integer.valueof(minute) > 59 ){
		    minute = '0';
		    hour = string.valueOf(system.now().hour() + 1);
		}
		if (Integer.valueof(hour) > 23){
		    hour = '0';
		    day = string.valueOf(system.now().day() + 1);
		}
		String second = string.valueOf(system.now().second());
		String year = string.valueOf(system.now().year());

		String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
		system.debug('@@strSchedule:'+strSchedule);
		PendingAction_PushUpdates_Schedulable papus = new PendingAction_PushUpdates_Schedulable();
		system.schedule(cjbName, strSchedule, papus); */

    }


}