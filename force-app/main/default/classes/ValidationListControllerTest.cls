/**
 * @File Name          : ValidationListControllerTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 20/12/2019, 2:28:59 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/09/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
 * 1.1    20/12/2019   jesfer.baculod@positivelendingsolutions.com.au     Added test for Validation Override
**/
@isTest
public class ValidationListControllerTest {

    //Checks for actual Objecty Validatio Display and Object Validation Setting Custom Metadata. Deactivate "Case-TestData" Object Validation Settings data once done testing

    @testsetup static void setupData(){

        //Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_P_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

        //Create test data for Location
        Location__c loc = new Location__c(
            Name = 'Location test',
            Country__c = 'Australia',
            Postcode__c = '132455',
            State__c = 'Australian Capital Territory',
            State_Acr__c = 'ACT'
        );
        insert loc;

        //Create test data for Address
        Address__c address = TestDataFactory.createGenericAddress(false, 'Australia', '2052', 'New South Wales', 'High St', '', 'Kensington', false);
        address.Location__c = loc.Id;
        insert address;

		//Create Test Role 
		List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
        rolelist[0].Address__c = address.Id;
        rolelist[0].Trustee__c = acclist[0].Id;
        rolelist[0].Age_of_Dependants__c = '3,4';
		Database.insert(roleList);
        
        //Create Role Address test data
        Role_Address__c raddr = TestDataFactory.createGenericRoleAddress(null, true, rolelist[0].Id, null);
        raddr.Address__c = address.Id;
        insert raddr;

        //Create test data for Reference
        Reference2__c ref2 = new Reference2__c(
            RecordTypeId = Schema.SObjectType.Reference2__c.getRecordTypeInfosByName().get('Personal').getRecordTypeId(),
            Case__c = caselist[0].Id,
            Application__c = applist[0].Id,
            Relationship__c = 'Friend',
            Reference_Type__c = 'Personal',
            Reference_Account__c = acclist[0].Id
        );
        insert ref2;

        //Create test data for Purchase
        Id rtPAssetID = Schema.SObjectType.Purchased_Asset__c.getRecordTypeInfosByName().get('Motor Vehicle').getRecordTypeId();
        Purchased_Asset__c passet = new Purchased_Asset__c(
            RecordTypeId = rtPAssetID,
            Case__c = caseList[0].Id,
            Address__c = address.Id
        );
        insert passet;

        //Create FOs
        Income1__c inc = new Income1__c(
            RecordTypeId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get('PAYG Employment').getRecordTypeId(),
            Income_Type__c = 'PAYG Employment',
            Income_Situation__c = 'Current Income',
            Role__c = rolelist[0].Id,
            Net_Standard_Pay__c = 5000,
            Application1__c = applist[0].Id,
            Investment_Property_Address__c = address.Id,
            Employer_Address__c = address.Id,
            Address__c = address.Id
        );
        insert inc;

        Expense1__c exp = new Expense1__c(
            RecordTypeId = Schema.SObjectType.Expense1__c.getRecordTypeInfosByName().get('Accommodation Expense').getRecordTypeId(),
            Expense_Type__c = 'Boarder',
            Payment_Frequency__c = 'Monthly',
            Payment_Amount__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = applist[0].Id,
            Address__c = address.Id
        );
        insert exp;

        Asset1__c ass = new Asset1__c(
            RecordTypeId = Schema.SObjectType.Asset1__c.getRecordTypeInfosByName().get('Property - Investment').getRecordTypeId(),
            Asset_Type__c = 'Investment Property',
            Asset_Value__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = applist[0].Id,
            Address__c = address.Id
        );
        insert ass;

        Liability1__c lia = new Liability1__c(
            RecordTypeId = Schema.SObjectType.Liability1__c.getRecordTypeInfosByName().get('Loan - Real Estate').getRecordTypeId(),
            Liability_Type__c = 'Mortgage - Investment',
            Amount_Owing__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = applist[0].Id,
            Address__c = address.Id
        );
        insert lia;

    }

    static testmethod void testgetBasicInfoAndRecord(){

        list <Case> cselist = [Select Id, Application_Name__c, RecordType.Name From Case];
        String appQuery = ' Select Id, RecordType.Name, ';
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name, Employer_Address__c, Employer_Address__r.RecordType.Name, Investment_Property_Address__c, Investment_Property_Address__r.RecordType.Name From Incomes1__r), '; //Retrieve Incomes
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name From Expenses1__r), '; //Retrieve Expenses
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name From Assets1__r), '; //Retrieve Assets
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name From Liabilities1__r) '; //Retrieve Liabilities
        appQuery+= ' From Application__c';
        list <Application__c> applist = database.query(appQuery);
        cselist[0].Application_Name__c = applist[0].Id;
        update cselist;
        list <Role__c> rolelist = [Select Id, RecordType.Name From Role__c];
        list <Reference2__c> ref2list = [Select Id From Reference2__c];
        list <Purchased_Asset__c> passetlist = [Select Id From Purchased_Asset__c];

        PageReference pref = Page.ValidationListContainer;
        Test.setCurrentPage(pref);
        Test.startTest();

            //Test for getting Basic Info on custom objects
            ValidationListController vlcon = new ValidationListController();
            ValidationListController.basicInfoAndRecordWrapper biaRWr = ValidationListController.getBasicInfoAndRecord(rolelist[0].Id);

            //Test for getting Basic Info on Case
            ValidationListController.basicInfoAndRecordWrapper biaRWr2 = ValidationListController.getBasicInfoAndRecord(cselist[0].Id);

            //Test for getting Basic Info on Application
            ValidationListController.basicInfoAndRecordWrapper biaRWr3 = ValidationListController.getBasicInfoAndRecord(applist[0].Id);

        Test.stopTest();

    }

    static testmethod void testgetValidationListForObject(){

        list <Case> cselist = [Select Id, Application_Name__c, RecordType.Name, Validation_Override__c From Case];
        String appQuery = ' Select Id, RecordType.Name, ';
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name, Employer_Address__c, Employer_Address__r.RecordType.Name, Investment_Property_Address__c, Investment_Property_Address__r.RecordType.Name From Incomes1__r), '; //Retrieve Incomes
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name From Expenses1__r), '; //Retrieve Expenses
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name From Assets1__r), '; //Retrieve Assets
        appQuery+= ' (Select Id, RecordType.Name, Address__c, Address__r.RecordType.Name From Liabilities1__r) '; //Retrieve Liabilities
        appQuery+= ' From Application__c';
        list <Application__c> applist = database.query(appQuery);
        cselist[0].Application_Name__c = applist[0].Id;
        update cselist;
        list <Role__c> rolelist = [Select Id, RecordType.Name From Role__c];
        list <Role_Address__c> roleAddrlist = [Select Id, Address__c From Role_Address__c];
        list <Address__c> addrlist = [Select Id From Address__c];
        list <Reference2__c> ref2list = [Select Id, RecordType.Name From Reference2__c];
        list <Purchased_Asset__c> passetlist = [Select Id, RecordType.Name From Purchased_Asset__c];
        list <Income1__c> inclist = [Select Id, RecordType.Name From Income1__c];

        PageReference pref = Page.ValidationListContainer;
        Test.setCurrentPage(pref);
        Test.startTest();

            //Test for getting Validation List on custom objects
            ValidationListController.wrappedValidationsOnObject biaRWr = ValidationListController.getValidationListForObject(rolelist[0].Id, rolelist[0].RecordType.Name, null, false);

            //Test for getting Validation List on Application
            ValidationListController.wrappedValidationsOnObject biaRWr2 = ValidationListController.getValidationListForObject(applist[0].Id, applist[0].RecordType.Name, null, false);

            //Test Override Validation (20/12/19 - SFBAU-93)
            String overrideCase = ValidationListController.overrideValidation(cselist[0].Id, 'Test Override Reason');

            //Test for getting Validation List on Case
            ValidationListController.wrappedValidationsOnObject biaRWr3 = ValidationListController.getValidationListForObject(cselist[0].Id, cselist[0].RecordType.Name, null, true);

            //Test for getting Validation List on Role
            ValidationListController.wrappedValidationsOnObject biaRWr4 = ValidationListController.getValidationListForObject(cselist[0].Id, cselist[0].RecordType.Name, null, false);

            //Test for getting Validation List on Income
            ValidationListController.wrappedValidationsOnObject biaRWr5 = ValidationListController.getValidationListForObject(inclist[0].Id, inclist[0].RecordType.Name, null, false);

        Test.stopTest();

    }

}