@isTest(SeeAllData=true)
public class LkIntegrationTest{
    
    public static testMethod void testLkIntegration() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        LkintegrationTestFactory.testRequestApi();
        Test.startTest() ;
        LkintegrationTestFactory.testCallAPI();    
        Test.stopTest();
    }
    
    
}