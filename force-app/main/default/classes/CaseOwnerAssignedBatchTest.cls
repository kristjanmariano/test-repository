@isTest
private class CaseOwnerAssignedBatchTest {

    @testsetup static void setup(){

        Id rtPOSAssetFinance = Schema.SObjectType.Case.getRecordTypeInfosByName().get('POS: Consumer - Asset Finance').getRecordTypeId();
        Case cse = new Case(
            Status = 'New',
            Stage__c = 'Owner Assigned',
            Partition__c = 'Positive',
            Channel__c = 'PLS',
            RecordTypeId = rtPOSAssetFinance
        );
        insert cse;

    }

    static testmethod void testCaseOwnerAssignedBatch(){

        //Retrieve test Case
        Case cse = [Select Id, CaseNumber, Stage__c From Case limit 1];
        
        Test.startTest();

            CaseOwnerAssignedBatch coab = new CaseOwnerAssignedBatch();
            database.executeBatch(coab);

        Test.stopTest();

        //Retrieve updated Case 
        Case upCse = [Select Id, CaseNumber, Stage__c From Case Where Id = : cse.Id];
        system.assertEquals(upCse.Stage__c, 'Attempted Contact');


    }

    static testmethod void testCaseOwnerAssignedSchedule(){

        //Retrieve test Case
        Case cse = [Select Id, CaseNumber, Stage__c From Case limit 1];

		Test.startTest();

			CaseOwnerAssignedBatch_Schedule coabs = new CaseOwnerAssignedBatch_Schedule();
			String sch = '0 0 23 * * ?'; 
			system.schedule('TEST-POS Case Check Owner Assigned', sch, coabs);

		Test.stopTest();



	}

}