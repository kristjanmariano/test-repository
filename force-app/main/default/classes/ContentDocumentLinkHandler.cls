/**
 * @File Name          : ContentDocumentLinkHandler.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 08/01/2020, 8:43:17 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/01/2020   jesfer.baculod@positivelendingsolutions.com.au     Initial Version - SFBAU-161
**/
public without sharing class ContentDocumentLinkHandler implements ITrigger {

    private map <id, ContentVersion> cdVersionMap = new map <id, ContentVersion>();
    private map <id, Lead> upLeadMap = new map <id, lead>();
    private map <id, Partner__c> upPartnerMap = new map <id, Partner__c>();
    private General_Settings1__c genSet = General_Settings1__c.getOrgDefaults();

    /** 
	* @FileName: bulkBefore
	* @Description: This method is called prior to execution of a BEFORE trigger. Use this to cache any data required into maps prior execution of the trigger.
	**/ 
	public void bulkBefore(){
	}
	
	public void bulkAfter(){

        if (Trigger.isInsert){
            set <id> cdIdSet = new set <id>();
            for (ContentDocumentLink cdl : (List<ContentDocumentLink>) trigger.new){ //newCDLlist){
                system.debug('@@cdl: '+cdl);
                if (TriggerFactory.trigset.Enable_CDLink_Latest_Comment_From_Note__c){ 
                    if (String.valueof(cdl.LinkedEntityId).startsWith('00Q') || //Note linked to a Lead
                        ( String.valueof(cdl.LinkedEntityId).startsWith(genset.Object_Prefix_Partner__c) && !TriggerFactory.fromNewPartnerPage)  //Note linked to a Partner and not CDL not created from NewPartnerPage
                    ){ // || String.valueof(cdl.LinkedEntityId).startsWith('006') ){  //Note linked to a Opportunity
                        cdIDSet.add(cdl.ContentDocumentId);
                    }
                }
            }
            system.debug('@@cdIDSet: '+cdIDSet);

            //Retrieve all created Enhanced Notes
            for (ContentVersion cversion : [Select Id, ContentDocumentId, Title, FileType, VersionData From ContentVersion Where FileType = 'SNOTE' AND ContentDocumentId in : cdIDSet]){
                if (!cdVersionMap.containskey(cversion.ContentDocumentId)){
                    cdVersionMap.put(cversion.ContentDocumentId, cversion);
                }
            }
        }

	}
		
	public void beforeInsert(SObject so){
	}
	
	public void beforeUpdate(SObject oldSo, SObject so){
	}

	/** 
	* @FileName: beforeDelete
	* @Description: This method is called iteratively for each record to be deleted during a BEFORE trigger
	**/ 
	public void beforeDelete(SObject so){	
	}
	
	public void afterInsert(SObject so){

        ContentDocumentLink cdl = (ContentDocumentLink) so;
        system.debug('@@cdVersionMap:'+cdVersionMap);
        if (cdVersionMap.containskey(cdl.ContentDocumentId) ){
            if ( String.valueof(cdl.LinkedEntityId).startsWith('00Q')){ //Note linked to Lead
                //Store Latest added Note as Latest Comment on Lead
                upLeadMap.put(cdl.LinkedEntityId, ContentDocumentLinkGateway.updateLatestCommentonLead(cdVersionMap.get(cdl.ContentDocumentId), cdl.LinkedEntityId) ); //Used map to overwrite value on same record
            }
            else if (String.valueof(cdl.LinkedEntityId).startsWith(genset.Object_Prefix_Partner__c) ){
                //Store Latest added Note as Latest Comment on Partner
                upPartnerMap.put(cdl.LinkedEntityId, ContentDocumentLinkGateway.updateLatestCommentonPartner(cdVersionMap.get(cdl.ContentDocumentId), cdl.LinkedEntityId) );
            }
        }

	}
	
	public void afterUpdate(SObject oldSo, SObject so){

	}
	
	public void afterDelete(SObject so){

	}

	/** 
	* @FileName: andFinally
	* @Description: This method is called once all records have been processed by the trigger. Use this method to accomplish any final operations such as creation or updates of other records. 
	**/ 
	public void andFinally(){
        if (upLeadMap.size() > 0){
            update upLeadMap.values();
        }
        if (upPartnerMap.size() > 0){
            update upPartnerMap.values();
        }
	}

}