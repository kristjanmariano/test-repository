/**
 * @File Name          : Box_LeadAutoCreateCC.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 07/10/2019, 3:09:23 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/10/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
 * 1.1    07/10/2019   jesfer.baculod@positivelendingsolutions.com.au     Fixed issue on loading Box Folder on Lead creation
**/
public class Box_LeadAutoCreateCC {
        public Id curID {get;set;} //current Object ID
        public string objType {get;set;}
        public boolean noBoxAccess {get;set;}
        private static box.Toolkit boxToolkit = new box.Toolkit();
        //public boolean BoxSuccess {get;set;}
        public Lead curlead {get;set;}
        public string test_objRecordFolderID {get;set;}
        public string test_error {get;set;}
        private list <Box_Partition_Settings__mdt> boxpset {get;set;}
    
    
    public Box_LeadAutoCreateCC(ApexPages.StandardController con) {
        curID = con.getId();
        system.debug('@@curID:'+curID);
        noBoxAccess = false;
        //Retrieve Box Partition Settings
        boxpset = new list <Box_Partition_Settings__mdt>();
        boxpset = [Select Id, Partition__c, Recordtype_Name__c, Object__c, Box_Folder_ID__c, Sub_Folders__c, MasterLabel From Box_Partition_Settings__mdt WHERE Object__c = 'Lead'];

        if (curID != null){ 
            if (curID.getSobjectType() == Lead.SobjectType){ //ID is from Lead
                //Retrieve Lead
                curlead = [Select Id, Name, Lead_Number__c, Box_Folder_ID__c, RecordType.Name, Partition__c From Lead Where Id = : curID]; 
            }
        }
    }

    public pageReference autocreateBoxFolder(){

        if (TriggerFactory.trigset.Enable_Lead_Box_Auto_Create__c){
            // Instantiate the Toolkit object
        
            String objRecordFolderID;
            if (curlead.Box_Folder_ID__c == null){
                if (boxToolkit.getFolderIdByRecordId(curID) == null){
                    //No created Box Folder found on current record        
                    system.debug('curlead.Id == '+curlead.Id);
                    //set Box Folder Name to Lead - ##### for Nodifi Partner Lead 
                    objRecordFolderID = boxToolkit.createFolderForRecordId(curlead.Id, 'Lead - ' + curlead.Lead_Number__c, true); //Create Box folder
                    system.debug('most recent error: ' + boxToolkit.mostRecentError);
                   
                }
                else {
                    system.debug('ELSE curlead.Id == '+curlead.Id);
                    objRecordFolderID = boxToolkit.getFolderIdByRecordId(curlead.Id); //Retrieve existing Box Folder
                }
            }
            else objRecordFolderID = curlead.Box_Folder_ID__c;
            system.debug('@@objRecordFolderID: '+objRecordFolderID);
            if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
            system.debug('@@new item folder id: ' + objRecordFolderID);
            if(objRecordFolderID == null){
                system.debug('most recent error: ' + boxToolkit.mostRecentError);
                //Box Link was clicked where Box Folder ID is not yet loaded on page detail but already has a value on Backend
                if (boxToolkit.mostRecentError.contains('already exists.') || test_error != null){
                    // ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
                    boxToolkit.commitChanges();
                    //Retry load to check if Box Folder ID is already being detected
                    PageReference pref = new PageReference('/apex/Box_LeadAutoCreate?id='+curID);
                    pref.setRedirect(true);
                    return pref; 
                }
                else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,boxToolkit.mostRecentError));
                    return null;
                }
            }
            else{
                for (Box_Partition_Settings__mdt bpset : boxpset){
                    system.debug('@@bpSet partition:'+bpSet.Partition__c);

                    if (bpset.RecordType_Name__c.containsIgnoreCase(curlead.RecordType.Name)){

                        system.debug('@@bpSet partition Name :'+bpSet.MasterLabel);

                        if (Test.isRunningTest()) objRecordFolderID = test_objRecordFolderID;
                        
                        system.debug('@@objRecordFolderId:'+objRecordFolderId);
                        system.debug('most recent error: ' + boxToolkit.mostRecentError);

                        boolean moveFolder = boxToolkit.moveFolder(objRecordFolderID, bpset.Box_Folder_ID__c,'');
                        system.debug('@@moveFolder:'+moveFolder);
                        
                        if(!String.isBlank(bpset.Sub_Folders__c)){
                            string sfolder = bpset.Sub_Folders__c;
                            set <string> subfolders = new set <string>();
                            boolean breakfolder = false;
                            do {
                                string mtype = sfolder.substringBetween('[',']');
                                if (mtype == null) breakfolder = true;
                                if (mtype != null) {
                                    subfolders.add(mtype);
                                    sfolder = sfolder.remove('['+mtype+']');
                                }
                            }
                            while (!breakfolder);
                            for (string strfolderName : subfolders){
                                if(strfolderName.containsIgnoreCase('>')){
                                    String[] arrTest = strfolderName.split('>'); 
                                    String childfolder = boxToolkit.createFolder(arrTest[0], objRecordFolderID, ''); 
                                    String subchildfolder = boxToolkit.createFolder(arrTest[1], childfolder, ''); 
                                    
                                }
                                else{
                                    String childfolder = boxToolkit.createFolder(strfolderName, objRecordFolderID, ''); 
                                }
                            }
                        }
                        break;
                    }
                }

                if (curlead.Box_Folder_ID__c == null){ //Store ID of Box Folder created for Lead
                    curlead.Box_Folder_ID__c = objRecordFolderID;
                    update curlead;
                }
                // ALWAYS call this method when finished.Since salesforce doesn't allow http callouts after dml operations, we need to commit the pending database inserts/updates or we will lose the associations created
                boxToolkit.commitChanges();
                //Redirect to object's BoxSection VF
                PageReference pref = new PageReference('/apex/box__LeadBoxSection?isdtp=vw&id='+curID);
                pref.setRedirect(true);
                return pref; 
            }
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Box Folder creation currently disabled. Please contact your system administrator'));
            return null;
        }
    }
}