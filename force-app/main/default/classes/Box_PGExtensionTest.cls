/*
    @author: Jesfer Baculod - Positive Group
    @history: 10/11/17 - Created
    @description: test class of Box_PGExtension 
*/
@isTest
private class Box_PGExtensionTest {

	private static string PROFILE_SYSADMIN = Label.Profile_Name_System_Administrator; //System Administrator
	
	@testsetup static void setup(){

		//create test data for Sys Admin
        ID pId_sysadmin = [Select Id, Name From Profile Where Name = : PROFILE_SYSADMIN ].Id;
        User sysadminUsr = new User(
                UserName = 'sysadmin@casetriggertest.com.uat',
                LastName = 'TestSysAdminCASE',
                Email = 'sysadmin@casetriggertest.com',
                EmailEncodingKey = 'UTF-8',
                TimeZoneSidKey = 'GMT',
                Alias = 'tSACSE',
                LocaleSidKey = 'en_AU',
                LanguageLocaleKey = 'en_US',
                ProfileId =  pId_sysadmin
            );
        insert sysadminUsr;

		system.runAs(sysadminUsr){

			Box_Controls_Settings__c boxconset = Box_Controls_Settings__c.getOrgDefaults();

			//create test data for Account
	        Account acc = new Account();
	        acc.Name = 'test Account';
	        insert acc;
	        
			//create test data for Case
	        Case cs = new Case(
	        		Status = 'New',
	            	Stage__c = 'Open',
	            	AccountId = acc.Id
	        	);
	        insert cs;
    	}

	}

	static testmethod void testBoxAuthJWT(){

		Test.startTest();

			Box_PGExtension bpe = new Box_PGExtension();

			SingleRequestMock fakeBoxAuthMockResp = new SingleRequestMock(
										200,
										'OK',
										'{"access_token":"bgfbrQgKCRuNpOnjLGe9wwgH19aNFQrC","expires_in":3710,"restricted_to":[],"token_type":"bearer"}',
										null
									);

			SingleRequestMock fakeBoxGetCurUserMockResp = new SingleRequestMock(
										200,
										'OK',
										'{"total_count":1,"entries":[{"type":"user","id":"2408945121"}],"limit":100,"offset":0}',
										null
									);


			Map<String, HttpCalloutMock> endpoint2TestResp =new Map<String,HttpCalloutMock>();
	        endpoint2TestResp.put('https://api.box.com/oauth2/token',fakeBoxAuthMockResp);
	        endpoint2TestResp.put('https://api.box.com/2.0/users?user_type=all&fields=id&offset=0&filter_term='+UserInfo.getUserEmail(),fakeBoxGetCurUserMockResp);

	        HttpCalloutMock multiCalloutMock =new MultiRequestMock(endpoint2TestResp);

	        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

			bpe.BoxAuthentication(); //get Enterprise Token
			bpe.access_token = 'bgfbrQgKCRuNpOnjLGe9wwgH19aNFQrC'; 
			bpe.BoxAuthentication(); //get Box Admin User App Token
			bpe.boxauthtype = 'user';
			bpe.BoxAuthentication(); //get current User App Token

			//verify that Connection Status is Ok 
			system.assertEquals(bpe.con_status,'OK'); 

		Test.stopTest();

	}

	static testmethod void testGetCurrentFolder(){

		Test.startTest();

			Box_PGExtension bpe = new Box_PGExtension();

			SingleRequestMock fakeBoxGetFolderResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"folder","id":"37297699165","sequence_id":"0","etag":"0","name":"00003406","created_at":"2017-09-05T23:29:30-07:00","modified_at":"2017-09-28T20:14:25-07:00","description":"","size":27989012,"path_collection":{"total_count":3,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},{"type":"folder","id":"37104256686","sequence_id":"2","etag":"2","name":"PGA SF"},{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"}]},"created_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"modified_by":{"type":"user","id":"2430710515","name":"Courtney Pavitt","login":"courtney@positivelendingsolutions.com.au"},"trashed_at":null,"purged_at":null,"content_created_at":"2017-09-05T23:29:30-07:00","content_modified_at":"2017-09-28T20:14:25-07:00","owned_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"shared_link":null,"folder_upload_email":null,"parent":{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"},"item_status":"active","item_collection":{"total_count":19,"entries":[{"type":"folder","id":"38089728576","sequence_id":"0","etag":"0","name":"All Attachments"},{"type":"folder","id":"39227596233","sequence_id":"1","etag":"1","name":"INVOICE"},{"type":"folder","id":"39226528719","sequence_id":"1","etag":"1","name":"LOAN DOCS"},{"type":"file","id":"230775179392","file_version":{"type":"file_version","id":"243582223488","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"63S020557CMP-QUOC NGO DO.pdf"},{"type":"file","id":"224732342987","file_version":{"type":"file_version","id":"237326421195","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da"},"sequence_id":"0","etag":"0","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da","name":"Approval Notice.pdf"},{"type":"file","id":"230792636732","file_version":{"type":"file_version","id":"243600247868","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f"},"sequence_id":"1","etag":"1","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f","name":"Combined Insurance Certificate of Currency.pdf"},{"type":"file","id":"224732401952","file_version":{"type":"file_version","id":"237326486304","sha1":"2531b656289c4474be85f444917599bfd7a8d125"},"sequence_id":"0","etag":"0","sha1":"2531b656289c4474be85f444917599bfd7a8d125","name":"E-motor approval.JPG"},{"type":"file","id":"230765942859","file_version":{"type":"file_version","id":"243572743755","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b"},"sequence_id":"3","etag":"3","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b","name":"Email-20170928_1440-00003406 DO- Loan Documents.pdf"},{"type":"file","id":"231193245995","file_version":{"type":"file_version","id":"244019744811","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9"},"sequence_id":"0","etag":"0","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9","name":"Email-20170929_1219-00003406 - Quoc Do.pdf"},{"type":"file","id":"231194599762","file_version":{"type":"file_version","id":"244021208914","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819"},"sequence_id":"1","etag":"1","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819","name":"Email-20170929_1230-00003406 - Quoc Do.pdf"},{"type":"file","id":"230735182747","file_version":{"type":"file_version","id":"243540810395","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87"},"sequence_id":"0","etag":"0","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87","name":"Final Tax Invoice (2).pdf"},{"type":"file","id":"230766737200","file_version":{"type":"file_version","id":"243573584432","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"Insuarance COC.pdf"},{"type":"file","id":"230766982748","file_version":{"type":"file_version","id":"243573850972","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997"},"sequence_id":"0","etag":"0","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997","name":"PaySlip.pdf"},{"type":"file","id":"230748278232","file_version":{"type":"file_version","id":"243554353112","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e"},"sequence_id":"0","etag":"0","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e","name":"proposalForm.pdf"},{"type":"file","id":"229467659567","file_version":{"type":"file_version","id":"242230085423","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28"},"sequence_id":"0","etag":"0","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28","name":"Quoc Do Car Contract.pdf"},{"type":"file","id":"230767255204","file_version":{"type":"file_version","id":"243574124196","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd"},"sequence_id":"0","etag":"0","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd","name":"Quoc Do ID .pdf"},{"type":"file","id":"230765828942","file_version":{"type":"file_version","id":"243572630862","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43"},"sequence_id":"3","etag":"3","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43","name":"Quoc Hung Ngo Do Signed Loan Docs.pdf"},{"type":"file","id":"224732264328","file_version":{"type":"file_version","id":"237326345864","sha1":"283960e79688cebe69f3e0605249822aa49aee31"},"sequence_id":"0","etag":"0","sha1":"283960e79688cebe69f3e0605249822aa49aee31","name":"Supp docs.pdf"},{"type":"file","id":"229557307787","file_version":{"type":"file_version","id":"242323252619","sha1":"13d16e9d962172763872e1a94987b79818361d93"},"sequence_id":"0","etag":"0","sha1":"13d16e9d962172763872e1a94987b79818361d93","name":"VehicleInvoice (2).pdf"}],"offset":0,"limit":100,"order":[{"by":"type","direction":"ASC"},{"by":"name","direction":"ASC"}]}}',
										null
									);

			Test.setMock(HttpCalloutMock.class, fakeBoxGetFolderResp);

			bpe.BoxGetCurrentFolder();
			bpe.usermode = 'mainadmin';
			bpe.BoxGetCurrentFolder();

			//verify that Connection Status is Ok 
			system.assertEquals(bpe.con_status,'OK'); 


		Test.stopTest();

	}

	static testmethod void testEditFolder(){

		Test.startTest();

			Box_PGExtension bpe = new Box_PGExtension();

			SingleRequestMock fakeBoxGetFolderResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"folder","id":"37297699165","sequence_id":"0","etag":"0","name":"00003406","created_at":"2017-09-05T23:29:30-07:00","modified_at":"2017-09-28T20:14:25-07:00","description":"","size":27989012,"path_collection":{"total_count":3,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},{"type":"folder","id":"37104256686","sequence_id":"2","etag":"2","name":"PGA SF"},{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"}]},"created_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"modified_by":{"type":"user","id":"2430710515","name":"Courtney Pavitt","login":"courtney@positivelendingsolutions.com.au"},"trashed_at":null,"purged_at":null,"content_created_at":"2017-09-05T23:29:30-07:00","content_modified_at":"2017-09-28T20:14:25-07:00","owned_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"shared_link":null,"folder_upload_email":null,"parent":{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"},"item_status":"active","item_collection":{"total_count":19,"entries":[{"type":"folder","id":"38089728576","sequence_id":"0","etag":"0","name":"All Attachments"},{"type":"folder","id":"39227596233","sequence_id":"1","etag":"1","name":"INVOICE"},{"type":"folder","id":"39226528719","sequence_id":"1","etag":"1","name":"LOAN DOCS"},{"type":"file","id":"230775179392","file_version":{"type":"file_version","id":"243582223488","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"63S020557CMP-QUOC NGO DO.pdf"},{"type":"file","id":"224732342987","file_version":{"type":"file_version","id":"237326421195","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da"},"sequence_id":"0","etag":"0","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da","name":"Approval Notice.pdf"},{"type":"file","id":"230792636732","file_version":{"type":"file_version","id":"243600247868","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f"},"sequence_id":"1","etag":"1","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f","name":"Combined Insurance Certificate of Currency.pdf"},{"type":"file","id":"224732401952","file_version":{"type":"file_version","id":"237326486304","sha1":"2531b656289c4474be85f444917599bfd7a8d125"},"sequence_id":"0","etag":"0","sha1":"2531b656289c4474be85f444917599bfd7a8d125","name":"E-motor approval.JPG"},{"type":"file","id":"230765942859","file_version":{"type":"file_version","id":"243572743755","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b"},"sequence_id":"3","etag":"3","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b","name":"Email-20170928_1440-00003406 DO- Loan Documents.pdf"},{"type":"file","id":"231193245995","file_version":{"type":"file_version","id":"244019744811","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9"},"sequence_id":"0","etag":"0","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9","name":"Email-20170929_1219-00003406 - Quoc Do.pdf"},{"type":"file","id":"231194599762","file_version":{"type":"file_version","id":"244021208914","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819"},"sequence_id":"1","etag":"1","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819","name":"Email-20170929_1230-00003406 - Quoc Do.pdf"},{"type":"file","id":"230735182747","file_version":{"type":"file_version","id":"243540810395","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87"},"sequence_id":"0","etag":"0","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87","name":"Final Tax Invoice (2).pdf"},{"type":"file","id":"230766737200","file_version":{"type":"file_version","id":"243573584432","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"Insuarance COC.pdf"},{"type":"file","id":"230766982748","file_version":{"type":"file_version","id":"243573850972","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997"},"sequence_id":"0","etag":"0","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997","name":"PaySlip.pdf"},{"type":"file","id":"230748278232","file_version":{"type":"file_version","id":"243554353112","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e"},"sequence_id":"0","etag":"0","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e","name":"proposalForm.pdf"},{"type":"file","id":"229467659567","file_version":{"type":"file_version","id":"242230085423","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28"},"sequence_id":"0","etag":"0","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28","name":"Quoc Do Car Contract.pdf"},{"type":"file","id":"230767255204","file_version":{"type":"file_version","id":"243574124196","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd"},"sequence_id":"0","etag":"0","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd","name":"Quoc Do ID .pdf"},{"type":"file","id":"230765828942","file_version":{"type":"file_version","id":"243572630862","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43"},"sequence_id":"3","etag":"3","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43","name":"Quoc Hung Ngo Do Signed Loan Docs.pdf"},{"type":"file","id":"224732264328","file_version":{"type":"file_version","id":"237326345864","sha1":"283960e79688cebe69f3e0605249822aa49aee31"},"sequence_id":"0","etag":"0","sha1":"283960e79688cebe69f3e0605249822aa49aee31","name":"Supp docs.pdf"},{"type":"file","id":"229557307787","file_version":{"type":"file_version","id":"242323252619","sha1":"13d16e9d962172763872e1a94987b79818361d93"},"sequence_id":"0","etag":"0","sha1":"13d16e9d962172763872e1a94987b79818361d93","name":"VehicleInvoice (2).pdf"}],"offset":0,"limit":100,"order":[{"by":"type","direction":"ASC"},{"by":"name","direction":"ASC"}]}}',
										null
									);

			SingleRequestMock fakeBoxGetFolderCollabResp = new SingleRequestMock(
										200,
										'OK',
										'{"total_count":0,"entries":[]}',
										null
									);

			SingleRequestMock fakeBoxAddFolderCollabResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"collaboration","id":"11385920292","created_by":{"type":"user","id":"2408945121","name":"Jesfer Baculod","login":"jesfer.baculod@positivelendingsolutions.com.au"},"created_at":"2017-10-10T17:59:25-07:00","modified_at":"2017-10-10T17:59:25-07:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"2436439530","name":"Jesfer (UAT Test)","login":"janjesfer02@yahoo.com"},"role":"editor","acknowledged_at":"2017-10-10T17:59:25-07:00","item":{"type":"folder","id":"40302946110","sequence_id":"1","etag":"1","name":"Test Folder - UAT1"}}',
										null
									);


			Map<String, HttpCalloutMock> endpoint2TestResp =new Map<String,HttpCalloutMock>();
	        endpoint2TestResp.put('https://api.box.com/2.0/folders/' + bpe.box_curfolder,fakeBoxGetFolderResp);
	        endpoint2TestResp.put('https://api.box.com/2.0/folders/'+ bpe.box_curfolder + '/collaborations',fakeBoxGetFolderCollabResp);
	        endpoint2TestResp.put('https://api.box.com/2.0/collaborations?notify=false',fakeBoxAddFolderCollabResp);

	        HttpCalloutMock multiCalloutMock =new MultiRequestMock(endpoint2TestResp);

	        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

	        bpe.EditFolder(); //Main Admin has no access to current folder
		
		Test.stopTest();

		//verify that Connection Status is Ok 
		system.assertEquals(bpe.con_status,'OK'); 

	}

	static testmethod void testsaveChangesEditFolder(){

		Test.startTest();

			Box_PGExtension bpe = new Box_PGExtension();

			SingleRequestMock fakeBoxGetFolderResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"folder","id":"37297699165","sequence_id":"0","etag":"0","name":"00003406","created_at":"2017-09-05T23:29:30-07:00","modified_at":"2017-09-28T20:14:25-07:00","description":"","size":27989012,"path_collection":{"total_count":3,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},{"type":"folder","id":"37104256686","sequence_id":"2","etag":"2","name":"PGA SF"},{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"}]},"created_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"modified_by":{"type":"user","id":"2430710515","name":"Courtney Pavitt","login":"courtney@positivelendingsolutions.com.au"},"trashed_at":null,"purged_at":null,"content_created_at":"2017-09-05T23:29:30-07:00","content_modified_at":"2017-09-28T20:14:25-07:00","owned_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"shared_link":null,"folder_upload_email":null,"parent":{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"},"item_status":"active","item_collection":{"total_count":19,"entries":[{"type":"folder","id":"38089728576","sequence_id":"0","etag":"0","name":"All Attachments"},{"type":"folder","id":"39227596233","sequence_id":"1","etag":"1","name":"INVOICE"},{"type":"folder","id":"39226528719","sequence_id":"1","etag":"1","name":"LOAN DOCS"},{"type":"file","id":"230775179392","file_version":{"type":"file_version","id":"243582223488","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"63S020557CMP-QUOC NGO DO.pdf"},{"type":"file","id":"224732342987","file_version":{"type":"file_version","id":"237326421195","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da"},"sequence_id":"0","etag":"0","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da","name":"Approval Notice.pdf"},{"type":"file","id":"230792636732","file_version":{"type":"file_version","id":"243600247868","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f"},"sequence_id":"1","etag":"1","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f","name":"Combined Insurance Certificate of Currency.pdf"},{"type":"file","id":"224732401952","file_version":{"type":"file_version","id":"237326486304","sha1":"2531b656289c4474be85f444917599bfd7a8d125"},"sequence_id":"0","etag":"0","sha1":"2531b656289c4474be85f444917599bfd7a8d125","name":"E-motor approval.JPG"},{"type":"file","id":"230765942859","file_version":{"type":"file_version","id":"243572743755","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b"},"sequence_id":"3","etag":"3","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b","name":"Email-20170928_1440-00003406 DO- Loan Documents.pdf"},{"type":"file","id":"231193245995","file_version":{"type":"file_version","id":"244019744811","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9"},"sequence_id":"0","etag":"0","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9","name":"Email-20170929_1219-00003406 - Quoc Do.pdf"},{"type":"file","id":"231194599762","file_version":{"type":"file_version","id":"244021208914","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819"},"sequence_id":"1","etag":"1","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819","name":"Email-20170929_1230-00003406 - Quoc Do.pdf"},{"type":"file","id":"230735182747","file_version":{"type":"file_version","id":"243540810395","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87"},"sequence_id":"0","etag":"0","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87","name":"Final Tax Invoice (2).pdf"},{"type":"file","id":"230766737200","file_version":{"type":"file_version","id":"243573584432","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"Insuarance COC.pdf"},{"type":"file","id":"230766982748","file_version":{"type":"file_version","id":"243573850972","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997"},"sequence_id":"0","etag":"0","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997","name":"PaySlip.pdf"},{"type":"file","id":"230748278232","file_version":{"type":"file_version","id":"243554353112","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e"},"sequence_id":"0","etag":"0","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e","name":"proposalForm.pdf"},{"type":"file","id":"229467659567","file_version":{"type":"file_version","id":"242230085423","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28"},"sequence_id":"0","etag":"0","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28","name":"Quoc Do Car Contract.pdf"},{"type":"file","id":"230767255204","file_version":{"type":"file_version","id":"243574124196","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd"},"sequence_id":"0","etag":"0","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd","name":"Quoc Do ID .pdf"},{"type":"file","id":"230765828942","file_version":{"type":"file_version","id":"243572630862","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43"},"sequence_id":"3","etag":"3","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43","name":"Quoc Hung Ngo Do Signed Loan Docs.pdf"},{"type":"file","id":"224732264328","file_version":{"type":"file_version","id":"237326345864","sha1":"283960e79688cebe69f3e0605249822aa49aee31"},"sequence_id":"0","etag":"0","sha1":"283960e79688cebe69f3e0605249822aa49aee31","name":"Supp docs.pdf"},{"type":"file","id":"229557307787","file_version":{"type":"file_version","id":"242323252619","sha1":"13d16e9d962172763872e1a94987b79818361d93"},"sequence_id":"0","etag":"0","sha1":"13d16e9d962172763872e1a94987b79818361d93","name":"VehicleInvoice (2).pdf"}],"offset":0,"limit":100,"order":[{"by":"type","direction":"ASC"},{"by":"name","direction":"ASC"}]}}',
										null
									);

			Test.setMock(HttpCalloutMock.class, fakeBoxGetFolderResp);

			bpe.BoxGetCurrentFolder();

			SingleRequestMock fakeBoxUpdateFolderResp = new SingleRequestMock(
										200,
										'OK',
										'{"name":"Test Folder - UAT"}',
										null
									);

			Map<String, HttpCalloutMock> endpoint2TestResp =new Map<String,HttpCalloutMock>();
			for (String foldermapkey : bpe.folderWrMap.keyset()){
				bpe.folderWrMap.get(foldermapkey).isUpdated = true;
				endpoint2TestResp.put('https://api.box.com/2.0/folders/' + foldermapkey,fakeBoxUpdateFolderResp);
			}
	        endpoint2TestResp.put('https://api.box.com/2.0/folders/' + bpe.box_curfolder,fakeBoxGetFolderResp);

	        HttpCalloutMock multiCalloutMock =new MultiRequestMock(endpoint2TestResp);

			Test.setMock(HttpCalloutMock.class, multiCalloutMock);

			bpe.saveChangesEditFolder();

			//verify that Connection Status is Ok 
			system.assertEquals(bpe.con_status,'OK'); 

		Test.stopTest();

	}

	static testmethod void testEditSaveFiles(){

		Test.startTest();

			Box_PGExtension bpe = new Box_PGExtension();

			SingleRequestMock fakeBoxGetFolderResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"folder","id":"37297699165","sequence_id":"0","etag":"0","name":"00003406","created_at":"2017-09-05T23:29:30-07:00","modified_at":"2017-09-28T20:14:25-07:00","description":"","size":27989012,"path_collection":{"total_count":3,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},{"type":"folder","id":"37104256686","sequence_id":"2","etag":"2","name":"PGA SF"},{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"}]},"created_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"modified_by":{"type":"user","id":"2430710515","name":"Courtney Pavitt","login":"courtney@positivelendingsolutions.com.au"},"trashed_at":null,"purged_at":null,"content_created_at":"2017-09-05T23:29:30-07:00","content_modified_at":"2017-09-28T20:14:25-07:00","owned_by":{"type":"user","id":"2430320453","name":"Positive Group","login":"info@positivelendingsolutions.com.au"},"shared_link":null,"folder_upload_email":null,"parent":{"type":"folder","id":"37104946954","sequence_id":"0","etag":"0","name":"Cases"},"item_status":"active","item_collection":{"total_count":19,"entries":[{"type":"folder","id":"38089728576","sequence_id":"0","etag":"0","name":"All Attachments"},{"type":"folder","id":"39227596233","sequence_id":"1","etag":"1","name":"INVOICE"},{"type":"folder","id":"39226528719","sequence_id":"1","etag":"1","name":"LOAN DOCS"},{"type":"file","id":"230775179392","file_version":{"type":"file_version","id":"243582223488","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"63S020557CMP-QUOC NGO DO.pdf"},{"type":"file","id":"224732342987","file_version":{"type":"file_version","id":"237326421195","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da"},"sequence_id":"0","etag":"0","sha1":"94bd2ce6d19d3a48b7bf58591a775269db83c3da","name":"Approval Notice.pdf"},{"type":"file","id":"230792636732","file_version":{"type":"file_version","id":"243600247868","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f"},"sequence_id":"1","etag":"1","sha1":"9d38c7f416ad625b2db587a1bded2482e8d5b77f","name":"Combined Insurance Certificate of Currency.pdf"},{"type":"file","id":"224732401952","file_version":{"type":"file_version","id":"237326486304","sha1":"2531b656289c4474be85f444917599bfd7a8d125"},"sequence_id":"0","etag":"0","sha1":"2531b656289c4474be85f444917599bfd7a8d125","name":"E-motor approval.JPG"},{"type":"file","id":"230765942859","file_version":{"type":"file_version","id":"243572743755","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b"},"sequence_id":"3","etag":"3","sha1":"9a3eb9e18c9a287b21e875e3f4a8dc6bfae6a12b","name":"Email-20170928_1440-00003406 DO- Loan Documents.pdf"},{"type":"file","id":"231193245995","file_version":{"type":"file_version","id":"244019744811","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9"},"sequence_id":"0","etag":"0","sha1":"ab0c25a0268a649a428ba6046e1b1dbd048c21a9","name":"Email-20170929_1219-00003406 - Quoc Do.pdf"},{"type":"file","id":"231194599762","file_version":{"type":"file_version","id":"244021208914","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819"},"sequence_id":"1","etag":"1","sha1":"1c718f0c9a355627272a03ee54ba8287c34e6819","name":"Email-20170929_1230-00003406 - Quoc Do.pdf"},{"type":"file","id":"230735182747","file_version":{"type":"file_version","id":"243540810395","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87"},"sequence_id":"0","etag":"0","sha1":"21609074a1cfe89a8190b302a6a5db38be11ae87","name":"Final Tax Invoice (2).pdf"},{"type":"file","id":"230766737200","file_version":{"type":"file_version","id":"243573584432","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0"},"sequence_id":"1","etag":"1","sha1":"fbf02813d24e20f0c35a9b57ca3ebf38960b2dc0","name":"Insuarance COC.pdf"},{"type":"file","id":"230766982748","file_version":{"type":"file_version","id":"243573850972","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997"},"sequence_id":"0","etag":"0","sha1":"d32e55d22c8eb431fe5b3c2ce3e104e719e10997","name":"PaySlip.pdf"},{"type":"file","id":"230748278232","file_version":{"type":"file_version","id":"243554353112","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e"},"sequence_id":"0","etag":"0","sha1":"73e07e5b9eb225ca64b8ed5cc3d4bbc062d6fa3e","name":"proposalForm.pdf"},{"type":"file","id":"229467659567","file_version":{"type":"file_version","id":"242230085423","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28"},"sequence_id":"0","etag":"0","sha1":"dad29f1cba7dbb587d4f52fd0108d91c3f566f28","name":"Quoc Do Car Contract.pdf"},{"type":"file","id":"230767255204","file_version":{"type":"file_version","id":"243574124196","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd"},"sequence_id":"0","etag":"0","sha1":"97e99ea9dbcf9985717c9c91d4989990dd62b5bd","name":"Quoc Do ID .pdf"},{"type":"file","id":"230765828942","file_version":{"type":"file_version","id":"243572630862","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43"},"sequence_id":"3","etag":"3","sha1":"50d4a6307897172a8a0825200fc425438c6bdd43","name":"Quoc Hung Ngo Do Signed Loan Docs.pdf"},{"type":"file","id":"224732264328","file_version":{"type":"file_version","id":"237326345864","sha1":"283960e79688cebe69f3e0605249822aa49aee31"},"sequence_id":"0","etag":"0","sha1":"283960e79688cebe69f3e0605249822aa49aee31","name":"Supp docs.pdf"},{"type":"file","id":"229557307787","file_version":{"type":"file_version","id":"242323252619","sha1":"13d16e9d962172763872e1a94987b79818361d93"},"sequence_id":"0","etag":"0","sha1":"13d16e9d962172763872e1a94987b79818361d93","name":"VehicleInvoice (2).pdf"}],"offset":0,"limit":100,"order":[{"by":"type","direction":"ASC"},{"by":"name","direction":"ASC"}]}}',
										null
									);

			SingleRequestMock fakeBoxGetFolderCollabResp = new SingleRequestMock(
										200,
										'OK',
										'{"total_count":0,"entries":[]}',
										null
									);

			SingleRequestMock fakeBoxAddFolderCollabResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"collaboration","id":"11385920292","created_by":{"type":"user","id":"2408945121","name":"Jesfer Baculod","login":"jesfer.baculod@positivelendingsolutions.com.au"},"created_at":"2017-10-10T17:59:25-07:00","modified_at":"2017-10-10T17:59:25-07:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"2436439530","name":"Jesfer (UAT Test)","login":"janjesfer02@yahoo.com"},"role":"editor","acknowledged_at":"2017-10-10T17:59:25-07:00","item":{"type":"folder","id":"40302946110","sequence_id":"1","etag":"1","name":"Test Folder - UAT1"}}',
										null
									);

			SingleRequestMock fakeBoxUpdateFileResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"file","id":"225912542882","file_version":{"type":"file_version","id":"238557027234","sha1":"67d03fa04356080fa93ca40c3ebf2f6e19b2d589"},"sequence_id":"9","etag":"9","sha1":"67d03fa04356080fa93ca40c3ebf2f6e19b2d589","name":"1b.pdf","description":"","size":803999,"path_collection":{"total_count":2,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},{"type":"folder","id":"38363668143","sequence_id":"12","etag":"12","name":"Test Folder3"}]},"created_at":"2017-09-18T18:24:04-07:00","modified_at":"2017-10-10T18:48:20-07:00","trashed_at":null,"purged_at":null,"content_created_at":"2017-09-18T18:24:04-07:00","content_modified_at":"2017-09-18T18:24:04-07:00","created_by":{"type":"user","id":"2408945121","name":"Jesfer Baculod","login":"jesfer.baculod@positivelendingsolutions.com.au"},"modified_by":{"type":"user","id":"2408945121","name":"Jesfer Baculod","login":"jesfer.baculod@positivelendingsolutions.com.au"},"owned_by":{"type":"user","id":"2436439530","name":"Jesfer (UAT Test)","login":"janjesfer02@yahoo.com"},"shared_link":null,"parent":{"type":"folder","id":"38363668143","sequence_id":"12","etag":"12","name":"Test Folder3"},"item_status":"active"}',
										null
									);


			Map<String, HttpCalloutMock> endpoint2TestResp =new Map<String,HttpCalloutMock>();
			endpoint2TestResp.put('https://api.box.com/2.0/folders/' + bpe.box_curfolder,fakeBoxGetFolderResp);
	        endpoint2TestResp.put('https://api.box.com/2.0/folders/'+ bpe.box_curfolder + '/collaborations',fakeBoxGetFolderCollabResp);
	        endpoint2TestResp.put('https://api.box.com/2.0/collaborations?notify=false',fakeBoxAddFolderCollabResp);

	        HttpCalloutMock multiCalloutMock =new MultiRequestMock(endpoint2TestResp);

	        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

	        bpe.BoxGetCurrentFolder();
	        bpe.EditFiles(); 

	        for (Box_PGExtension.fileWrapper fwr : bpe.fileWrlist){
				fwr.isSelected = true;
				endpoint2TestResp.put('https://api.box.com/2.0/files/'+ fwr.fileID,fakeBoxUpdateFileResp);
			}

	        bpe.saveChangesEditFiles(); 

	        //verify that Files have been displayed for Edit
			system.assert(bpe.fileWrList.size() > 0); 
			//verify that Connection Status is Ok 
			system.assertEquals(bpe.con_status,'OK'); 
		
		Test.stopTest();

	}

	static testmethod void testGetCurrentRecordFolder(){

		//Retrieve Box Control Settings custom setting
		Box_Controls_Settings__c boxconset = Box_Controls_Settings__c.getOrgDefaults();
		//Retrieve Case
		Case cse = [Select Id, CaseNumber From Case];

		Test.startTest();
			ApexPages.StandardController st0 = new ApexPages.StandardController(cse);
			Box_PGExtension bpe = new Box_PGExtension(st0);

			SingleRequestMock fakeBoxGetRecFolderResp = new SingleRequestMock(
										200,
										'OK',
										'{"total_count":17,"entries":[{"type":"folder","id":"37274703857","sequence_id":"0","etag":"0","name":"00002587"},{"type":"folder","id":"37274063577","sequence_id":"2","etag":"2","name":"00002610"},{"type":"folder","id":"37372303935","sequence_id":"0","etag":"0","name":"00002725"},{"type":"folder","id":"37286592492","sequence_id":"0","etag":"0","name":"00002729"},{"type":"folder","id":"37291028002","sequence_id":"0","etag":"0","name":"00002739"},{"type":"folder","id":"37274384598","sequence_id":"0","etag":"0","name":"00002985"},{"type":"folder","id":"37273325849","sequence_id":"0","etag":"0","name":"00002991"},{"type":"folder","id":"37273577156","sequence_id":"0","etag":"0","name":"00003330"},{"type":"folder","id":"37286488174","sequence_id":"0","etag":"0","name":"00003363"},{"type":"folder","id":"37281765625","sequence_id":"0","etag":"0","name":"00003483"},{"type":"folder","id":"39131249037","sequence_id":"0","etag":"0","name":"00003638"},{"type":"folder","id":"39876853179","sequence_id":"0","etag":"0","name":"00003673"},{"type":"folder","id":"39876813978","sequence_id":"0","etag":"0","name":"00003675"},{"type":"folder","id":"40157049281","sequence_id":"0","etag":"0","name":"00003852"},{"type":"folder","id":"40261833950","sequence_id":"0","etag":"0","name":"00003863"},{"type":"folder","id":"40262646353","sequence_id":"0","etag":"0","name":"'+cse.CaseNumber+'"},{"type":"folder","id":"39876871858","sequence_id":"0","etag":"0","name":"00004851"}],"offset":0,"limit":100,"order":[{"by":"type","direction":"ASC"},{"by":"name","direction":"ASC"}]}',
										null
									);

			SingleRequestMock fakeBoxGetFolderResp = new SingleRequestMock(
										200,
										'OK',
										'{"type":"folder","id":"40262646353","sequence_id":"0","etag":"0","name":"'+cse.CaseNumber+'","created_at":"2017-10-10T01:00:44-07:00","modified_at":"2017-10-10T01:00:44-07:00","description":"","size":0,"path_collection":{"total_count":3,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},{"type":"folder","id":"37272335804","sequence_id":"0","etag":"0","name":"Box For Salesforce"},{"type":"folder","id":"37272364656","sequence_id":"0","etag":"0","name":"Cases"}]},"created_by":{"type":"user","id":"2436439530","name":"Jesfer (UAT Test)","login":"janjesfer02@yahoo.com"},"modified_by":{"type":"user","id":"2436439530","name":"Jesfer (UAT Test)","login":"janjesfer02@yahoo.com"},"trashed_at":null,"purged_at":null,"content_created_at":"2017-10-10T01:00:44-07:00","content_modified_at":"2017-10-10T01:00:44-07:00","owned_by":{"type":"user","id":"2436439530","name":"Jesfer (UAT Test)","login":"janjesfer02@yahoo.com"},"shared_link":null,"folder_upload_email":null,"parent":{"type":"folder","id":"37272364656","sequence_id":"0","etag":"0","name":"Cases"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100,"order":[{"by":"type","direction":"ASC"},{"by":"name","direction":"ASC"}]}}',
										null
									);

			Map<String, HttpCalloutMock> endpoint2TestResp =new Map<String,HttpCalloutMock>();
			endpoint2TestResp.put('https://api.box.com/2.0/folders/' + boxconset.Box_SF_Case_Folder_ID__c + '/items?limit=1000&fields=type,id,name&direction=DESC',fakeBoxGetRecFolderResp);
			endpoint2TestResp.put('https://api.box.com/2.0/folders/' + '40262646353',fakeBoxGetFolderResp);

			HttpCalloutMock multiCalloutMock =new MultiRequestMock(endpoint2TestResp);

	        Test.setMock(HttpCalloutMock.class, multiCalloutMock);

	        bpe.BoxGetCurrentRecordFolder();

	        //verify that Box folder of current record has been found
			system.assertEquals(bpe.box_curfolderName,cse.CaseNumber); 


		Test.stopTest();

	}

}