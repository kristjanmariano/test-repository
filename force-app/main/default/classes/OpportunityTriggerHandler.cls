/*
	@Description: handler class of CaseTrigger
	@Author: Jesfer Baculod - Positive Group
	@History:
		-	11/02/2017 - Created, Transferred existing functionalities (createContactforcreatedOpp and updateCommissionCT)
					  -  Created methods for calculating SLAs on Case (calculateSLATimeStamps, calculateSLATime)
		-	11/03/2017 - Updated generateTrustPilotLink to allow update beyond Approved Stage
		-	11/17/2017 - Updated generateTrustPilotLink to allow update on No Longer Proceeding Stage
		-	11/24/2017 - Updated, due to issue with Trigger Settings
		- 	12/15/2017 - Rex David - Positive Group- [LIG-724 SF - Update NVM Fields - LimitException due to Bizible Update > LIG-726]
		-   12/20/2017 - Rex David - [LIG-743 SF - Query Bug Fix] Added "ALL ROWS" in Task query
        -   02/14/2017 - Jesfer Baculod - Updated Expenses_Considered__c calculation due to reported wrong calculation
*/
public class OpportunityTriggerHandler {

	static Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());

	public static boolean firstRunBU = true; //prevents before update trigger to run multiple times
	public static boolean firstRunAU = true; //prevents after update trigger to run multiple times

	private static final string OPP_STAGE_APPROVED = Label.Opportunity_Stage_Approved; //Approved
	private static final string OPP_STAGE_READYFORSUB = Label.Opportunity_Stage_Ready_for_Submission; //Ready for Submission
	private static final string OPP_STAGE_QUALIFIED = Label.Opportunity_Stage_Qualified; //Qualified
	private static final string OPP_STAGE_SETTLED = Label.Opportunity_Stage_Settled; //Settled
	private static final string OPP_STAGE_APPSENT = Label.Opportunity_Stage_Application_Forms_Sent; //Application Forms Sent
	private static final string OPP_STAGE_APPTAKEN = Label.Opportunity_Stage_Applications_Forms_Taken; //Application Foms Taken
	private static final string OPP_STAGE_APPDOCSRECEIVED = Label.Opportunity_Stage_Application_and_Docs_Received; //Application and Docs Received
	private static final string OPP_STAGE_LOANDOCSRECEIVED = Label.Opportunity_Stage_Loan_Docs_Received; //Loan Docs Received
	private static final string OPP_STAGE_LOANDOCSSENT = Label.Opportunity_Stage_Loan_Documents_Sent; //Loan Documents Sent
	private static final string OPP_STAGE_LOANDOCSREQUESTED = Label.Opportunity_Stage_Loan_Docs_Requested; //Loan Docs Requested
	private static final string OPP_STAGE_TAXINVOICEREQUESTED = Label.Opportunity_Stage_Tax_Invoice_Requested; //Tax Invoice Requested
	private static final string OPP_STAGE_SENTFORSETTLEMENT = Label.Opportunity_Stage_Sent_for_Settlement; //Sent for Settlement
	private static final string OPP_STAGE_NOLONGERPROCEEDING = Label.Opportunity_Stage_No_Longer_Proceeding; //No Longer Proceeding 

	//execute all Before Update event triggers
	public static void onBeforeInsert(list <Opportunity> newOppList){
		if (trigSet.Enable_Opp_updateVariousFlagsAndCalc__c) updateVariousFlagsAndCalculations(newOppList);
	}

	//execute all After Insert event triggers
	public static void onAfterInsert(list <Opportunity> newOppList){
		if (trigSet.Enable_Opp_createContactforcreatedOpp__c) createContactforcreatedOpp(newOppList);
	}


	//execute all Before Update event triggers
	public static void onBeforeUpdate(map<Id,Opportunity> oldOppMap, map <Id,Opportunity> newOppMap){
		list <Opportunity> newOppList = newOppMap.values();
		
		Set<Id> updatedOppIdsForNVMUpdate = new Set<Id>();//LIG-726
		Boolean enableNVMFieldUpdates = false;//LIG-726

		if (trigSet.Enable_Opp_updateVariousFlagsAndCalc__c) updateVariousFlagsAndCalculations(newOppList);
		if (trigSet.Enable_Opp_updateStageFlags__c) updateStageFlags(oldOppMap,newOppMap);
		if (trigSet.Enable_Opp_generateTrustPilotLink__c) generateTrustPilotLink(oldOppMap,newOppMap);
		
		for(Opportunity opp : newOppList){//LIG-726
			if(trigSet.Enable_NVM_Field_Updates__c){
		    	enableNVMFieldUpdates = true;
		    	updatedOppIdsForNVMUpdate.add(opp.Id);
		    }
		}

		if(enableNVMFieldUpdates && updatedOppIdsForNVMUpdate.size() > 0) updateNVMRelatedFieldsInOpp(updatedOppIdsForNVMUpdate, newOppList);//LIG-726
	}

	//execute all After Update event triggers
	public static void onAfterUpdate(map<Id,Opportunity> oldOppMap, map <Id,Opportunity> newOppMap){
		if (trigSet.Enable_Opp_setOppSettledonCommissions__c) setOppSettledonCommissions(oldOppMap,newOppMap);
	}

	//method for generating Trust Pilot unique Link which will be pulled and used in Marketing Cloud for customer's reviews
	private static void generateTrustPilotLink(map<Id,Opportunity> oldOppMap, map <Id,Opportunity> newOppMap){
		for (Opportunity opp : [Select Id, StageName, TrustPilot_Unique_Link__c, Contact__c, Contact__r.Name, Contact__r.Email  From Opportunity Where Id in : newOppMap.keyset()]){
			system.debug('@@opp.StageName:'+opp.StageName);
			//if (newOppMap.get(opp.Id).StageName == OPP_STAGE_APPROVED && newOppMap.get(opp.Id).TrustPilot_Unique_Link__c == null){ //populate only when TrustPilot Unique Link is not yet generated/updated
			if ( (newOppMap.get(opp.Id).TrustPilot_Unique_Link__c == null || newOppMap.get(opp.Id).TrustPilot_Unique_Link__c == '') ){ /*&& 
				(	newOppMap.get(opp.Id).StageName == OPP_STAGE_APPROVED || 
					newOppMap.get(opp.Id).StageName == OPP_STAGE_TAXINVOICEREQUESTED || 
					newOppMap.get(opp.Id).StageName == OPP_STAGE_LOANDOCSREQUESTED || 
					newOppMap.get(opp.Id).StageName == OPP_STAGE_LOANDOCSRECEIVED || 
					newOppMap.get(opp.Id).StageName == OPP_STAGE_LOANDOCSSENT || 
					newOppMap.get(opp.Id).StageName == OPP_STAGE_SENTFORSETTLEMENT || 
					newOppMap.get(opp.Id).StageName == OPP_STAGE_SETTLED || 
					newOppMap.get(opp.Id).StageName == OPP_STAGE_NOLONGERPROCEEDING || ) 
			    ){ //populate only when TrustPilot Unique Link is not yet generated/updated on Approve Stage beyond */
				if (opp.Contact__c != null){
					if (opp.Contact__r.Email != null){ 
						newOppMap.get(opp.Id).TrustPilot_Unique_Link__c = TrustPilot_Util.generateTrustPilotLink(opp.Id, opp.Contact__r.Email, opp.Contact__r.Name); //params for TrustPilotLink ReferenceNumber, Customer Email, Customer Name
						newOppMap.get(opp.Id).TrustPilot_Link_Generated__c = true;
					}
				}
			}
		}
	}

	
	//method for updating Lender Flag, NPS Score Flag, Living Expense, Gross Revenue Calculations (transferred method from Old Opp trigger)
	private static void updateVariousFlagsAndCalculations(list <Opportunity> newOppList){

		map <String, Lender_Flag_for_Emails__c> lenflag = Lender_Flag_for_Emails__c.getAll(); //Get All valid Lenders for Emails

		for (Opportunity opp : newOppList){

			//Auto Correct Loan Type
			if(opp.Loan_Type2__c == 'Car'){
            	opp.Loan_Type2__c = 'Car Loan';
            }
            else if(opp.Loan_Type2__c == 'Bike'){
            	opp.Loan_Type2__c = 'Bike Loan';
            }
            else if(opp.Loan_Type2__c == 'Truck'){
            	opp.Loan_Type2__c = 'Truck Loan';
            }
            else if(opp.Loan_Type2__c == 'Personal'){
            	opp.Loan_Type2__c = 'Personal Loan';
            }
            else if(opp.Loan_Type2__c == 'Boat / Marine'){
            	opp.Loan_Type2__c = 'Boat Loan';
            }
            else if(opp.Loan_Type2__c == 'Commercial Equipment' || opp.Loan_Type2__c == 'Construction Equipment'){
            	opp.Loan_Type2__c = 'Equipment Loan';
            }
            else if(opp.Loan_Type2__c == 'Other'){
            	opp.Loan_Type2__c = '-';
            }
            else if(opp.Loan_Type2__c == 'Trailer'){
            	opp.Loan_Type2__c = 'Trailer Loan';
            }
            else if(opp.Loan_Type2__c == 'Caravan'){
            	opp.Loan_Type2__c = 'Caravan Loan';
            }
            else if(opp.Loan_Type2__c == 'Camper Trailer'){
            	opp.Loan_Type2__c = 'Camper Trailer Loan';
            }
            else if(opp.Loan_Type2__c == 'Horse Float'){
            	opp.Loan_Type2__c = '-';
            }
            else if(opp.Loan_Type2__c == 'Jet Ski'){
            	opp.Loan_Type2__c = 'Jet Ski Loan';
            }

			//Calculation of Living Expenses (Customer) 
			decimal le_customer = 0;
            if (opp.Child_Expenses__c != null) le_customer += opp.Child_Expenses__c;
            if (opp.Phone_TV_Internet__c != null) le_customer += opp.Phone_TV_Internet__c;
            if (opp.Insurance__c != null) le_customer += opp.Insurance__c;
            if (opp.Petrol_Public_Transport__c != null) le_customer += opp.Petrol_Public_Transport__c;
            if (opp.Groceries_Food__c != null) le_customer += opp.Groceries_Food__c;
            if (opp.Entertainment__c != null) le_customer += opp.Entertainment__c;
            if (opp.Utilities_Rates__c != null) le_customer += opp.Utilities_Rates__c;
            if (opp.Education__c != null) le_customer += opp.Education__c;
            if (opp.All_Other__c != null) le_customer += opp.All_Other__c;
            opp.Living_Expenses_Customer__c = le_customer;
            

			//Calculation of Living Expenses
			if(	opp.Lender_and_Dependant_Expenses__c > opp.Living_Expenses2_Customer__c){ // (opp.Living_Expenses__c + opp.Dependant_Expenses__c) 
				opp.Expenses_Considered__c = opp.Lender_and_Dependant_Expenses__c;
			}
            else opp.Expenses_Considered__c = opp.Living_Expenses2_Customer__c;

            //Update the below flag fields when Opp stagename changes which is used in the Report.
            if(opp.StageName == OPP_STAGE_APPROVED) opp.Approved_Count__c = 1;

            // Calculate Gross Revenue
            if(opp.StageName != null) opp.Total_Gross_Revenue__c = opp.Total_Commision__c - opp.Total_Referral_Fees__c - opp.Total_Outgoings__c;

			//Update NPS Score Flag
			if (opp.NPS_Score__c >= 1 && opp.NPS_Score__c <=6) opp.NPS_Score_Flag__c = -1;
			else if (opp.NPS_Score__c == 7 || opp.NPS_Score__c == 8) opp.NPS_Score_Flag__c = 0;
			else if (opp.NPS_Score__c >=9) opp.NPS_Score_Flag__c = 1;

			//Update Lender Flag (triggers sending of emails)
			if (opp.Lender__c != null){
				if (lenflag.containskey(opp.Lender__c)){
					if (lenflag.get(opp.Lender__c).Name == opp.Lender__c){
						opp.Lender_Flag__c = '1';
					}
				}
				else {
					opp.Lender_Flag__c = '0';
				}
			}
			else opp.Lender_Flag__c = '';
		}
	}
 
	//method for updating Stage Flag on Ready for Submission (transferred method from Old Opp trigger)
	private static void updateStageFlags(map<Id,Opportunity> oldOppMap, map<Id,Opportunity> newOppMap){

		for (Opportunity opp : newOppMap.values()){

			if(oldOppMap.get(opp.Id).Lender__c != opp.Lender__c){
				opp.Stage_Flag2__c = '1';
			}
			if (oldOppMap.get(opp.Id).StageName == OPP_STAGE_READYFORSUB && opp.StageName != OPP_STAGE_READYFORSUB){
				opp.Stage_Flag2__c = '0';
			}
			if(oldOppMap.get(opp.Id).StageName != OPP_STAGE_READYFORSUB && opp.StageName == OPP_STAGE_READYFORSUB && oldOppMap.get(opp.Id).Lender__c == null && opp.Lender__c != null){
				opp.Stage_Flag__c = '0';
				}
			if(oldOppMap.get(opp.Id).StageName != OPP_STAGE_READYFORSUB && opp.StageName == OPP_STAGE_READYFORSUB && oldOppMap.get(opp.Id).Lender__c == opp.Lender__c){
				opp.Stage_Flag__c = '1';
				}
			if(oldOppMap.get(opp.Id).Lender__c != opp.Lender__c && opp.StageName == OPP_STAGE_READYFORSUB){
				opp.Stage_Flag__c = '0';
				}
			if(opp.StageName != oldOppMap.get(opp.Id).StageName && opp.StageName == OPP_STAGE_READYFORSUB && opp.Lender__c == oldOppMap.get(opp.Id).Lender__c && opp.Stage_Flag2__c == '1' ){
				opp.Stage_Flag__c = '0';   
				}                
			if(oldOppMap.get(opp.Id).StageName != OPP_STAGE_READYFORSUB && opp.StageName == OPP_STAGE_READYFORSUB && oldOppMap.get(opp.Id).Increment_StageName__c == null && opp.Increment_StageName__c == 1){
				opp.Stage_flag__c = '0';
			}
			
			// Application Validated field should be '0' for the below StageNames:			
			if(opp.StageName == OPP_STAGE_QUALIFIED || opp.StageName == OPP_STAGE_APPSENT || opp.StageName == OPP_STAGE_APPTAKEN || opp.StageName == OPP_STAGE_APPDOCSRECEIVED){
				opp.Application_Validated__c = '0';
			}

		}
	}


	//method for creating Contact when an Opportunity is created (transferred method from Old Opp trigger)
	private static void createContactforcreatedOpp(list <Opportunity> newOppList){
		list <Contact> conlist = new list <Contact>();
		for (Opportunity opp : newOppList){
			if (opp.Contact__c == null){
				Contact con = new Contact(
						LastName = opp.Person_Account_Last_Name__c,
						FirstName = opp.Person_Account_First_Name__c,
						Email = opp.acc_Email__c,
						MobilePhone = opp.Acc_Mobile__c,
						Opportunity__c = opp.Id,
						OwnerId = opp.Acc_Owner__c
					);
				conlist.add(con);
			}
		}
		if (conlist.size() > 0) database.insert(conList,false);
	}

	//method for updating Opportunity Settled on Commissions and updating Mobile on VS Referrals (transferred method from Old Opp trigger)
	private static void setOppSettledonCommissions( map<Id,Opportunity> oldOppMap, map<Id,Opportunity> newOppMap ){

		List<contact> conList = new List<contact>();
		Map<id, Opportunity> oppEmptyCons = new Map<id,Opportunity>();
        set<Id> oppIds = new set<Id>();
        Set<Id> commIds = new Set<Id>();

        for(Opportunity opp : newOppMap.values()){
            if(opp.contact__c==null) oppEmptyCons.put(opp.id,opp);
            if(oldOppMap.get(opp.Id).StageName != opp.StageName && opp.StageName == OPP_STAGE_SETTLED) commIds.add(opp.Id);
            oppIds.add(opp.id);
        }

        if(oppIds.size()>0){

            Map<id, List<VS_Referral__c> > oppToRefferalMap = new Map<id,List<VS_Referral__c>>();
            for(VS_Referral__c vsRefferObj:[SELECT id, Name, Mobile__c, Opportunity__c FROM VS_Referral__c WHERE Opportunity__c IN: oppIds]){
                List<VS_Referral__c> vsRefLst = new List<VS_Referral__c>();
                vsRefLst.add(vsRefferObj);
                oppToRefferalMap.put(vsRefferObj.opportunity__c,vsRefLst);
            }
            List<VS_Referral__c> vsRefList = new List<VS_Referral__c>();
            for(opportunity opp: newOppMap.values() ){
                if(oppToRefferalMap.containsKey(opp.id) && opp.Mobile__c != oldOppMap.get(opp.id).Mobile__c){
                    for(VS_Referral__c vsRef : oppToRefferalMap.get(opp.id)){
                        vsRef.Mobile__c = opp.Mobile__c;  
                        vsRefList.add(vsRef);  
                    }
                }
            }

            if(vsRefList.size()>0){
                update vsRefList;
            }
            
        }
        
        for(Opportunity oppCons : oppEmptyCons.values()){
            Contact con=new contact(
            	LastName = oppCons.Person_Account_Last_Name__c,
            	FirstName = oppCons.Person_Account_First_Name__c,
            	Email = oppCons.Email__c,
            	MobilePhone = oppCons.mobile__c,
            	Opportunity__c = oppCons.Id,
            	OwnerId = oppCons.OwnerId
            );  
            conList.add(con);
        }
        if(!conList.isEmpty()){
            database.insert(conList,false);
        }
        system.debug('@@conlist:'+conlist);

        List<Commission__c> commToUpdate = new List<Commission__c>();
        for(Commission__c co: [SELECT Id, Opportunity_Settled__c, Opportunity__c from commission__c where Opportunity__c IN: commIds]){
            co.Opportunity_Settled__c = true;
            commToUpdate.add(co);
        }
        //Update Commissions
        update commToUpdate;
	}

	//LIG-726 - Method for updating NVM Related Fields based on Pending_Action__c records.
	private static void updateNVMRelatedFieldsInOpp(Set<Id> updatedOppIdsForNVMUpdate, List<Opportunity> updatedOpp){

		List<Pending_Action__c> pendingActionList = CommonTriggerController.queryPendingActionList(updatedOppIdsForNVMUpdate,'updateNVMRelatedFields');

		Map<Id,Pending_Action__c> pendingActionMap = new Map<Id,Pending_Action__c>();

		if(pendingActionList.size() > 0){

			for(Pending_Action__c pa : pendingActionList){
				if(!pendingActionMap.containsKey(pa.Related_Record_Id__c)){
					pendingActionMap.put(pa.Related_Record_Id__c,pa);
				}
			}

			Map<Id,List<Task>> oppTaskMap = new Map<Id,List<Task>>();
			
			for(Task tsk : [SELECT Id, WhatId, CallType FROM Task WHERE CallType=:CommonConstants.TASK_CALLTYPE_OUTBOUND AND Status=:CommonConstants.TASK_STATUS_COMPLETED AND WhatId IN : updatedOppIdsForNVMUpdate ALL ROWS]){
				if(!oppTaskMap.containsKey(tsk.WhatId)){
					oppTaskMap.put(tsk.WhatId,new List<Task>());
				}
				oppTaskMap.get(tsk.WhatId).add(tsk);
			}

			for(Opportunity oppInstance : updatedOpp){
				if(oppTaskMap.containsKey(oppInstance.Id)){
					oppInstance.Calls_Made__c = oppTaskMap.get(oppInstance.Id).size();
				}
				if(pendingActionMap.containsKey(oppInstance.Id)){
					if(pendingActionMap.get(oppInstance.Id).CW_Call_End_Time__c != NULL){
						oppInstance.Last_Call_Time_Ended__c = pendingActionMap.get(oppInstance.Id).CW_Call_End_Time__c;
					}
				}
			}
			if(pendingActionList.size()>0) delete pendingActionList;
		}
	}
}