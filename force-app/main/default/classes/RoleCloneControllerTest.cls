/** 
* @FileName: RoleCloneControllerTest
* @Description: Test class for RoleCloneController
* @Copyright: Positive (c) 2019
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification
* 1.0 5/01/19 JBACULOD Created class, added testinitClone
* 1.1 5/05/19 JBACULOD added tests on FOs
**/ 
@isTest
public class RoleCloneControllerTest {

    @testSetup static void setupData() {

        //Create Test Account
		List<Account> accList = TestDataFactory.createGenericAccount('Test Account ',CommonConstants.ACC_RT_TRUST_IAT, 1);
		Database.insert(accList);

		//Create Test Case
		List<Case> caseList = TestDataFactory.createGenericCase(CommonConstants.CASE_RT_N_CONS_AF, 1);
		Database.insert(caseList);
        
		//Create Test Application
		List<Application__c> appList = TestDataFactory.createGenericApplication(CommonConstants.APP_RT_CONS_I, caseList[0].Id, 1);
		Database.insert(appList);

		//Create Test Role 
		List<Role__c> roleList = TestDataFactory.createGenericRole(CommonConstants.ROLE_RT_APP_INDI, caseList[0].Id, accList[0].Id, appList[0].Id,  1);
		Database.insert(roleList);
        
        //Create Role Address test data
        Role_Address__c raddr = TestDataFactory.createGenericRoleAddress(null, true, rolelist[0].Id, null);
        insert raddr;

        //Create FOs
        Income1__c inc = new Income1__c(
            RecordTypeId = Schema.SObjectType.Income1__c.getRecordTypeInfosByName().get('PAYG Employment').getRecordTypeId(),
            Income_Type__c = 'PAYG Employment',
            Income_Situation__c = 'Current Income',
            Role__c = rolelist[0].Id,
            Net_Standard_Pay__c = 5000,
            Application1__c = applist[0].Id
        );
        insert inc;

        Expense1__c exp = new Expense1__c(
            RecordTypeId = Schema.SObjectType.Expense1__c.getRecordTypeInfosByName().get('Accommodation Expense').getRecordTypeId(),
            Expense_Type__c = 'Boarder',
            Payment_Frequency__c = 'Monthly',
            Payment_Amount__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = applist[0].Id
        );
        insert exp;

        Asset1__c ass = new Asset1__c(
            RecordTypeId = Schema.SObjectType.Asset1__c.getRecordTypeInfosByName().get('Property - Investment').getRecordTypeId(),
            Asset_Type__c = 'Investment Property',
            Asset_Value__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = applist[0].Id
        );
        insert ass;

        Liability1__c lia = new Liability1__c(
            RecordTypeId = Schema.SObjectType.Liability1__c.getRecordTypeInfosByName().get('Loan - Real Estate').getRecordTypeId(),
            Liability_Type__c = 'Mortgage - Investment',
            Amount_Owing__c = 5000,
            h_FO_Share_Role_IDs__c = rolelist[0].Id + ';',
            Application1__c = applist[0].Id
        );
        insert lia;

        FO_Link__c folink = new FO_Link__c(
            Income__c = inc.Id,
            Expense__c = exp.Id,
            Asset__c = ass.Id,
            Liability__c = lia.Id,
            h_Application__c = applist[0].Id
        );
        insert folink;


    }

    static testmethod void testinitClone(){

        list <Role__c> rolelist = [Select Id, Name, Case__c, Case__r.Partition__c, Mobile_Phone__c, RecordTypeId, RecordType.Name From Role__c];
        Application__c app = [Select Id, (Select Id From Assets1__r), (Select Id From Expenses1__r), (Select Id From Liabilities1__r), (Select Id, Role__c From Incomes1__r), 
                              (Select Id, Name, h_Case__c, h_Application__c, h_Path_Option__c, h_Last_Saved_Path_Step__c, FO_Source__c, Asset__c, Asset__r.Name, Asset__r.Asset_Value__c, Asset__r.RecordTypeId, Asset__r.RecordType.Name, Expense__c, Expense__r.Name, Expense__r.RecordTypeId, Expense__r.RecordType.Name, Expense__r.Monthly_Payment__c, Income__c, Income__r.Name, Income__r.RecordTypeId, Income__r.RecordType.Name, Income__r.Monthly_Income__c, Liability__c, Liability__r.Name, Liability__r.RecordTypeId, Liability__r.RecordType.Name, Liability__r.Amount_Owing__c, Asset_Type__c, Liability_Type__c, Income_Type__c, Expense_Type__c From FO_Links__r) From Application__c limit 1];
        PageReference pref = Page.RoleClone;
        Test.setCurrentPage(pref);
        Test.startTest();

            list <string> fsnames = new list <string>();
            fsnames.add('Role_Income_Fields');
            RoleCloneController rccon = new RoleCloneController();
            RoleCloneController.wrappedRoleData wRD = RoleCloneController.getExistingRoleAndRelatedData(rolelist[0].Id, fsnames);
            //Verify Role and related info have been retrieved
            system.assert(wRD.role != null);
            system.assert(wRD.raddrlist.size() > 0);

            User objeUser = [Select Id from User where Id=: UserInfo.getUserId()];
            System.runAs(objeUser){
                Process_Flow_Definition_Settings1__c processFlowSet = new Process_Flow_Definition_Settings1__c(
                    Enable_Role_Flow_Definitions__c = false
                );
                insert processFlowSet;
            }

            Role__c toCloneRole = rolelist[0].clone(false, false, false, false);
            toCloneRole.Mobile_Phone__c = '0412345678';
            Role__c creRole = RoleCloneController.cloneRole(toCloneRole);

            //Test Clone Income
            list <RoleCloneController.incomeWrapper> incWrlist = new list <RoleCloneController.incomeWrapper>();
            for (Income1__c inc : app.Incomes1__r){
                RoleCloneController.incomeWrapper incWr = new RoleCloneController.incomeWrapper();
                incWr.ogid = inc.Id;
                incWr.inc = inc.clone(false, false, false, false);
                incWrlist.add(incWr);
            }
            list <RoleCloneController.incomeWrapper> newIncWrlist = RoleCloneController.cloneIncome(incWrlist);

            //Test Clone Expenses
            list <RoleCloneController.expenseWrapper> expWrlist = new list <RoleCloneController.expenseWrapper>();
            for (Expense1__c exp : app.Expenses1__r){
                RoleCloneController.expenseWrapper expWr = new RoleCloneController.expenseWrapper();
                expWr.ogid = exp.Id;
                expWr.exp = exp.clone(false, false, false, false);
                expWrlist.add(expWr);
            }
            list <RoleCloneController.expenseWrapper> newExpWRlist = RoleCloneController.cloneExpenses(expWrlist);

            //Test Clone Assets
            list <RoleCloneController.assetWrapper> assWrlist = new list <RoleCloneController.assetWrapper>();
            for (Asset1__c ass : app.Assets1__r){
                RoleCloneController.assetWrapper assWr = new RoleCloneController.assetWrapper();
                assWr.ogid = ass.Id;
                assWr.ass = ass.clone(false, false, false, false);
                assWrlist.add(assWr);
            }
            list <RoleCloneController.assetWrapper> newAssWrlist = RoleCloneController.cloneAssets(assWrlist);

            //Test Clone Liabilities
            list <RoleCloneController.liabilityWrapper> liaWrlist = new list <RoleCloneController.liabilityWrapper>();
            for (Liability1__c lia : app.Liabilities1__r){
                RoleCloneController.liabilityWrapper liaWr = new RoleCloneController.liabilityWrapper();
                liaWr.ogid = lia.Id;
                liaWr.lia = lia.clone(false, false, false, false);
                liaWrlist.add(liaWr);
            }
            list <RoleCloneController.liabilityWrapper> newLiaWrlist = RoleCloneController.cloneLiabilities(liaWrlist);

            //Test Clone FO Links
            list <FO_Link__c> foLinklist = new list <FO_Link__c>();
            for (FO_Link__c folink : app.FO_Links__r){
                FO_Link__c newFOLink = foLink.clone(false,false,false,false);
                foLinklist.add(newFOLink);
            }
            list <FO_Link__c> newFOLink = RoleCloneController.cloneFOLinks(newIncWrlist, newExpWRlist, newAssWrlist, newLiaWrlist, foLinklist, app.Id, null);
            
            list <Role_Address__c> tocloneRAddr = new list <Role_Address__c>();
            for (Role_Address__c raddr : wRD.raddrlist){
                Role_Address__c newraddr = raddr.clone(false,false,false,false);
                newraddr.Role__c = creRole.Id;
                tocloneRAddr.add(newraddr);
            }
            list <Role_Address__c> roleaddrlist = RoleCloneController.cloneRoleAddresses(tocloneRAddr);

        Test.stopTest();

    }


}