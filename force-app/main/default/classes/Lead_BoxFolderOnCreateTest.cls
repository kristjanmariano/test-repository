/**
 * @File Name          : Lead_BoxFolderOnCreateTest.cls
 * @Description        : 
 * @Author             : jesfer.baculod@positivelendingsolutions.com.au
 * @Group              : 
 * @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
 * @Last Modified On   : 06/10/2019, 10:49:07 am
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/10/2019   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
**/
@isTest
public class Lead_BoxFolderOnCreateTest {
    @testsetup static void setup(){
        //Create test data for Trigger Settings
        Trigger_Settings1__c trigSet = Trigger_Settings1__c.getOrgDefaults();
        trigSet.Enable_Triggers__c = false;
        trigSet.Enable_Lead_Box_Auto_Create__c = true;
        upsert trigSet Trigger_Settings1__c.Id;
	}

    static testmethod void testLeadBoxFolderCreate(){

        Test.startTest();
            Id rtId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Nodifi Partner').getRecordTypeId();
			List<Lead> leadList = new List<Lead>();
            Lead curlead = new Lead(FirstName = 'TestF', LastName = 'TestL', RecordTypeId = rtId);
            leadList.add(curlead);
            insert leadList;

            list <ID> leadListIDs = new list <ID>();
            for (Lead ld : leadList){
                leadListIDs.add(ld.Id);
            }

            Lead_BoxFolderOnCreate.test_objRecordFolderID = '12334555';
            Lead_BoxFolderOnCreate.leadBoxFolderCreate(leadListIDs);
            Lead_BoxFolderOnCreate cbox = new Lead_BoxFolderOnCreate(leadListIDs);
            cbox.createBoxFolder(leadListIDs);
            ID jobID = System.enqueueJob(cbox);
            system.debug('@@JobID:'+jobID);


            for (Lead ld : [Select Id, Box_Folder_ID__c From Lead Where Id in : leadListIDs]){
                system.assert(ld.Box_Folder_ID__c != null);
                system.assertEquals(ld.Box_Folder_ID__c, Lead_BoxFolderOnCreate.test_objRecordFolderID);
            }

        Test.stopTest();

    }

}