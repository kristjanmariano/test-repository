@isTest
Public class ReferralFeesTriggerTest{
    public static testMethod void refTest(){
    
    Opportunity opp = new Opportunity(name='Test', StageName='Open', closeDate = date.Today());
    insert Opp;

    Referral_fees__c ref = new Referral_fees__c(Referral_fee__c = 100, Opportunity__c=opp.Id, Referral_Company__c = 'a00p0000003M73Y');
    insert ref;
    
    ref.Referral_fee__c = 101;
    update ref;
    delete ref;
   
    Referral_fees__c ref2 = new Referral_fees__c(Referral_fee__c = null, Opportunity__c=opp.Id);
    insert ref2;
    update ref2;
    }
}