@IsTest
public class LkintegrationTestFactory {
     //public static Contact con  ;
     public static Opportunity opp ;
     public static Asset_Mapping__c assetRE;
     public static void testRequestApi(){       
                Account  acc =  LkIntegrationInsertTestData.insertAccount();
                opp = LkIntegrationInsertTestData.insertOpportunity(acc.id);
                Applicant_2_Mapping__c appl2 = LkIntegrationInsertTestData.insertApplicant2Mapping(opp);
                
                Asset_Mapping__c assetM = LkIntegrationInsertTestData.insertAssetMapping(opp,'Deposit Account');
                LK_Asset_Liability_Share__c aLSforAsset = LkIntegrationInsertTestData.insertALSForAssetandAccount(assetM,acc,opp);
                LK_Asset_Liability_Share__c aLSforAssetSAM = LkIntegrationInsertTestData.insertALSForAssetandApplicant2(assetM,appl2,opp);
                
                Asset_Mapping__c assetSAM = LkIntegrationInsertTestData.insertAssetMapping(opp,'Household goods');
                LK_Asset_Liability_Share__c aLSforAsset2 = LkIntegrationInsertTestData.insertALSForAssetandAccount(assetSAM,acc,opp);
                LK_Asset_Liability_Share__c aLSforAssetSAM2 = LkIntegrationInsertTestData.insertALSForAssetandApplicant2(assetSAM,appl2,opp);
         
                assetRE = LkIntegrationInsertTestData.insertAssetMapping(opp,'Real Estate');
                LK_Asset_Liability_Share__c aLSforAssetRE = LkIntegrationInsertTestData.insertALSForAssetandAccount(assetRE,acc,opp);
         
                /*LK_Asset__c assetMV = LkIntegrationInsertTestData.insertAsset(con,'Motor Vehicle');
                LK_Asset_Liability_Share__c aLSforAssetMV = LkIntegrationInsertTestData.insertAssetLiabilityForAsset(assetMV,con);*/

         
                Liability_Mapping__c libM = LkIntegrationInsertTestData.insertLiabilityMapping(opp);
                LK_Asset_Liability_Share__c aLSforLiability1= LkIntegrationInsertTestData.insertALSForLiabilityandAccount(libM,acc,opp);
                LK_Asset_Liability_Share__c aLSforLiability1Apl2 = LkIntegrationInsertTestData.insertALSForLiabilityandApplicant2(libM,appl2,opp);
                Liability_Mapping__c libM2 = LkIntegrationInsertTestData.insertLiabilityMapping(opp);
                LK_Asset_Liability_Share__c aLSforLiability2 = LkIntegrationInsertTestData.insertALSForLiabilityandAccount(libM2,acc,opp);
                LK_Asset_Liability_Share__c aLSforLiability2Apl2 = LkIntegrationInsertTestData.insertALSForLiabilityandApplicant2(libM2,appl2,opp);
                Employer_Mapping__c  emp = LkIntegrationInsertTestData.insertEmploymentMapping(opp);
     }
     public static void testCallAPI() {
         LKIntegration.requestApi(opp.id);
     }
}