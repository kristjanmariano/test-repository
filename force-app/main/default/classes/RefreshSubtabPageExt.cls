public class RefreshSubtabPageExt {
    
    public Support_Request__c suppReq {get;set;}
    public boolean refresh {get;set;}
    
    public RefreshSubtabPageExt(ApexPages.StandardController stdController){
        this.suppReq = (Support_Request__c)stdController.getRecord();
        this.suppReq = [SELECT Id, Status__c, Stage__c, Refresh_Case__c FROM Support_Request__c WHERE Id =: this.suppReq.Id];
        this.refresh = false;
        System.debug('refresh Case -> '+this.suppReq.Refresh_Case__c);
    }
    
    public PageReference updateSupporReq(){
        if(this.suppReq.Status__c == 'Closed' && this.suppReq.Refresh_Case__c){
            this.suppReq.Refresh_Case__c = false;
            this.refresh = true;
            System.debug('updateSupporReq -> '+this.suppReq);
        	Database.update(this.suppReq);
        }
        return null;
    }
}