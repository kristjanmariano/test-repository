global class updateOpportunityDealValues_Schedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		Date currentDate = System.today();
		updateOpportunityDealValues uOPV = new updateOpportunityDealValues(currentDate,false); //allows partial success
		if(!Test.isRunningTest()) Database.executeBatch(uOPV,10); //limit batch update to 10
		else Database.executeBatch(uOPV);
	}
}