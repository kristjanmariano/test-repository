/*
	@Description: This invocable class is currently being used in Process Builder 
                    - Frontend Submission - On Create
	@Author: Rexie David - Positive Group
	@History:
        -   11/09/2019 - extra/task25953355-NODFlowV3SFAutomations - "Logic to create Communication - PLS - Request a Call"
        -   24/09/2019 - extra/task25953355-NODFlowV3SFAutomations - Added condition to bypass timezone-based routing when the FE is "nod-request-call", This will enforce the routing to run at the clients preferred time (ADL)
        -   27/09/2019 - extra/task25953355-NODFlowV3SFAutomations - Bug Fix in Timezonebased routing
        -   22/11/2019 - extra/task26890354-InitialNVMCallRoutingRebuild - Added logic to support creation of SR Call Request on Case Object
*/
public class SR_CallRequest implements Queueable{
    private static final Trigger_Settings1__c trigSet = Trigger_Settings1__c.getInstance(UserInfo.getUserId());
    private List <Support_Request__c> srListToInsert = new List<Support_Request__c>();
    private static final String FRONTENDSUBMISSIONOBJAPINAME = 'Frontend_Submission__c';
    private static final String CASEOBJAPINAME = 'Case';
    private static final String POSITIVEPARTITION = 'Positive';
    private static final String WOPACASESTAGE = 'WOPA';
    private static final String COMMPLSREQUESTCALLSRRT = 'Communication - PLS - Request a Call';
    private static final String LFPCALLREASON = 'I need to discuss an application from LFP Site.';
    private static final String DEFAULTSTATE = 'SA'; //Default State South Australia
    private static final String INITIALCALL = 'Initial Call';
    public static List<Case> caseList = new List<Case>();

    public SR_CallRequest(List <Support_Request__c> srListToInsert){
        this.srListToInsert = srListToInsert;
    }

    public void execute (QueueableContext context){
        System.debug('SR_CallRequest execute');
        if (srListToInsert.size() > 0){
            System.debug('SR_CallRequest execute srListToInsert ' +srListToInsert.size());
            if(trigSet.Enable_SR_Call_Request_NVM_Routing__c) Database.insert(srListToInsert);
        }
    } 

    /**
	* @Description: RDAVID extra/task25953355-NODFlowV3SFAutomations 
                    Create a Support Request record for Call Request
	* @Arguments:	list <String> params format RecordId (e.g. 'a444Y00000005qn'}')
                    - If Object is Frontend Submission, Params is List of FE ids
                    - If Object is Case, Params is List of Case Ids
	* @Returns:		n/a
	*/
    @InvocableMethod(label='Create SR (Request a Call)' description='Create SR (Request a Call)')
	public static void createSRCallRequest (list <Id> params) {
        List<Support_Request__c> callRequestSRListToIns = new List<Support_Request__c>();
        String sObjName = (params.size()>0) ? params[0].getSObjectType().getDescribe().getName() : '';
        if(!String.isBlank(sObjName)) callRequestSRListToIns = getCallRequestSRListToIns(callRequestSRListToIns,sObjName,params);
        if(callRequestSRListToIns.size() > 0){
            SR_CallRequest srCallRequestQueueable = new SR_CallRequest(callRequestSRListToIns);
            ID jobID = System.enqueueJob(srCallRequestQueueable); 
        }
    }

    /**
	* @Description: RDAVID extra/task25953355-NODFlowV3SFAutomations 
                    Generate a List of SR to Create based on specified logic
                    Supported Objects
                        - Case : for Case Assignment PLS
                        - Frontend Submission : for LFP Nod V3
	* @Arguments:	List<Support_Request__c> callRequestSRListToIns, String sObjName, List<Id> params
	* @Returns:		List<Support_Request__c> getCallRequestSRListToIns
	*/
    public static List<Support_Request__c> getCallRequestSRListToIns (List<Support_Request__c> callRequestSRListToIns, String sObjName, List<Id> params){
        //RDAVID 12/09/2019 extra/task25953355-NODFlowV3SFAutomations - Logic if this apex was invoked by the Frontend_Submission__c object
        if(sObjName == FRONTENDSUBMISSIONOBJAPINAME){
            for(Frontend_Submission__c feSub : [SELECT Id, Positive_Form_Type__c, Case__c, Case__r.Latest_Frontend_Submission__c, Case__r.Partition__c, Preferred_Call_Time__c,Preferred_Call_Date_Time_Local__c,Case__r.State_Code__c FROM Frontend_Submission__c WHERE Id IN: params]){
                if(feSub.Case__r.Latest_Frontend_Submission__c == feSub.Positive_Form_Type__c && feSub.Case__r.Partition__c == POSITIVEPARTITION){
                    // If Case State Code is blank, set to SA
                    String auState = (String.isBlank(fesub.Case__r.State_Code__c)) ? DEFAULTSTATE : fesub.Case__r.State_Code__c; 
                    // If feSub.Positive_Form_Type__c is NOT 'nod-request-call' respect State Timezones ELSE bypass Timezones
                    Datetime readyToDialTime = (!String.isBlank(feSub.Positive_Form_Type__c) && feSub.Positive_Form_Type__c == FrontEndSubmissionHandler.NOD_REQUEST_CALL) ? TimeUtils.setTimezoneBasedReadyToDialTime(auState,feSub.Preferred_Call_Date_Time_Local__c) : (feSub.Preferred_Call_Date_Time_Local__c != NULL) ? feSub.Preferred_Call_Date_Time_Local__c : TimeUtils.setTimezoneBasedReadyToDialTime(auState,null); //27/09/2019 - extra/task25953355-NODFlowV3SFAutomations
                    Support_Request__c srCallRequest = SupportRequestGateway.initSRCallRequest(COMMPLSREQUESTCALLSRRT,feSub.Case__c,LFPCALLREASON,readyToDialTime,feSub.Case__r.Partition__c); 
                    callRequestSRListToIns.add(srCallRequest);    
                }
            }
        }
        
        // extra/task26890354-InitialNVMCallRoutingRebuild - Logic to support SR Call Request on Case 
        if(sObjName == CASEOBJAPINAME){
            System.debug('Create SR Call Request for '+CASEOBJAPINAME);
            List<Case> caseToUpdate = new List<Case>();
            if(TriggerFactory.trigSet.Enable_Case_NVM_Timezone_based_Routing__c){
                for(Case cse : [SELECT Id, Lead_Bucket__c, Channel__c, Latest_Frontend_Submission__c, Partition__c,State_Code__c FROM Case WHERE Id IN: params AND Lead_Bucket__c != 'LFP Flow' AND Partition__c =: POSITIVEPARTITION]){ //Exclude LFP
                    // If Case State Code is blank, set to SA
                    String auState = (String.isBlank(cse.State_Code__c)) ? DEFAULTSTATE : cse.State_Code__c; 
                    Datetime readyToDialTime = TimeUtils.setTimezoneBasedReadyToDialTime(auState,null); //changed to set to default.
                    Support_Request__c srCallRequest = SupportRequestGateway.initSRCallRequest(COMMPLSREQUESTCALLSRRT,cse.Id,INITIALCALL,readyToDialTime,cse.Partition__c); 
                    srCallRequest.Call_Request_Log__c = (String.isBlank(cse.State_Code__c)) ? 'Case State Code is blank, Set to SA' : 'Case State Code is '+cse.State_Code__c;
                    
                    if(TimeUtils.callNow) {
                        srCallRequest.Urgency__c = 'Urgent';
                        // cse.Ready_To_Dial_Time__c = readyToDialTime; // moved to process builder
                        srCallRequest.Status__c = 'Closed';
                        srCallRequest.Stage__c = 'Actioned';
                        caseToUpdate.add(cse);
                    }
                    else{
                        srCallRequest.Urgency__c = 'Scheduled';
                    }

                    callRequestSRListToIns.add(srCallRequest);  
                }
            }
            if(caseToUpdate.size() > 0) Database.update(caseToUpdate);
        }
        return callRequestSRListToIns;
    }
}