@isTest
private class Case_CallbackNewCase_Test {

    @testsetup static void setup(){
        
        Case cse = new Case(
            Status = 'Closed',
            Stage__c = 'Lost',
            Partition__c = 'Positive',
            On_Hold__c = 'Callback',
            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonConstants.CASE_RT_N_CONS_AF).getRecordTypeId() //n: Consumer - Asset Finance
        );
        insert cse;

        CaseComment cseComment = new CaseComment(
            CommentBody = 'Test Comment',
            ParentId = cse.Id,
            IsPublished = true
        );
        insert cseComment;

        Account acc = new Account(
            FirstName = 'testf',
            LastName = 'testl',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(CommonConstants.PACC_RT_I).getRecordTypeId() //nm: Individual
        );
        insert acc;

        Role__c role = new Role__c(
            Account__c = acc.Id,
            Case__c = cse.Id,
            RecordTypeId = Schema.SObjectType.Role__c.getRecordTypeInfosByName().get(CommonConstants.ROLE_RT_APP_INDI).getRecordTypeId(), //Applicant - Individual
            Primary_Contact_for_Application__c = true
        );
        insert role;

        Role_Address__c raddr = new Role_Address__c(
            Role__c = role.Id
        );
        insert raddr;

    }

    static testmethod void testCallbackNewCase(){

        Case cse = [Select Id, CaseNumber From Case];
        list <Role__c> rolelist = [Select Id, Name From Role__c];
        list <Role_Address__c> roleAddrList = [Select Id, Name From Role_Address__c];
        list <Id> cseIDs = new list <Id>();
        cseIDs.add(cse.Id);

        Test.startTest();
            Case_CallbackNewCase.createNewCaseOnCallback(cseIDs);
        Test.stopTest();

    }

}