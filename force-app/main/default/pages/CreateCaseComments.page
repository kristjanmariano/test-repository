<!--
//@author: Jesfer Baculod - Positive Group
//@history: 08/24/17 - Created
            08/25/17 - Updated
            10/17/17 - Updated, changed comment link action to open CaseCommentList page
            07/02/18 - JBACULOD - Fixed Loading on Case Create, added actionStatus
            07/16/19 - JBACULOD - Extended Case Comment input box and replace Comment list with button and Latest Comment
            07/18/19 - JBACULOD - Added Create, View Case Comments button on Top
//@description: VF page for creating Case Comments on a specific object  - (page should be included as a console component on page layouts)
-->
<apex:page showHeader="false" controller="CreateCaseCommentsCC" >
    <head>
        <meta charset="utf-8" />
          <meta http-equiv="x-ua-compatible" content="ie=edge" />
          <title>Case Comments</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <!-- Import the Design System style sheet -->
          <apex:stylesheet value="{!URLFOR($Resource.SLDS, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" />
          <style>
                #overlay{
                    position: fixed; /* Sit on top of the page content */
                    display: none; /* Hidden by default */
                    width: 100%; /* Full width (cover the whole page) */
                    height: 100%; /* Full height (cover the whole page) */
                    top: 0; 
                    left: 0;
                    right: 0;
                    bottom: 0;
                    background-color: rgba(255,255,255,0.5); /* rgba(0,0,0,0.2); Black background with opacity */
                    z-index: 3; /* Specify a stack order in case you're using a different order for other elements */
                    cursor: pointer; /* Add a pointer on hover */
                }

                #ovdiv{
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    font-size: 50px;
                    color: white;
                    transform: translate(-50%,-50%);
                    -ms-transform: translate(-50%,-50%);
                }
          </style>
    </head>

     <apex:includeScript value="/support/console/39.0/integration.js"/>
     <apex:includeScript value="/soap/ajax/39.0/connection.js" />
     <apex:includeScript value="/soap/ajax/39.0/apex.js" />

     <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en">
     <div class="PLS">
        <div class="slds-page-header">
          <div class="slds-media">
            <div class="slds-media__figure">
              <span class="slds-icon_container slds-icon-standard-{!LOWER(objType)}" title="{!objType}">
                <svg class="slds-icon" aria-hidden="true">
                  <use xlink:href="{!URLFOR($Resource.SLDS,'/assets/icons/standard-sprite/svg/symbols.svg#' & LOWER(objType))}"></use>
                </svg>
              </span>
            </div>
            <div class="slds-media__body">
              <h1 class="slds-page-header__title slds-truncate slds-align-middle" style="position:relative; top: 5px;" title="{!ObjType} Comments">Case Comments</h1>
            </div>
          </div>
        </div>
     </div>

    <apex:form id="mainFrm">
        <apex:actionFunction name="saveComment" action="{!saveComment}" status="status1" id="btnCreate" reRender="mainFrm, opScripts" oncomplete="if('{!withErrors}' == 'false') refreshcurrentTab();" />
        <apex:actionFunction name="refreshComments" action="{!refreshComments}" reRender="mainFrm" />
        <apex:actionStatus id="status1" onstart="document.getElementById('overlay').style.display = 'block';" onstop="document.getElementById('overlay').style.display = 'none'; if ('{!saveSuccess}' == 'true') document.getElementById('overlay').style.display = 'block'; " style="align:left;" />

        <div id="overlay">
            <div id="ovdiv"><img src="/img/loading.gif" /></div>
        </div>

        <apex:outputpanel rendered="{!IF(ISNULL(cseID), true,false)}">
            <apex:pageMessage summary="Create Case to add Case Comments" severity="INFO" strength="2" rendered="{!IF(ISNULL(cseID), true,false)}" />
        </apex:outputpanel>

        <apex:outputpanel rendered="{!IF(ISNULL(cseID), false,true)}" >
            <apex:pageBlock mode="detail">
                <apex:pageMessage summary="Case Comment added" severity="confirm" strength="2" rendered="{!saveSuccess}" />
                <apex:pageMessages />
                <!-- <apex:pageBlockButtons location="bottom"> -->
                    <!-- <apex:commandButton value="Create" action="{!saveComment}" status="status1" id="btnCreate" reRender="mainFrm, opScripts" oncomplete="if('{!withErrors}' == 'false') refreshcurrentTab();" /> -->
                    <!-- <apex:outputlabel value="Create" styleclass="btn" style="padding: 4px;" onclick="validateSave();" />
                </apex:pageBlockButtons> -->
                <div class="pbHeader" style="padding:10px;">
                    <center>
                        <apex:outputlabel value="Create" styleclass="btn" style="padding: 4px;" onclick="validateSave();" />
                        <apex:outputlabel value="View Case Comments" styleclass="btn" style="padding: 4px;"  onclick="openComment('{!cseID}');" rendered="{!IF(comments.empty,false,true)}" />
                    </center>
                </div>
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Public" />
                        <apex:inputCheckbox value="{!comment.IsPublished}" id="icpub" />
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:inputTextArea value="{!comment.CommentBody}" id="itbody" rows="34" style="width: 100%;"  />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                <div class="pbHeader" style="padding:10px;">
                    <center>
                        <apex:outputlabel value="Create" styleclass="btn" style="padding: 4px;" onclick="validateSave();" />
                        <apex:outputlabel value="View Case Comments" styleclass="btn" style="padding: 4px;"  onclick="openComment('{!cseID}');" rendered="{!IF(comments.empty,false,true)}" />
                    </center>
                </div>
            </apex:pageBlock>

            <apex:pageBlock mode="detail" >
                    <apex:pageBlockSection columns="1" rendered="{!IF(NOT(ISBLANK(latestcomment)), true,false)}">
                        <apex:pageBlockSectionItem >
                            <apex:outputLabel value="Latest Comment" />
                            <apex:outputLabel value="{!LEFT(latestcomment,50)} {!IF(LEN(latestcomment) >=50,'...','')}" />
                        </apex:pageBlockSectionItem>
                    </apex:pageBlockSection>
                    <!-- <br/>
                    <center>
                        <apex:outputlabel value="View Case Comments" styleclass="btn" style="padding: 4px;"  onclick="openComment('{!cseID}');" rendered="{!IF(comments.empty,false,true)}" />
                    </center> -->
            </apex:pageBlock>

            <!-- <apex:pageBlock title="Case Comments" id="pbcomments">
                    <apex:outputPanel rendered="{!IF(comments.empty,false,true)}">
                        <apex:pageBlockTable value="{!comments}" var="com" width="100%">
                            <apex:column headerValue="Comment">
                                <apex:commandLink onclick="openComment('{!com.ParentId}','{!JSENCODE(com.CreatedBy.Name)}');" rerender="mainFrm">{!LEFT(com.CommentBody,50)} {!IF(LEN(com.CommentBody) >=50,'...','')}</apex:commandLink>
                            </apex:column>
                            <!-- <apex:column headerValue="Public" value="{!com.IsPublished}" />
                            <apex:column headerValue="Created By">
                                {!com.CreatedBy.Name} &nbsp;<apex:outputText value="{!com.CreatedDate} " />
                            </apex:column> -->
                        <!-- </apex:pageBlockTable>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!IF(comments.empty,true,false)}">
                        No records to display
                    </apex:outputPanel>
            </apex:pageBlock> -->
        </apex:outputpanel>
    </apex:form>

    <apex:outputpanel id="opScripts">
        <script type="text/javascript">

            var callback = function (result) {};
            var curTabID;

            getFocusedPrimaryTabId();
            getFocusedSubtabId();
            setSidebarVisible(true);

            function validateSave(){
                saveComment();
            }
            
            function setSidebarVisible(show) {
                sforce.console.setSidebarVisible(show,curTabID,sforce.console.Region.RIGHT,callback);
            }

            function refreshcurrentTab(){
                getFocusedSubtabId();
                getFocusedPrimaryTabId();
            }

            function getFocusedSubtabId(){
                sforce.console.getFocusedSubtabId(showTabId);
            }

            function getFocusedPrimaryTabId() {
                sforce.console.getFocusedPrimaryTabId(showTabId);
            }

            var showTabId = function showTabId(result) {
                //Display the tab ID
                console.log('Tab ID: ' + result.id);
                curTabID = result.id;
                console.log('Primary Tab:'+curTabID);
                console.log('saveSuccess: {!saveSuccess}');
                if ('{!saveSuccess}' == 'true'){ 
                    sforce.console.refreshSubtabById(curTabID, true, function(result){
                            //sforce.console.onEnclosingTabRefresh(onRefreshEvent);
                            console.log(result);
                            if (result.success == false){
                                location.href = '/apex/CreateCaseComments?id=' + curID; //Open Case Comments Sidebar
                            }
                    });
                }
            };

            var commID;
            var commParID;
            var commURL;
            var commBody;
            var curID = '{!cseID}';
            var onRefresh = false;
            function openComment(recParID){ //recID, recParID, recName
                //commID = recID;
                commParID = recParID;
                commBody = 'Comments'; //recName + '`s comment';
                //commURL = '/' + commID + '/e?parent_id='+ commParID + '&isdtp=vw';
                commURL = '/apex/CaseCommentsList?id='+commParID +'&isdtp=vw';
                sforce.console.getEnclosingPrimaryTabId(openSubtab);
            }

            var openSubtab = function openSubtab(result) {
                var primaryTabId = result.id;
                console.log(commURL);
                var s = sforce.console.openSubtab(primaryTabId , commURL, false, commBody, null,openSuccess);
            };

            var openSuccess = function openSuccess(result){
                var subtabId = result.id;
                console.log(subtabId);
                sforce.console.focusSubtabById(subtabId);
            }

            function getFocusedSubtabObjectId() {
                sforce.console.getFocusedSubtabObjectId(showObjectId);
            }

            var showObjectId = function showObjectId(result) {
                console.log('Primary Tab:'+curTabID);
                // Display the object ID
                console.log('Object ID: ' + result.id);
                console.log('onRefresh: ' + onRefresh);
                console.log('cseID: {!cseID}');
                var resID = result.id;
                if (resID.startsWith('500')){ //SubTab is a Case
                    if (!onRefresh || '{!cseID}' == ''){
                        console.log('test');
                        location.href = '/apex/CreateCaseComments?id='+resID; //Open Case Comments Sidebar
                        setSidebarVisible(true);
                    }
                }
                else if(resID.startsWith('006') || resID.startsWith('00Q') ){ //Focused SabTab is an Opportunity / Lead
                    location.href = '/apex/CreateNotes?id=' + resID; //Open Notes Sidebar
                    setSidebarVisible(true);
                }
                else{
                    location.href = '/apex/ConsoleSideBarParentVF';
                    setSidebarVisible(false);
                }
            };

            var onFocusEvent = function (result){
                    console.log('Case Comment in focus');
                    refreshComments();
                    onRefresh = false;
                    getFocusedSubtabObjectId();
            };
            
            var onRefreshEvent = function (result){
                    console.log('Record Tab refresh');
                    //refreshNotes();
                    //getEnclosingPrimaryTabObjectId();
                    getFocusedSubtabObjectId();
                    onRefresh = true;
                    //getFocusedPrimaryTabObjectId();
            };

            sforce.console.onFocusedSubtab(onFocusEvent);
            sforce.console.onEnclosingTabRefresh(onRefreshEvent);
            console.log(curID);

        </script>
    </apex:outputpanel>


    </html>
    
</apex:page>