<!--
  @Page Name          : NewReference.page
  @Description        : 
  @Author             : jesfer.baculod@positivelendingsolutions.com.au
  @Group              : 
  @Last Modified By   : jesfer.baculod@positivelendingsolutions.com.au
  @Last Modified On   : 09/10/2019, 2:15:06 pm
  @Modification Log   : 
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0    16/07/2019, 1:00:20 pm   jesfer.baculod@positivelendingsolutions.com.au     Initial Version
  1.1    09/10/2019, 2:14:47 pm   jesfer.baculod@positivelendingsolutions.com.au     Required Reference Date
-->
<apex:page tabStyle="Reference2__c" controller="NewReferenceController">
    <apex:includeScript value="/support/console/30.0/integration.js"/>
    <style>
        #DupeFoudOverlay{
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0; 
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.2); /*Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
        }

        #overlay{
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0; 
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,0.5); /* rgba(0,0,0,0.2); Black background with opacity */
            z-index: 3; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
        }

        #ovdiv{
            position: absolute;
            top: 50%;
            left: 50%;
            font-size: 50px;
            color: white;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }

        .ovdiv2{
            position: absolute;
            top: 50%;
            left: 50%;
            width: 60%;
            background-color: #EEE5E3;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
        }
    </style>

    <apex:sectionHeader title="Reference Edit" subtitle="New Reference" />
    <apex:form id="mainFrm">

        <div id="overlay">
            <div id="ovdiv"><img src="/img/loading.gif" /></div>
        </div>
        <apex:actionStatus id="status1" onstart="document.getElementById('overlay').style.display = 'block';" onstop="document.getElementById('overlay').style.display = 'none'; " style="align:left;" />
        <apex:actionFunction name="afCancelDupeSel" action="{!cancelSelectDupe}" status="status1" rerender="mainFrm" oncomplete="document.getElementById('DupeFoudOverlay').style.display = 'none';" />
        <apex:actionFunction name="afSaveReference" action="{!saveReference}" status="status1" rerender="mainFrm" oncomplete="afterSave(false);" >
            <apex:param name="afSaveRef" value="" assignTo="{!saveAndNew}" />
        </apex:actionFunction>
        <apex:actionFunction name="afSaveReferenceAndNew" action="{!saveReference}" status="status1" rerender="mainFrm" oncomplete="afterSave(true);" >
            <apex:param name="afSaveRef" value="" assignTo="{!saveAndNew}" />
        </apex:actionFunction>
        <apex:actionFunction name="afNewReference" action="{!newReference}" status="status1" rerender="mainFrm" />
        <apex:actionFunction name="afEditReference" action="{!editReference}" status="status1" rerender="mainFrm" >
            <apex:param name="afCurRefID" value="" assignTo="{!curRefID}" />
        </apex:actionFunction>
        <apex:actionFunction name="afCancel" action="{!refcancel}" status="status1" rerender="mainFrm" />

        <apex:outputpanel rendered="{!IF(refWrlist.size == 0, false, true)}" >
            <apex:pageBlock >
                <apex:pageBlockButtons location="top">
                    <a style="text-decoration: none;" href="#divRef"><apex:outputlabel styleclass="btn" value="Add New Reference" onclick="afNewReference();" style="padding:4px;" /></a>
                    <apex:commandButton value="Exit" onclick="exit(); return false;" />
                    <!-- <apex:outputlabel styleclass="btn" value="Exit" onclick="exit();" style="padding:4px;" /> -->
                </apex:pageBlockButtons>
                <apex:pageBlockSection title="References" columns="1">
                    <apex:pageMessage severity="INFO" strength="1" summary="Select a Reference ID to update an existing Reference" />
                    <apex:pageBlockTable value="{!refWrlist}" var="exrefWr">
                        <apex:column headerValue="Reference ID" >
                            <a href="#" onclick="selRef('{!exrefWr.ref.Id}'); "> <apex:outputText value="{!exrefWr.ref.Name}" /></a>
                        </apex:column>
                        <apex:column headerValue="Reference Type">
                            <apex:outputText value="{!exrefWr.ref.Reference_Type__c}" />
                        </apex:column>
                        <apex:column headerValue="Reference Account" value="{!exrefWr.ref.Reference_Account__c}" />
                        <apex:column headerValue="Relationship" value="{!exrefWr.ref.Relationship__c}" />
                        <apex:column headerValue="Address" value="{!exrefWr.ref.Full_Address__c}" />
                        <apex:column headerValue="Phone" value="{!exrefWr.ref.Phone1__c}" />
                    </apex:pageBlockTable>
                </apex:pageBlockSection>
            </apex:pageBlock>
        </apex:outputpanel>

        <apex:pageBlock id="pbNewRef" mode="edit" rendered="{!showReferenceEdit}">
            <apex:pageMessages rendered="{!hasErrors}" />
            <apex:pageBlockButtons >
                <apex:commandButton value="Save" onclick="afSaveReference(false); return false;" />
                <apex:commandButton value="Save & New" onclick="afSaveReferenceAndNew(true); return false;" />
                <apex:commandButton value="Cancel" onclick="exit(); return false;" rendered="{!IF(refWrlist.size == 0, true, false)}" />
                <apex:commandButton value="Cancel" onclick="afCancel(); return false;" rendered="{!IF(refWrlist.size == 0, false, true)}" />
                <!-- <apex:outputlabel styleclass="btn" tabIndex="9" onclick="afSaveReference(false);" style="padding:4px;">Save</apex:outputlabel>
                <apex:outputlabel styleclass="btn" tabIndex="10" value="Save & New" onclick="afSaveReferenceAndNew(true);" style="padding:4px;"  />
                <apex:outputlabel styleclass="btn" tabIndex="11" onclick="exit();" style="padding:4px;" rendered="{!IF(refWrlist.size == 0, true, false)}">Cancel</apex:outputlabel>
                <apex:outputlabel styleclass="btn" tabIndex="12" onclick="afCancel();" style="padding:4px;" rendered="{!IF(refWrlist.size == 0, false, true)}">Cancel</apex:outputlabel>  -->
            </apex:pageBlockButtons>

            <div id="divRef">
            <apex:outputpanel id="opRefDetails">
                <apex:pageBlockSection columns="1" title="Reference Details">
                    <apex:outputField value="{!refWr.ref.Name}" rendered="{!IF(refWR.ref.Id == null, false,true)}" />
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Reference Type" />
                        <apex:inputField value="{!refWr.ref.RecordTypeId}" >
                            <apex:actionSupport event="onchange" rerender="mainFrm" status="status1" action="{!setIsBusiness}"/>
                        </apex:inputField>
                    </apex:pageBlockSectionItem>
                    <apex:inputField value="{!refWr.ref.Is_Business__c}" rendered="{!AND(refRTNameByIdMap[refWr.ref.RecordTypeId] != 'Personal')}">
                        <apex:actionSupport event="onchange" rerender="mainFrm" status="status1" />
                    </apex:inputField>
                    <apex:inputField value="{!con.FirstName}" /> 
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Last Name" />
                        <apex:outputpanel layout="block" styleclass="{!IF(refWr.ref.Is_Business__c == true, '', 'requiredInput')}" >
                            <div class="{!IF(refWr.ref.Is_Business__c == true, '', 'requiredBlock')}"></div>
                            <apex:inputText value="{!conLastName}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem> 
                    <apex:inputField value="{!refWr.ref.Email__c}" rendered="{!IF(refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Accountant',true,false)}"/>
                    <apex:pageBlockSectionItem rendered="{!IF(refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Accountant',false,true)}"/>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Phone" />
                        <apex:outputpanel layout="block" styleclass="requiredInput" >
                            <div class="requiredBlock"></div>
                            <apex:inputField value="{!con.MobilePhone}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:inputField value="{!refWr.ref.Relationship__c}" />
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Reference Date" />
                        <apex:outputpanel layout="block" styleclass="requiredInput" >
                            <div class="requiredBlock"></div>
                            <apex:inputField value="{!refWr.ref.Reference_Date__c}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem rendered="{!IF(OR(refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Landlord', refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Accountant', refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Business'),true,false)}">
                        <apex:outputLabel value="Business Name" />
                        <apex:outputpanel layout="block" styleclass="{!IF(refWr.ref.Is_Business__c == false, '', 'requiredInput')}" >
                            <div class="{!IF(refWr.ref.Is_Business__c == false, '', 'requiredBlock')}"></div>
                            <apex:inputField value="{!refWr.ref.Business_Name__c}" rendered="{!IF(OR(refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Landlord', refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Accountant', refRTNameByIdMap[refWr.ref.RecordTypeId] == 'Business'),true,false)}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
            </apex:outputpanel>

            <apex:outputpanel id="opAddress">
                <apex:pageBlockSection columns="2" title="Address Information">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Street Number" />
                        <apex:outputpanel layout="block" styleclass="requiredInput" >
                            <div class="requiredBlock"></div>
                            <apex:inputField value="{!refWr.addr.Street_Number__c}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Location" />
                        <apex:outputpanel layout="block" styleclass="requiredInput" >
                            <div class="requiredBlock"></div>
                            <apex:inputField value="{!refWr.addr.Location__c}">
                                    <apex:actionSupport event="onchange" status="status1" action="{!populateAddresswithLocation}" rerender="opAddress" />
                            </apex:inputField>
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Street Name" />
                        <apex:outputpanel layout="block" styleclass="requiredInput" >
                            <div class="requiredBlock"></div>
                            <apex:inputField value="{!refWr.addr.Street_Name__c}"  />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Suburb" />
                        <apex:outputpanel >
                            <apex:outputField value="{!refwr.addr.Suburb__c}" rendered="{!IF(refwr.addr.Location__c != null, true,false)}"/>
                            <apex:outputText style="color: red;" value="-Select Location-" rendered="{!IF(refwr.addr.Location__c != null, false,true)}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Street Type" />
                        <apex:outputpanel layout="block" styleclass="requiredInput" >
                            <div class="requiredBlock"></div>
                            <apex:inputField value="{!refWr.addr.Street_Type__c}"  />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="State" />
                        <apex:outputpanel >
                            <apex:outputField value="{!refwr.addr.State__c}" rendered="{!IF(refwr.addr.Location__c != null, true,false)}"/>
                            <apex:outputText style="color: red;" value="-Select Location-" rendered="{!IF(refwr.addr.Location__c != null, false,true)}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem />
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Postcode" />
                        <apex:outputpanel >
                            <apex:outputField value="{!refwr.addr.Postcode__c}" rendered="{!IF(refwr.addr.Location__c != null, true,false)}"/>
                            <apex:outputText style="color: red;" value="-Select Location-" rendered="{!IF(refwr.addr.Location__c != null, false,true)}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                    <apex:pageBlockSectionItem />
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Country" />
                        <apex:outputpanel >
                            <apex:outputField value="{!refwr.addr.Country__c}" rendered="{!IF(refwr.addr.Location__c != null, true,false)}"/>
                            <apex:outputText style="color: red;" value="-Select Location-" rendered="{!IF(refwr.addr.Location__c != null, false,true)}" />
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
            </apex:outputpanel>
            </div>
            
        </apex:pageBlock>

        <div id="DupeFoudOverlay">
            <apex:outputpanel id="opAccDupeFound" styleclass="ovdiv2" layout="block">
                <div style="padding: 10px;" >
                    <apex:pageBlock mode="edit" title="Reference Account">
                        <apex:pageMessage severity="WARNING" summary="Duplicate Account found. Please select Reference Account" />
                        <apex:pageBlockSection title="Confirm" columns="1">
                            <apex:pageBlockSectionItem >
                                <apex:outputLabel value="Reference Account" />
                                <apex:selectList value="{!selRefAccID}" size="1">
                                    <apex:selectOptions value="{!exDupeAccounts}" />
                                </apex:selectList>
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                    </apex:pageBlock>
                    <hr/>
                    <p align="middle">
                        <apex:outputlabel styleclass="btn" onclick="savePath();" style="padding:4px;">Save</apex:outputlabel>
                        <apex:outputlabel styleclass="btn" onclick="afCancelDupeSel();" style="padding:4px;">Cancel</apex:outputlabel>
                    </p>
                </div>
            </apex:outputpanel>
        </div>


        <script>
            var closeSubtab = function closeSubtab(result) {
                var tabId = result.id;
                console.log(tabId);
                sforce.console.closeTab(tabId);
                };
            
            var showTabId = function showTabId(result){
                    var tabId = result.id;
                    console.log(tabId);
                    sforce.console.refreshPrimaryTabById(tabId , true);
            };

            function selRef(refID){
                afEditReference(refID);
            }

            function afterSave(sn){
                console.log('selRecAccID: {!selRefAccID}');
                console.log('hasErrors: {!hasErrors}');
                console.log('dupeAccFound: {!dupeAccFound}');
                console.log('saveSuccess: {!saveSuccess}');
                if (sn){ //Save and New
                    console.log('a');
                    if ('{!hasErrors}' == 'false'){ 
                        if ('{!dupeAccFound}' == 'true' && '{!saveSuccess}' == 'false'){ //Confirm Reference Account
                            console.log('a1');
                            document.getElementById('DupeFoudOverlay').style.display = 'block';
                        }
                        else {
                            console.log('a2');
                            document.getElementById('DupeFoudOverlay').style.display = 'none';
                            saveandnewRedirect(); 
                        }
                    }
                }
                else{ //Save
                    console.log('b');
                    if ('{!hasErrors}' == 'false'){  
                        if ('{!dupeAccFound}' == 'true' && '{!saveSuccess}' == 'false'){ //Confirm Reference Account
                            console.log('b1');
                            document.getElementById('DupeFoudOverlay').style.display = 'block';
                        }
                        else {
                            console.log('b2');
                            document.getElementById('DupeFoudOverlay').style.display = 'none';
                            saveRedirect(); 
                        }
                    }
                }
            }

            function savePath(){
                if ('{!saveAndNew}' == 'true') afSaveReferenceAndNew(true);
                else afSaveReference(false);
            }
            
            function saveRedirect(){
                if (sforce.console.isInConsole()){ 
                    sforce.console.getEnclosingPrimaryTabId(showTabId);
                    sforce.console.setTabTitle('{!sveRef.Name}');
                    window.location.href = '/{!refWr.ref.Id}?isdtp=vw';
                }
                else window.location.href = '/{!refWr.ref.Id}';
            }

            function saveandnewRedirect(){
                if (sforce.console.isInConsole()){ 
                    sforce.console.getEnclosingPrimaryTabId(showTabId);
                    window.location.href = '/apex/NewReference?id={!curID}&isdtp=vw';
                }
                else window.location.href = '/apex/NewReference?id={!curID}';
            }

            function exit(){
                console.log(sforce.console.isInConsole());
                if (sforce.console.isInConsole()){
                    console.log('a');
                    console.log(sforce.console.getEnclosingTabId(closeSubtab));
                    sforce.console.getEnclosingPrimaryTabId(showTabId);
                    sforce.console.getEnclosingTabId(closeSubtab);
                    //setSidebarVisible(false);
                }
                else{
                    console.log('b');
                    window.location.href = '/{!refWr.ref.Id}';
                }
            }

            function setTabTitle() {
                console.log('IsinConsole:'+sforce.console.isInConsole());
                if (sforce.console.isInConsole()){
                    sforce.console.setTabTitle('New Reference');
                }
            }

            var previousOnload = window.onload;        
            window.onload = function() { 
                if (previousOnload) { 
                    previousOnload();
                }
                if (sforce.console.isInConsole()){                
                    setTimeout('setTabTitle()', '500'); 
                }
            }
        </script>

    </apex:form>
</apex:page>