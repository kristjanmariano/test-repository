<!--
  @Page Name          : NewPartnerPageLightning.page
  @Description        : To invoke New Partner Lightning component (works in classic)
  @Author             : Rex.David
  ==============================================================================
  Ver         Date                     Author      		      Modification
  ==============================================================================
  1.0    30/05/2019, 2:19 pm          Rex.David             Initial Version
  1.1    18/06/2019, 7:16 am          Rex.David             extra/task24746570-NewPartnerPageLightning 
                                                            - Added css for helptext workaround https://www.salesforcexyz.com/salesforce/lightning-helptext-component-in-lighting-out/
  1.2    24/06/2019, 2:48 pm          Rex.David             extra/task24746570-NewPartnerPageLightning - Added Partner Branch creation via custom button
  1.3    12/09/2019, 1:09 pm          Jesfer.Baculod        extra/task25900826-LeadToPartner - Added parameters for converting Nodifi Partner Lead to a Partner record
-->
<apex:page tabStyle="Partner__c" standardController="Partner__c" standardStylesheets="true" docType="html-5.0">
    <!--Hello Lightning!-->
    <apex:includeLightning />
    <apex:includeScript value="/support/console/45.0/integration.js"/>
    <script src="/soap/ajax/43.0/connection.js"/>    
    <div id="lightning" />
    <style type="text/css">
        .slds-popover__body,
        .slds-popover__footer,
        .slds-popover__header {
            position: relative;
            padding: .5rem .75rem;
            word-wrap: break-word
        }

        .slds-popover {
            position: relative;
            border-radius: .25rem;
            width: 20rem;
            min-height: 2rem;
            z-index: 6000;
            background-color: #fff;
            display: inline-block;
            box-shadow: 0 2px 3px 0 rgba(0, 0, 0, .16);
            border: 1px solid #d9dbdd
        }

        .slds-popover--tooltip,
        .slds-popover_tooltip {
            width: auto;
            max-width: 20rem;
            background: #16325c;
            border: 0
        }
        .slds-popover--tooltip .slds-popover__body,
        .slds-popover_tooltip .slds-popover__body {
            font-size: .75rem;
            color: #fff
        }

        .slds-fall-into-ground {
            visibility: hidden;
            opacity: 0;
            transform: translate(0, 0);
            transition: opacity .1s linear, visibility .1s linear, transform .1s linear;
            will-change: transform
        }

        .slds-nubbin--bottom-left:before,
        .slds-nubbin_bottom-left-corner:before,
        .slds-nubbin_bottom-left:before {
            width: 1rem;
            height: 1rem;
            position: absolute;
            transform: rotate(45deg);
            content: '';
            background-color: inherit;
            bottom: -.5rem;
            margin-left: -.5rem
        }

        .slds-nubbin--bottom-left:after,
        .slds-nubbin--bottom-left:before,
        .slds-nubbin_bottom-left-corner:after,
        .slds-nubbin_bottom-left-corner:before,
        .slds-nubbin_bottom-left:after,
        .slds-nubbin_bottom-left:before {
            left: 1.5rem;
            top: 100%;
            margin-top: -.5rem
        }

        .slds-nubbin--bottom-left:after,
        .slds-nubbin_bottom-left-corner:after,
        .slds-nubbin_bottom-left:after {
            box-shadow: 2px 2px 4px 0 rgba(0, 0, 0, .16);
            z-index: -1
        }

        .slds-popover :last-child {
            margin-bottom: 0
        }
    </style>
    <script>
        var recId;
        var title;
        var branchParentId;
        var page = "{!$CurrentPage.parameters.page}";
        // alert("{!Partner__c.Id}");
        var bypassRT;
        var pId;
        var pTabId;
        var isPartner;
        var isRelationship;
        var isPartnerBranch;

        if("{!Partner__c.Id}" == ''){   
            bypassRT = false;
            pId = '';
        }
        else {
            bypassRT = true;
            pId = "{!Partner__c.Id}";
        }
        // alert(bypassRT);
        
        if( page == 'PC') title = 'New Partner Contact';
        else if (page == 'PB'){
            title = 'New Partner Branch';
            branchParentId = pId;
        } 
        else title = 'New Partner';

        console.log('VF PAGE - bypassRT = ',bypassRT);
        console.log('VF PAGE - Partner__c.RecordType.Name = ',"{!Partner__c.RecordType.Name}");
        console.log('VF PAGE - Partner__c.RecordTypeId  = ',"{!Partner__c.RecordTypeId}");
        console.log('VF PAGE - recordId = ',pId);
        console.log('VF PAGE - page = ',"{!$CurrentPage.parameters.page}");
        console.log('VF PAGE - leadId = ',"{!$CurrentPage.parameters.leadId}");
        console.log('VF PAGE - FromLead = ',"{!$CurrentPage.parameters.FromLead}");
        console.log('VF PAGE - title = ',title);

        sforce.console.setTabTitle(title);
        sforce.console.setTabLink();
        
        if(page != 'PB'){
            // For creating Partner and Partner Contact 
            $Lightning.use("c:NewPartnerApp", function() {
                $Lightning.createComponent( "c:NewPartnerCmp",
                                            {   "selRTName" : "{!Partner__c.RecordType.Name}",
                                                "selValidPartnerRTId" : "{!Partner__c.RecordTypeId}",
                                                "recordId" : pId,
                                                "byPassRTSelection" : bypassRT,
                                                "page" : "{!$CurrentPage.parameters.page}",
                                                "leadId" : "{!$CurrentPage.parameters.leadId}",
                                                "fromLead" : "{!$CurrentPage.parameters.fromLead}"
                                            },
                                                "lightning",
                function(cmp) { 
                    console.log('NewPartnerApp -> Partner OR Partner Contact.');
                    $A.eventService.addHandler({
                    event: 'force:navigateToSObject',
                        handler: function(event) {
                            recId = event.Zb.recordId;
                            isPartner = recId.startsWith("a48");
                            isRelationship = recId.startsWith("a4F");
                            console.log('recId   -- ',recId);
                            console.log('isPartner a48 -- ',isPartner);
                            console.log('isRelationship a4F -- ',isRelationship);
                            // alert(event.getParams().Objtype);
                            sforce.console.getEnclosingPrimaryTabId(closeSubtab);
                            
                        }
                    });
                });
            });
        }
        else if(page == 'PB'){
            // For creating Partner Branch
            $Lightning.use("c:NewPartnerApp", function() {
                $Lightning.createComponent( "c:NewPartnerCmp",
                                            {   "branchParentId" : pId,
                                                "selRTName" : "{!Partner__c.RecordType.Name}",
                                                "byPassRTSelection" : bypassRT,
                                                "page" : "{!$CurrentPage.parameters.page}",
                                                "leadId" : "{!$CurrentPage.parameters.leadId}",
                                                "fromLead" : "{!$CurrentPage.parameters.fromLead}"
                                            },
                                                "lightning",
                function(cmp) { 
                    console.log('NewPartnerApp -> Partner Branch.');
                    $A.eventService.addHandler({
                    event: 'force:navigateToSObject',
                        handler: function(event) {
                            recId = event.Zb.recordId;
                            console.log('recId   -- ',recId);
                            isPartnerBranch = true;
                            console.log('isPartnerBranch  -- ',isPartnerBranch);
                            // console.log(event.Zb.Obj);
                            sforce.console.getEnclosingPrimaryTabId(closeSubtab);
                            
                        }
                    });
                });
            });
        }
        
        var closeSubtab = function closeSubtab(result) {
            pTabId = result.id;
            // alert(pTabId);
            if(isPartner){
                sforce.console.openPrimaryTab(pTabId, '/'+recId, true);
            }
            else if(isRelationship){
                sforce.console.getEnclosingTabId(closeSubtabOpenRecord);

            }
            else if(isPartnerBranch){
                sforce.console.getEnclosingTabId(closeSubtabOpenRecord);
            }
            // sforce.console.openSubtab(tabId , '/'+recId, true, null , tabId, openSuccess, 'customSubTab');
            //2/08 sforce.console.getEnclosingTabId(closeSubtabOpenRecord);
        };

        var openSuccess = function openSuccess(result) {
            //Report whether we succeeded in opening the subtab
            if (result.success == true) {
                console.log('subtab successfully opened');
            } else {
                console.log('subtab cannot be opened');
            }
        };

        function showToast() {
            sforce.one.showToast({
                "title": "Success!",
                "message": "Welcome to salesforce code crack.",
                "type": "success"
            });
            // console.log('showToast2');
        }
        
        var closeSubtabOpenRecord = function closeSubtabOpenRecord(result) {
            //Now that we have the tab ID, we can close it
            var tabId = result.id;
            
            if (page.length == 0){ 
                console.log('page is blank');
                sforce.console.openPrimaryTab(pTabId, '/'+recId, true);
            }  	
            else{
                console.log('page is NOT blank');
                sforce.console.refreshPrimaryTabById(pTabId , true);
                sforce.console.openSubtab(pTabId , '/'+recId, true, '', tabId, openSuccess, null);
            }
        };
        
    </script>
    
</apex:page>