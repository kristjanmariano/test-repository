trigger Trig_Lead on Lead (after insert, after update) {

	static boolean allowrun;
    if (!Test.isRunningTest()) allowRun = true;
    else allowRun = false;

	if (allowrun){ //Added due to issue on deployment 
		et4ae5.triggerUtility.automate('Lead');
	}
}