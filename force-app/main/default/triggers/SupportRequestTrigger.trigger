/** 
* @FileName: SupportRequestTrigger
* @Description: Main Trigger for Support_Request__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 18/10/18 RDAVID - feature/SupportRequestTrigger - Created Trigger
**/ 
trigger SupportRequestTrigger on Support_Request__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
    if (TriggerFactory.trigset.Enable_Triggers__c){        
		if (TriggerFactory.trigset.Enable_Support_Request_Trigger__c){            
			TriggerFactory.createHandler(Support_Request__c.sObjectType);
		}
	}
}