/*
    @Description: handlers all DML events on Lead
    @Author: Sri Babu, Jesfer Baculod - Positive Group 
    @History:
        11/29/17 - Updated, restructured LeadTrigger
*/
trigger LeadTrigger on Lead (before update, after update) {

	Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    
    if (trigset.Enable_Lead_Trigger__c){

    	if (trigger.isUpdate){
    		//call before update event triggers
    		if (trigger.isBefore){
    			if (LeadTriggerHandler.firstRunBU){
    				LeadTriggerHandler.onBeforeUpdate(trigger.oldMap, trigger.newMap);
    				LeadTriggerHandler.firstRunBU = false;
    			}
    		}
    		//call after update event triggers
    		//else if (trigger.isAfter){
    		else if (trigger.isAfter){
    			if (LeadTriggerHandler.firstRunAU){
    				LeadTriggerHandler.onAfterUpdate(trigger.oldMap, trigger.newMap);
    				LeadTriggerHandler.firstRunAU = false;
    			}
    		}
    	}

    }


}