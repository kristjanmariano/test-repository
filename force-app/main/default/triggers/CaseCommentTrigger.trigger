trigger CaseCommentTrigger on CaseComment (before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
    if (TriggerFactory.trigset.Enable_Triggers__c){        
		if (TriggerFactory.trigset.Enable_Case_Comment_Trigger__c){            
			TriggerFactory.createHandler(CaseComment.sObjectType);
		}
	}
}