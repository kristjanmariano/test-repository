trigger RemoveNullApplicant2 on Applicant_2__c (after insert, after update) {
    Applicant_2__c m = [select Drivers_Licence_Number__c FROM Applicant_2__c where id =:trigger.new[0].id];
    if(m.Drivers_Licence_Number__c == null){
        delete m;
    }
}