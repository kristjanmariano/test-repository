trigger RemoveNullEmployers on Employee__c (after insert, after update) {
    Employee__c m = [select Contact_Phone__c from Employee__c where id =:trigger.new[0].id];
    if(m.Contact_Phone__c == null){
        delete m;
    }
}