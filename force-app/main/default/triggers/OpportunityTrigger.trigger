/*
    @Description: handlers all DML events on Case
    @Author: Sri Babu, Jesfer Baculod - Positive Group 
    @History:
        11/2/17 - Updated, restructured OpportunityTrigger
*/
Trigger OpportunityTrigger on Opportunity (before update, before insert, after update, after Insert) {

    Trigger_Settings__c trigSet = Trigger_Settings__c.getInstance(UserInfo.getUserId());
    
    if (trigset.Enable_Opportunity_Trigger__c){

            if (trigger.isInsert){
                if (trigger.isBefore){
                    //call before insert event triggers
                    OpportunityTriggerHandler.onBeforeInsert(trigger.new);
                }
                else if (trigger.isAfter){
                    //call after insert event triggers
                    OpportunityTriggerHandler.onAfterInsert(trigger.new);   
                }
            }
            else if (trigger.isUpdate){
                if (trigger.isBefore){
                    if (OpportunityTriggerHandler.firstRunBU){ //prevents before update trigger to run multiple times
                        //call before update event triggers
                        OpportunityTriggerHandler.onBeforeUpdate(trigger.oldMap,trigger.newMap);
                        if (!Test.isRunningTest() && !trigSet.Bulk_Mode__c) OpportunityTriggerHandler.firstRunBU = false;
                    }
                }
                else if (trigger.isAfter){
                    if (OpportunityTriggerHandler.firstRunAU){ //prevents after update trigger to run multiple times
                        //call after update event triggers 
                        OpportunityTriggerHandler.onAfterUpdate(trigger.oldMap,trigger.newMap);
                        if (!Test.isRunningTest() && !trigSet.Bulk_Mode__c) OpportunityTriggerHandler.firstRunAU = false;
                    }
                }
            }
    }


/*
    Set<Id> commIds = new Set<Id>();
    Map<Id,Id>refIdsMap = new Map<Id,Id>();
   
    if(trigger.isBefore){
        for(Opportunity opp:Trigger.New){
            if(opp.Loan_Type2__c == 'Car'){
            opp.Loan_Type2__c = 'Car Loan';
            }
            else if(opp.Loan_Type2__c == 'Bike'){
            opp.Loan_Type2__c = 'Bike Loan';
            }
            else if(opp.Loan_Type2__c == 'Truck'){
            opp.Loan_Type2__c = 'Truck Loan';
            }
            else if(opp.Loan_Type2__c == 'Personal'){
            opp.Loan_Type2__c = 'Personal Loan';
            }
            else if(opp.Loan_Type2__c == 'Boat / Marine'){
            opp.Loan_Type2__c = 'Boat Loan';
            }
            else if(opp.Loan_Type2__c == 'Commercial Equipment' || opp.Loan_Type2__c == 'Construction Equipment'){
            opp.Loan_Type2__c = 'Equipment Loan';
            }
            else if(opp.Loan_Type2__c == 'Other'){
            opp.Loan_Type2__c = '-';
            }
            else if(opp.Loan_Type2__c == 'Trailer'){
            opp.Loan_Type2__c = 'Trailer Loan';
            }
            else if(opp.Loan_Type2__c == 'Caravan'){
            opp.Loan_Type2__c = 'Caravan Loan';
            }
            else if(opp.Loan_Type2__c == 'Camper Trailer'){
            opp.Loan_Type2__c = 'Camper Trailer Loan';
            }
            else if(opp.Loan_Type2__c == 'Horse Float'){
            opp.Loan_Type2__c = '-';
            }
            else if(opp.Loan_Type2__c == 'Jet Ski'){
            opp.Loan_Type2__c = 'Jet Ski Loan';
            }
            
// Calculation of Living expenses 

            if(opp.Lender_and_Dependant_Expenses__c > opp.Living_Expenses_Customer__c){
            opp.Expenses_Considered__c = opp.Lender_and_Dependant_Expenses__c;
            }else{
            opp.Expenses_Considered__c = opp.Living_Expenses_Customer__c;
            }
            System.debug('@@@ Loan Type: '+ opp.Loan_Type2__c);
            
// NPS Score flag update

            if(opp.NPS_Score__c >=1 && opp.NPS_Score__c <=6){
                opp.NPS_Score_Flag__c = -1;
            }
            else if (opp.NPS_Score__c == 7 || opp.NPS_Score__c == 8){
                opp.NPS_Score_Flag__c = 0;
            }
            else if (opp.NPS_Score__c >=9){
                opp.NPS_Score_Flag__c = 1;
            }
            
// Update Lender flag field as 1 or 0 based on Lender
            
            if(opp.Lender__c == 'ANZ' || opp.Lender__c == 'BOQ' || opp.Lender__c == 'Capital' 
            || opp.Lender__c == 'Esanda' || opp.Lender__c == 'Finance1' || opp.Lender__c == 'GE' 
            || opp.Lender__c == 'Liberty' || opp.Lender__c == 'Macquarie' || opp.Lender__c == 'Money 3'
            || opp.Lender__c == 'NOW Finance' || opp.Lender__c == 'Pepper' || opp.Lender__c == 'Yamaha Finance' 
            || opp.Lender__c == 'Centre 1' || opp.Lender__c == 'RateSetter' || opp.Lender__c == 'Finance 1 Economy' 
            || opp.Lender__c == 'Micro Money 3' || opp.Lender__c == 'E-Motors' || opp.Lender__c == 'St. George' || opp.Lender__c == 'Latitude' || opp.Lender__c == 'U Me Loans' || opp.Lender__c == 'FirstMac' || opp.Lender__c == 'RACV' 
            || opp.Lender__c == 'Green Light Auto' || opp.Lender__c == 'Ammf' || opp.Lender__c == 'Afs'){ //added 10/17/17 by Jesfer Baculod (Positive Group)
                opp.Lender_Flag__c = '1';
            } 
            else 
            {
                if (opp.Lender__c == null)
                {
                    opp.Lender_Flag__c = '';
                } 
                else 
                {
                    opp.Lender_Flag__c = '0';
                }
            }
            
// Update the below flag fields when Opp stagename changes which is used in the Report.
                            
            if(opp.StageName == 'Approved')
                opp.Approved_Count__c = 1;
                
// Calculate Gross Revenue

            if(opp.StageName != null){
                opp.Total_Gross_Revenue__c = opp.Total_Commision__c - opp.Total_Referral_Fees__c - opp.Total_Outgoings__c;
            }

// change Stage flag value based on Opp stage - Ready for Submission and Lender changes.
    
            if(Trigger.isUpdate){
            
            if(Trigger.oldMap.get(opp.Id).Lender__c != opp.Lender__c){
                opp.Stage_Flag2__c = '1';
            }
            if (Trigger.oldMap.get(opp.Id).StageName == 'Ready for Submission' && opp.StageName != 'Ready for Submission'){
                opp.Stage_Flag2__c = '0';
            }
            

            if(Trigger.oldMap.get(opp.Id).StageName != 'Ready for Submission' && opp.StageName == 'Ready for Submission' && Trigger.oldMap.get(opp.Id).Lender__c == null && opp.Lender__c != null){
                opp.Stage_Flag__c = '0';
                }
            if(Trigger.oldMap.get(opp.Id).StageName != 'Ready for Submission' && opp.StageName == 'Ready for Submission' && Trigger.oldMap.get(opp.Id).Lender__c == opp.Lender__c){
                opp.Stage_Flag__c = '1';
                }
            if(Trigger.oldMap.get(opp.Id).Lender__c != opp.Lender__c && opp.StageName == 'Ready for Submission'){
                opp.Stage_Flag__c = '0';
                }
            if(opp.StageName != Trigger.oldMap.get(opp.Id).StageName && opp.StageName == 'Ready for Submission' && opp.Lender__c == Trigger.oldMap.get(opp.Id).Lender__c && opp.Stage_Flag2__c == '1' ){
                opp.Stage_Flag__c = '0';   
                }                
            if(Trigger.oldMap.get(opp.Id).StageName != 'Ready for Submission' && opp.StageName == 'Ready for Submission' && Trigger.oldMap.get(opp.Id).Increment_StageName__c == null && opp.Increment_StageName__c == 1){
                opp.Stage_flag__c = '0';
            }
            
    // Application Validated field should be '0' for the below StageNames:
        
        if(opp.StageName == 'Qualified' || opp.StageName == 'Application Forms Sent' || opp.StageName == 'Application Forms Taken' || opp.StageName == 'Application and Docs Received'){
            opp.Application_Validated__c = '0';
        }

//       if(opp.StageName == 'Submitted To Lender' || opp.StageName == 'Approved'){
//            opp.Application_Validated__c = '1';
//        }  
//          
            }
                          
        }
    } 

// Create a Contact when an Opportunity is created.
    List<contact> conList = new List<contact>();
    if(trigger.isAfter ){
        if(trigger.isInsert){
            set<Id> oppOwnerId = new set<Id>();
            for(Opportunity opp:Trigger.New){
                Contact con=new contact();
                con.LastName = opp.Person_Account_Last_Name__c;
                con.FirstName = opp.Person_Account_First_Name__c;
                con.Email = Opp.acc_Email__c;
                con.MobilePhone = opp.acc_Mobile__c;
                con.Opportunity__c =opp.Id;
                con.OwnerId = opp.acc_Owner__c;
                conList.add(con);
            }
            if(!conList.isEmpty())
                database.insert(conList,false);
        
        }
        
        // When Opportunity is Settled then change Commissions - Opportunity Settled field in to true 
        if(trigger.isUpdate){
            
            Map<id, Opportunity> oppEmptyCons=new Map<id,Opportunity>();
            set<Id> oppId = new set<Id>();
            for(Opportunity opp:Trigger.New){
                if(opp.contact__c==null){
                    oppEmptyCons.put(opp.id,opp);
                }
                if(opp.StageName == 'Settled'){
                    commIds.add(opp.Id);
                }
                oppId.add(opp.id);
            }
            if(oppId.size()>0 && !RecursionControlCls.isReferalUpdated){
                Map<id,List<VS_Referral__c>> oppToRefferalMap = new Map<id,List<VS_Referral__c>>();
                for(VS_Referral__c vsRefferObj:[SELECT id, Name, Mobile__c,opportunity__c FROM VS_Referral__c WHERE Opportunity__c IN: oppId]){
                    List<VS_Referral__c> vsRefLst = new List<VS_Referral__c>();
                    vsRefLst.add(vsRefferObj);
                    oppToRefferalMap.put(vsRefferObj.opportunity__c,vsRefLst);
                }
                List<VS_Referral__c> vsRefList = new List<VS_Referral__c>();
                for(opportunity oppObj:trigger.new){
                    if(oppToRefferalMap.containsKey(oppObj.id) && oppObj.Mobile__c != trigger.oldMap.get(oppObj.id).Mobile__c){
                        for(VS_Referral__c vsObj:oppToRefferalMap.get(oppObj.id)){
                            vsObj.Mobile__c = oppObj.Mobile__c;  
                            vsRefList.add(vsObj);  
                        }
                    }
                }
                if(vsRefList.size()>0){
                    RecursionControlCls.isReferalUpdated=true;
                    update vsRefList;
                }
                
            }
            
            for(Opportunity oppCons:oppEmptyCons.values()){
                Contact con=new contact();  
                con.LastName = oppCons.Person_Account_Last_Name__c;
                con.FirstName = oppCons.Person_Account_First_Name__c;
                con.Email = oppCons.Email__c;
                con.MobilePhone = oppCons.mobile__c;
                con.Opportunity__c = oppCons.Id;
                con.OwnerId = oppCons.OwnerId;
                conList.add(con);
            }
            if(!conList.isEmpty()){
                database.insert(conList,false);
            }
            List<Commission__c> commToUpdate = new List<Commission__c>();
            for(Commission__c co: [SELECT Id, Opportunity_Settled__c, Opportunity__c from commission__c where Opportunity__c IN: commIds]){
                co.Opportunity_Settled__c = true;
                commToUpdate.add(co);
            }
            update commToUpdate; */

// Send an Email to Referral Company - Contacts (with deal updates field as checked) about opportunity StageName updates.

// code start ***************************************
           
            /*for(Opportunity opp:Trigger.New){
                if(opp != null && opp.Referral_Company__c != null){
                    if(Trigger.oldMap.get(opp.Id).StageName != 'Submitted to Lender' && opp.StageName == 'Submitted to Lender'
                    || Trigger.oldMap.get(opp.Id).StageName != 'Approved' && opp.StageName == 'Approved'
                    || Trigger.oldMap.get(opp.Id).StageName != 'Loan Documents Sent' && opp.StageName == 'Loan Documents Sent'
                    || Trigger.oldMap.get(opp.Id).StageName != 'Settled' && opp.StageName == 'Settled'){
                        refIdsMap.put(opp.Id, opp.Referral_Company__c);                  
                    }
                }
            }
            if(!refIdsMap.isEmpty() && refIdsMap != null){
                Map<Id,Referral_Company__c> refMap = new Map<Id,Referral_Company__c>([SELECT Id, Name FROM Referral_Company__c WHERE Id IN:refIdsMap.values()]);
                Map<Id,Contact> conMap = new Map<Id,Contact>([SELECT Id, FirstName, deal_Updates__c, email, Referral_Company__c FROM Contact where deal_Updates__c = true AND Referral_Company__c IN:refIdsMap.values()]);
                Map<Id,List<Contact>> refToConMap = new Map<Id,List<Contact>>();
            
                for(Contact conObj: conMap.values()){
                    if(reftoConMap.containsKey(conObj.Referral_Company__c)){
                        refToConMap.get(conObj.Referral_Company__c).add(conObj);    
                    } else {
                        refToConMap.put(conObj.Referral_Company__c, new List<Contact>{conObj});
                    }
                }
                                    
                List<Messaging.SingleEmailMessage> listMail = new List<Messaging.SingleEmailMessage>();
                    for(Opportunity opp:Trigger.New){
                        if(opp != null && refMap.containsKey(opp.Referral_Company__c)){
                        Referral_Company__c ref = refMap.get(opp.Referral_Company__c);
                        if(ref != null){
                        if(refToConMap != null && refToConMap.containsKey(opp.Referral_Company__c)){
                            for(Contact con:refToConMap.get(opp.Referral_Company__c)){
                                if(con != null){
                                if(con.deal_Updates__c == true){
                                    String contactEmail = con.Email;
                                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: Label.DelaerSupport]; 
                                    String replyTo = Label.DelaerSupport;
                                    emailTemplate template = [SELECT Id, Subject, htmlValue, body FROM emailTemplate WHERE name = 'Opportunity Update Email'];
                                    String subject = template.subject;
                                    String htmlBody = template.htmlValue;
                                    
                                    subject = subject.replace('{!Opportunity.First_name_opportunity_hidden__c}', opp.Person_Account_First_Name__c);
                                    
                                    htmlBody = htmlBody.replace('{!Contact.FirstName}', con.FirstName); 
                                    htmlBody = htmlBody.replace('{!Opportunity.Person_Account_First_Name__c}', opp.Person_Account_First_Name__c);
                                    htmlBody = htmlBody.replace('{!Opportunity.Person_Account_Last_Name__c}', opp.Person_Account_Last_Name__c);
                                    htmlBody = htmlBody.replace('{!Opportunity.StageName}', opp.StageName);
                    
                                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                    String[] toAddress = new String[] {contactEmail};
                                    mail.setReplyTo(replyTo);
                                    mail.setToAddresses(toAddress);
                                    mail.setSubject(subject);
                                    mail.sethtmlBody(htmlBody);
                                                                    
                                    if(owea.size()>0){
                                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                                    }
                                    listmail.add(mail);
                                    
                                    //  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                            }    
                        }
                    }
                }
                }
                }
                }
                Messaging.sendEmail(listMail);
                System.debug('refToConMap ==============> ' + refToConMap);
                System.debug('Email List ===============> ' + listMail);    
            } */
            
            

// code end **************************** sending email to Referral Company Contacts.

       // }
    //}
}