/** 
* @FileName: FrontEndSubmissionTrigger
* @Description: Main Trigger for Frontend_Submission__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 4/8/18 RDAVID Created Trigger
**/ 
trigger FrontEndSubmissionTrigger on Frontend_Submission__c (	before insert, after insert, before update, after update, before delete, after delete) {
	
	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Front_End_Submission_Trigger__c){
			TriggerFactory.createHandler(Frontend_Submission__c.sObjectType);
		}
	}
}