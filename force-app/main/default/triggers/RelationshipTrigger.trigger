/** 
* @FileName: RelationshipTrigger
* @Description: Main Trigger for Relatinship__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 6/29 JBACULOD Created Trigger
**/ 
trigger RelationshipTrigger on Relationship__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {

	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Relationship_Trigger__c){
			TriggerFactory.createHandler(Relationship__c.sObjectType);
		}
	}

}