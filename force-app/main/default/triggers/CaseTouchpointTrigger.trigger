/** 
* @FileName: CaseTouchpointTrigger
* @Description: Main Trigger for Case_Touch_point__c object
* @Copyright: Positive (c) 2019 
* @author: Jesfer Baculod
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 27/03/19 JBACULOD Created Trigger extra/BizibleTou
**/ 
trigger CaseTouchpointTrigger on Case_Touch_Point__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if (TriggerFactory.trigset.Enable_Triggers__c){ 
		if (TriggerFactory.trigset.Enable_Case_TP_Trigger__c){
			TriggerFactory.createHandler(Case_Touch_Point__c.sObjectType);
		}
	}

}