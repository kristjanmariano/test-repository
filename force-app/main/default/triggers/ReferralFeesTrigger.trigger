Trigger ReferralFeesTrigger on Referral_Fees__c (before insert, before update, after insert, after update,after delete) {
    
    Map<Id,opportunity> oppMap;
    List<Referral_Fees__c>  rfsLst;
    Set<Id> refIds = new Set<Id>();
    
        if(Trigger.isInsert || Trigger.isUpdate ){
           for(Referral_fees__c ref:Trigger.New){
               refIds.add(ref.Opportunity__c);
            }
        }
        if(trigger.isDelete){
           for(Referral_fees__c ref:Trigger.old){
               refIds.add(ref.Opportunity__c);
            }
        }
        if(refIds!=null){
           oppMap = new Map<Id, Opportunity>([SELECT Id, Name, Gross_Revenue2__c, Total_Commision__c,Referral_Company__c from Opportunity where Id IN: refIds]);
           rfsLst=[Select id,name,Referral_fee__c, Referral_Company__c, Opportunity__c FROM Referral_Fees__c WHERE opportunity__c IN:refIds];
        }
        if(trigger.isAfter){
            Decimal grossRevenue=0.0;
            
            if(rfsLst.size()>0){
            if(!oppMap.isempty()){
                for(Referral_Fees__c ref:[Select id,name,Referral_fee__c,Opportunity__c FROM Referral_Fees__c WHERE opportunity__c IN:refIds]){
            if(ref.Referral_fee__c!= null){
                grossRevenue+=ref.Referral_fee__c;
            }
         
            for(Opportunity opp:oppMap.values()){
                if(grossRevenue!=0.0 ){
                oppMap.get(ref.Opportunity__c).Gross_Revenue2__c=grossRevenue;
                }
                if(ref.Referral_Fee__c == null){
                    oppMap.get(ref.Opportunity__c).Gross_Revenue2__c=0.0;
                        }
                    }
                }
            }
            }else{
            }
            if(oppMap.size()>0){
                database.update(oppMap.values(),false);
            }
        }
        if(trigger.isBefore){
        system.debug('oppMap ===================== '+oppMap);
            for(Referral_Fees__c ref:Trigger.New){
                if(Ref.Referral_Fee__c != null){
                Opportunity op = oppMap.get(ref.Opportunity__c);
                ref.Referral_Company__c = op.Referral_Company__c;
                System.debug('Opp Referral Company ===================== ' + ref.Referral_Company__c);
                }
            }
        }

}