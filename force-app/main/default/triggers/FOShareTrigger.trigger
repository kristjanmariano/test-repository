/** 
* @FileName: FOShareTrigger
* @Description: Main Trigger for FO_Share__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 4/23/15 RDAVID Created Trigger
**/ 
trigger FOShareTrigger on FO_Share__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {

	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_FOShare_Trigger__c){
			TriggerFactory.createHandler(FO_Share__c.sObjectType);
		}
	}

}