/** 
* @FileName: RoleAddressTrigger
* @Description: Main Trigger for Role_Address__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 2/15 RDAVID Created Trigger
**/ 
trigger RoleAddressTrigger on Role_Address__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
	
	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Role_Address_Trigger__c){
			TriggerFactory.createHandler(Role_Address__c.sObjectType);
		}
	}
}