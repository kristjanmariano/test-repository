trigger ContactTrigger on Contact (after insert) {
set<Id> oppIds = new set<id>();
    for(Contact con:trigger.new){
        oppIds.add(con.Opportunity__c);  
    }

    Map<Id,Opportunity> oppMap=new Map<Id,Opportunity>([Select Id, name, Contact__c FROM opportunity WHERE Id IN : oppIds]);
    
    
    for(Contact con:trigger.new){
       if(oppMap.containsKey(con.Opportunity__c)){
           oppMap.get(con.opportunity__c).contact__c=con.id;
       } 
    }
    
    try{
        if(!oppMap.isEmpty()){
            database.update(oppMap.values(), false);
        }
    }catch(exception e){
    
    
    }
    
}