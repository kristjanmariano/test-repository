trigger VsReferralTrigger on VS_Referral__c (before insert, after update) {
    
    String userName = UserInfo.getName();
    set<id> oppIds = new set<id>();
    for(VS_Referral__c vs:Trigger.New){
        oppIds.add(vs.Opportunity__c);
    }
    
    Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([SELECT Id, VSR_Latest_Comment__c,Mobile__c FROM Opportunity WHERE Id IN: oppIds]);
//    if(!RecursionControlCls.isOppUpdated && trigger.isUpdate){
        for(Vs_Referral__c vsr:Trigger.New){
            if(oppMap != null && oppMap.size()>0){
                if(vsr.VSR_Latest_Comment__c != null){
                    Opportunity myOpp = oppMap.get(vsr.Opportunity__c);
                    myOpp.VSR_Latest_Comment__c = vsr.VSR_Latest_Comment__c;
                }
            }
        }
		//NN:1-8-17 Added logic to exclude connection user updates via S2S
        if(oppMap!=null && (!userName.equalsIgnoreCase('connection user'))){
//            RecursionControlCls.isOppUpdated=true;
            database.update(oppMap.values());
        }
//    }
    if(trigger.isInsert && trigger.isBefore){
        for(Vs_Referral__c vsr:Trigger.New){
            
            if(oppMap.containsKey(vsr.opportunity__c)){
                vsr.Mobile__c = oppMap.get(vsr.opportunity__c).Mobile__c;
            }
        
        }
    }
    
}