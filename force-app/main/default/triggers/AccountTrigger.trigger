/** 
* @FileName: AccountTrigger
* @Description: Main Trigger for Account object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 8/15/2018 RDAVID Created Trigger
**/ 
trigger AccountTrigger on Account (before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {
	
	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Account_Trigger__c){
			TriggerFactory.createHandler(Account.sObjectType);
		}
	}
}