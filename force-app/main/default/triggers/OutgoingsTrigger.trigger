trigger OutgoingsTrigger on Outgoings__c(after insert, after update, after delete){
    
    Map<Id, Opportunity> oppMap;
    List<Outgoings__c> outList;
    Set<Id> outIds = new Set<Id>();
    
    if(Trigger.isInsert || Trigger.isUpdate){
        for(Outgoings__c og:Trigger.New){
            outIds.add(og.Opportunity__c);
        }
    }
    if(Trigger.isDelete){
        for(Outgoings__c og:Trigger.Old){
        outIds.add(og.Opportunity__c);
        }
    }
    if(outIds != null){
    oppMap = new Map<Id, Opportunity>([SELECT Id, Outgoing_Amount2__c from Opportunity where Id IN: outIds]);
    outList = [SELECT Id, Outgoing_Amount__c, Opportunity__c from Outgoings__c where Opportunity__c IN: outIds]; 
    }
    decimal OutgoingAmount = 0.0;
    if(outList.size()>0){
        if(!oppMap.isEmpty()){
            for(Outgoings__c ogs:[SELECT Id, Outgoing_Amount__c, Deduct_from_Broker__c , Opportunity__c from Outgoings__c where Opportunity__c IN: outIds]){
                if(ogs.Deduct_from_Broker__c == true && ogs.Outgoing_Amount__c != null){
                    OutgoingAmount += ogs.Outgoing_Amount__c;
                }    
            for(Opportunity opp:oppMap.values()){
                if(ogs.Deduct_from_Broker__c == true && OutgoingAmount != 0.0){
                if(ogs.Outgoing_Amount__c != null){
                
                oppMap.get(ogs.Opportunity__c).Outgoing_Amount2__c = OutgoingAmount;
                }
                if(ogs.Deduct_from_Broker__c == true && ogs.Outgoing_Amount__c == null || ogs.Outgoing_Amount__c == 0.0){
                oppMap.get(ogs.Opportunity__c).Outgoing_Amount2__c = 0.0;
                        }
                    }               
                }   
            }
            }
    
                }else{
                for(Outgoings__c ogs:Trigger.Old){
                oppMap.get(ogs.Opportunity__c).Outgoing_Amount2__c = 0.0;
                }
                }
        if(oppMap.size()>0){
        database.update(oppMap.values(), false);
        }                   
}