trigger RemoveNullLiability on Liability__c (after insert, after update) {
    private integer deletethis = 0;
    Liability__c m = [select Monthly_Payment__c, Type__c, Credit_Limit__c from Liability__c where id =:trigger.new[0].id];
    if(  m.Type__c != 'Credit Card' && (m.Monthly_Payment__c == null || m.Monthly_Payment__c == 0 )){
        deletethis = 1;
    }
    if( m.Type__c == 'Credit Card' && (m.Credit_Limit__c == 0 || m.Credit_Limit__c == null)){
        deletethis = 1;
    }
    
    if(deletethis > 0){
        delete m;
    }
}