/** 
* @FileName: LiabilityTrigger
* @Description: Main Trigger for Liability1__c object used in New Model
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 2/15 RDAVID Created Trigger
* 1.1 2/16 JBACULOD Added Trigger toggle for object
**/ 
trigger LiabilityTrigger1 on Liability1__c (	before insert, before update, 
										before delete, after insert, 
										after update, after delete, after undelete) {

	if (TriggerFactory.trigset.Enable_Triggers__c){
		if (TriggerFactory.trigset.Enable_Liability_Trigger__c){
			TriggerFactory.createHandler(Liability1__c.sObjectType);
		}
	}

}