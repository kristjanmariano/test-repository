/** 
* @FileName: SectorTrigger
* @Description: Main Trigger for Sector object used in New Model
* @Copyright: Positive (c) 2019
* @author: Rexie David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 10/04/19 RDAVID Created
**/ 
trigger SectorTrigger on Sector__c (before insert, before update, 
                                    before delete, after insert, 
                                    after update, after delete, after undelete) {

    if (TriggerFactory.trigset.Enable_Triggers__c){
        if (TriggerFactory.trigset.Enable_Sector_Trigger__c){
            TriggerFactory.createHandler(Sector__c.sObjectType);
        }
    }
}