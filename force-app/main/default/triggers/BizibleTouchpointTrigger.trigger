/** 
* @FileName: BizibleTouchpointTrigger
* @Description: Main Trigger for bizible2__Bizible_Touchpoint__c object
* @Copyright: Positive (c) 2018 
* @author: Rexie Aaron A. David
* @Modification Log =============================================================== 
* Ver Date Author Modification --- ---- ------ -------------
* 1.0 26/10 RDAVID Created Trigger extra/BizibleTouchpointTrigger
**/ 

trigger BizibleTouchpointTrigger on bizible2__Bizible_Touchpoint__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (TriggerFactory.trigset.Enable_Triggers__c){ 
		if (TriggerFactory.trigset.Enable_Bizible_Trigger__c){
			TriggerFactory.createHandler(bizible2__Bizible_Touchpoint__c.sObjectType);
		}
	}
}